package com.volt.asap.login.repository;

import com.volt.asap.config.HomeApplianceErpSystemAsapApplication;
import com.volt.asap.login.service.LoginService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * {@link LoginService}의 테스트코드가 담기는 테스트 클래스
 *
 * @see LoginService
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        HomeApplianceErpSystemAsapApplication.class
})
public class LoginServiceTests {

    @Autowired
    private LoginService loginService;

    @Test
    public void 사원_정보_가져오기_테스트() {
        UserDetails user = loginService.loadUserByUsername("200305171");

        System.out.println("UserDetails: " + user);

        assertFalse(user.getUsername().isEmpty());
    }
}
