package com.volt.asap.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.volt.asap")
public class HomeApplianceErpSystemAsapApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeApplianceErpSystemAsapApplication.class, args);
	}

}
