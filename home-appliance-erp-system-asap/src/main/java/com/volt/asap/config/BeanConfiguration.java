package com.volt.asap.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public ModelMapper modelMapper() {

    	ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
            .setMatchingStrategy(MatchingStrategies.STRICT);
//            .setSkipNullEnabled(true)															// 소스 및 대상 토큰이 일치하는 방법을 결정합니다.
//            .setFieldMatchingEnabled(true)													// 필드가 일치에 적합한지 여부를 나타냅니다.
//            .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);	//접근성을 기반으로 일치에 적합한 메서드와 필드를 결정합니다.
        return modelMapper;

    }
}
