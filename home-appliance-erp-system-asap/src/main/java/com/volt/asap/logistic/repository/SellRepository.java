package com.volt.asap.logistic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.Sell;
import com.volt.asap.sales.entity.Order;


public interface SellRepository extends JpaRepository<Sell, Integer>{

	Sell findSellEntityBySellNumber(int sellNumber);

}
