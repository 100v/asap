package com.volt.asap.logistic.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "ReceivingHistory")
@Table(name = "TBL_RECEIVING_HISTORY")
public class ReceivingHistory {

	@Id
	@Column(name = "RECEIVING_HISTORY_NO")
	private int receivingHistoryNo;
	
	@Column(name = "RECEIVING_AMOUNT")
	private int receivingAmount;
	
	@Column(name = "RECEIVING_COMENT")
	private String receivingComent;
	
	@Column(name = "INVENTORY_DATE")
	private java.sql.Date inventoryDate;
	
	@Column(name = "PRODUCT_CODE")
	private String productCode;
	
	@Column(name = "STORAGE_NO")
	private int storageNo;
	
	@Column(name = "CLIENT_NO")
	private int clientNo;

	public ReceivingHistory() {
	}

	public ReceivingHistory(int receivingHistoryNo, int receivingAmount, String receivingComent, Date inventoryDate,
			String productCode, int storageNo, int clientNo) {
		this.receivingHistoryNo = receivingHistoryNo;
		this.receivingAmount = receivingAmount;
		this.receivingComent = receivingComent;
		this.inventoryDate = inventoryDate;
		this.productCode = productCode;
		this.storageNo = storageNo;
		this.clientNo = clientNo;
	}

	public int getReceivingHistoryNo() {
		return receivingHistoryNo;
	}

	public void setReceivingHistoryNo(int receivingHistoryNo) {
		this.receivingHistoryNo = receivingHistoryNo;
	}

	public int getReceivingAmount() {
		return receivingAmount;
	}

	public void setReceivingAmount(int receivingAmount) {
		this.receivingAmount = receivingAmount;
	}

	public String getReceivingComent() {
		return receivingComent;
	}

	public void setReceivingComent(String receivingComent) {
		this.receivingComent = receivingComent;
	}

	public java.sql.Date getInventoryDate() {
		return inventoryDate;
	}

	public void setInventoryDate(java.sql.Date inventoryDate) {
		this.inventoryDate = inventoryDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public int getClientNo() {
		return clientNo;
	}

	public void setClientNo(int clientNo) {
		this.clientNo = clientNo;
	}

	@Override
	public String toString() {
		return "ReceivingHistory [receivingHistoryNo=" + receivingHistoryNo + ", receivingAmount=" + receivingAmount
				+ ", receivingComent=" + receivingComent + ", inventoryDate=" + inventoryDate + ", productCode="
				+ productCode + ", storageNo=" + storageNo + ", clientNo=" + clientNo + "]";
	}
	
}
