package com.volt.asap.logistic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.SellDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.service.SellService;
import com.volt.asap.sales.dto.ClientDTO;

@Controller
@RequestMapping("/sell")
public class SellController {

	private final SellService sellService; 
	
	@Autowired
	public SellController(SellService sellService) {
		this.sellService = sellService;
	}
	
//	@GetMapping("/list")
//	public ModelAndView findOrderList(HttpServletRequest request, ModelAndView mv) {
//		
//		String currentPage = request.getParameter("currentPage");
//		int pageNo = 1;
//		
//		if(currentPage != null && !"".equals(currentPage)) {
//			pageNo = Integer.parseInt(currentPage);
//		}
//		
//		
//		String searchValue1 = request.getParameter("searchValue1");
//		String searchValue2 = request.getParameter("searchValue2");
//		String searchCondition1 = request.getParameter("searchCondition1");
//		String searchCondition2 = request.getParameter("searchCondition2");
//		
//		int totalCount = sellService.selectTotalCount(searchCondition1, searchCondition2, searchValue1, searchValue2);
//		System.out.println("totalCount" + totalCount);
//		
//		int limit = 10;
//		int buttonAmount = 5;
//		
//		SelectCriteriaforDoubleValue selectCriteriaforDoubleValue = null;
//		
//		if(searchValue1 != null && searchValue2 != null) {
//			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount, searchCondition1,searchValue1, searchCondition2, searchValue2);
//		} else {
//			/* 맨 처음일 때*/
//			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount);
//		}
//		List<SellDTO> sellList = sellService.findSellList(selectCriteriaforDoubleValue);
//		List<ProductDTO> productList = sellService.findProductList();
//		List<ClientDTO> clientList = sellService.findClientList();
//		
//		mv.addObject("sellList", sellList);
//		mv.addObject("clientList", clientList);
//		mv.addObject("productlist", productList);
//		mv.addObject("selectCriteriaforDoubleValue", selectCriteriaforDoubleValue);
//		mv.setViewName("sell/list");
//		
//		return mv;
//	}
	@GetMapping("/list")
	public ModelAndView findOrderList(String startDate, String endDate, String searchProduct, String searchClient, Integer page,
            ModelAndView mv) {
		
		CustomSelectCriteria selectCriteria;
		/* 버튼 넘기는거 */
		int limit = 10;
		int buttonAmount = 5;
		
        /* 페이지 파라미터가 없으면 1페이지로 간주 */
        if (page == null) {
            page = 1;
        }
        Map<String, String> parameterMap = new HashMap<>();
        if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(searchProduct) && isEmptyString(searchClient)) {
			
            int totalCount = sellService.countSellByQueries(null);	//새로만듬
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, limit,buttonAmount);
        } else {
            /* 검색 쿼리로 사용할 값들을 Map에 설정 */

            parameterMap.put("startDate", startDate);
            parameterMap.put("endDate", endDate);
            parameterMap.put("searchProduct", searchProduct == null ? "none" : searchProduct);
            parameterMap.put("searchClient", searchClient == null ? "none" : searchClient);

            /* 검색어에 대한 전체 엔티티 개수를 설정 */
            int totalCount = sellService.countSellByQueries(parameterMap);

            /* 검색어 객체의 생성 및 검색어 할당 */
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, limit,
                    buttonAmount, parameterMap);
        }
       
		List<SellDTO> sellList = sellService.findSellByQueries(selectCriteria);
		List<ProductDTO> productList = sellService.findProductList();
		List<ClientDTO> clientList = sellService.findClientList();
		List<StorageDTO> storageList = sellService.findStorageList();
		System.out.println("값" + parameterMap);
		
		mv.addObject("parameterMap", parameterMap);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("sellList", sellList);
		mv.addObject("clientList", clientList);
		mv.addObject("productlist", productList);
		mv.addObject("storageList", storageList);
		mv.setViewName("sell/list");
		
		return mv;
	}
	@GetMapping("/{sellNumber}")
	public ModelAndView findOrderByNumber(ModelAndView mv, @PathVariable int sellNumber) {
		
		SellDTO sell = sellService.findSellByNumber(sellNumber);
		
		List<ClientDTO> clientList = sellService.findClientList();
		List<ProductDTO> productList = sellService.findProductList();
		List<StorageDTO> storageList = sellService.findStorageList();
		
		mv.addObject("sell", sell);
		mv.addObject("productList",productList);
		mv.addObject("clientList",clientList);
		mv.addObject("storageList", storageList);
		mv.setViewName("sell/detail");
		
		return mv;
	}
	
	@GetMapping("/regist")
	public ModelAndView registPage(ModelAndView mv) {
		List<ClientDTO> clientList = sellService.findClientList();
		List<ProductDTO> productList = sellService.findProductList();
		List<StorageDTO> storageList = sellService.findStorageList();
		
		mv.addObject("productList",productList);
		mv.addObject("clientList",clientList);
		mv.addObject("storageList",storageList);
		return mv;
	}
	
	@PostMapping("/regist")
	public ModelAndView registSell (ModelAndView mv, SellDTO newSell, RedirectAttributes rttr) {

		sellService.registNewSell(newSell);

		rttr.addFlashAttribute("registSuccessMessage", "판매 등록에 성공하셨습니다");
		mv.setViewName("redirect:/sell/list");
		// 여기서 무언가를 해주면
		return mv;
	}
	@GetMapping("/registNopop")
	public ModelAndView registNopopPage(ModelAndView mv) {
		List<ClientDTO> clientList = sellService.findClientList();
		List<ProductDTO> productList = sellService.findProductList();
		List<StorageDTO> storageList = sellService.findStorageList();
		
		mv.addObject("productList",productList);
		mv.addObject("clientList",clientList);
		mv.addObject("storageList",storageList);
		return mv;
	}
	
	@PostMapping("/registNopop")
	public ModelAndView registNopopSell (ModelAndView mv, SellDTO newSell, RedirectAttributes rttr) {

		sellService.registNewSell(newSell);

		rttr.addFlashAttribute("registSuccessMessage", "판매 등록에 성공하셨습니다");
		mv.setViewName("redirect:/sell/list");
		// 여기서 무언가를 해주면
		return mv;
	}
	@PostMapping("/modify")
	public ModelAndView modifyPage(ModelAndView mvModify, RedirectAttributes rttr, SellDTO updateSell) {
		System.out.println("여기로 오나" + updateSell);
		sellService.modifySell(updateSell);
		
		rttr.addFlashAttribute("modifySuccessMessage", "판매 수정에 성공하셨습니다");
		mvModify.setViewName("redirect:/sell/list");
		
		return mvModify;
	}
	@PostMapping(value = "/showRegistProductText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> showRegistProductText(int storage ){
		
		System.out.println(storage);
		List<ProductDTO> productList = new ArrayList<>();
		
		if(storage == 0) {
			System.out.println("전체 조회");
			productList = sellService.findAllProduct();
		}else {
			System.out.println("부분 조회");
			productList = sellService.findProductByStorageCode(storage);
		}
		
		List<String> productNameList = new ArrayList<>();
		for(int i = 0; i < productList.size(); i++) {
			String name = productList.get(i).getProductName();
			productNameList.add(name);
		}

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put("productNameDrop", productList);
		
		return resultMap;
	}
	
	/* ajax로 조회 하기,,, */
	@PostMapping(value = "/showModifyProductText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> modifyOldProduct(int no, String newProductName) {
		
		Map<String, Object> sendMessage = sellService.modifyOldProduct(no,newProductName); 
		
		return sendMessage;
	}
	
	@PostMapping(value = "/showRegistProductInfoText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> showRegistProductText(String productCode){
		
		Map<String, Object> resultMap = new HashMap<>();
		String errorMessage = "";
		
		if(isEmptyString(productCode)) {
			errorMessage = "품목명은 반드시 입력해야 합니다.";
			resultMap.put("errorMessage", errorMessage);
			return resultMap;
		}else {
			List<ProductDTO> productList = sellService.findProductByProductCode(productCode);
			resultMap.put("productList", productList);
		}
		
		
		return resultMap;
	}
	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다. */
		return str == null || "".equals(str);
	}

	private java.sql.Date toSqlDate(String dateString) { // 넘어온 파라미터를 담은 변수가 비었는지 체크하는 메소드
		java.sql.Date endDate = null;

		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}

		return endDate;
	}
}
