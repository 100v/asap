package com.volt.asap.logistic.dto;

public class ProductCategoryDTO {

	private String categoryCode;
	private String categoryName;
	
	public ProductCategoryDTO() {
		super();
	}
	public ProductCategoryDTO(String categoryCode, String categoryName) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	@Override
	public String toString() {
		return "ProductCategoryDTO [categoryCode=" + categoryCode + ", categoryName=" + categoryName + "]";
	}
	
	
}
