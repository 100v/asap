package com.volt.asap.logistic.dto;

import java.io.Serializable;
import java.util.List;

import com.volt.asap.sales.dto.ClientDTO;

public class InventoryDTO implements Serializable {

	private static final long serialVersionUID = -6986089495993434874L;

	private int inventoryNo;
	private int stockNo;
	private String productCode;
	private int storageNo;
	private int clientNo;
	private int receivingHistoryNo;
	private int sellNo;
	private StockHistoryDTO stockHistory;
	private ProductDTO product;
	private StorageDTO storage;
	private ClientDTO client;
	private ReceivingHistoryDTO receivingHistory;
	private List<SellDTO> sellList;
	
	public InventoryDTO() {
	}

	public InventoryDTO(int inventoryNo, int stockNo, String productCode, int storageNo, int clientNo,
			int receivingHistoryNo, int sellNo, StockHistoryDTO stockHistory, ProductDTO product, StorageDTO storage,
			ClientDTO client, ReceivingHistoryDTO receivingHistory, List<SellDTO> sellList) {
		this.inventoryNo = inventoryNo;
		this.stockNo = stockNo;
		this.productCode = productCode;
		this.storageNo = storageNo;
		this.clientNo = clientNo;
		this.receivingHistoryNo = receivingHistoryNo;
		this.sellNo = sellNo;
		this.stockHistory = stockHistory;
		this.product = product;
		this.storage = storage;
		this.client = client;
		this.receivingHistory = receivingHistory;
		this.sellList = sellList;
	}

	public int getInventoryNo() {
		return inventoryNo;
	}

	public void setInventoryNo(int inventoryNo) {
		this.inventoryNo = inventoryNo;
	}

	public int getStockNo() {
		return stockNo;
	}

	public void setStockNo(int stockNo) {
		this.stockNo = stockNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public int getClientNo() {
		return clientNo;
	}

	public void setClientNo(int clientNo) {
		this.clientNo = clientNo;
	}

	public int getReceivingHistoryNo() {
		return receivingHistoryNo;
	}

	public void setReceivingHistoryNo(int receivingHistoryNo) {
		this.receivingHistoryNo = receivingHistoryNo;
	}

	public int getSellNo() {
		return sellNo;
	}

	public void setSellNo(int sellNo) {
		this.sellNo = sellNo;
	}

	public StockHistoryDTO getStockHistory() {
		return stockHistory;
	}

	public void setStockHistory(StockHistoryDTO stockHistory) {
		this.stockHistory = stockHistory;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	public StorageDTO getStorage() {
		return storage;
	}

	public void setStorage(StorageDTO storage) {
		this.storage = storage;
	}

	public ClientDTO getClient() {
		return client;
	}

	public void setClient(ClientDTO client) {
		this.client = client;
	}

	public ReceivingHistoryDTO getReceivingHistory() {
		return receivingHistory;
	}

	public void setReceivingHistory(ReceivingHistoryDTO receivingHistory) {
		this.receivingHistory = receivingHistory;
	}

	public List<SellDTO> getSellList() {
		return sellList;
	}

	public void setSellList(List<SellDTO> sellList) {
		this.sellList = sellList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InventoryDTO [inventoryNo=" + inventoryNo + ", stockNo=" + stockNo + ", productCode=" + productCode
				+ ", storageNo=" + storageNo + ", clientNo=" + clientNo + ", receivingHistoryNo=" + receivingHistoryNo
				+ ", sellNo=" + sellNo + ", stockHistory=" + stockHistory + ", product=" + product + ", storage="
				+ storage + ", client=" + client + ", receivingHistory=" + receivingHistory + ", sellList=" + sellList
				+ "]";
	}

}
