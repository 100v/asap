package com.volt.asap.logistic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.logistic.dto.InventoryDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.service.LogisticService;

@Controller
@RequestMapping("/logistic/*")
public class LogisticController {

	private final LogisticService logisticService;
	
	@Autowired
	public LogisticController(LogisticService logisticService) {
		this.logisticService = logisticService;
	}
	
	@GetMapping("/stock")
	public ModelAndView searchFindList(HttpServletRequest request, ModelAndView mv) {
		
		/* 화면단에 숨겨둔 input의 value(현재 페이지 넘버)를 받았습니다. */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		System.out.println("currentPage!!!!!!!!!!!!!! : "+ currentPage);
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		int searchCondition = 0;
		if(!isEmptyString(request.getParameter("select1"))) {
			searchCondition = Integer.parseInt(request.getParameter("select1"));
		}
		
		String searchCondition2 = null;
		if(!isEmptyString(request.getParameter("select1"))) {
			searchCondition2 = request.getParameter("select1");
		}
		
		String searchValue = null;
		if(!isEmptyString(request.getParameter("select2"))) {
			searchValue = request.getParameter("select2");
		}
		
		/* 한 페이지에 보여질 목록의 수 입니다. */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 개수 입니다. */
		int buttonAmount = 5;
		
		/* 페이징 처리에 대한 정보를 담고있는 녀석을 일단 만들어 둡니다. */
		SelectCriteria selectCriteria = null;
		
		if(!(searchCondition == 0 && searchCondition2 == null && searchValue == null )) {
			/* 창고코드랑 상품코드를 화면단에서 파라미터로 넘겨벋아서 변수에 넣었어요. */

			System.out.println("storageNo : " + searchCondition);
			System.out.println("productCode : " + searchValue);
			
			/* 창고명과 상품명으로 조건에 맞는 친구가 몇개 있는지 개수를 세어봅니다.*/
			int totalcount = logisticService.selectTotalCount(searchCondition, searchValue);
			
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalcount, limit, buttonAmount, searchCondition2, searchValue);
			
			System.out.println("이것은 토탈카운트다 : " + totalcount);
			
			/* 검색 조건에 맞는 목록을 조회해옵니다. */
			List<StockHistoryDTO> searchList = logisticService.selectStockList(searchCondition, selectCriteria);
			
			System.out.println("조회가 잘 되어오니?????????????? : " + searchList);
			
			/* select option으로 들어갈 친구들입니다 (창고명, 상품명) */
			List<StorageDTO> storageList = logisticService.findStorageList();
			List<ProductDTO> productList = logisticService.findProductList();
			
			mv.addObject("storageList", storageList);
			mv.addObject("productList", productList);
			
			mv.addObject("searchList", searchList);
			
			mv.addObject("selectCriteria", selectCriteria);
			mv.setViewName("logistic/stock");
		} else {
		
			int totalcount = logisticService.selectTotalCount(searchCondition, searchValue);
			
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalcount, limit, buttonAmount);
			
			/* select option으로 들어갈 친구들입니다 (창고명, 상품명) */
			List<StorageDTO> storageList = logisticService.findStorageList();
			List<ProductDTO> productList = logisticService.findProductList();
			
			List<StockHistoryDTO> searchList = logisticService.selectStockList(searchCondition, selectCriteria);
			
			
			mv.addObject("storageList", storageList);
			mv.addObject("productList", productList);
			
			mv.addObject("searchList", searchList);
			
			mv.addObject("selectCriteria", selectCriteria);
			mv.setViewName("logistic/stock");
		
		}
		
		return mv;
		
	}
	private boolean isEmptyString(String str) {
		return str == null || "".equals(str);
	}
	@GetMapping("/{stockNo}")
	public ModelAndView stockDetail(ModelAndView mv, @PathVariable int stockNo) {

		System.out.println("너는 잘 나오는게 맞지??????" + stockNo);
		
		List<InventoryDTO> invenList = logisticService.findInvenList(stockNo);
		
		System.out.println("육중한 육중조인아 넌 성공했니??? : " + invenList);
		
		mv.addObject("invenList", invenList);
		mv.setViewName("logistic/inventory");
		
		return mv;
	}
}
