package com.volt.asap.logistic.dto;

import java.io.Serializable;

public class StockHistoryDTO implements Serializable {

	private static final long serialVersionUID = 6607863798598469676L;
	
	private int stockNo;
	private int stockQuantity;
	private String productCode;
	private int storageNo;
	private ProductDTO product;
	private StorageDTO storage;
	
	public StockHistoryDTO() {
	}

	public StockHistoryDTO(int stockNo, int stockQuantity, String productCode, int storageNo, ProductDTO product,
			StorageDTO storage) {
		this.stockNo = stockNo;
		this.stockQuantity = stockQuantity;
		this.productCode = productCode;
		this.storageNo = storageNo;
		this.product = product;
		this.storage = storage;
	}

	public int getStockNo() {
		return stockNo;
	}

	public void setStockNo(int stockNo) {
		this.stockNo = stockNo;
	}

	public int getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	public StorageDTO getStorage() {
		return storage;
	}

	public void setStorage(StorageDTO storage) {
		this.storage = storage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "StockHistoryDTO [stockNo=" + stockNo + ", stockQuantity=" + stockQuantity + ", productCode="
				+ productCode + ", storageNo=" + storageNo + ", product=" + product + ", storage=" + storage + "]";
	}

	
}
