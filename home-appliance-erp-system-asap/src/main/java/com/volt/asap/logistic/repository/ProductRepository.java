package com.volt.asap.logistic.repository;

import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.Streamable;

import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.Storage;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String>{

	Product findProductentityByproductCode(String productCode);

	Product findByProductNameAndProductStatus(String newProductName, String productStatus);
    List<Product> findAllByProductStatus(String status);

	List<Product> findAllByProductStatusAndStorageNo(String string, int storageNum);

	List<Product> findByProductCode(String productCode);

	List<Product> findAllByStorageNo(int storage);

	Product findByProductName(String newProductName);

//	int countByproductCodeContainingAndproductNameContaining(String searchValue1, String searchValue2);

//	int countByproductCode(String searchValue1);
//
//	int countByproductNameContaining(String searchValue2);

//	List<Product> findByproductCodeContainingAndproductNameContaining(String searchValue1, String searchValue2);

//	List<Product> findByproductCodeContaining(String searchValue1);

//	List<Product> findByproductNameContaining(String searchValue2);

	
}
