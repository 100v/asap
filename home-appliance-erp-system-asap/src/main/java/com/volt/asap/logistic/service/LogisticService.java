package com.volt.asap.logistic.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.logistic.dto.InventoryDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.JoinInventory;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.StockHistoryAndProductAndStorage;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.JoinInventoryRepository;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.StockHistoryAndProductAndStorageRepository;
import com.volt.asap.logistic.repository.StockHistoryRepository;
import com.volt.asap.logistic.repository.StorageRepository;

@Service
public class LogisticService {
	
	private final StorageRepository storageRepository;
	private final ProductRepository productRepository;
	private final StockHistoryRepository stockHistoryRepository;
	private final StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository;
	private final JoinInventoryRepository joinInventoryRepository;
	private final ModelMapper modelMapper;

	@Autowired
	public LogisticService(StorageRepository storageRepository, ProductRepository productRepository, StockHistoryRepository stockHistoryRepository, StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository, JoinInventoryRepository joinInventoryRepository, ModelMapper modelMapper) {
		this.storageRepository = storageRepository;
		this.productRepository = productRepository;
		this.stockHistoryRepository = stockHistoryRepository;
		this.stockHistoryAndProductAndStorageRepository = stockHistoryAndProductAndStorageRepository;
		this.joinInventoryRepository = joinInventoryRepository;
		this.modelMapper = modelMapper;
	}

	public List<StorageDTO> findStorageList() {

		String status = "N";

		List<Storage> storageList = storageRepository.findAllByStorageStatus(status);
		
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findProductList() {

		String status = "N";
		
		List<Product> productList = productRepository.findAllByProductStatus(status);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public List<StockHistoryDTO> findStockHistoryList() {
		List<StockHistoryAndProductAndStorage> stockHistoryAndProductAndStorageList = stockHistoryAndProductAndStorageRepository.findAll();
		
		return stockHistoryAndProductAndStorageList.stream().map(stockHistoryAndProductAndStorage -> modelMapper.map(stockHistoryAndProductAndStorage, StockHistoryDTO.class)).collect(Collectors.toList());
	}

	public int selectTotalCount(int searchCondition, String searchValue) {

		int result = 0;
		
		if(searchCondition > 0 && searchValue != null) {
			result = stockHistoryRepository.countByStorageNoAndProductCode(searchCondition, searchValue);
		} else {
			result = (int)stockHistoryRepository.count();
		}
		
		return result;
	}

	public List<StockHistoryDTO> selectStockList(int SearchCondition, SelectCriteria selectCriteria) {
		
		int index = selectCriteria.getPageNo() - 1;			// Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();

		/* 페이징 처리와 정렬을 위한 객체 생성 */
		Pageable paging = PageRequest.of(index, count, Sort.by("stockNo"));
		
		List<StockHistoryAndProductAndStorage> searchList = new ArrayList<StockHistoryAndProductAndStorage>();

		String status = "N";

		if(SearchCondition > 0 && selectCriteria != null ) {
			searchList = stockHistoryAndProductAndStorageRepository.findByStorageStorageNoAndProductProductCode(SearchCondition, selectCriteria.getSearchValue(), paging);
		} else {
			searchList = stockHistoryAndProductAndStorageRepository.findAll(paging).toList();
		}
		return searchList.stream().map(stockHistoryAndProductAndStorage -> modelMapper.map(stockHistoryAndProductAndStorage, StockHistoryDTO.class)).collect(Collectors.toList());
	}

	public List<InventoryDTO> findInvenList(int stockNo) {
		
		List<JoinInventory> invenList = joinInventoryRepository.findJoinInventoryByStockHistoryStockNo(stockNo);
		
		System.out.println("JoinInventory 리스트!!!!!!!!!!!!!!!!!!!! : " + invenList);
		
		return invenList.stream().map(joinInventory -> modelMapper.map(joinInventory, InventoryDTO.class)).collect(Collectors.toList());
	}
	
	

}
