package com.volt.asap.logistic.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.volt.asap.sales.entity.Client;

@Entity(name="SellAndClient")
@Table(name="TBL_SELL_LIST")
public class SellAndClient {
	
	@Id
	@Column(name = "SELL_NO")
	private int sellNumber;
	
	@Column(name = "SELL_DATE")
	private java.sql.Date sellDate;
	
	@Column(name = "SELL_AMOUNT")
	private int sellAmount;
	
	@Column(name = "SELL_TOTAL_PRICE")
	private long sellTotalPrice;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "CLIENT_NO")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "STORAGE_NO")
	private Storage storage;
	
	@Column(name = "SELL_PRICE")
	private long sellPrice;

	public SellAndClient() {
		super();
	}

	public SellAndClient(int sellNumber, java.sql.Date sellDate, int sellAmount, long sellTotalPrice, Product product,
			Client client, Storage storage, long sellPrice) {
		super();
		this.sellNumber = sellNumber;
		this.sellDate = sellDate;
		this.sellAmount = sellAmount;
		this.sellTotalPrice = sellTotalPrice;
		this.product = product;
		this.client = client;
		this.storage = storage;
		this.sellPrice = sellPrice;
	}

	public int getSellNumber() {
		return sellNumber;
	}

	public void setSellNumber(int sellNumber) {
		this.sellNumber = sellNumber;
	}

	public java.sql.Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(java.sql.Date sellDate) {
		this.sellDate = sellDate;
	}

	public int getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(int sellAmount) {
		this.sellAmount = sellAmount;
	}

	public long getSellTotalPrice() {
		return sellTotalPrice;
	}

	public void setSellTotalPrice(long sellTotalPrice) {
		this.sellTotalPrice = sellTotalPrice;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public long getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(long sellPrice) {
		this.sellPrice = sellPrice;
	}

	@Override
	public String toString() {
		return "SellAndClient [sellNumber=" + sellNumber + ", sellDate=" + sellDate + ", sellAmount=" + sellAmount
				+ ", sellTotalPrice=" + sellTotalPrice + ", product=" + product + ", client=" + client + ", storage="
				+ storage + ", sellPrice=" + sellPrice + "]";
	}

	
}
