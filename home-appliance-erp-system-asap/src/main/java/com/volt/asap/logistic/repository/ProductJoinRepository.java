package com.volt.asap.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.ProductAndStoAndProCtg;
import com.volt.asap.logistic.entity.StockHistoryAndProductAndStorage;

public interface ProductJoinRepository extends JpaRepository<ProductAndStoAndProCtg, String>{

	int countByproductNameContaining(String searchValue2);

	int countByproductCodeContaining(String searchValue1);

	ProductAndStoAndProCtg findProductentityByproductCode(String productCode);

	List<ProductAndStoAndProCtg> findByproductCodeContaining(String searchValue1, Pageable paging);

	List<ProductAndStoAndProCtg> findByproductNameContaining(String searchValue2, Pageable paging);

	List<ProductAndStoAndProCtg> findByProductCodeContainingAndProductNameContaining(String searchValue1,
			String searchValue2, Pageable paging);

	int countByProductCodeAndProductNameContaining(String searchValue1, String searchValue2);

	List<ProductAndStoAndProCtg> findByProductStatus(String string);

	List<ProductAndStoAndProCtg> findByProductNameAndProductStatus(String product, String string);

	List<ProductAndStoAndProCtg> findByStorageStorageNameAndProductStatus(String storage, String string);

	List<ProductAndStoAndProCtg> findByStorageStorageNameAndProductNameAndProductStatus(String storage, String product,
			String string);



}
