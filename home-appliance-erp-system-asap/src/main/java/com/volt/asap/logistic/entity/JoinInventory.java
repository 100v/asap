package com.volt.asap.logistic.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.volt.asap.sales.entity.Client;

@Entity(name = "JoinInventory")
@Table(name = "TBL_INVENTORY")
public class JoinInventory {

	@Id
	@Column(name = "INVENTORY_NO")
	private int inventoryNo;
	
	@ManyToOne
	@JoinColumn(name = "STOCK_NO")
	private StockHistory stockHistory;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "STORAGE_NO")
	private Storage storage;
	
	@ManyToOne
	@JoinColumn(name = "CLIENT_NO")
	private Client client;
	
	@ManyToOne
	@JoinColumn(name = "RECEIVING_HISTORY_NO")
	private ReceivingHistory receivingHistory;
	
	@OneToMany(targetEntity = Sell.class)
	@JoinColumn(name = "SELL_NO")
	private List<Sell> sellList;

	public JoinInventory() {
	}

	public JoinInventory(int inventoryNo, StockHistory stockHistory, Product product, Storage storage, Client client,
			ReceivingHistory receivingHistory, List<Sell> sellList) {
		this.inventoryNo = inventoryNo;
		this.stockHistory = stockHistory;
		this.product = product;
		this.storage = storage;
		this.client = client;
		this.receivingHistory = receivingHistory;
		this.sellList = sellList;
	}

	public int getInventoryNo() {
		return inventoryNo;
	}

	public void setInventoryNo(int inventoryNo) {
		this.inventoryNo = inventoryNo;
	}

	public StockHistory getStockHistory() {
		return stockHistory;
	}

	public void setStockHistory(StockHistory stockHistory) {
		this.stockHistory = stockHistory;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public ReceivingHistory getReceivingHistory() {
		return receivingHistory;
	}

	public void setReceivingHistory(ReceivingHistory receivingHistory) {
		this.receivingHistory = receivingHistory;
	}

	public List<Sell> getSellList() {
		return sellList;
	}

	public void setSellList(List<Sell> sellList) {
		this.sellList = sellList;
	}

	@Override
	public String toString() {
		return "JoinInventory [inventoryNo=" + inventoryNo + ", stockHistory=" + stockHistory + ", product=" + product
				+ ", storage=" + storage + ", client=" + client + ", receivingHistory=" + receivingHistory
				+ ", sellList=" + sellList + "]";
	}

}
