package com.volt.asap.logistic.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.SelectCriteriaforDoubleValue;
import com.volt.asap.logistic.dto.ProductCategoryDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Category;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.ProductAndStoAndProCtg;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.CategoryRepository;
import com.volt.asap.logistic.repository.ProductJoinRepository;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.StorageRepository;

@Service
public class ProductService {

	private final ProductRepository productRepository;
	private final ProductJoinRepository productJoinRepository;
	private final CategoryRepository categoryRepository;
	private final ModelMapper modelMapper;
	private final StorageRepository storageRepository;
	
	@Autowired
	public ProductService(ProductRepository productRepository, ModelMapper modelMapper, StorageRepository storageRepository
			, ProductJoinRepository productJoinRepository, CategoryRepository categoryRepository) {
		this.productRepository = productRepository;
		this.productJoinRepository = productJoinRepository;
		this.categoryRepository = categoryRepository;
		this.modelMapper = modelMapper;
		this.storageRepository = storageRepository;
	}

	public List<ProductDTO> productList(SelectCriteriaforDoubleValue selectCriteriaforDoubleValue) {
		
		int index = selectCriteriaforDoubleValue.getPageNo() - 1;
		int count = selectCriteriaforDoubleValue.getLimit();
		String searchValue1 = selectCriteriaforDoubleValue.getSearchValue1();
		String searchValue2 = selectCriteriaforDoubleValue.getSearchValue2();

		Pageable paging = PageRequest.of(index, count, Sort.by("productCode"));
		
		List<ProductAndStoAndProCtg> productList = productJoinRepository.findAll(Sort.by("productStatus"));
		
		if(searchValue1 != null && searchValue2 != null) {
			productList = productJoinRepository.findByProductCodeContainingAndProductNameContaining(searchValue1, searchValue2, paging);
		} else if(searchValue1 != null && searchValue2 == null){
			productList = productJoinRepository.findByproductCodeContaining(searchValue1, paging);
		} else if(searchValue1 == null && searchValue2 != null){
			productList = productJoinRepository.findByproductNameContaining(searchValue2, paging);
		} else {
			productList = productJoinRepository.findAll(paging).toList();
		}
		
		 
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public void registProduct(ProductDTO productInsert) {

		productRepository.save(modelMapper.map(productInsert, Product.class));
	}

	@Transactional
	public void modifyProduct(ProductDTO productUpdate) {
		
		Product updateProduct = productRepository.findProductentityByproductCode(productUpdate.getProductCode());
		
		updateProduct.setProductName(productUpdate.getProductName());
		updateProduct.setProductRelease(productUpdate.getProductRelease());
		updateProduct.setProductWaearing(productUpdate.getProductWaearing());
		updateProduct.setStorageNo(productUpdate.getStorageNo());
		updateProduct.setCategoryCode(productUpdate.getCategoryCode());
		updateProduct.setProductStatus(productUpdate.getProductStatus());
		
	}

	public int selectTotalCount(String searchCondition1, String searchCondition2, String searchValue1,
			String searchValue2) {
		
		int count = 0;
		
		if(searchValue1 != null && searchValue2 != null) {
			if(!searchValue1.isBlank() && !searchValue2.isEmpty()) {
				count = productJoinRepository.countByProductCodeAndProductNameContaining(searchValue1,searchValue2);
			}else if(!searchValue1.isEmpty() && searchValue2.isEmpty()) {
				count = productJoinRepository.countByproductCodeContaining(searchValue1);
			}else if(searchValue1.isEmpty() && !searchValue2.isEmpty()) {
				count = productJoinRepository.countByproductNameContaining(searchValue2);
			} else {
				count = (int)productRepository.count();
			}
		} else {
			count = (int)productRepository.count();
		}

		return count;
	}

	public ProductDTO productDetail(String productCode) {
		
		ProductAndStoAndProCtg productdetail = productJoinRepository.findProductentityByproductCode(productCode);
		
		return modelMapper.map(productdetail, ProductDTO.class);
	}
	
	public List<StorageDTO> storageList() {
		List<Storage> storageList = storageRepository.findAll();
		
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}

	public List<ProductCategoryDTO> categoryList() {
		
		List<Category> categoryList = categoryRepository.findAll();
		
		return categoryList.stream().map(storage -> modelMapper.map(storage, ProductCategoryDTO.class)).collect(Collectors.toList());
	}



}













