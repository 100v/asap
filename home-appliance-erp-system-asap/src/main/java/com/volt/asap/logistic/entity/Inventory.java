package com.volt.asap.logistic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Inventory")
@Table(name = "TBL_INVENTORY")
public class Inventory {

	@Id
	@Column(name = "INVENTORY_NO")
	private int inventoryNo;
	
	@Column(name = "STOCK_NO")
	private int stockNo;
	
	@Column(name = "PRODUCT_NO")
	private String productCode;
	
	@Column(name = "STORAGE_NO")
	private int storageNo;
	
	@Column(name = "RECEIVING_HISTORY_NO")
	private int receivingHistoryNo;
	
	@Column(name = "CLIENT_NO")
	private int clientNo;
	
	@Column(name = "SELL_NO")
	private int sellNo;
	
	public Inventory() {
	}

	public Inventory(int inventoryNo, int stockNo, String productCode, int storageNo, int receivingHistoryNo,
			int clientNo, int sellNo) {
		this.inventoryNo = inventoryNo;
		this.stockNo = stockNo;
		this.productCode = productCode;
		this.storageNo = storageNo;
		this.receivingHistoryNo = receivingHistoryNo;
		this.clientNo = clientNo;
		this.sellNo = sellNo;
	}

	public int getInventoryNo() {
		return inventoryNo;
	}

	public void setInventoryNo(int inventoryNo) {
		this.inventoryNo = inventoryNo;
	}

	public int getStockNo() {
		return stockNo;
	}

	public void setStockNo(int stockNo) {
		this.stockNo = stockNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public int getReceivingHistoryNo() {
		return receivingHistoryNo;
	}

	public void setReceivingHistoryNo(int receivingHistoryNo) {
		this.receivingHistoryNo = receivingHistoryNo;
	}

	public int getClientNo() {
		return clientNo;
	}

	public void setClientNo(int clientNo) {
		this.clientNo = clientNo;
	}

	public int getSellNo() {
		return sellNo;
	}

	public void setSellNo(int sellNo) {
		this.sellNo = sellNo;
	}

	@Override
	public String toString() {
		return "Inventory [inventoryNo=" + inventoryNo + ", stockNo=" + stockNo + ", productCode=" + productCode
				+ ", storageNo=" + storageNo + ", receivingHistoryNo=" + receivingHistoryNo + ", clientNo=" + clientNo
				+ ", sellNo=" + sellNo + "]";
	}

}
