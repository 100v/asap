package com.volt.asap.logistic.dto;

import java.io.Serializable;

public class StorageDTO implements Serializable {

	private static final long serialVersionUID = -6504254266426535355L;
	
	private int storageNo;
	private String storageName;
	private String storageManagerPhone;
	private int empCode;

	private String storageStatus;
	
	public StorageDTO() {
	}

	public StorageDTO(int storageNo, String storageName, String storageManagerPhone, int empCode, String storageStatus) {
		this.storageNo = storageNo;
		this.storageName = storageName;
		this.storageManagerPhone = storageManagerPhone;
		this.empCode = empCode;
		this.storageStatus = storageStatus;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getStorageManagerPhone() {
		return storageManagerPhone;
	}

	public void setStorageManagerPhone(String storageManagerPhone) {
		this.storageManagerPhone = storageManagerPhone;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getStorageStatus() {
		return storageStatus;
	}

	public void setStorageStatus(String storageStatus) {
		this.storageStatus = storageStatus;
	}

	@Override
	public String toString() {
		return "StorageDTO{" +
				"storageNo=" + storageNo +
				", storageName='" + storageName + '\'' +
				", storageManagerPhone='" + storageManagerPhone + '\'' +
				", empCode=" + empCode +
				", storageStatus='" + storageStatus + '\'' +
				'}';
	}
}
