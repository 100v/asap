package com.volt.asap.logistic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteriaforDoubleValue;
import com.volt.asap.logistic.dto.ProductCategoryDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController {

	private final ProductService productService;
	
	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping("/list")
	public ModelAndView productList(HttpServletRequest request,ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchValue1 = request.getParameter("searchValue1");
		String searchValue2 = request.getParameter("searchValue2");
		String searchCondition1 = request.getParameter("searchCondition1");
		String searchCondition2 = request.getParameter("searchCondition2"); 
		
		int totalCount = productService.selectTotalCount(searchCondition1, searchCondition2, searchValue1, searchValue2);
		System.out.println("totalCount" + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		SelectCriteriaforDoubleValue selectCriteriaforDoubleValue = null;
		
		if(searchValue1 != null && searchValue2 != null) {
			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount, searchCondition1,searchValue1, searchCondition2, searchValue2);
		} else {
			/* 맨 처음일 때*/
			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount);
		}
		
		List<ProductDTO> productlist = productService.productList(selectCriteriaforDoubleValue);
		List<StorageDTO> storageList = productService.storageList();
		List<ProductCategoryDTO> categoryList = productService.categoryList();
		
		System.out.println("상품리스트" + productlist);
		System.out.println("창고리스트" + storageList);
		System.out.println("케타고리리스트" + categoryList);
		
		
		mv.addObject("productlist", productlist);
		mv.addObject("storageList", storageList);
		mv.addObject("categoryList", categoryList);
		mv.addObject("selectCriteriaforDoubleValue", selectCriteriaforDoubleValue);
		mv.setViewName("product/list");
		
		System.out.println("dto 조회를 한번 해볼까요" + productlist);
		
		return mv;
	}
	
	@GetMapping("/{productCode}")
	public ModelAndView productDetail(ModelAndView mv, @PathVariable String productCode) {
		ProductDTO productDetail = productService.productDetail(productCode);
		
		List<StorageDTO> storageList = productService.storageList();
		List<ProductCategoryDTO> categoryList = productService.categoryList();
		
		System.out.println("상세보기 코드 확인용입니당 " + productCode);
		System.out.println("상세보기 코드 확인용입니당(조인) " + productDetail);
		
		mv.addObject("storageList", storageList);
		mv.addObject("categoryList", categoryList);
		mv.addObject("productDetail", productDetail);
		mv.setViewName("/product/productdetail");
		
		return mv;
	}
	
	@GetMapping("/regist")
	public void registPage() {}
	
	@PostMapping("/regist")
	public ModelAndView registProduct(ModelAndView mv, ProductDTO productInsert, RedirectAttributes rttr) {
		System.out.println("입력값 확인용 " + productInsert);
		
		String maintain = productInsert.getProductStatus();
		
		if(maintain == null || "".equals(maintain) ) {
			productInsert.setProductStatus("N");
		}
		
		productService.registProduct(productInsert);
		
		rttr.addFlashAttribute("registSuccessMessage", "상품등록성공했습니다.");
		mv.setViewName("redirect:/product/list");
		
		return mv;
	}
	
	@PostMapping("/modify")
	public String productmodifypage(RedirectAttributes rttr, @ModelAttribute ProductDTO productUpdate) {
		
		productService.modifyProduct(productUpdate);
		
		rttr.addFlashAttribute("modifySuccessMessage", "상품 수정 성공!");
		
		return "redirect:/product/list";
	}
}

























