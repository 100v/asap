package com.volt.asap.logistic.dto;

import java.io.Serializable;
import java.sql.Date;

import com.volt.asap.sales.dto.ClientDTO;

public class ReceivingHistoryDTO implements Serializable {

	private static final long serialVersionUID = -5384564811508014305L;
	
	private int receivingHistoryNo;
	private int receivingAmount;
	private String receivingComent;
	private java.sql.Date inventoryDate;
	private String productCode;
	private int storageNo;
	private int clientNo;
	
	private ProductDTO productDTO;
	private StorageDTO storageDTO;
	private ClientDTO clientDTO;
	
	public ReceivingHistoryDTO() {
	}

	public ReceivingHistoryDTO(int receivingHistoryNo, int receivingAmount, String receivingComent, Date inventoryDate,
			String productCode, int storageNo, int clientNo, ProductDTO productDTO, StorageDTO storageDTO,
			ClientDTO clientDTO) {
		this.receivingHistoryNo = receivingHistoryNo;
		this.receivingAmount = receivingAmount;
		this.receivingComent = receivingComent;
		this.inventoryDate = inventoryDate;
		this.productCode = productCode;
		this.storageNo = storageNo;
		this.clientNo = clientNo;
		this.productDTO = productDTO;
		this.storageDTO = storageDTO;
		this.clientDTO = clientDTO;
	}

	public int getReceivingHistoryNo() {
		return receivingHistoryNo;
	}

	public void setReceivingHistoryNo(int receivingHistoryNo) {
		this.receivingHistoryNo = receivingHistoryNo;
	}

	public int getReceivingAmount() {
		return receivingAmount;
	}

	public void setReceivingAmount(int receivingAmount) {
		this.receivingAmount = receivingAmount;
	}

	public String getReceivingComent() {
		return receivingComent;
	}

	public void setReceivingComent(String receivingComent) {
		this.receivingComent = receivingComent;
	}

	public java.sql.Date getInventoryDate() {
		return inventoryDate;
	}

	public void setInventoryDate(java.sql.Date inventoryDate) {
		this.inventoryDate = inventoryDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public int getClientNo() {
		return clientNo;
	}

	public void setClientNo(int clientNo) {
		this.clientNo = clientNo;
	}

	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}

	public StorageDTO getStorageDTO() {
		return storageDTO;
	}

	public void setStorageDTO(StorageDTO storageDTO) {
		this.storageDTO = storageDTO;
	}

	public ClientDTO getClientDTO() {
		return clientDTO;
	}

	public void setClientDTO(ClientDTO clientDTO) {
		this.clientDTO = clientDTO;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ReceivingHistoryDTO [receivingHistoryNo=" + receivingHistoryNo + ", receivingAmount=" + receivingAmount
				+ ", receivingComent=" + receivingComent + ", inventoryDate=" + inventoryDate + ", productCode="
				+ productCode + ", storageNo=" + storageNo + ", clientNo=" + clientNo + ", productDTO=" + productDTO
				+ ", storageDTO=" + storageDTO + ", clientDTO=" + clientDTO + "]";
	}
	
}
