package com.volt.asap.logistic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.logistic.entity.Storage;

import java.util.List;

public interface StorageRepository extends JpaRepository<Storage, Integer> {

    List<Storage> findAllByStorageStatus(String status);
}
