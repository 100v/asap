package com.volt.asap.logistic.dto;

import java.io.Serializable;

public class ProductDTO implements Serializable{

	private static final long serialVersionUID = -2632827694512993140L;
	
	private String productCode;
	private String productName;
	private int productRelease;
	private int productWaearing;
	private int storageNo;
	private String categoryCode;
	private String productStatus;
	
	private ProductCategoryDTO category;
	private StorageDTO storage;
	
	public ProductDTO() {
	}

	public ProductDTO(String productCode, String productName, int productRelease, int productWaearing, int storageNo,
			String categoryCode, String productStatus, ProductCategoryDTO category, StorageDTO storage) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productRelease = productRelease;
		this.productWaearing = productWaearing;
		this.storageNo = storageNo;
		this.categoryCode = categoryCode;
		this.productStatus = productStatus;
		this.category = category;
		this.storage = storage;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductRelease() {
		return productRelease;
	}

	public void setProductRelease(int productRelease) {
		this.productRelease = productRelease;
	}

	public int getProductWaearing() {
		return productWaearing;
	}

	public void setProductWaearing(int productWaearing) {
		this.productWaearing = productWaearing;
	}

	public int getStorageNo() {
		return storageNo;
	}

	public void setStorageNo(int storageNo) {
		this.storageNo = storageNo;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public ProductCategoryDTO getCategory() {
		return category;
	}

	public void setCategory(ProductCategoryDTO category) {
		this.category = category;
	}

	public StorageDTO getStorage() {
		return storage;
	}

	public void setStorage(StorageDTO storage) {
		this.storage = storage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProductDTO [productCode=" + productCode + ", productName=" + productName + ", productRelease="
				+ productRelease + ", productWaearing=" + productWaearing + ", storageNo=" + storageNo
				+ ", categoryCode=" + categoryCode + ", productStatus=" + productStatus + ", category=" + category
				+ ", storage=" + storage + "]";
	}

	
}
