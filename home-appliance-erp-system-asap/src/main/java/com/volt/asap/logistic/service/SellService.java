package com.volt.asap.logistic.service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.SellDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.Sell;
import com.volt.asap.logistic.entity.SellAndClient;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.SellAndClientRepository;
import com.volt.asap.logistic.repository.SellRepository;
import com.volt.asap.logistic.repository.StorageRepository;
import com.volt.asap.sales.dto.ClientDTO;
import com.volt.asap.sales.entity.Client;
import com.volt.asap.sales.repository.ClientRepository;

@Service
public class SellService {

	private final SellRepository sellRepository;
	private final ClientRepository clientRepository;
	private final ProductRepository productRepository;
	private final SellAndClientRepository sellAndClientRepository;
	private final StorageRepository storageRepository;
	private final ModelMapper modelMapper;

	@Autowired
	public SellService(SellRepository sellRepository, ModelMapper modelMapper, ClientRepository clientRepository,
			SellAndClientRepository sellAndClientRepository, ProductRepository productRepository, StorageRepository storageRepository) {
		this.sellRepository = sellRepository;
		this.clientRepository = clientRepository;
		this.productRepository = productRepository;
		this.sellAndClientRepository = sellAndClientRepository;
		this.storageRepository = storageRepository;
		this.modelMapper = modelMapper;
	}

//	public List<SellDTO> findSellList(SelectCriteriaforDoubleValue selectCriteriaforDoubleValue) {
//		int index = selectCriteriaforDoubleValue.getPageNo() - 1;			// Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
//		int count = selectCriteriaforDoubleValue.getLimit();
//		String searchValue1 = selectCriteriaforDoubleValue.getSearchValue1();
//		String searchValue2 = selectCriteriaforDoubleValue.getSearchValue2();
//		/* 페이징 처리와 정렬을 위한 객체 생성 */
//		Pageable paging = PageRequest.of(index, count, Sort.by(Sort.Direction.DESC,"sellNumber"));
//		
//		List<SellAndClient> sellList = sellAndClientRepository.findAll(Sort.by("sellNumber"));
//		
//		if(searchValue1 != null && searchValue2 != null) {}
//		else if(searchValue1 != null && searchValue2 == null) {
//			/* 주문일자로 검색할때 */
//			sellList = sellAndClientRepository.findByProductProductNameContaining(searchValue1,paging);
//		}
//		else if(searchValue1 == null && searchValue2 != null) {
//			/* 상품명로 검색할때 */
//			sellList = sellAndClientRepository.findByClientNameContaining(searchValue2,paging);
//		}else {
//			sellList = sellAndClientRepository.findAll(paging).toList();
//		}
//		
//		System.out.println("판매내역 조회 : " + sellList);
//		return sellList.stream().map(sell -> modelMapper.map(sell, SellDTO.class)).collect(Collectors.toList());
//	}

	public SellDTO findSellByNumber(int sellNumber) {

		SellAndClient sellDetail = sellAndClientRepository.findSellAndClientEntityBySellNumber(sellNumber);

		return modelMapper.map(sellDetail, SellDTO.class);
	}

	@Transactional
	public void registNewSell(SellDTO newSell) {

		sellRepository.save(modelMapper.map(newSell, Sell.class));
	}

	@Transactional
	public void modifySell(SellDTO sellUpdate) {

		Sell updateSell = sellRepository.findSellEntityBySellNumber(sellUpdate.getSellNumber());

		updateSell.setSellDate(sellUpdate.getSellDate());
		updateSell.setSellAmount(sellUpdate.getSellAmount());
		updateSell.setSellTotalPrice(sellUpdate.getSellTotalPrice());
		updateSell.setProductCode(sellUpdate.getProductCode());
		updateSell.setClientNumber(sellUpdate.getClientNumber());
		updateSell.setStorageNo(sellUpdate.getStorageNo());
		updateSell.setSellPrice(sellUpdate.getSellPrice());
	}

	public List<ClientDTO> findClientList() {
		List<Client> clientList = clientRepository.findAll();
		return clientList.stream().map(client -> modelMapper.map(client, ClientDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findProductList() {
		List<Product> productList = productRepository.findAll();

		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class))
				.collect(Collectors.toList());
	}

//	public int selectTotalCount(String searchCondition1, String searchCondition2, String searchValue1,
//			String searchValue2) {
//		int count = 0;
//
//		if (searchValue1 != null && searchValue2 != null) {
//			if (!searchValue1.isBlank() && !searchValue2.isEmpty()) {
////				count =	sellAndClientRepository.countByProductProductNameAndClientNameContaining(searchValue1,searchValue2);
//			} else if (!searchValue1.isEmpty() && searchValue2.isEmpty()) {
//				count = sellAndClientRepository.countByProductProductNameContaining(searchValue1);
//			} else if (searchValue1.isEmpty() && !searchValue2.isEmpty()) {
//				count = sellAndClientRepository.countByClientNameContaining(searchValue2);
//			} else {
//				count = (int) sellAndClientRepository.count();
//			}
//		} else {
//			System.out.println("노 검색");
//			count = (int) sellAndClientRepository.count();
//			System.out.println("count" + count);
//		}
//
//		return count;
//	}

	public int countSellByQueries(Map<String, String> parameterMap) {
		if (parameterMap == null) {

			/* 전체 조회 */
			return (int) sellAndClientRepository.count();
		} else {
			/* 검색어 입력을 통한 부분 조회 */
			Date startDate = toSqlDate(parameterMap.get("startDate"));
			Date endDate = toSqlDate(parameterMap.get("endDate"));
			String searchProduct = parameterMap.get("searchProduct");
			String searchClient = parameterMap.get("searchClient");

			if (startDate != null && endDate != null) {
				if ("".equals(searchProduct) && "".equals(searchClient)) {

					return sellAndClientRepository.countBySellDateBetween(startDate, endDate);

				} else if ("".equals(searchClient)) {
					return sellAndClientRepository.countBySellDateBetweenAndProductProductNameContaining(startDate,
							endDate, searchProduct);

				} else {
					return sellAndClientRepository.countBySellDateBetweenAndClientNameContaining(startDate, endDate,
							searchClient);
				}
			} else if ("".equals(searchClient)) {
				return sellAndClientRepository.countByProductProductNameContaining(searchProduct);
			} else {
				return sellAndClientRepository.countByClientNameContaining(searchClient);
			}
		}
	}

	public List<SellDTO> findSellByQueries(CustomSelectCriteria selectCriteria) {
		/* Pageable 객체는 1페이지를 0번으로 인식 */
		int index = selectCriteria.getPageNo() - 1;
		int count = selectCriteria.getLimit();
		
		
		Pageable paging = PageRequest.of(index, count, Sort.Direction.DESC,("sellNumber"));
		
		System.out.println("넘어오나 " + index);
		List<SellAndClient> sellList = null;
		if (selectCriteria.getSearchValueMap() == null) {
			/* 전체 조회 */

			sellList = sellAndClientRepository.findAll(paging).toList();
		} else {
			/* 검색어 입력을 통한 부분 조회 */
			java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
			java.sql.Date endDate = toSqlDate(selectCriteria.getSearchValueMap().get("endDate"));
			String searchProduct = selectCriteria.getSearchValueMap().get("searchProduct");
			String searchClient = selectCriteria.getSearchValueMap().get("searchClient");
			
			if (startDate != null && endDate != null) {
				if ("".equals(searchProduct) && "".equals(searchClient)) {
					sellList = sellAndClientRepository.findAllBySellDateBetween(startDate, endDate, paging);
				} else if ("".equals(searchClient)) {
					sellList = sellAndClientRepository.findAllBySellDateBetweenAndProductProductNameContaining(startDate, endDate,
							searchProduct, paging);
				} else if("".equals(searchProduct)){
					sellList = sellAndClientRepository.findAllBySellDateBetweenAndClientNameContaining(startDate, endDate,
							searchClient, paging);
				} else {
					sellList = sellAndClientRepository.findAllBySellDateBetweenAndClientNameAndProductProductNameContaining(startDate, endDate,
							searchClient,searchProduct, paging);
				}
			}else if("".equals(searchProduct)) {
				sellList = sellAndClientRepository.findAllByClientNameContaining(searchClient,paging);
			}else if ("".equals(searchClient)) {
				sellList = sellAndClientRepository.findAllByProductProductNameContaining(searchProduct,paging);
			} else {
				sellList = sellAndClientRepository.findAllByProductProductNameContainingAndClientNameContaining(searchProduct,searchClient,paging);
			}
		}

		return sellList.stream().map(sell -> modelMapper.map(sell, SellDTO.class)).collect(Collectors.toList());
	}

	private java.sql.Date toSqlDate(String dateString) {
		java.sql.Date endDate = null;

		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}

		return endDate;
	}

	public List<StorageDTO> findStorageList() {
		List<Storage> storageList = storageRepository.findAll();
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findAllProduct() {
		List<Product> productList = productRepository.findAll();
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findProductByStorageCode(int storage) {
		List<Product> productList = productRepository.findAllByStorageNo(storage);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public Map<String, Object> modifyOldProduct(int no, String newProductName) {
		Product product = productRepository.findByProductName(newProductName);
		
		System.out.println(product);
		System.out.println(product.getProductCode());
		System.out.println(product.getProductRelease());
		System.out.println(product.getProductRelease());
		
		Map<String, Object> result = new HashMap<>();
		result.put("productCode", product.getProductCode());
		result.put("productW", product.getProductWaearing());
		result.put("productR", product.getProductRelease());
		
		return result;
	}

	public List<ProductDTO> findProductByProductCode(String productCode) {
		List<Product> productList = productRepository.findByProductCode(productCode);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

}
