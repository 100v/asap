package com.volt.asap.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 메인 페이지 담당 컨트롤러
 * @see Controller
 */
@Controller
public class MainController {

    /**
     * '/' 의 GET 요청시 '/dashboard'로 이동
     * @return redirect to '/dashboard'
     * @see org.springframework.web.servlet.ViewResolver
     */
    @RequestMapping("/")
    public String main() {
        return "redirect:/dashboard";
    }
}
