package com.volt.asap.hrm.entity;

import java.sql.Date;

import javax.persistence.*;

@Entity(name = "Annual")
@Table(name = "TBL_ANNUAL")
@SequenceGenerator(
        name = "ANNUAL_CODE_SEQ_GENERATOR",
        sequenceName = "SEQ_ANNUAL_CODE",
        initialValue = 14,
        allocationSize = 1
)
public class Annual {
	
	@Id
	@GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "ANNUAL_CODE_SEQ_GENERATOR"
    )
	@Column(name = "ANNUAL_CODE")
	private int annualCode;
	
	@Column(name = "ANNUAL_NAME")
	private String annualName;
	
	@Column(name = "DEPT_CODE")
	private String deptCode;
	
	@Column(name = "ANNUAL_START")
	private java.sql.Date annualStart;
	
	@Column(name = "ANNUAL_LEAVE_DAT")
	private int annualLeaveDat;
	
	@Column(name = "EMP_CODE")
	private int empCode;
	
	@Column(name = "ANNUAL_END")
	private java.sql.Date annualEnd;
	
	@Column(name = "ANNUAL_YN")
	private String annualYn;

	public Annual() {
	}

	public Annual(int annualCode, String annualName, String deptCode, Date annualStart, int annualLeaveDat, int empCode,
			Date annualEnd, String annualYn) {
		this.annualCode = annualCode;
		this.annualName = annualName;
		this.deptCode = deptCode;
		this.annualStart = annualStart;
		this.annualLeaveDat = annualLeaveDat;
		this.empCode = empCode;
		this.annualEnd = annualEnd;
		this.annualYn = annualYn;
	}

	public int getAnnualCode() {
		return annualCode;
	}

	public void setAnnualCode(int annualCode) {
		this.annualCode = annualCode;
	}

	public String getAnnualName() {
		return annualName;
	}

	public void setAnnualName(String annualName) {
		this.annualName = annualName;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public java.sql.Date getAnnualStart() {
		return annualStart;
	}

	public void setAnnualStart(java.sql.Date annualStart) {
		this.annualStart = annualStart;
	}

	public int getAnnualLeaveDat() {
		return annualLeaveDat;
	}

	public void setAnnualLeaveDat(int annualLeaveDat) {
		this.annualLeaveDat = annualLeaveDat;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public java.sql.Date getAnnualEnd() {
		return annualEnd;
	}

	public void setAnnualEnd(java.sql.Date annualEnd) {
		this.annualEnd = annualEnd;
	}

	public String getAnnualYn() {
		return annualYn;
	}

	public void setAnnualYn(String annualYn) {
		this.annualYn = annualYn;
	}

	@Override
	public String toString() {
		return "Annual [annualCode=" + annualCode + ", annualName=" + annualName + ", deptCode=" + deptCode
				+ ", annualStart=" + annualStart + ", annualLeaveDat=" + annualLeaveDat + ", empCode=" + empCode
				+ ", annualEnd=" + annualEnd + ", annualYn=" + annualYn + "]";
	}
}
