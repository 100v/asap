package com.volt.asap.hrm.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.AttendanceDTO;
import com.volt.asap.hrm.entity.Attendance;
import com.volt.asap.hrm.entity.AttendanceAndEmployee;
import com.volt.asap.hrm.repository.AttendanceAndEmployeeRepository;
import com.volt.asap.hrm.repository.AttendanceRepository;
import com.volt.asap.hrm.repository.EmployeeRepository;
import com.volt.asap.logistic.dto.ProductCategoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Category;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.CategoryRepository;
import com.volt.asap.logistic.repository.StorageRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.NonUniqueResultException;
import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AttendanceService {
	
	private final AttendanceRepository attendanceRepository;
	private final AttendanceAndEmployeeRepository attendanceAndEmployeeRepository;
	private final ModelMapper modelMapper;
	private final EmployeeRepository employeeRepository;
	private final StorageRepository storageRepository;
	private final CategoryRepository categoryRepository;
	
	public AttendanceService(AttendanceRepository attendanceRepository, ModelMapper modelMapper, 
			AttendanceAndEmployeeRepository attendanceAndEmployeeRepository, EmployeeRepository employeeRepository
			, StorageRepository storageRepository, CategoryRepository categoryRepository) {
		this.attendanceRepository = attendanceRepository;
		this.attendanceAndEmployeeRepository = attendanceAndEmployeeRepository;
		this.modelMapper = modelMapper;
		this.employeeRepository = employeeRepository;
		this.storageRepository = storageRepository;
		this.categoryRepository = categoryRepository;
	}
	


//	public List<AttendanceDTO> attendanceList(SelectCriteriaforDate selectCriteriaforDate) {
//		
//		int index = selectCriteriaforDate.getPageNo() - 1;
//		int count = selectCriteriaforDate.getLimit();
//		Date startDate = selectCriteriaforDate.getStartDate();
//		Date endDate = selectCriteriaforDate.getEndDate();
//		String serachWord = selectCriteriaforDate.getSearchWord();
//		
//		Pageable paging = PageRequest.of(index, count);
//		
//		List<AttendanceAndEmployee> attendanceList = attendanceAndEmployeeRepository.findAll(Sort.by("attendanceCode").descending());
//		
//		if("".equals(serachWord)) {
//			serachWord = null;
//		}
//		
//		System.out.println("startDate" + startDate);
//		System.out.println("endDate" + endDate);
//		System.out.println("serachWord" + serachWord);
//		
//		if(startDate != null && endDate != null && serachWord == null) {
//			/* 날짜만 */
//			attendanceList = attendanceAndEmployeeRepository.findByWorkDateBetween(startDate, endDate, paging);
//			System.out.println("1번 if문에서 넘어오는 값 확인" + startDate + "  " + endDate);
//		} else if(startDate != null && endDate != null && serachWord != null){
//			/* 날짜, 이름 */
//			attendanceList = attendanceAndEmployeeRepository.findByWorkDateBetweenAndEmpEmpName(startDate,endDate,serachWord, paging);
//			System.out.println("2번 if문에서 넘어오는 값 확인" + startDate + "  " + endDate + "   검색어 확인 " + serachWord);
//		} else if(startDate == null && endDate == null && serachWord != null) {
//			attendanceList = attendanceAndEmployeeRepository.findByEmpEmpName(serachWord, paging);
//			System.out.println("3번 if문에서 넘어오는 값 확인 " + serachWord);
//		}
//		
//		else {
//			/* 전체 */
//			attendanceList = attendanceAndEmployeeRepository.findAll(paging).toList();
//		}	
//		
//		return attendanceList.stream().map(attendance -> modelMapper.map(attendance, AttendanceDTO.class)).collect(Collectors.toList());
//	}

	public AttendanceDTO attendanceDetail(int attendanceCode) {
		
		AttendanceAndEmployee attendancedetail = attendanceAndEmployeeRepository.findAttendanceByattendanceCode(attendanceCode);
		
		return modelMapper.map(attendancedetail, AttendanceDTO.class);
	}

	@Transactional
	public void ModifyAttendance(AttendanceDTO attendanceUpdate) {
		
		Attendance updateAttendance = attendanceRepository.findAttendanceByattendanceCode(attendanceUpdate.getAttendanceCode());
		
		updateAttendance.setAttendanceCode(attendanceUpdate.getAttendanceCode());
		updateAttendance.setEmpCode(attendanceUpdate.getEmpCode());
		updateAttendance.setGoworkTime(attendanceUpdate.getGoworkTime());
		updateAttendance.setOutworkTime(attendanceUpdate.getOutworkTime());
		updateAttendance.setOvertimeAllowance(attendanceUpdate.getOvertimeAllowance());
		updateAttendance.setWorkDate(attendanceUpdate.getWorkDate());
		updateAttendance.setWorkTime(attendanceUpdate.getWorkTime());
	}

	public int selectTotalCount() {
		int count = 0;
		count = (int)attendanceAndEmployeeRepository.count();
		return count;
	}



//	public int selectTotalCount1(Date startDate, Date endDate, String searchWord) {
//		int count = 0;
//		
//		if(startDate != null && endDate != null) {
//			count = attendanceAndEmployeeRepository.countByWorkDateBetween(startDate, endDate);
//		} else {
//			count = (int)attendanceAndEmployeeRepository.count();
//		}
//
//		return count;
//	}



	public void registAttendance(AttendanceDTO attendanceInsert) {

		attendanceRepository.save(modelMapper.map(attendanceInsert, Attendance.class));
	}



//	public int selectTotalCount2(String searchWord) {
//		
//		int count = (int)attendanceAndEmployeeRepository.count();
//		return count;
//	}



	public int countAttendance(Map<String, String> parameterMap) {
		if (parameterMap == null) {
			/* 전체 조회 */

			return (int) attendanceAndEmployeeRepository.count();
		} else {
			/* 검색어 입력을 통한 부분 조회 */
			Date startDate = toSqlDate(parameterMap.get("startDate"));
			Date endDate = toSqlDate(parameterMap.get("endDate"));
			String searchWord = parameterMap.get("searchWord");

			if (startDate != null && endDate != null) {
				if ("".equals(searchWord)) {
					/* 날짜 검색 */
					return attendanceAndEmployeeRepository.countByWorkDateBetween(startDate, endDate);
							
				} else {
					/* 날짜, 검색어 */
					return attendanceAndEmployeeRepository.countByWorkDateBetweenAndEmpEmpNameContaining(startDate, endDate, searchWord);
				}
			} else {
				/* 검색어 */
				return attendanceAndEmployeeRepository.countByEmpEmpNameContaining(searchWord);
			}
		}
	}



	public List<AttendanceDTO> attendanceList(CustomSelectCriteria selectCriteria) {
		int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();
        
        Pageable paging = PageRequest.of(index, count);
        
		List<AttendanceAndEmployee> attendanceList = null;
		
		if(selectCriteria.getSearchValueMap() == null) {
            /* 전체 조회 */
			attendanceList = attendanceAndEmployeeRepository.findAll(paging).toList();
        } else {
            /* 검색어 입력을 통한 부분 조회 */
            java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
            java.sql.Date endDate = toSqlDate(selectCriteria.getSearchValueMap().get("endDate"));
            String searchWord = selectCriteria.getSearchValueMap().get("searchWord");
            
            if(startDate != null && endDate != null) {
                if("".equals(searchWord)) {
                    /* 날짜 검색 */
                	attendanceList =attendanceAndEmployeeRepository.findByWorkDateBetween(paging, startDate, endDate);
                } else {
                    /* 날짜, 검색어 */
                	attendanceList = attendanceAndEmployeeRepository.findByWorkDateBetweenAndEmpEmpNameContaining(paging, startDate, endDate, searchWord);
                }
            } else {
                /* 검색어 */
            		attendanceList = attendanceAndEmployeeRepository.findByEmpEmpNameContaining(paging, searchWord);
            }
        }
		return attendanceList.stream().map(attendance -> modelMapper.map(attendance, AttendanceDTO.class)).collect(Collectors.toList());
		
	}

	
	private java.sql.Date toSqlDate(String dateString) {
		java.sql.Date endDate = null;

		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}

		return endDate;
	}



	public List<StorageDTO> findStorage() {
		
		List<Storage> storageList = storageRepository.findAll();
		
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}



	public List<ProductCategoryDTO> findCategory() {
		List<Category> categoryList = categoryRepository.findAll();
		
		return categoryList.stream().map(storage -> modelMapper.map(storage, ProductCategoryDTO.class)).collect(Collectors.toList());
	}

	@Transactional
	public void registGoWork(AttendanceDTO attendanceDTO) {
		int empCode = attendanceDTO.getEmpCode();
		Date now = Date.valueOf(LocalDate.now().toString());
		if(!attendanceRepository.existsByEmpCodeAndWorkDate(empCode, now)) {
			/* 출근 버튼 처음 누름 */
			try {
				attendanceRepository.save(modelMapper.map(attendanceDTO, Attendance.class));
			} catch (IllegalArgumentException e) {
				throw new RuntimeException("출근 등록에 실패했습니다.");
			}
		} else {
			throw new RuntimeException("이미 출근 했습니다.");
		}
	}

	@Transactional
	public void registOutWork(int empCode, LocalDate now) {
		Optional<Attendance> optionalAttendance;
		try {
			optionalAttendance = attendanceRepository.findByEmpCodeAndWorkDate(empCode, Date.valueOf(now.toString()));
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("퇴근 등록에 실패했습니다. 잘못된 접근입니다.");
		} catch (NonUniqueResultException e) {
			throw new RuntimeException("퇴근 등록에 실패했습니다. 출근이 두개이상 발견되었습니다.");
		}

		if(optionalAttendance.isEmpty()) throw new RuntimeException("퇴근 등록에 실패했습니다. 출근을 먼저 진행해 주세요.");


		Attendance attendance = optionalAttendance.get();
		LocalTime outWorkTime = LocalTime.now();
		LocalTime workTime = LocalTime.parse(attendance.getGoworkTime());
		int between = (int) ChronoUnit.HOURS.between(workTime, outWorkTime);

		attendance.setOutworkTime(outWorkTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		attendance.setWorkTime(between);
		if(between > 8) {
			attendance.setOvertimeAllowance(between - 8);
		}
	}

	public AttendanceDTO findAttendanceByEmpCodeAndWorkDate(int empCode, LocalDate now) {
		Optional<Attendance> optionalAttendance;

		try {
			Date date = Date.valueOf(now.toString());
			optionalAttendance = attendanceRepository.findByEmpCodeAndWorkDate(empCode, date);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("잘못된 접근입니다.");
		} catch (NonUniqueResultException e) {
			throw new RuntimeException("출근 및 퇴근 기록이 두개 이상 발견되었습니다.");
		}
		if(optionalAttendance.isEmpty()) throw new RuntimeException("출근 및 퇴근 기록이 존재하지 않습니다.");

		return modelMapper.map(optionalAttendance.get(), AttendanceDTO.class);
	}
}