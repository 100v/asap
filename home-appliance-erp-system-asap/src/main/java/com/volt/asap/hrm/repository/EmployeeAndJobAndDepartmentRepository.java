package com.volt.asap.hrm.repository;

import java.util.List;

import com.volt.asap.hrm.entity.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.EmployeeAndJobAndDepartment;

public interface EmployeeAndJobAndDepartmentRepository extends JpaRepository<EmployeeAndJobAndDepartment, Integer>{

	List<EmployeeAndJobAndDepartment> findByEmpNameContaining(String searchValue, Pageable paging);

	List<EmployeeAndJobAndDepartment> findByDepartmentNameContaining(String searchValue, Pageable paging);

	List<EmployeeAndJobAndDepartment> findByJobGradeJobNameContaining(String searchValue, Pageable paging);

	int countByEmpNameContaining(String searchValue);

	int countByDepartmentNameContaining(String searchValue);

	int countByJobGradeJobNameContaining(String searchValue);

	EmployeeAndJobAndDepartment findEmployeeByEmpCode(int empCode);

	List<EmployeeAndJobAndDepartment> findByEmpLeaveYnLike(String leaveYn);

	EmployeeAndJobAndDepartment findByEmpCodeAndEmpLeaveYn(int empCode, String empLeaveYn);

}
