package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.Certification;

public interface CertificationRepository extends JpaRepository<Certification, String>{

}
