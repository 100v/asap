package com.volt.asap.hrm.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.dto.AnnualDTO;
import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.entity.Annual;
import com.volt.asap.hrm.entity.AnnualAndEmployee;
import com.volt.asap.hrm.entity.Department;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.repository.AnnualAndEmployeeRepository;
import com.volt.asap.hrm.repository.AnnualRepository;
import com.volt.asap.hrm.repository.DepartmentRepository;
import com.volt.asap.hrm.repository.EmployeeRepository;

@Service
public class AnnualService {

	private final AnnualAndEmployeeRepository annualAndEmployeeRepository;
	private final AnnualRepository annualRepository;
	private final DepartmentRepository departmentRepository;
	private final EmployeeRepository employeeRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public AnnualService(AnnualAndEmployeeRepository annualAndEmployeeRepository, AnnualRepository annualRepository
			, ModelMapper modelMapper, DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
		this.annualAndEmployeeRepository = annualAndEmployeeRepository;
		this.annualRepository = annualRepository;
		this.departmentRepository = departmentRepository;
		this.employeeRepository = employeeRepository;
		this.modelMapper = modelMapper;
	}

	/* 연차 사용 내역 조회 */
	public List<AnnualDTO> findUseAnnualList() {
		
		List<AnnualAndEmployee> useList = annualAndEmployeeRepository.findAll();
		return useList.stream().map(annualAndEmployeeRepository -> modelMapper.map(annualAndEmployeeRepository, AnnualDTO.class)).collect(Collectors.toList());
	}

	/* 연차 보유 현황 페이징을 위해 게시글 조회 */
	public int selectTotalCount(String searchCondition, String searchValue) {
		
		int count = 0;
		
		if(searchValue != null) {
			if("empName".equals(searchCondition)) {
				count = annualAndEmployeeRepository.countByEmployeeEmpNameContaining(searchValue);
			}
			if("deptName".equals(searchCondition)) {
				count = annualAndEmployeeRepository.countByEmployeeDepartmentNameContaining(searchValue);
			}
		} else {
			count = (int)annualAndEmployeeRepository.count();
			System.out.println("게시글 수 : " + count);
		}
		
		return count;
	}

	/* 연차 보유 현황 조회 */
	public List<AnnualDTO> findAnnualOwnList(SelectCriteria selectCriteria) {
		
		int index = selectCriteria.getPageNo() - 1;
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();

		/* 페이징 처리와 정렬을 위한 객체 생성 */
		Pageable paging = PageRequest.of(index, count, Sort.by("empLeaveYn").and(Sort.by("empCode").descending()));
		
		List<AnnualAndEmployee> ownList = new ArrayList<AnnualAndEmployee>();
		
		System.out.println("SearchCondition : " + selectCriteria.getSearchCondition());
		System.out.println("SearchValue : " + selectCriteria.getSearchValue());
		
		if(searchValue != null) {
			
			/* 이름 검색일 경우 */
			if("empName".equals(selectCriteria.getSearchCondition())) {
				ownList = annualAndEmployeeRepository.findByEmployeeEmpNameContaining(selectCriteria.getSearchValue(), paging);
			}
			
			/* 부서 검색일 경우 */
			if("deptName".equals(selectCriteria.getSearchCondition())) {
				ownList = annualAndEmployeeRepository.findByEmployeeDepartmentNameContaining(selectCriteria.getSearchValue(), paging);
			}
		} else {
			ownList = annualAndEmployeeRepository.findAll(paging).toList();
		}
		return ownList.stream().map(annualAndEmployeeRepository -> modelMapper.map(annualAndEmployeeRepository, AnnualDTO.class)).collect(Collectors.toList());
	}

	/* 연차 등록 */
	@Transactional
	public void registAnnual(AnnualDTO annual) {

		System.out.println("연차 등록 내용 : " + annual);
		
		annualRepository.save(modelMapper.map(annual, Annual.class));
		
	}

	/* 연차 사용 내역 수정 */
	public AnnualDTO findUseAnnualByCode(int annualCode) {
		
		AnnualAndEmployee annual = annualAndEmployeeRepository.findByAnnualCodeLike(annualCode);
		
		return modelMapper.map(annual, AnnualDTO.class);
	}

	/* 부서 목록 조회 */
	public List<DepartmentDTO> findDepartmentList() {
		
		List<Department> deptList = departmentRepository.findAll();
		return deptList.stream().map(department -> modelMapper.map(department, DepartmentDTO.class)).collect(Collectors.toList());
	}

	/* 연차 사용 내역 수정 */
	@Transactional
	public void modifyAnnualUse(AnnualDTO annual) {
		
		Annual useAnnual = annualRepository.findByAnnualCodeLike(annual.getAnnualCode());
		useAnnual.setDeptCode(annual.getDeptCode());
		useAnnual.setAnnualStart(annual.getAnnualStart());
		useAnnual.setAnnualEnd(annual.getAnnualEnd());
		useAnnual.setAnnualName(annual.getAnnualName());
		useAnnual.setAnnualLeaveDat(annual.getAnnualLeaveDat());
	}

	/* 연차 사용 내역 게시글 수 조회 */
	public int selectUseListTotalCount(String searchCondition1, String searchCondition2, String searchValue1,
			String searchValue2) {
		
		java.sql.Date startDate = toSqlDate(searchValue1);
		
		int count = 0;
		if(startDate != null && searchValue2 != null) {
			if(startDate != null && searchValue2 != null) {
				count = annualAndEmployeeRepository.countByAnnualStartAndEmployeeEmpNameContaining(startDate, searchValue2);
				System.out.println("날짜 및 이름으로 검색할 때 : " + count);
			} 
		} else {
			count = (int)annualAndEmployeeRepository.count();
			System.out.println("날짜, 이름 둘다 검색하지 않을 때 : " + count);
		}
		
		return count;
	}

	/* 연차 사용 내역 페이징 처리를 위한 게시글 수 조회 */
	public int selectUseListTotalCount(Map<String, String> parameter) {
		
		int count = 0;
		
		java.sql.Date searchValue1 = toSqlDate(parameter.get("searchValue1"));
		String searchValue2 = parameter.get("searchValue2");
		
		if(searchValue1 != null && searchValue2 !=null) {
			count = annualAndEmployeeRepository.countByAnnualStartAndEmployeeEmpNameContaining(searchValue1, searchValue2);
		} else if(searchValue1 != null) {
			count = annualAndEmployeeRepository.countByAnnualStart(searchValue1);
		} else if(searchValue2 !=null) {
			count = annualAndEmployeeRepository.countByEmployeeEmpNameContaining(searchValue2);
		} else {
			count = (int)annualAndEmployeeRepository.count();
		}
		return count;
	}

	/* 연차 사용 내역 검색 조건별 조회 */
	public List<AnnualDTO> findUseAnnualList(CustomSelectCriteria selectCriteria) {
		int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();

        Pageable paging = PageRequest.of(index, count, Sort.by("annualStart").descending());
        
        List<AnnualAndEmployee> useList = new ArrayList<AnnualAndEmployee>();
        
        java.sql.Date searchValue1 = toSqlDate(selectCriteria.getSearchValueMap().get("searchValue1"));
		String searchValue2 = selectCriteria.getSearchValueMap().get("searchValue2");
		String searchCondition1 = selectCriteria.getSearchValueMap().get("searchCondition1");
		String searchCondition2 = selectCriteria.getSearchValueMap().get("searchCondition2");
        
		if(searchValue1 != null && searchValue2 !=null) {
			
			/* 날짜, 이름 검색일 경우 */
			useList = annualAndEmployeeRepository.findByAnnualStartAndEmployeeEmpNameContaining(searchValue1, searchValue2, paging);
		} else if(searchValue1 != null) {
			
			/* 날짜 검색일 경우 */
			useList = annualAndEmployeeRepository.findByAnnualStart(searchValue1, paging);
		} else if(searchValue2 !=null) {
			
			/* 이름 검색일 경우 */
			useList = annualAndEmployeeRepository.findByEmployeeEmpNameContaining(searchValue2, paging);
		} else {
			
			/* 전체 조회(검색값 없을 때) */
			useList = annualAndEmployeeRepository.findAll(paging).toList();
		}
        return useList.stream().map(annualAndEmployeeRepository -> modelMapper.map(annualAndEmployeeRepository, AnnualDTO.class)).collect(Collectors.toList());
	}

	/* 연차 잔여 횟수 차감 */
	@Transactional
	public void modifyAnnualLeftDay(AnnualDTO annual) {
		Employee modifyEmployee = employeeRepository.findByEmpCodeLike(annual.getEmployee().getEmpCode());
		System.out.println("서비스에서 조회되었니? : " + modifyEmployee);
		int annualLeaveDat = annual.getAnnualLeaveDat();
		int annualLeftDay = annual.getEmployee().getAnnualLeftDay();
		System.out.println("사용일 : " + annualLeaveDat + ", 잔여횟수 : " + annualLeftDay);
		
		int updateLeftDay = annualLeftDay - annualLeaveDat;
		System.out.println("서비스에서 차감되었니? : " + updateLeftDay);
		
		modifyEmployee.setAnnualLeftDay(updateLeftDay);
		
		String annualYn = "Y";
		Annual useAnnual = annualRepository.findByAnnualCodeLike(annual.getAnnualCode());
		useAnnual.setAnnualYn(annualYn);
		
	}
	
	/* date형 변환 */
	private java.sql.Date toSqlDate(String dateString) {
        java.sql.Date endDate = null;

        if(dateString != null && !"".equals(dateString)) {
            endDate = java.sql.Date.valueOf(dateString);
        }

        return endDate;
    }


	/* 부서명 조회 */
	public DepartmentDTO findDepartmentByName(String deptName) {
		Department department = departmentRepository.findDepartmentByName(deptName);
		return modelMapper.map(department, DepartmentDTO.class);
	}

	

}
