package com.volt.asap.hrm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.service.DepartmentService;

@Controller
@RequestMapping("/department")
public class DepartmentContorller {

	private final DepartmentService departmentService;
	
	@Autowired
	public DepartmentContorller(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}
	
	/* 부서목록 조회 */
	@GetMapping("/list")
	public ModelAndView findDepartmentList(ModelAndView mv) {
		List<DepartmentDTO> deptList = departmentService.findDepartmentList();
		System.out.println("부서목록 조회 : " + deptList);
		
		mv.addObject("deptList", deptList);
		mv.setViewName("department/list");
		
		return mv;
	}
	
	/* 신규부서 등록(GET) */
	@GetMapping("/regist")
	public void registDepartment() {}
	
	/* 신규부서 등록(POST) */
	@PostMapping("/regist")
	public ModelAndView registDepartment(ModelAndView mv, DepartmentDTO department, RedirectAttributes rttr) {
		departmentService.registDepartment(department);
		System.out.println("부서등록 성공?");
		
		rttr.addFlashAttribute("registSuccessMessage", "부서 등록에 성공하셨습니다.");
		mv.setViewName("redirect:/department/list");
		
		return mv;
	}
	
	/* 부서명, 부서 사용 상태 수정(GET) */
	@GetMapping("/modifyName/{code}")
	public ModelAndView findDepartmentByCode(ModelAndView mv, @PathVariable String code) {
		System.out.println("컨트롤러 도착? : " + code);
		DepartmentDTO department = departmentService.findDepartmentByCode(code);
		
		mv.addObject("department", department);
		mv.setViewName("department/modifyName");
		
		return mv;
	}
	
	/* 부서명, 부서 사용 상태 수정(POST) */
	@PostMapping("/modifyName")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute DepartmentDTO department) {
		System.out.println("부서내용 : " + department);
		departmentService.modifyDepartment(department);
		
		rttr.addFlashAttribute("modifySuccessMessage", "부서명 수정에 성공하셨습니다.");
		
		return "redirect:/department/list";
	}
}
