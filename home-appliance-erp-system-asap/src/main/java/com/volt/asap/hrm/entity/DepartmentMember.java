package com.volt.asap.hrm.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "DepartmentMember")
@Table(name = "TBL_DEPT")
public class DepartmentMember {
    @Id
    @Column(name = "DEPT_CODE")
    private String code;

    @Column(name = "DEPT_NAME")
    private String name;

    @Column(name = "DEPT_USE_YN")
    private String useYn;

    @OneToMany
    @JoinColumn(name = "DEPT_CODE")
    private List<Employee> employee;

    public DepartmentMember() {
    }

    public DepartmentMember(String code, String name, String useYn, List<Employee> employee) {
        this.code = code;
        this.name = name;
        this.useYn = useYn;
        this.employee = employee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public List<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(List<Employee> employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "DepartmentMember{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", useYn='" + useYn + '\'' +
                ", employee=" + employee +
                '}';
    }
}
