package com.volt.asap.hrm.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.AttendanceAndEmployee;

public interface AttendanceAndEmployeeRepository extends JpaRepository<AttendanceAndEmployee, Integer>{

	AttendanceAndEmployee findAttendanceByattendanceCode(int attendanceCode);

	List<AttendanceAndEmployee> findByWorkDateBetween(Pageable paging, Date startDate, Date endDate);

//	List<AttendanceAndEmployee> findByWorkDateBetweenAndEmpEmpName(Pageable paging, Date startDate, Date endDate, String searchWord);

//	List<AttendanceAndEmployee> findByEmpEmpName(Pageable paging, String searchWord);

	int countByWorkDateBetween(Date startDate, Date endDate);

//	int countByWorkDateBetweenAndEmpEmpName(Date startDate, Date endDate, String searchWord);

//	int countByEmpEmpName(String searchWord);

	List<AttendanceAndEmployee> findByWorkDateBetweenAndEmpEmpNameContaining(Pageable paging, Date startDate,
			Date endDate, String searchWord);

	List<AttendanceAndEmployee> findByEmpEmpNameContaining(Pageable paging, String searchWord);

	int countByEmpEmpNameContaining(String searchWord);

	int countByWorkDateBetweenAndEmpEmpNameContaining(Date startDate, Date endDate, String searchWord);

//	List<AttendanceAndEmployee> findByWorkDateBetween(Date startDate, Date endDate, Pageable paging);
//
//	int countByWorkDateBetween(Date startDate, Date endDate);
//
//	List<AttendanceAndEmployee> findByWorkDateBetweenAndEmpEmpName(Date startDate, Date endDate, String serachWord,
//			Pageable paging);
//
//	List<AttendanceAndEmployee> findByEmpEmpName(String serachWord, Pageable paging);

}
