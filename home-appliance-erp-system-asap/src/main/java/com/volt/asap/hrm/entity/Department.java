package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name="Department")
@Table(name="TBL_DEPT")
@SequenceGenerator(
		name = "DEPT_SEQ_GENERATOR",
		sequenceName = "SEQ_DEPT_CODE",
		initialValue = 25,
		allocationSize = 1
)
public class Department {
	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "DEPT_SEQ_GENERATOR"
			)
	@Column(name = "DEPT_CODE")
	private String code;
	
	@Column(name = "DEPT_NAME")
	private String name;
	
	@Column(name = "DEPT_USE_YN")
	private String useYn;

	public Department() {
	}

	public Department(String code, String name, String useYn) {
		super();
		this.code = code;
		this.name = name;
		this.useYn = useYn;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	@Override
	public String toString() {
		return "Department [code=" + code + ", name=" + name + ", useYn=" + useYn + "]";
	}
	
	
}
