package com.volt.asap.hrm.controller;

import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.volt.asap.hrm.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.dto.CertificateDTO;
import com.volt.asap.hrm.dto.CertificationDTO;
import com.volt.asap.hrm.dto.CompanyInfoDTO;
import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.dto.JobGradeDTO;
import com.volt.asap.hrm.dto.PermissionDTO;
import com.volt.asap.hrm.dto.UserPermissionDTO;
import com.volt.asap.hrm.dto.UserPermissionPK;
import com.volt.asap.hrm.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	private final EmployeeService employeeService;

	private final PasswordEncoder passwordEncoder;
	
	@Autowired
	public EmployeeController(EmployeeService employeeService, PasswordEncoder passwordEncoder) {
		this.employeeService = employeeService;
		this.passwordEncoder = passwordEncoder;
	}
	
	/* 사원 목록 조회(검색, 페이징 포함) */
	@GetMapping("list")
	public ModelAndView searchPage(HttpServletRequest request, ModelAndView mv) {
		System.out.println("검색컨트롤러 도착!!!!!!!!!!!");
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		System.out.println("searchCondition 확인 : " + searchCondition);
		System.out.println("searchValue 확인 : " + searchValue);
		
		int totalCount = employeeService.selectTotalCount(searchCondition, searchValue);
		
		System.out.println("전체 게시글 수 : " + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		if(searchValue != null && !"".equals(searchValue)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);
		
		List<EmployeeDTO> empList = employeeService.findEmployeeList(selectCriteria);
		
		mv.addObject("empList", empList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("employee/list");
		
		return mv;
	}
	
	/* 사원 상세 정보 조회 */
	@GetMapping("/detail/{empCode}")
	public ModelAndView findEmployeeByEmpCode(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("컨트롤러 도착? " + empCode);
		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("상세조회 되었나?????" + employeeDetail);
		
		mv.addObject("employeeDetail", employeeDetail);
		mv.setViewName("employee/detail");
		return mv;
	}
	
	/* 신규 사원 등록(GET) */
	@GetMapping("/regist")
	public void registPage() {}
	
	/* ajax 통해서 부서명 조회 */
	@GetMapping(value = "/deptName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<DepartmentDTO> findDepartmentList(){
		return employeeService.findAllDepartment();
	}
	
	/* ajax 통해서 직급명 조회 */
	@GetMapping(value = "/jobName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<JobGradeDTO> findJobGradeList(){
		return employeeService.findJobGradeList();
	}
	
	/* ajax 통해서 사원명 조회 */
	@GetMapping(value = "/empName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<EmployeeDTO> findEmpNameList(){
		return employeeService.findEmployeeList();
	}
	
	/* 신규 사원 등록(POST) */
	@PostMapping("/regist")
	public String registEmployee(@ModelAttribute EmployeeDTO newEmployee, HttpServletRequest request,
			@RequestParam("profileImg") MultipartFile profileImg, RedirectAttributes rttr) {

		String addressNo = request.getParameter("addressNo");
		String address = request.getParameter("address");
		String addressDetail = request.getParameter("addressDetail");

		System.out.println("주소 확인 : " + addressNo + ", " + address + ", " + addressDetail);

		String empAddress = addressNo + " " + address + " " + addressDetail;
		System.out.println("주소 합치기 : " + empAddress);

		newEmployee.setEmpAddress(empAddress);

		/* 초기비밀번호 설정 */
		String pwd = newEmployee.getEmpPwd();
		if(pwd == null || "".equals(pwd)) {
			newEmployee.setEmpPwd("$2a$10$RVQxLVyzmRJjk1gZcH.M1udH61IMN9O.Ts2.IPYSm364odCGg18RK");
		}
		
		/* 연차는 기본으로 15개 설정 */
		int annual = newEmployee.getAnnualLeftDay();
		if(annual == 0) {
			newEmployee.setAnnualLeftDay(15);
		}
		
		/* 직급별로 기본급 설정하여 insert 진행 */
		String jobCode = newEmployee.getJobCode();
		switch(jobCode) {
			case "J1":
				newEmployee.setSalaryBase(2200000);
				break;
			case "J2":
				newEmployee.setSalaryBase(2800000);
				break;
			case "J3":
				newEmployee.setSalaryBase(3000000);
				break;
			case "J4":
				newEmployee.setSalaryBase(3500000);
				break;
			case "J5":
				newEmployee.setSalaryBase(5000000);
				break;
			case "J6":
				newEmployee.setSalaryBase(2200000);
				break;
			case "J7":
				newEmployee.setSalaryBase(7000000);
				break;
			case "J8":
				newEmployee.setSalaryBase(10000000);
				break;
		}

		System.out.println("컨트롤러 도착!!!!!!!!!!!!!!!!!!!!" + newEmployee);

		/* 프로필 사진 업로드 */
		String rootLocation = System.getProperty("user.dir") + "/src/main/resources/static";
		String fileLocation = "/profile"; 
		
		String fileUploadDirectory = rootLocation + fileLocation;
		
		File directory = new File(fileUploadDirectory);
		
		if(!directory.exists()) {
			System.out.println("폴더 생성 : " + directory.mkdirs());
		}
		
		List<MultipartFile> fileList = new ArrayList<>();
		fileList.add(profileImg);
		
		try {
			for(MultipartFile profile : fileList) {
				if(profile.getSize() > 0) {
					String originFileName = profile.getOriginalFilename();
					System.out.println("프로필 사진 원본 이름 : " + originFileName);
					String ext = originFileName.substring(originFileName.lastIndexOf("."));
					String savedFileName = UUID.randomUUID().toString().replace("-", "") + ext;
					System.out.println("변경한 사진의 이름 : " + savedFileName);
					
					profile.transferTo(new File(fileUploadDirectory + "/" + savedFileName));
					newEmployee.setProfileName(originFileName);
					newEmployee.setProfileRename(savedFileName);
					newEmployee.setProfilePath(fileLocation);
				}
				
			}
			
			System.out.println("fileList : " + fileList);
			
			System.out.println("newEmployee : " + newEmployee);
			
			employeeService.registEmployee(newEmployee);
			
			rttr.addFlashAttribute("message", "신규 사원 등록에 성공하셨습니다.");
			
		} catch (IllegalStateException | IOException e) {
			
			int cnt = 0;
			for(int i = 0; i < fileList.size(); i++) {
				
				File deleteFile = new File(fileUploadDirectory + "/savedFileName");
				boolean isDelete = deleteFile.delete();
				
				if(isDelete) {
					cnt++;
				}
			}
			
			if(cnt == fileList.size()) {
				System.out.println("엽로드에 실패한 사진 삭제 완료!");
			}
			e.printStackTrace();
		}
		
		return "redirect:/employee/list";
	}
	
	/* 사원 정보 수정(GET) */
	@GetMapping("/modify/{empCode}")
	public ModelAndView findEmployeeForModify(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("수정하기 위해 컨트롤러 도착? : " + empCode);
		EmployeeDTO employee = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("수정 컨트롤러에서 사원 상세 조회 : " + employee);
		
		List<JobGradeDTO> jobList = employeeService.findJobGradeList();
		List<DepartmentDTO> deptList = employeeService.findDepartmentList();
		
		mv.addObject("employee", employee);
		mv.addObject("jobList", jobList);
		mv.addObject("deptList", deptList);
		mv.setViewName("employee/modify");
		
		return mv;
	}
	
	/* 사원 정보 수정(POST) */
	@PostMapping("/modify")
	public String modifyEmployee(RedirectAttributes rttr, @ModelAttribute EmployeeDTO employee
			, @RequestParam("profileImg") MultipartFile profileImg, HttpServletRequest request) {
		System.out.println("사원내용 : " + employee);
		
		/* 프로필 사진 업로드 */
		String rootLocation = System.getProperty("user.dir") + "/src/main/resources/static";
		String fileLocation = "/profile"; 
		
		String fileUploadDirectory = rootLocation + fileLocation;
		
		File directory = new File(fileUploadDirectory);
		
		if(!directory.exists()) {
			System.out.println("폴더 생성 : " + directory.mkdirs());
		}
		
		List<MultipartFile> fileList = new ArrayList<>();
		fileList.add(profileImg);
		
		try {
			for(MultipartFile profile : fileList) {
				if(profile.getSize() > 0) {
					String originFileName = profile.getOriginalFilename();
					System.out.println("프로필 사진 원본 이름 : " + originFileName);
					String ext = originFileName.substring(originFileName.lastIndexOf("."));
					String savedFileName = UUID.randomUUID().toString().replace("-", "") + ext;
					System.out.println("변경한 사진의 이름 : " + savedFileName);
					
					profile.transferTo(new File(fileUploadDirectory + "/" + savedFileName));
					employee.setProfileName(originFileName);
					employee.setProfileRename(savedFileName);
					employee.setProfilePath(fileLocation);
				}
				
			}
			
			System.out.println("fileList : " + fileList);
			
			System.out.println("employee : " + employee);
			
			employeeService.modifyEmployee(employee);
			
			rttr.addFlashAttribute("modifySuccessMessage", "사원 정보 수정에 성공하셨습니다.");
			
		} catch (IllegalStateException | IOException e) {
			
			int cnt = 0;
			for(int i = 0; i < fileList.size(); i++) {
				
				File deleteFile = new File(fileUploadDirectory + "/savedFileName");
				boolean isDelete = deleteFile.delete();
				
				if(isDelete) {
					cnt++;
				}
			}
			
			if(cnt == fileList.size()) {
				System.out.println("업로드에 실패한 사진 삭제 완료!");
			}
			e.printStackTrace();
		}
		
		return "redirect:/employee/list";
	}
	
	/* 퇴사 신청자 목록 조회 */
	@GetMapping("leaveList")
	public ModelAndView findLeaveList(ModelAndView mv) {
		String leaveYn = "A";
		List<EmployeeDTO> leaveList = employeeService.findLeaveList(leaveYn);
		System.out.println("퇴사 신청자 조회 : " + leaveList);
		
		mv.addObject("leaveList", leaveList);
		mv.setViewName("employee/leaveList");
		
		return mv;
	}
	
	/* 퇴사 신청자 상세 정보 조회 */
	@GetMapping("/leaveDetail/{empCode}")
	public ModelAndView findLeaveEmployeeByEmpCode(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("컨트롤러 도착? " + empCode);
		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("상세조회 되었나?????" + employeeDetail);
		
		mv.addObject("employeeDetail", employeeDetail);
		mv.setViewName("employee/leaveDetail");
		
		return mv;
	}
	
	/* 퇴사 신청 승인 처리(GET) */
	@GetMapping("/modifyLeaveYn")
	public ModelAndView modifyLeaveYn(ModelAndView mv, int empCode) {
		System.out.println("도착?" + empCode);
		
		EmployeeDTO leaveDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("퇴사자 상세 조회 성공? : " + leaveDetail);
		
		mv.addObject("leaveDetail", leaveDetail);
		mv.setViewName("employee/modifyLeaveYn");
		
		return mv;
	}
	
	/* 퇴사 신청 승인 처리(POST) */
	@PostMapping("/modifyLeaveYn")
	public ModelAndView modifyEmployeeLeave(ModelAndView mv, RedirectAttributes rttr, @ModelAttribute EmployeeDTO employee) {
		System.out.println("퇴사내용 : " + employee);
		employeeService.modifyEmployeeLeave(employee);
		
		rttr.addFlashAttribute("modifySuccessMessage", "퇴사 승인 처리에 성공하셨습니다.");
		mv.setViewName("redirect:/employee/leaveList");
		
		return mv;
	}
	
	/* 증명서 발급 내역 조회 */
	@GetMapping("/certificateList")
	public ModelAndView findCertificateList(HttpServletRequest request, ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		System.out.println("searchCondition 확인 : " + searchCondition);
		System.out.println("searchValue 확인 : " + searchValue);
		
		int totalCount = employeeService.selectCertificateTotalCount(searchCondition, searchValue);
		
		System.out.println("전체 게시글 수 : " + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		if(searchValue != null && !"".equals(searchValue)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);
		
		List<CertificateDTO> certificateList = employeeService.findCertificateList(selectCriteria);
		System.out.println("증명서 발급 내역 조회 : " + certificateList);
		
		mv.addObject("certificateList", certificateList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("employee/certificateList");
		
		return mv;
	}
	
	/* 증명서 상세 정보 조회 */
	@GetMapping("/certificateDetail/{certificateCode}")
	public ModelAndView findCertificateByCertificateCode(ModelAndView mv, @PathVariable int certificateCode) {
		System.out.println("증명서 컨트롤러 도착? : " + certificateCode);
		CertificateDTO certificateDetail = employeeService.findCertificateByCertificateCode(certificateCode);
		int empCode = certificateDetail.getEmpCode();
		System.out.println("증명서 상세 조회 성공? : " + certificateDetail);
		System.out.println("증명서 상세 조회 성공? : " + empCode);
		
		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("사원 정보 조회? : " + employeeDetail);
		
		
		String companyName = "ASAP";
		CompanyInfoDTO companyInfo = employeeService.findCompanyInfo(companyName);
		System.out.println("회사 정보 조회?: " + companyInfo);
		
		mv.addObject("certificateDetail", certificateDetail);
		mv.addObject("employeeDetail", employeeDetail);
		mv.addObject("companyInfo", companyInfo);
		mv.setViewName("employee/certificateDetail");
		return mv;
	}
	
	/* 증명서 발급 등록(GET) */
	@GetMapping("/issue")
	public void issuePage() {}
	
	/* ajax 통해서 증명서관련 조회 */
	@GetMapping(value = "/certificateTypeCode", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<CertificationDTO> findCertificationList(){
		return employeeService.findCertificationList();
	}
	
	/* ajax 통해서 이름에 해당하는 사원코드 조회 */
	@ResponseBody()
	@PostMapping(value = "/getEmpNameEmpCode", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Integer> getEmpNameEmpCode(@RequestParam(name = "empName") String empName) {
		System.out.println("ajax 포스트 도착");
		EmployeeDTO employee = employeeService.findEmployeeByEmpName(empName);
		System.out.println("ajax 포스트 매핑 성공?" + employee);
		int data = employee.getEmpCode();
		return ResponseEntity.ok(data);
	}
	
	/* 증명서 발급 등록(POST) */
	@PostMapping("/issue")
	public ModelAndView registCertificate(ModelAndView mv, CertificateDTO certificate, RedirectAttributes rttr) {
		System.out.println("증명서 발급 내용 잘 도착했니? : " + certificate);
		employeeService.registCertificate(certificate);
		System.out.println("증명서 발급 등록 성공?");
		
		rttr.addFlashAttribute("registSuccessMessage", "증명서 발급 등록에 성공하셨습니다.");
		mv.setViewName("redirect:/employee/certificateList");
		
		return mv;
	}
	
	/* 권한 설정 등록(GET) */
	@GetMapping("/permission/{empCode}")
	public ModelAndView findEmployeePermission(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("권한 설정하기 위해 컨트롤러 도착? : " + empCode);
		
		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("권한 설정하기 위해 상세조회 되었나?????" + employeeDetail);
		
		mv.addObject("employeeDetail", employeeDetail);
		mv.setViewName("employee/permission");
		
		return mv;
	}
	
	/* ajax 통해서 권한관련 조회 */
	@GetMapping(value = "/permissionCode", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<PermissionDTO> findpermissionList(){
		return employeeService.findpermissionList();
	}
	
	/* 권한 설정 등록(POST) */
	@PostMapping("/permission")
	public ModelAndView registPermission(ModelAndView mv, UserPermissionDTO userPermission, RedirectAttributes rttr) {
		System.out.println("권한 정보 넘어옴? : " + userPermission);
		employeeService.registUserPermission(userPermission);
		System.out.println("권한 설정 성공?");
		
		rttr.addFlashAttribute("registSuccessMessage", "권한 설정에 성공하셨습니다.");
		mv.setViewName("redirect:/employee/list");
		
		return mv;
	}
	
	/* 권한 관련 삭제(GET) */
	@GetMapping("/permission/delete/{empCode}")
	public ModelAndView findUserPermissionByCode(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("권한 삭제 컨트롤러 도착: " + empCode);
		
		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("권한 설정하기 위해 상세조회 되었나?????" + employeeDetail);
		
		List<UserPermissionDTO> userPermissionList = employeeService.findUserPermissionByCode(empCode);
		System.out.println("권한 삭제를 위한 리스트 조회 : " + userPermissionList);
		
		mv.addObject("employeeDetail", employeeDetail);
		mv.addObject("userPermissionList", userPermissionList);
		mv.setViewName("employee/permissionDelete");
		
		return mv;
	}
	
	/* 권한 관련 삭제(POST) */
	@PostMapping("/permission/delete")
	public ModelAndView deleteUserPermission(ModelAndView mv, UserPermissionPK userPermission, RedirectAttributes rttr) {
		System.out.println("권한 삭제 컨트롤러 도착? : " + userPermission);
		
		int result = employeeService.deleteUserPermission(userPermission);
		
		rttr.addFlashAttribute("registSuccessMessage", "권한 삭제에 성공하셨습니다.");
		mv.setViewName("redirect:/employee/list");
		
		return mv;
	}

	/* 권한 관련 수정(GET) */
	@GetMapping("/permission/modify/{empCode}")
	public ModelAndView findUserPermission2ByCode(ModelAndView mv, @PathVariable int empCode) {
		System.out.println("권한 수정 컨트롤러 도착: " + empCode);

		EmployeeDTO employeeDetail = employeeService.findEmployeeByEmpCode(empCode);
		System.out.println("권한 설정하기 위해 상세조회 되었나?????" + employeeDetail);

		List<UserPermissionDTO> userPermissionList = employeeService.findUserPermissionByCode(empCode);
		System.out.println("권한 삭제를 위한 리스트 조회 : " + userPermissionList);

		mv.addObject("employeeDetail", employeeDetail);
		mv.addObject("userPermissionList", userPermissionList);
		mv.setViewName("employee/permissionModify");

		return mv;
	}

	/* 권한 관련 수정(POST) */
	@PostMapping("/permission/modify")
	public String modifyUserPermission(@ModelAttribute UserPermissionPK userPermission, String code, RedirectAttributes rttr) {
		System.out.println("권한 수정 컨트롤러 도착? : " + userPermission + ", 변경 후 " + code);
		employeeService.modifyUserPermission(userPermission, code);
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("수정 성공했을까?!!!!!!!!!!!!!!!!!!!!!!!!!!");
		rttr.addFlashAttribute("registSuccessMessage", "권한 수정에 성공하셨습니다.");

		return "redirect:/employee/list";
	}

	/**
	 * REST 방식으로 특정 부서에 속한 부서원 리스트를 전달하는 컨트롤러 메소드 (POST, REST)
	 * @param dept 부서번호
	 * @return 해당 부서에 속한 부서원 정보 리스트
	 */
	@ResponseBody()
	@PostMapping(value = "/getMembersFromDepartment", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Map<String, String>> getDepartmentMembers(@RequestParam("dept") String dept) {
		List<EmployeeDTO> employeeList = employeeService.findEmployeeByDeptNo(dept);

		Map<String, String> data = new HashMap<>();

		if(employeeList == null) {
			data.put("message", "없는 부서거나 잘못된 접근입니다.");
			return ResponseEntity.badRequest().body(data);
		}

		for (EmployeeDTO employee : employeeList) {
			data.put(String.valueOf(employee.getEmpCode()), employee.getEmpName());
		}

		return ResponseEntity.ok(data);
	}

	@ResponseBody()
	@PostMapping(value = "/getEmployeeInfo", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Map<String, String>> getEmployeeInfo(@RequestParam("empCode") int empCode) {
		System.out.println("empCode = " + empCode);
		EmployeeDTO employeeDTO = employeeService.findById(empCode);

		Map<String, String> data = new HashMap<>();

		if(employeeDTO == null) {
			data.put("message", "없는 사원이거나 퇴사한 사원입니다.");
			return ResponseEntity.badRequest().body(data);
		}

		data.put("jobName", employeeDTO.getJobGrade().getJobName());
		data.put("empName", employeeDTO.getEmpName());

		return ResponseEntity.ok(data);
	}

	@GetMapping("/password")
	public ModelAndView passwordForm(ModelAndView mv) {
		mv.setViewName("/employee/password");
		return mv;
	}

	@PostMapping("/passwordModify")
	public ModelAndView passwordModify(
			ModelAndView mv,
			EmployeeDTO employeeDTO,
			@RequestParam("before") String before,
			@RequestParam("after") String after,
			@RequestParam("after") String afterCheck,
			Authentication auth
	) {

		System.out.println(before);
		System.out.println(after);
		System.out.println(afterCheck);
		System.out.println(auth.getName());

		int empCode = Integer.parseInt(auth.getName());

		employeeDTO = employeeService.findById(empCode);

		int code = employeeDTO.getEmpCode();

		boolean chcek = passwordEncoder.matches(before, employeeDTO.getEmpPwd());

		if(chcek && after.equals(afterCheck)) {
			String encodepass = passwordEncoder.encode(after);
			System.out.println("암호화가 되었니?: " + encodepass);

			Employee employee = employeeService.findByEmpCode(encodepass, code);

			System.out.println("반환된 값을 확인해보자: " + employee);

			if(!ObjectUtils.isEmpty(employee)) {
				System.out.println("비밀번호에 성공했다면 출력하시오");
				SecurityContextHolder.clearContext();
				mv.addObject("message", "success");
				mv.addObject("comment", "비밀번호 수정완료. 다시 로그인해 주세요.");
			}
		} else {
			mv.addObject("message", "fail");
			mv.addObject("comment", "비밀번호 수정실패");
		}

		mv.setViewName("/blank");

		return mv;
	}
}
