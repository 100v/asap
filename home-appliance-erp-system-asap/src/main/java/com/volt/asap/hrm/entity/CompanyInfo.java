package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name = "CompanyInfo")
@Table(name = "TBL_COMPANY_INFO")
public class CompanyInfo {

	@Id
	@Column(name = "COMPANY_NAME")
	private String companyName;
	
	@Column(name = "CEO_NAME")
	private String ceoName;
	
	@Column(name = "COMPANY_PHONE")
	private String companyPhone;
	
	@Column(name = "COMPANY_ADDRESS")
	private String companyAddress;

	public CompanyInfo() {
	}

	public CompanyInfo(String companyName, String ceoName, String companyPhone, String companyAddress) {
		this.companyName = companyName;
		this.ceoName = ceoName;
		this.companyPhone = companyPhone;
		this.companyAddress = companyAddress;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCeoName() {
		return ceoName;
	}

	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	@Override
	public String toString() {
		return "CompanyInfo [companyName=" + companyName + ", ceoName=" + ceoName + ", companyPhone=" + companyPhone
				+ ", companyAddress=" + companyAddress + "]";
	}
}
