package com.volt.asap.hrm.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.AttendanceDTO;
import com.volt.asap.hrm.service.AttendanceService;
import com.volt.asap.logistic.dto.ProductCategoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/attendance")
public class AttendanceController {

	private final AttendanceService attendanceService;
	
	
	@Autowired
	public AttendanceController(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;

	}
	
//	@GetMapping("/list")
//	public ModelAndView attendanceList(HttpServletRequest request, ModelAndView mv, AttendanceDTO attdto) {
//		SelectCriteriaforDate selectCriteriaforDate=null;
//		String currentPage = request.getParameter("currentPage");
//		String searchword = request.getParameter("searchWord");
//		if("".equals(searchword)) {
//			searchword = null;
//		}
//		
//		if(request.getParameter("startDate") == null && request.getParameter("endDate") == null && request.getParameter("searchWord") == null) {
//		int pageNo = 1;
//		System.out.println("1번 if문 확인");
//		System.out.println("이름 입력값 확인" + request.getParameter("searchWord"));
//		if(currentPage != null && !"".equals(currentPage)) {
//			pageNo = Integer.parseInt(currentPage);
//		}
//		
//		int totalCount = attendanceService.selectTotalCount();
//		
//		int limit = 10;
//		
//		int buttonAmount = 5;
//		
//		selectCriteriaforDate = Pagenation.getSelectCriteriaforDate(pageNo, totalCount, limit, buttonAmount);
//		
//		} else if (request.getParameter("startDate") != null && request.getParameter("endDate") != null){
//			java.sql.Date startDate = java.sql.Date.valueOf(request.getParameter("startDate"));
//			java.sql.Date endDate = java.sql.Date.valueOf(request.getParameter("endDate"));
//			String searchWord = request.getParameter("searchWord");
//			
//			String currentPage1 = request.getParameter("currentPage");
//			int pageNo = 1;
//			System.out.println("2번 if문 확인");
//			if(currentPage1 != null && !"".equals(currentPage1)) {
//				pageNo = Integer.parseInt(currentPage1);
//			}
//			
//			int totalCount = attendanceService.selectTotalCount1(startDate, endDate, searchWord);
//			
//			int limit = 10;
//			
//			int buttonAmount = 5;
//			
//			System.out.println("당신은 몇개입니까?" + totalCount);
//			
//			if(startDate != null && endDate != null) {
//				selectCriteriaforDate = Pagenation.getSelectCriteriaforDate(pageNo, totalCount, limit, buttonAmount, startDate, endDate, searchWord);
//			} else {
//				selectCriteriaforDate = Pagenation.getSelectCriteriaforDate(pageNo, totalCount, limit, buttonAmount);
//			}
//		} 
//	else if (request.getParameter("startDate") == null && request.getParameter("endDate") == null && request.getParameter("searchWord") != null){
//			System.out.println("3번 if문 확인");
//			String searchWord = request.getParameter("searchWord");
//			System.out.println(searchWord);
//			String currentPage1 = request.getParameter("currentPage");
//			int pageNo = 1;
//			System.out.println("3번 if문 확인");
//			if(currentPage1 != null && !"".equals(currentPage1)) {
//				pageNo = Integer.parseInt(currentPage1);
//			}
//			
//			int totalCount = attendanceService.selectTotalCount2(searchWord);
//			
//			int limit = 10;
//			
//			int buttonAmount = 5;
//			
//			System.out.println("당신은 몇개입니까?" + totalCount);
//			
//			selectCriteriaforDate = Pagenation.getSelectCriteriaforDate(pageNo, totalCount, limit, buttonAmount, searchWord);
//		}
//		
//		List<AttendanceDTO> attendanceList = attendanceService.attendanceList(selectCriteriaforDate);
//		
//		mv.addObject("attendanceList", attendanceList);
//		mv.addObject("selectCriteriaforDate", selectCriteriaforDate);
//		mv.setViewName("attendance/list");
//		return mv;
//	}
	

	@GetMapping("/list")
	public ModelAndView attendanceList(String startDate, String endDate, String searchWord, Integer page,
            Authentication auth, ModelAndView mv) {
		CustomSelectCriteria selectCriteria;
		
        if (page == null) {
            page = 1;
        }
		
		if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(searchWord)) {

            int totalCount = attendanceService.countAttendance(null);	
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 10, 5);
        } else {
            /* 검색 쿼리로 사용할 값들을 Map에 설정 */
            Map<String, String> parameterMap = new HashMap<>();

            parameterMap.put("startDate", startDate);
            parameterMap.put("endDate", endDate);
            parameterMap.put("searchWord", searchWord == null ? "" : searchWord);


            /* 검색어에 대한 전체 엔티티 개수를 설정 */
            int totalCount = attendanceService.countAttendance(parameterMap);

            /* 검색어 객체의 생성 및 검색어 할당 */
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 10,
                    5, parameterMap);
        }
		
		List<AttendanceDTO> attendanceList = attendanceService.attendanceList(selectCriteria);
		System.out.println("리스트 직어보기 " + attendanceList);
		
		mv.addObject("attendanceList", attendanceList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("attendance/list");
		return mv;
		
	}

	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다. */
		return str == null || "".equals(str);
	}
	
	
	@GetMapping("/{attendanceCode}")
	public ModelAndView attendanceDetail(ModelAndView mv, @PathVariable int attendanceCode) {
		
		AttendanceDTO attendanceDetail = attendanceService.attendanceDetail(attendanceCode);
		
		mv.addObject("attendanceDetail", attendanceDetail);
		mv.setViewName("attendance/detailview");
		
		return mv;
	}
	
	@PostMapping("/modify")
	public String attdendanceModify(RedirectAttributes rttr, @ModelAttribute AttendanceDTO attendanceUpdate) {
		
		attendanceService.ModifyAttendance(attendanceUpdate);
		
		rttr.addFlashAttribute("modifySuccessMessage", "근태 수정 성공!");
		
		return "redirect:/attendance/list";
	}
	
	@GetMapping("/regist")
	public void registPage() {}
	
	@PostMapping("/regist")
	public ModelAndView registAttendance(ModelAndView mv, AttendanceDTO attendanceInsert, RedirectAttributes rttr) {
		System.out.println("입력값 확인용 " + attendanceInsert);
		attendanceService.registAttendance(attendanceInsert);
		
		rttr.addFlashAttribute("registSuccessMessage", "근태 등록 성공.");
		
		mv.setViewName("redirect:/attendance/list");
		
		return mv;
	}

	/**
	 * 출근 등록을 담당하는 컨트롤러 메소드 (POST, REST)
	 * @param auth 출근하는 사원의 정보가 담긴 {@link Authentication} 객체
	 * @return 출근 등록 성공 메시지
	 */
	@ResponseBody
	@PostMapping(value = "/goWork", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Map<String, String>> goWork(Authentication auth) {
		Map<String, String> messages = new HashMap<>();

		AttendanceDTO attendanceDTO = new AttendanceDTO();
		attendanceDTO.setEmpCode(Integer.parseInt(auth.getName()));
		attendanceDTO.setWorkDate(Date.valueOf(LocalDate.now().toString()));
		attendanceDTO.setGoworkTime(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		attendanceDTO.setOutworkTime(" ");
		attendanceDTO.setWorkTime(0);
		attendanceDTO.setOvertimeAllowance(0);

		try {
			attendanceService.registGoWork(attendanceDTO);
		} catch (RuntimeException e) {
			messages.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(messages);
		}

		messages.put("message", "출근 등록에 성공했습니다.");
		return ResponseEntity.ok(messages);
	}

	/**
	 * 퇴근 등록을 담당하는 컨트롤러 메소드 (POST, REST)
	 * @param auth 퇴근하는 사원의 정보가 담긴 {@link Authentication} 객체
	 * @return 퇴근 등록 성공 메시지
	 */
	@ResponseBody
	@PostMapping(value = "/outWork", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Map<String, String>> outWork(Authentication auth) {
		Map<String, String> messages = new HashMap<>();

		try {
			attendanceService.registOutWork(Integer.parseInt(auth.getName()), LocalDate.now());
		} catch (RuntimeException e) {
			messages.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(messages);
		}

		messages.put("message", "퇴근 등록에 성공했습니다.");
		return ResponseEntity.ok(messages);
	}

	@ResponseBody
	@GetMapping(value = "/getAttendanceInfo", produces = "application/json; charset=UTF-8")
	public ResponseEntity<Map<String, String>> getAttendanceInfo(Authentication auth) {
		Map<String, String> messages = new HashMap<>();

		int empCode = Integer.parseInt(auth.getName());

		AttendanceDTO attendanceDTO = null;
		try {
			attendanceDTO = attendanceService.findAttendanceByEmpCodeAndWorkDate(empCode, LocalDate.now());
		} catch (RuntimeException e) {
			messages.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(messages);
		}

		messages.put("goWorkTime", attendanceDTO.getGoworkTime());
		messages.put("outWorkTime", " ".equals(attendanceDTO.getOutworkTime()) ? "-" : attendanceDTO.getOutworkTime());
		messages.put("message", "출퇴근 정보 로드에 성공했습니다.");
		return ResponseEntity.ok(messages);
	}
	
	
	@GetMapping(value = "/storageName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<StorageDTO> findStorage(){
		return attendanceService.findStorage();
	}
	
	@GetMapping(value = "/categoryName", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public List<ProductCategoryDTO> findCategory(){
		return attendanceService.findCategory();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}






















