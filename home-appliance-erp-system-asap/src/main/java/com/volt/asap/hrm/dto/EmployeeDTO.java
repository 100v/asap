package com.volt.asap.hrm.dto;

import java.sql.Date;

public class EmployeeDTO {

	private int empCode;
	private String empName;
	private String empBirth;
	private String deptCode;
	private String empPhone;
	private String empEmail;
	private String empAddress;
	private java.sql.Date empJoindate;
	private java.sql.Date empLeavedate;
	private String empLeaveYn;
	private String jobCode;
	private int annualLeftDay;
	private String signImg;
	private String profileName;
	private String profileRename;
	private String profilePath;
	private String empGender;
	private String empPwd;
	private int salaryBase;
	private DepartmentDTO department;
	private JobGradeDTO jobGrade;
	
	public EmployeeDTO() {
	}

	public EmployeeDTO(int empCode, String empName, String empBirth, String deptCode, String empPhone, String empEmail,
			String empAddress, Date empJoindate, Date empLeavedate, String empLeaveYn, String jobCode,
			int annualLeftDay, String signImg, String profileName, String profileRename, String profilePath,
			String empGender, String empPwd, int salaryBase, DepartmentDTO department, JobGradeDTO jobGrade) {
		this.empCode = empCode;
		this.empName = empName;
		this.empBirth = empBirth;
		this.deptCode = deptCode;
		this.empPhone = empPhone;
		this.empEmail = empEmail;
		this.empAddress = empAddress;
		this.empJoindate = empJoindate;
		this.empLeavedate = empLeavedate;
		this.empLeaveYn = empLeaveYn;
		this.jobCode = jobCode;
		this.annualLeftDay = annualLeftDay;
		this.signImg = signImg;
		this.profileName = profileName;
		this.profileRename = profileRename;
		this.profilePath = profilePath;
		this.empGender = empGender;
		this.empPwd = empPwd;
		this.salaryBase = salaryBase;
		this.department = department;
		this.jobGrade = jobGrade;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpBirth() {
		return empBirth;
	}

	public void setEmpBirth(String empBirth) {
		this.empBirth = empBirth;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public java.sql.Date getEmpJoindate() {
		return empJoindate;
	}

	public void setEmpJoindate(java.sql.Date empJoindate) {
		this.empJoindate = empJoindate;
	}

	public java.sql.Date getEmpLeavedate() {
		return empLeavedate;
	}

	public void setEmpLeavedate(java.sql.Date empLeavedate) {
		this.empLeavedate = empLeavedate;
	}

	public String getEmpLeaveYn() {
		return empLeaveYn;
	}

	public void setEmpLeaveYn(String empLeaveYn) {
		this.empLeaveYn = empLeaveYn;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public int getAnnualLeftDay() {
		return annualLeftDay;
	}

	public void setAnnualLeftDay(int annualLeftDay) {
		this.annualLeftDay = annualLeftDay;
	}

	public String getSignImg() {
		return signImg;
	}

	public void setSignImg(String signImg) {
		this.signImg = signImg;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getProfileRename() {
		return profileRename;
	}

	public void setProfileRename(String profileRename) {
		this.profileRename = profileRename;
	}

	public String getProfilePath() {
		return profilePath;
	}

	public void setProfilePath(String profilePath) {
		this.profilePath = profilePath;
	}

	public String getEmpGender() {
		return empGender;
	}

	public void setEmpGender(String empGender) {
		this.empGender = empGender;
	}

	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

	public int getSalaryBase() {
		return salaryBase;
	}

	public void setSalaryBase(int salaryBase) {
		this.salaryBase = salaryBase;
	}

	public DepartmentDTO getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentDTO department) {
		this.department = department;
	}

	public JobGradeDTO getJobGrade() {
		return jobGrade;
	}

	public void setJobGrade(JobGradeDTO jobGrade) {
		this.jobGrade = jobGrade;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [empCode=" + empCode + ", empName=" + empName + ", empBirth=" + empBirth + ", deptCode="
				+ deptCode + ", empPhone=" + empPhone + ", empEmail=" + empEmail + ", empAddress=" + empAddress
				+ ", empJoindate=" + empJoindate + ", empLeavedate=" + empLeavedate + ", empLeaveYn=" + empLeaveYn
				+ ", jobCode=" + jobCode + ", annualLeftDay=" + annualLeftDay + ", signImg=" + signImg
				+ ", profileName=" + profileName + ", profileRename=" + profileRename + ", profilePath=" + profilePath
				+ ", empGender=" + empGender + ", empPwd=" + empPwd + ", salaryBase=" + salaryBase + ", department="
				+ department + ", jobGrade=" + jobGrade + "]";
	}
}
