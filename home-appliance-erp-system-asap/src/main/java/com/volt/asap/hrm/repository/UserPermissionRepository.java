package com.volt.asap.hrm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.dto.UserPermissionPK;
import com.volt.asap.hrm.entity.UserPermission;

public interface UserPermissionRepository extends JpaRepository<UserPermission, UserPermissionPK> {

	List<UserPermission> findByEmpCode(int empCode);

}
