package com.volt.asap.hrm.entity;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.volt.asap.hrm.dto.EmployeeDTO;

@Entity(name = "AttendanceEmployee")
@Table(name = "TBL_ATTENDANCE")
@SequenceGenerator(
		name = "SEQ_ATTENDANCE_CODE_GENERATOR",
		sequenceName = "SEQ_ATTENDANCE_CODE",
		initialValue = 66,
		allocationSize = 1		
		)
public class AttendanceAndEmployee {

	@Id
	@GeneratedValue(
		strategy = GenerationType.SEQUENCE,
        generator = "SEQ_ATTENDANCE_CODE_GENERATOR"
	)
	
	@Column(name = "ATTENDANCE_CODE")
	private int attendanceCode;
	
	@Column(name = "WORK_DATE")
	private Date workDate;
	
	@Column(name = "GOWORK_TIME")
	private String goworkTime;
	
	@Column(name = "OUTWORK_TIME")
	private String outworkTime;
	
	@Column(name = "WORK_TIME")
	private int workTime;
	
	@Column(name = "OVERTIME_ALLOWANCE")
	private int overtimeAllowance;
	
	@ManyToOne
	@JoinColumn(name = "EMP_CODE")
	private Employee emp;

	public AttendanceAndEmployee() {
		super();
	}

	public AttendanceAndEmployee(int attendanceCode, Date workDate, String goworkTime, String outworkTime, int workTime,
			int overtimeAllowance, Employee emp) {
		super();
		this.attendanceCode = attendanceCode;
		this.workDate = workDate;
		this.goworkTime = goworkTime;
		this.outworkTime = outworkTime;
		this.workTime = workTime;
		this.overtimeAllowance = overtimeAllowance;
		this.emp = emp;
	}

	public int getAttendanceCode() {
		return attendanceCode;
	}

	public void setAttendanceCode(int attendanceCode) {
		this.attendanceCode = attendanceCode;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getGoworkTime() {
		return goworkTime;
	}

	public void setGoworkTime(String goworkTime) {
		this.goworkTime = goworkTime;
	}

	public String getOutworkTime() {
		return outworkTime;
	}

	public void setOutworkTime(String outworkTime) {
		this.outworkTime = outworkTime;
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public int getOvertimeAllowance() {
		return overtimeAllowance;
	}

	public void setOvertimeAllowance(int overtimeAllowance) {
		this.overtimeAllowance = overtimeAllowance;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	@Override
	public String toString() {
		return "AttendanceAndEmployee [attendanceCode=" + attendanceCode + ", workDate=" + workDate + ", goworkTime="
				+ goworkTime + ", outworkTime=" + outworkTime + ", workTime=" + workTime + ", overtimeAllowance="
				+ overtimeAllowance + ", emp=" + emp + "]";
	}

	
	
}
