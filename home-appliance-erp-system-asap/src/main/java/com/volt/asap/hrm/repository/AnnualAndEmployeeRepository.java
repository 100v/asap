package com.volt.asap.hrm.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.AnnualAndEmployee;

public interface AnnualAndEmployeeRepository extends JpaRepository<AnnualAndEmployee, Integer>{

	int countByEmployeeEmpNameContaining(String searchValue);

	int countByEmployeeDepartmentNameContaining(String searchValue);

	List<AnnualAndEmployee> findByEmployeeEmpNameContaining(String searchValue, Pageable paging);

	List<AnnualAndEmployee> findByEmployeeDepartmentNameContaining(String searchValue, Pageable paging);

	AnnualAndEmployee findByAnnualCodeLike(int annualCode);

	int countByAnnualStartAndEmployeeEmpNameContaining(Date startDate, String searchValue2);

	int countByAnnualStart(Date searchValue1);

	List<AnnualAndEmployee> findByAnnualStartAndEmployeeEmpNameContaining(Date searchValue1, String searchValue2,
			Pageable paging);

	List<AnnualAndEmployee> findByAnnualStart(Date searchValue1, Pageable paging);

}
