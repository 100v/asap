package com.volt.asap.hrm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.dto.AnnualDTO;
import com.volt.asap.hrm.dto.DepartmentDTO;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.service.AnnualService;
import com.volt.asap.hrm.service.EmployeeService;

@Controller
@RequestMapping("/annual")
public class AnnualController {

	private final AnnualService annualService;
	private final EmployeeService employeeService;
	
	@Autowired
	public AnnualController(AnnualService annualService, EmployeeService employeeService) {
		this.annualService = annualService;
		this.employeeService = employeeService;
	}
	
	/* 연차 사용 내역 조회 */
	@GetMapping("/useList")
	public ModelAndView findUseAnnualList(HttpServletRequest request, ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchValue1 = request.getParameter("searchValue1");
	    String searchValue2 = request.getParameter("searchValue2");
	    String searchCondition1 = request.getParameter("searchCondition1");
	    String searchCondition2 = request.getParameter("searchCondition2");
		
	    System.out.println("값이 잘 넘어왔니? " + searchValue1 + searchValue2 + searchCondition1 + searchCondition2);
	    
	    Map<String, String> parameter = new HashMap<>();
	    
	    parameter.put("searchValue1", searchValue1);
	    parameter.put("searchValue2", searchValue2);
	    parameter.put("searchCondition1", searchCondition1);
	    parameter.put("searchCondition2", searchCondition2);
	    
	    int totalCount = annualService.selectUseListTotalCount(parameter);
	    System.out.println("totalCount" + totalCount);
	    
	    CustomSelectCriteria selectCriteria;
	    
	    int limit = 10;
	      
	    int buttonAmount = 5;
	      
	    selectCriteria = CustomPagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, parameter);
	    
		List<AnnualDTO> useList = annualService.findUseAnnualList(selectCriteria);
		
		System.out.println("연차 사용 내역 조회 : " + useList);
		System.out.println("selectCriteria : " + selectCriteria);
		
		mv.addObject("useList", useList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("annual/useList");
		
		return mv;
	}
	
	/* 연차 보유 현황 조회 */
	@GetMapping("/ownList")
	public ModelAndView findOwnAnnualList(HttpServletRequest request, ModelAndView mv) {
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		System.out.println("searchCondition 확인 : " + searchCondition);
		System.out.println("searchValue 확인 : " + searchValue);
		
		int totalCount = employeeService.selectTotalCount(searchCondition, searchValue);
		
		System.out.println("전체 게시글 수 : " + totalCount);
		
		int limit = 10;
		
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		if(searchValue != null && !"".equals(searchValue)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);
		
		List<EmployeeDTO> ownList = employeeService.findEmployeeList(selectCriteria);
		
		System.out.println("연차 보유 현황 조회 : " + ownList);
		
		mv.addObject("ownList", ownList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("annual/ownList");
		
		return mv;
	}
	
	/* 연차 등록(GET) */
	@GetMapping("/regist")
	public void registPage() {}
	
	/* 연차 등록(POST) */
	@PostMapping("/regist")
	public ModelAndView registAnnual(ModelAndView mv, AnnualDTO annual, RedirectAttributes rttr) {
		annual.setAnnualYn("N");
		System.out.println("annual : " + annual);
		System.out.println("annual : " + annual.getDeptCode());
		
		String deptName = annual.getDeptCode();
		DepartmentDTO department = annualService.findDepartmentByName(deptName);
		
		System.out.println("department : " + department);
		annual.setDeptCode(department.getCode());
		System.out.println("annual : " + annual);
		
		annualService.registAnnual(annual);
		System.out.println("연차 등록 성공?");
		
		rttr.addFlashAttribute("registSuccessMessage", "연차 등록에 성공하셨습니다.");
		mv.setViewName("redirect:/annual/useList");
		
		return mv;
	}
	
	/* 연차 사용 내역 수정(GET) */
	@GetMapping("/modify/{annualCode}")
	public ModelAndView findUseAnnualByCode(ModelAndView mv, @PathVariable int annualCode) {
		System.out.println("컨트롤러 도착: " + annualCode);
		AnnualDTO annual = annualService.findUseAnnualByCode(annualCode);
		
		List<DepartmentDTO> deptList = annualService.findDepartmentList();
		
		mv.addObject("annual", annual);
		mv.addObject("deptList", deptList);
		mv.setViewName("annual/modify");
		
		return mv;
	}
	
	/* 연차 사용 내역 수정(POST) */
	@PostMapping("modify")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute AnnualDTO annual) {
		System.out.println("연차 내용 : " + annual);
		annualService.modifyAnnualUse(annual);
		
		return "redirect:/annual/useList";
	}
	
	/* 연차 사용 후 차감 */
	@GetMapping("/confirm/{annualCode}")
	public String findUseAnnualDetailByCode(RedirectAttributes rttr, @PathVariable int annualCode) {
		AnnualDTO annual = annualService.findUseAnnualByCode(annualCode);
		System.out.println("컨트롤러 도착: " + annual);
		
		annualService.modifyAnnualLeftDay(annual);
		
		return "redirect:/annual/useList";
	}
	
	/* ajax 통해서 이름에 해당하는 부서명 조회 */
	@ResponseBody()
	@PostMapping(value = "/getEmpNameDeptName", produces = "plain/text; charset=UTF-8")
	public ResponseEntity<String> getEmpNameDeptName(@RequestParam(name = "empName") int empCode) {
		System.out.println("ajax 포스트 도착" + empCode);
		EmployeeDTO employee = employeeService.findEmployeeByEmpCode(empCode);
		
		System.out.println("ajax 포스트 매핑 성공? : " + employee.getDepartment().getName());
		String data = employee.getDepartment().getName();
		
		return ResponseEntity.ok().body(data);
	}
}
