package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name="JobGrade")
@Table(name="TBL_JOB_GRADE")
public class JobGrade {

	@Id
	private String jobCode;
	
	@Column(name = "JOB_NAME")
	private String jobName;

	public JobGrade() {
	}

	public JobGrade(String jobCode, String jobName) {
		this.jobCode = jobCode;
		this.jobName = jobName;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Override
	public String toString() {
		return "JobGrade [jobCode=" + jobCode + ", jobName=" + jobName + "]";
	}
}
