package com.volt.asap.hrm.entity;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "Attendance")
@Table(name = "TBL_ATTENDANCE")
@SequenceGenerator(
		name = "SEQ_ATTENDANCE_CODE_GENERATOR",
		sequenceName = "SEQ_ATTENDANCE_CODE",
		initialValue = 66,
		allocationSize = 1		
		)
public class Attendance {

	@Id
	@GeneratedValue(
		strategy = GenerationType.SEQUENCE,
        generator = "SEQ_ATTENDANCE_CODE_GENERATOR"
	)
	
	@Column(name = "ATTENDANCE_CODE")
	private int attendanceCode;
	
	@Column(name = "WORK_DATE")
	private Date workDate;
	
	@Column(name = "GOWORK_TIME")
	private String goworkTime;
	
	@Column(name = "OUTWORK_TIME")
	private String outworkTime;
	
	@Column(name = "WORK_TIME")
	private int workTime;
	
	@Column(name = "OVERTIME_ALLOWANCE")
	private int overtimeAllowance;
	
	@Column(name = "EMP_CODE")
	private int empCode;

	public Attendance() {
		super();
	}

	public Attendance(int attendanceCode, Date workDate, String goworkTime, String outworkTime, int workTime,
			int overtimeAllowance, int empCode) {
		super();
		this.attendanceCode = attendanceCode;
		this.workDate = workDate;
		this.goworkTime = goworkTime;
		this.outworkTime = outworkTime;
		this.workTime = workTime;
		this.overtimeAllowance = overtimeAllowance;
		this.empCode = empCode;
	}

	public int getAttendanceCode() {
		return attendanceCode;
	}

	public void setAttendanceCode(int attendanceCode) {
		this.attendanceCode = attendanceCode;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getGoworkTime() {
		return goworkTime;
	}

	public void setGoworkTime(String goworkTime) {
		this.goworkTime = goworkTime;
	}

	public String getOutworkTime() {
		return outworkTime;
	}

	public void setOutworkTime(String outworkTime) {
		this.outworkTime = outworkTime;
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public int getOvertimeAllowance() {
		return overtimeAllowance;
	}

	public void setOvertimeAllowance(int overtimeAllowance) {
		this.overtimeAllowance = overtimeAllowance;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	@Override
	public String toString() {
		return "Attendance [attendanceCode=" + attendanceCode + ", workDate=" + workDate + ", goworkTime=" + goworkTime
				+ ", outworkTime=" + outworkTime + ", workTime=" + workTime + ", overtimeAllowance=" + overtimeAllowance
				+ ", empCode=" + empCode + "]";
	}

	
	
}
