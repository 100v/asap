package com.volt.asap.hrm.entity;

import javax.persistence.*;

@Entity(name = "Certification")
@Table(name = "TBL_CERTIFICATION")
public class Certification {

	@Id
	@Column(name = "CERTIFICATE_TYPE_CODE")
	private String certificateTypeCode;
	
	@Column(name = "CERTIFICATE_TYPE")
	private String certificateType;

	public Certification() {
	}

	public Certification(String certificateTypeCode, String certificateType) {
		this.certificateTypeCode = certificateTypeCode;
		this.certificateType = certificateType;
	}

	public String getCertificateTypeCode() {
		return certificateTypeCode;
	}

	public void setCertificateTypeCode(String certificateTypeCode) {
		this.certificateTypeCode = certificateTypeCode;
	}

	public String getCertificateType() {
		return certificateType;
	}

	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}

	@Override
	public String toString() {
		return "Certification [certificateTypeCode=" + certificateTypeCode + ", certificateType=" + certificateType
				+ "]";
	}
}
