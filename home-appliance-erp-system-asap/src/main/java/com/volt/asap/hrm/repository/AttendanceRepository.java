package com.volt.asap.hrm.repository;

import com.volt.asap.hrm.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Integer>{

	Attendance findAttendanceByattendanceCode(int attendanceCode);

//	int countByfindByAttendanceCode(Integer parsedId);

	Optional<Attendance> findByEmpCodeAndWorkDate(int empCode, Date workDate);

	boolean existsByEmpCodeAndWorkDate(int empCode, Date workDate);
}
