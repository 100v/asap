package com.volt.asap.hrm.dto;

import java.io.Serializable;

public class UserPermissionPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int empCode;
	private String permissionCode;
	public UserPermissionPK() {
	}
	public UserPermissionPK(int empCode, String permissionCode) {
		this.empCode = empCode;
		this.permissionCode = permissionCode;
	}
	public int getEmpCode() {
		return empCode;
	}
	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	@Override
	public String toString() {
		return "UserPermissionPK [empCode=" + empCode + ", permissionCode=" + permissionCode + "]";
	}
	
	
}
