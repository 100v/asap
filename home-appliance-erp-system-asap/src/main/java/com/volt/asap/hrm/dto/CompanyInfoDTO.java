package com.volt.asap.hrm.dto;

public class CompanyInfoDTO {

	private String companyName;
	private String ceoName;
	private String companyPhone;
	private String companyAddress;
	
	public CompanyInfoDTO() {
	}
	public CompanyInfoDTO(String companyName, String ceoName, String companyPhone, String companyAddress) {
		this.companyName = companyName;
		this.ceoName = ceoName;
		this.companyPhone = companyPhone;
		this.companyAddress = companyAddress;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	@Override
	public String toString() {
		return "CompanyInfoDTO [companyName=" + companyName + ", ceoName=" + ceoName + ", companyPhone=" + companyPhone
				+ ", companyAddress=" + companyAddress + "]";
	}
}
