package com.volt.asap.hrm.entity;

import javax.persistence.*;

import com.volt.asap.hrm.dto.UserPermissionPK;

import java.io.Serializable;

/**
 * 유저별 권한정보가 담겨있는 Entity
 *
 * @see com.volt.asap.login.dto.UserPermissionDTO
 */
@Entity(name = "UserPermissionInsert")
@Table(name = "TBL_USER_PERMISSION")
@IdClass(UserPermissionPK.class)
public class UserPermissionInsert implements Serializable {

    private static final long serialVersionUID = 320899020039361296L;

    @Id
    @Column(name = "EMP_CODE")
    private int empCode;                        	// EMP_CODE

    @Id
    @Column(name = "PERMISSION_CODE")
    private String permissionCode;                  // PERMISSION_CODE

    public UserPermissionInsert() { }

	public UserPermissionInsert(int empCode, String permissionCode) {
		this.empCode = empCode;
		this.permissionCode = permissionCode;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserPermissionInsert [empCode=" + empCode + ", permissionCode=" + permissionCode + "]";
	}
}
