package com.volt.asap.hrm.repository;

import com.volt.asap.hrm.dto.EmployeeDTO;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.Employee;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

	Employee findByEmpCodeLike(int empCode);

	Employee findByEmpName(String empName);

	List<Employee> findByDeptCodeAndEmpLeaveYn(String deptCode, String empLeaveYn);

    Employee findByEmpCode(int code);
}
