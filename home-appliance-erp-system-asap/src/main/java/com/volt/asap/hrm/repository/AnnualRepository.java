package com.volt.asap.hrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.hrm.entity.Annual;

public interface AnnualRepository extends JpaRepository<Annual, Integer>{

	Annual findByAnnualCodeLike(int annualCode);

}
