package com.volt.asap.sales.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.volt.asap.logistic.entity.Product;

@Entity(name="OrderAndProduct")
@Table(name="TBL_ORDER_LIST")
public class OrderAndProduct {
	
	@Id
	@Column(name = "ORDER_NO")
	private int orderNumber;
	
	@Column(name = "ORDER_DATE")
	private java.sql.Date orderDate;

	@Column(name = "ORDER_PRICE")
	private long orderPrice;

	@Column(name = "ORDER_SHIPPING_NO")
	private long orderShippingNumber;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE")
	private Product product;
	
	@Column(name = "ORDER_SHIPPING_STATUS")
	private String orderShippingStatus;

	public OrderAndProduct() {
		super();
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public java.sql.Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(java.sql.Date orderDate) {
		this.orderDate = orderDate;
	}

	public long getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(long orderPrice) {
		this.orderPrice = orderPrice;
	}

	public long getOrderShippingNumber() {
		return orderShippingNumber;
	}

	public void setOrderShippingNumber(long orderShippingNumber) {
		this.orderShippingNumber = orderShippingNumber;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getOrderShippingStatus() {
		return orderShippingStatus;
	}

	public void setOrderShippingStatus(String orderShippingStatus) {
		this.orderShippingStatus = orderShippingStatus;
	}

	@Override
	public String toString() {
		return "OrderAndProduct [orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", orderPrice=" + orderPrice
				+ ", orderShippingNumber=" + orderShippingNumber + ", product=" + product + ", orderShippingStatus="
				+ orderShippingStatus + "]";
	}

}
