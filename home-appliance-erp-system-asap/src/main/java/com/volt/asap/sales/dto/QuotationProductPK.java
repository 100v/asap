package com.volt.asap.sales.dto;

import java.io.Serializable;

public class QuotationProductPK implements Serializable{
	private static final long serialVersionUID = -6735768418280407387L;

	private int estimateCode;
	private String product;
	
	public QuotationProductPK() {
	}

	public QuotationProductPK(int estimateCode, String product) {
		this.estimateCode = estimateCode;
		this.product = product;
	}

	public int getEstimateCode() {
		return estimateCode;
	}

	public void setEstimateCode(int estimateCode) {
		this.estimateCode = estimateCode;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "QuotationProductPK [estimateCode=" + estimateCode + ", product=" + product + "]";
	}
	
	

	
	
}
