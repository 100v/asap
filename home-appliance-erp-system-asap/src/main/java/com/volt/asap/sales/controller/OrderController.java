package com.volt.asap.sales.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.CompanyInfoDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.sales.dto.OrderListDTO;
import com.volt.asap.sales.service.OrderService;

@Controller
@RequestMapping("/order")
public class OrderController {

	private final OrderService orderService;

	@Autowired
	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}
/* 처음에 했던 코드 */
	
//	@GetMapping("/list")
//	public ModelAndView findOrderList(HttpServletRequest request, ModelAndView mv) {
//		
//		String currentPage = request.getParameter("currentPage");
//		int pageNo = 1;
//		
//		if(currentPage != null && !"".equals(currentPage)) {
//			pageNo = Integer.parseInt(currentPage);
//		}
//		String searchValue1 = request.getParameter("searchValue1");
//		String searchValue2 = request.getParameter("searchValue2");
//		String searchCondition1 = request.getParameter("searchCondition1");
//		String searchCondition2 = request.getParameter("searchCondition2"); 
//		
//		int totalCount = orderService.selectTotalCount(searchCondition1, searchCondition2, searchValue1, searchValue2);
//		int limit = 10;
//		int buttonAmount = 5;
//		
//		SelectCriteriaforDoubleValue selectCriteriaforDoubleValue = null;
//		
//		if(searchValue1 != null && searchValue2 != null) {
//			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount, searchCondition1,searchValue1, searchCondition2, searchValue2);
//		} else {
//			/* 맨 처음일 때*/
//			selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount);
//		}
//		List<OrderListDTO> orderList = orderService.findOrderList(selectCriteriaforDoubleValue);
//		List<ProductDTO> productList = orderService.productList();
//
//		
//		mv.addObject("orderList", orderList);
//		mv.addObject("productList", productList);
//		mv.addObject("selectCriteriaforDoubleValue", selectCriteriaforDoubleValue);
//		mv.setViewName("order/list");
//		
//		return mv;
//	}
//	@GetMapping("/list")
//	public ModelAndView findOrderList(HttpServletRequest request, ModelAndView mv) {
//
//		String currentPage = request.getParameter("currentPage");
//		int pageNo = 1;
//
//		/* currentPage가 null이 아니거나 빈 문자열이 아닐때 형변환 하여 변수에 저장한다. */
//		if (currentPage != null && !"".equals(currentPage)) {
//			pageNo = Integer.parseInt(currentPage);
//		}
//
//		String startDate = null;
//		String endDate = null;
//		Date sDate = null;
//		Date eDate = null;
//		
//		
//		
//		if (!isEmptyString(request.getParameter("prductName"))) {
//			java.sql.Date sDate = java.sql.Date.valueOf(request.getParameter("startDate"));
//			java.sql.Date eDate = java.sql.Date.valueOf(request.getParameter("endDate"));
//		}
//		
//		String proName = null;
//        if(!isEmptyString(request.getParameter("productName"))) {
//        	proName = request.getParameter("productName");
//        }
//        
//        /* 넘어온게 있는지 콘솔로 출력해서 확인 */
//        System.out.println("startDate: " + sDate);
//        System.out.println("endDate: " + eDate);
//        System.out.println("proName" + proName);
//        
//        /* 한 페이지에 보여질 목록의 수 입니다. */
//        int limit = 10;
//
//        /* 한 번에 보여질 페이징 버튼의 개수 입니다. */
//        int buttonAmount = 5;
//
//        /* 페이징 처리에 대한 정보를 담고있는 녀석을 일단 만들어 둡니다. */
//        SelectCriteria selectCriteria = null;
//
//        int totalcount = 0;
//        
//        /* 검색 조건에 따른 해당되는 값들에 대한 개수를 세어옵니다(페이징을 위해) */
//        if(sDate == null && eDate == null && proName == null ) {
//            totalcount = orderService.allCount();                                       // 검색조건이 아무것도 없을 때
//        } else if(proName == null) {
//            totalcount = orderService.dateTotalCount(sDate, eDate);                     // 검색조건이 기간일 때
//        } else if(sDate == null && eDate == null) {
//        	totalcount = orderService.orderProductCount(sDate, eDate, proName); 					// 검색조건이 상품명일때
//        }
//        System.out.println("검색조건에 따른 totalCount : " + totalcount);
//
//        selectCriteria = Pagenation.getSelectCriteria(pageNo, totalcount, limit, buttonAmount);
//
//        System.out.println("크리테리아야 뭘 가지고 있니: " + selectCriteria);
//        
//        
//		List<OrderListDTO> orderList = new ArrayList<>();
//		
//		if(sDate == null && eDate == null && proName == null ) {
//			orderList = orderService.findAllList();
//		} else if(sDate != null && eDate != null && proName != null) {
//			orderList = orderService.findOrderList(sDate, eDate, proName);
//		} else if(proName == null) {
//			orderList = orderService.findOrderListByDate(sDate, eDate);
//		} else if(sDate == null && eDate == null) {
//			orderList = orderService.findOrderListByProductName(proName);
//		}
//		
//		System.out.println("검색 조건에 맞게 가져온 리스트 :" + orderList);
//		
//		List<ProductDTO> productList = orderService.productList();
//		mv.addObject("orderList", orderList);
//		mv.addObject("productList", productList);
//		mv.addObject("selectCriteria", selectCriteria);
//		mv.setViewName("order/list");
//        return mv;
//	}
	@GetMapping("/list")
	public ModelAndView findOrderList(String startDate, String endDate, String searchWord, Integer page,
             ModelAndView mv) {

		CustomSelectCriteria selectCriteria;
		/* 버튼 넘기는거 */
		int limit = 10;
		int buttonAmount = 5;
		
        /* 페이지 파라미터가 없으면 1페이지로 간주 */
        if (page == null) {
            page = 1;
        }
		
        Map<String, String> parameterMap = new HashMap<>();
		if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(searchWord)) {
			
            int totalCount = orderService.countOrderByQueries(null);	//새로만듬
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, limit,buttonAmount);
        } else {
            /* 검색 쿼리로 사용할 값들을 Map에 설정 */

            parameterMap.put("startDate", startDate);
            parameterMap.put("endDate", endDate);
            parameterMap.put("searchWord", searchWord == null ? "" : searchWord);


            /* 검색어에 대한 전체 엔티티 개수를 설정 */
            int totalCount = orderService.countOrderByQueries(parameterMap);

            /* 검색어 객체의 생성 및 검색어 할당 */
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, limit,
                    buttonAmount, parameterMap);
        }
        
		System.out.println("단어" + searchWord);
		List<OrderListDTO> orderList = orderService.findOrderByQueries(selectCriteria);
		
		
		List<ProductDTO> productList = orderService.productList();
		mv.addObject("parameterMap",parameterMap);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("orderList", orderList);
		mv.addObject("productList", productList);
		mv.setViewName("order/list");
        return mv;
	}
	/* 주문번호에 해당하는 주문상세 내역*/
	@GetMapping("/{orderNumber}")
	public ModelAndView findOrderByNumber(ModelAndView mv, @PathVariable int orderNumber) {
		
		/* 주문번호를 받아와서해당하는 값 가져오기 */
		OrderListDTO order = orderService.findOrderByNumber(orderNumber);
		
		List<ProductDTO> productList = orderService.productList();
		
		mv.addObject("order", order);
		mv.addObject("productList", productList);
		mv.setViewName("order/detail");

		return mv;
	}
	
	/* 주문번호에 해당하는 주문서*/
	@GetMapping("/sheet/{orderNumber}")
	public ModelAndView findSheetByNumber(ModelAndView mv, @PathVariable int orderNumber) {
		
		/* 주문번호를 받아와서 해당하는 값 가져오기 */
		OrderListDTO order = orderService.findOrderByNumber(orderNumber);
		String companyName = "ASAP";
		CompanyInfoDTO companyInfo = orderService.findCompanyInfo(companyName);
		List<ProductDTO> productList = orderService.productList();
		
		mv.addObject("order", order);
		mv.addObject("companyInfo", companyInfo);
		mv.addObject("productList", productList);
		mv.setViewName("order/sheet");

		return mv;
	}

	@GetMapping("/regist")
	public ModelAndView registPage(ModelAndView mv) {
		/* 해당하는 상품명을 가져오기 위해 상품리스트를 가져오기 */
		List<ProductDTO> productList = orderService.productList();
		System.out.println("나오시나요?" + productList);
		mv.addObject("productList", productList);
		mv.setViewName("order/regist");
		return mv;
	}

	@PostMapping("/regist")
	public ModelAndView registOrder(ModelAndView mv, OrderListDTO newOrder, RedirectAttributes rttr) {
		
		String orderShippingStatus = newOrder.getOrderShippingStatus();
		if(orderShippingStatus == null || "".equals(orderShippingStatus) ) {
			newOrder.setOrderShippingStatus("N");
		}
		orderService.registNewOrder(newOrder);
		rttr.addFlashAttribute("registSuccessMessage", "주문 등록에 성공하셨습니다");
		mv.setViewName("redirect:/order/list");

		return mv;
	}
	@GetMapping("/registNopop")
	public ModelAndView registNopopPage(ModelAndView mv) {
		/* 해당하는 상품명을 가져오기 위해 상품리스트를 가져오기 */
		List<ProductDTO> productList = orderService.productList();
		System.out.println("나오시나요?" + productList);
		mv.addObject("productList", productList);
		mv.setViewName("order/registNopop");
		return mv;
	}

	@PostMapping("/registNopop")
	public ModelAndView registNopopOrder(ModelAndView mv, OrderListDTO newOrder, RedirectAttributes rttr) {
		
		String orderShippingStatus = newOrder.getOrderShippingStatus();
		if(orderShippingStatus == null || "".equals(orderShippingStatus) ) {
			newOrder.setOrderShippingStatus("N");
		}
		orderService.registNewOrder(newOrder);
		rttr.addFlashAttribute("registSuccessMessage", "주문 등록에 성공하셨습니다");
		mv.setViewName("redirect:/order/list");

		return mv;
	}
	@PostMapping("/modify")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute OrderListDTO orderUpdate) {

		orderService.modifyOrder(orderUpdate);

		rttr.addFlashAttribute("modifySuccessMessage", "주문 수정에 성공하셨습니다");

		return "redirect:/order/list";
	}

	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다. */
		return str == null || "".equals(str);
	}

	private java.sql.Date toSqlDate(String dateString) { // 넘어온 파라미터를 담은 변수가 비었는지 체크하는 메소드
		java.sql.Date endDate = null;

		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}

		return endDate;
	}
}
