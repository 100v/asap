package com.volt.asap.sales.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.sales.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {

	int countByRepresentativeNameContaining(String searchValue2);

	int countByNameContaining(String searchValue2);

	int countByNameAndRepresentativeNameContaining(String searchValue1, String searchValue2);

	List<Client> findByNameContainingAndRepresentativeNameContaining(String searchValue1, String searchValue2, Pageable paging);

	List<Client> findByNameContaining(String searchValue1,Pageable paging);

	List<Client> findByRepresentativeNameContaining(String searchValue2,Pageable paging);

	List<Client> findAllByMaintain(String status);
}
