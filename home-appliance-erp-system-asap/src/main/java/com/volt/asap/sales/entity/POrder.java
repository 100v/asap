package com.volt.asap.sales.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.volt.asap.hrm.entity.Employee;



@Entity(name = "PO")
@Table(name = "TBL_P_ORDER")
@SequenceGenerator(
		name = "PO_SEQ_P_ORDER_CODE",
		sequenceName = "SEQ_P_ORDER_CODE",
		initialValue = 24,
		allocationSize = 1
)
public class POrder {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "PO_SEQ_P_ORDER_CODE"
	)
	@Column(name = "P_ORDER_CODE")
	private int code;
	
	@Column(name = "P_ORDER_DATE")
	private Date date;
	
	@Column(name = "P_ORDER_DEUDATE")
	private Date dueDate;
	
	@Column(name = "P_ORDER_SUMMONEY")
	private int sumMoney;
	
	@Column(name = "P_ORDER_PROGRESS")
	private String progress;
	
	@OneToOne
	@JoinColumn(name = "EMP_CODE")
	private Employee emp;
	
	@OneToMany
	@JoinColumn(name = "P_ORDER_CODE")
	private List<POrderProduct> productList;

	public POrder() {
	}

	public POrder(int code, Date date, Date dueDate, int sumMoney, String progress, Employee emp,
			List<POrderProduct> productList) {
		this.code = code;
		this.date = date;
		this.dueDate = dueDate;
		this.sumMoney = sumMoney;
		this.progress = progress;
		this.emp = emp;
		this.productList = productList;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public int getSumMoney() {
		return sumMoney;
	}

	public void setSumMoney(int sumMoney) {
		this.sumMoney = sumMoney;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	public List<POrderProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<POrderProduct> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "POrder [code=" + code + ", date=" + date + ", dueDate=" + dueDate + ", sumMoney=" + sumMoney
				+ ", progress=" + progress + ", emp=" + emp + ", productList=" + productList + "]";
	}

	
	
	
	

	
	
	
	
	
	
}
