package com.volt.asap.sales.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.sales.dto.POrderDTO;
import com.volt.asap.sales.dto.ProductAndStoAndProCtgDTO;
import com.volt.asap.sales.entity.POrderProduct;
import com.volt.asap.sales.service.POrderService;


@Controller
@RequestMapping("/po")
public class POrderController {

	private POrderService pOrderService;
	
	@Autowired
	public POrderController(POrderService pOrderService) {
		this.pOrderService = pOrderService;
	}
	
	@GetMapping("/list")
	public ModelAndView findPOrderlist(HttpServletRequest request, String date, String empName, String product, String progress, ModelAndView mv) {
		CustomSelectCriteria selectCriteria;
		
		/* 버튼 넘기는거 */
		int limit = 4;
		int buttonAmount = 5;
		
		/* 페이지 번호 구해오기 */
		String currentPage = request.getParameter("currentPage");
	      int pageNo = 1;
	      
	      if(currentPage != null && !"".equals(currentPage)) {
	         pageNo = Integer.parseInt(currentPage);
	      }
	     
	     int totalCount = 0;

	     /* 검색 값들 받아내겠어. */
	     Map<String, String> searchValueMap = new HashMap<>();
	     searchValueMap.put("date", date == null ? "" : date);
	     searchValueMap.put("empName", empName == null ? "" :empName);
	     searchValueMap.put("product", product == null ? "" : product);
	     searchValueMap.put("progress", progress == null ? "" : progress);

	     if(isEmptyString(date) && isEmptyString(empName) && isEmptyString(product) && isEmptyString(progress)) {
	    	 /* 검색어 없이 모두 검색 */
	    	 totalCount = pOrderService.countTotalPOrder();
	    	 selectCriteria = CustomPagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
	     }else {
	    	 /*하나라도 검색어가 있는 경우*/
	    	 
	    	 totalCount = pOrderService.countTotalPOrder(searchValueMap);
	    	 selectCriteria = CustomPagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchValueMap);
	     }
	     
	     List<POrderDTO> poList = pOrderService.findPOrder(selectCriteria);
	    
	     /* 옵션에 넣을것 */
	     List<POrderProduct> productList = pOrderService.findProduct();
	     
	     /* 옵션에 중복 제거 하기 */
	     List<String> productNameList = new ArrayList<>();
	     
	     for(POrderProduct productName : productList) {
	    	 String PName = productName.getProduct().getProductName();
	    	 productNameList.add(PName);
	     }
	     
	     Set<String> productNameSet = new HashSet<String>(productNameList);
	     
	     mv.addObject("product", productNameSet);
	     mv.addObject("selectCriteria", selectCriteria);
	     mv.addObject("searchValueMap", searchValueMap);
	     mv.addObject("po", poList);
	     mv.setViewName("po/list");
	     return mv;
	}
	
	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }
	
	@GetMapping("/detail")
	public ModelAndView detailPage(int code, ModelAndView mv) {
		
		POrderDTO poDTO = pOrderService.findPoDetail(code);
		
		/* 입고단가 합계 */
		int sumMoneyW = 0;
		/* 수량 합계 */
		int sumCount = 0;
		/* 수량 합계 */
		int sumMoenyR = 0;
		/* 입고 단가,수량, 출고 단가 합계 구하기 */
		for(POrderProduct po : poDTO.getProductList()) {
			sumCount += po.getProductCount();
			sumMoenyR += po.getProduct().getProductRelease() * po.getProductCount();
			sumMoneyW += po.getProduct().getProductWaearing() * po.getProductCount();
		}
		
		
		
		mv.addObject("sumCount", sumCount);
		mv.addObject("sumMoenyR", sumMoenyR);
		mv.addObject("sumMoneyW", sumMoneyW);
		mv.addObject("po", poDTO);
		mv.setViewName("po/detail");
		
		return mv;
	}
	
	/* 발주 수정 페이지 보기 */
	@GetMapping("/modify")
	public ModelAndView modifyPage(ModelAndView mv, int code) {
		
		POrderDTO poDTO = pOrderService.findPoDetail(code);
		/* 입고단가 합계 */
		int sumMoneyW = 0;
		/* 수량 합계 */
		int sumCount = 0;
		/* 수량 합계 */
		int sumMoenyR = 0;
		
		/* 물품 하나의 단가 합계 */
		int eachSumMoneyW  = 0; 
		/* 입고 단가,수량, 출고 단가 합계 구하기 */
		for(POrderProduct po : poDTO.getProductList()) {
			sumCount += po.getProductCount();
			sumMoenyR += po.getProduct().getProductRelease() * po.getProductCount();
			sumMoneyW += po.getProduct().getProductWaearing() * po.getProductCount();
		}
		poDTO.getProductList().size();
		mv.addObject("sumCount", sumCount);
		mv.addObject("sumMoenyR", sumMoenyR);
		mv.addObject("sumMoneyW", sumMoneyW);
		mv.addObject("po", poDTO);
		mv.setViewName("po/modify");
		return mv;
	}
	/* 수정 시 상품 목록 보기 */
	@GetMapping("/searchProduct")
	public ModelAndView searchProductPage(HttpServletRequest request, ModelAndView mv, int code, String searchSto, String searchPro) {
		
		/* 옵션에 넣어줄 값 */
		List<ProductAndStoAndProCtgDTO> productList = pOrderService.selectOptionProductSearch();
		/* 검색 결과 값 */
		List<ProductAndStoAndProCtgDTO> resultList = pOrderService.selectStockList(searchSto, searchPro);
		
		List<String> storageNameList = new ArrayList<>();
		for(int i = 0; i < productList.size(); i++) {
			String storage = productList.get(i).getStorage().getStorageName();
			storageNameList.add(storage);
		}
		
		Set<String> storageNameSet = new HashSet<String>(storageNameList);
		List<String> realStorageName = new ArrayList<>();
		for(String name : storageNameSet) {
			realStorageName.add(name);
			System.out.println(name);
		}
		
		mv.addObject("resultList", resultList);
		mv.addObject("productList", productList);
		mv.addObject("storageName", realStorageName);
		mv.addObject("poCode", code);
		mv.setViewName("po/searchProduct");
		return mv;
	}
	
	@GetMapping("/searchProductwhenInsert")
	public ModelAndView searchProductwhenInsert(ModelAndView mv, String searchSto, String searchPro) {
		
		/* 옵션에 넣어줄 값 */
		List<ProductAndStoAndProCtgDTO> productList = pOrderService.selectOptionProductSearch();
		/* 검색 결과 값 */
		List<ProductAndStoAndProCtgDTO> resultList = pOrderService.selectStockList(searchSto, searchPro);
		
		mv.addObject("resultList", resultList);
		mv.addObject("productList", productList);
		mv.setViewName("po/searchProductwhenInsert");
		return mv;
	}
	
	/* 수정 시 상품 목록 에서 추가 하기 */
	@PostMapping("/modifyProduct")
	public String modifyProduct(HttpServletRequest request, RedirectAttributes rttr) {
		
		String pOrderNum = request.getParameter("poCode");
		String productCode = request.getParameter("productCode");
		
		if(isEmptyString(productCode)) {
			rttr.addFlashAttribute("mustChoiceProductMessage", "상품을 선택하세요!");
			return "redirect:searchProduct?code=" + pOrderNum;
		}else {

			int poNum = Integer.valueOf(pOrderNum);
			
			pOrderService.findProductAndRegistPoProduct(productCode, poNum);
			
			POrderProduct poProduct = new POrderProduct();
			poProduct.setpOrderCode(Integer.valueOf(pOrderNum));
			
			return "redirect:modify?code=" + pOrderNum ;
		}
	}
	
	/* 신규 등록 창 오픈 */
	@GetMapping("/registPop")
	public ModelAndView registPopPage(ModelAndView mv ) {

		/* 사원 뽑아 오기 */
		List<EmployeeDTO> empList = pOrderService.findEmpList();

		/* 모든 창고 가져오기 */
		List<StorageDTO> storageList = pOrderService.findAllStorage();
		
		List<ProductDTO> productList = pOrderService.findAllProduct();
		mv.addObject("empList", empList);
		mv.addObject("storageList", storageList);
		mv.addObject("productList", productList);
		mv.setViewName("po/registPop");
		return mv;
	}
	@GetMapping("/regist")
	public ModelAndView registPage(ModelAndView mv ) {
		
		/* 사원 뽑아 오기 */
		List<EmployeeDTO> empList = pOrderService.findEmpList();
		
		/* 모든 창고 가져오기 */
		List<StorageDTO> storageList = pOrderService.findAllStorage();
		
		List<ProductDTO> productList = pOrderService.findAllProduct();
		mv.addObject("empList", empList);
		mv.addObject("storageList", storageList);
		mv.addObject("productList", productList);
		mv.setViewName("po/regist");
		for(ProductDTO pro : productList) {
			System.out.println(pro.getProductName());
		}
		return mv;
	}
	
	@PostMapping(value = "/showRegistProductText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> showRegistProductText(int storage ){
		
		System.out.println(storage);
		List<ProductDTO> productList = new ArrayList<>();
		
		if(storage == 0) {
			System.out.println("전체 조회");
			productList = pOrderService.findAllProduct();
		}else {
			System.out.println("부분 조회");
			productList = pOrderService.findProductByStorageCode(storage);
		}
		
		List<String> productNameList = new ArrayList<>();
		for(int i = 0; i < productList.size(); i++) {
			String name = productList.get(i).getProductName();
			productNameList.add(name);
		}

		
		
		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put("productNameDrop", productList);

		return resultMap;
	}
	
	/* ajax로 조회 하기,,, */
	@PostMapping(value = "/showModifyProductText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> modifyOldProduct(int no, String newProductName) {
		
		Map<String, Object> sendMessage = pOrderService.modifyOldProduct(no,newProductName); 
		
		return sendMessage;
	}
	
	@Transactional
	@PostMapping("/modify")
	public String test(int poCode, String codeArr, String countArr, String sendDate, String sendDueDate) {

		/* 품목 코드 배열 */
		String[] productCodeArr = codeArr.split(",");
		
		/* 품목 수량 배열 */
		String[] poProductCountArr = countArr.split(",");
		/* 작성일자 date로 교체 */
		java.sql.Date date = java.sql.Date.valueOf(sendDate);
		/* 작성일자 date로 교체 */
		java.sql.Date dueDate = java.sql.Date.valueOf(sendDueDate);
		
		/* 기존 품목 코드 가져와야함 망할 복합키 */
		POrderDTO oldProduct = pOrderService.findPoDetail(poCode);
		/* 삭제하는 것임 */
		for(int i = 0; i < oldProduct.getProductList().size(); i++) {
			String oldProductCode = oldProduct.getProductList().get(i).getProduct().getProductCode();
			pOrderService.deletePoProduct(poCode, oldProductCode);
		}
		
		
		/* porderproduct 내역 채우기  */
		/* 1. product 정보가져와서 */
		/* 2. porderproduct에 차곡 차곡 넣어주기 */
		for(int i = 0; i < productCodeArr.length; i++) {
			pOrderService.addPoProduct(poCode, productCodeArr[i], poProductCountArr[i]);
		}
//		
//		/* 날짜 변경 */
		pOrderService.updateOnlyDate(date, dueDate, poCode);

		return "redirect:detail?code=" + poCode ;
	}
	
	@PostMapping("/modifyMaintain")
	public String modifyClient(RedirectAttributes rttr, String url, int no) {
		
		pOrderService.modifyMaintain(no);
		return "redirect:" + url ;
	}
	
	@PostMapping(value = "/showRegistProductInfoText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> showRegistProductText(String productCode){
		
		Map<String, Object> resultMap = new HashMap<>();
		String errorMessage = "";
		
		if(isEmptyString(productCode)) {
			errorMessage = "품목명은 반드시 입력해야 합니다.";
			resultMap.put("errorMessage", errorMessage);
			return resultMap;
		}else {
			List<ProductDTO> productList = pOrderService.findProductByProductCode(productCode);
			resultMap.put("productList", productList);
		}
		
		
		return resultMap;
	}
	@RequestMapping(value = "/regist", method = {RequestMethod.POST})
	@ResponseBody
	public String registPOrder(String date , @RequestParam(value = "productWaearingArr[]") List<String> productWaearingArr ,@RequestParam(value="codeArr[]") List<String> codeArr , @RequestParam(value="countArr[]") List<String> countArr , String dueDate, String empCode) {
		System.out.println(date);
		System.out.println(codeArr);
		System.out.println(countArr);
		System.out.println(dueDate);
		System.out.println(empCode);
		System.out.println(productWaearingArr);
		
		java.sql.Date sqlDate = java.sql.Date.valueOf(date);
		java.sql.Date sqlDueDate = java.sql.Date.valueOf(dueDate);
		
		pOrderService.registPOrder(sqlDate, sqlDueDate, empCode, codeArr, countArr, productWaearingArr);
		
		return "작성이 완료 되었습니다.";
	}
	
	@RequestMapping(value = "/delete", method = {RequestMethod.POST})
	@ResponseBody
	public String delete(int code, @RequestParam(value = "productCodeArr[]") List<String> productCodeArr ) {
		System.out.println("어서와~");
		System.out.println(code);
		System.out.println(productCodeArr);
		
		/* 물건 지우기~ */
		for(int i = 0; i < productCodeArr.size(); i++) {
			String productCode = productCodeArr.get(i);
			System.out.println(productCode);
			pOrderService.deletePoProduct(code, productCode);
		}

		pOrderService.deletePo(code);
		
		
		return "삭제 되었습니다";
	}
	
}



















