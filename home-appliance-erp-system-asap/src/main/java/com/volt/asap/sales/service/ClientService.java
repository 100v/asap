package com.volt.asap.sales.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.SelectCriteriaforDoubleValue;
import com.volt.asap.sales.dto.ClientDTO;
import com.volt.asap.sales.entity.Client;
import com.volt.asap.sales.repository.ClientRepository;

@Service
public class ClientService {

	private final ClientRepository clientRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public ClientService(ClientRepository clientRepository, ModelMapper modelMapper) {
		this.clientRepository = clientRepository;
		this.modelMapper = modelMapper;
	}
	
	public List<ClientDTO> findClientList(SelectCriteriaforDoubleValue selectCriteriaforDoubleValue) {
	      
	      int index = selectCriteriaforDoubleValue.getPageNo() - 1;         // Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
	      int count = selectCriteriaforDoubleValue.getLimit();
	      String searchValue1 = selectCriteriaforDoubleValue.getSearchValue1();
	      String searchValue2 = selectCriteriaforDoubleValue.getSearchValue2();

	      /* 페이징 처리와 정렬을 위한 객체 생성 */
	      Pageable paging = PageRequest.of(index, count, Sort.by("no").descending());
	      
	      List<Client> clientList = new ArrayList<>();

	      if(!isEmptyString(searchValue1) && !isEmptyString(searchValue2)) {
	    	  /* 거래처 및 대표 명으로 검색 할 때 */
	    	  clientList = clientRepository.findByNameContainingAndRepresentativeNameContaining(searchValue1,searchValue2, paging);
	      } else if(!isEmptyString(searchValue1) && isEmptyString(searchValue2)){
	    	  /* 거래처 명으로 검색할때 */
	    	  clientList = clientRepository.findByNameContaining(searchValue1, paging);
	      } else if(isEmptyString(searchValue1) && !isEmptyString(searchValue2)){
	    	  /* 대표 명으로 검색할때 */
	    	  clientList = clientRepository.findByRepresentativeNameContaining(searchValue2, paging);
	      } else {
	    	  /* 모든 거래처 조회 할 때 */
	    	  clientList = clientRepository.findAll(paging).toList();
	      }
	      
	      return clientList.stream().map(client -> modelMapper.map(client, ClientDTO.class)).collect(Collectors.toList());
	   }

	   public int selectTotalCount(String searchCondition1, String searchCondition2, String searchValue1,
	         String searchValue2) {
	      
	      int count = 0;
	      
	      if(searchValue1 != null && searchValue2 != null) {
	         if(!searchValue1.isEmpty() && !searchValue2.isEmpty()) {
	            System.out.println("거래처 및 대표자명 으로 검색");
	            count = clientRepository.countByNameAndRepresentativeNameContaining(searchValue1,searchValue2);
	         }else if(!searchValue1.isEmpty() && searchValue2.isEmpty()) {
	            System.out.println("거래처 명 검색");
	            count = clientRepository.countByNameContaining(searchValue1);
	            System.out.println("count" + count);
	         }else if(searchValue1.isEmpty() && !searchValue2.isEmpty()) {
	            System.out.println("대표자 명 검색");
	            count = clientRepository.countByRepresentativeNameContaining(searchValue2);
	            System.out.println("count" + count);
	         } else {
	            System.out.println("아무것도 없이 검색 버튼을 눌렀을 때");
	            count = (int)clientRepository.count();
	            System.out.println("count" + count);
	         }
	      } else {
	         System.out.println("검색 버튼 누르지 않음");
	         count = (int)clientRepository.count();
	         System.out.println("count" + count);
	      }

	      return count;
	   }

	@Transactional
	public void registNewClient(ClientDTO client) {
		
		clientRepository.save(modelMapper.map(client, Client.class));
	}

	@Transactional
	public void modifyMaintain(ClientDTO client) {
		
		Client foundClient = clientRepository.findById(client.getNo()).get();
		foundClient.setMaintain(client.getMaintain());
	}

	public ClientDTO findClientDetail(int no) {

		Client client = clientRepository.findById(no).get();
				
		
		return modelMapper.map(client, ClientDTO.class);
	}

	@Transactional
	public void modifyDetail(ClientDTO client) {
		
		Client foundClient = clientRepository.findById(client.getNo()).get();
		foundClient.setClient(client.getNo(), client.getName(), client.getRepresentativeName(), client.getPhone(), client.getTelephone(), client.getEmail(), client.getAddress(), client.getMaintain());
	}

	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }

}
