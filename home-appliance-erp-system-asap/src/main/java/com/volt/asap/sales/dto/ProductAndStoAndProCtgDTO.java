package com.volt.asap.sales.dto;

import com.volt.asap.logistic.entity.Category;
import com.volt.asap.logistic.entity.Storage;

public class ProductAndStoAndProCtgDTO {

	private String productCode;
	private String productName;
	private int productRelease;
	private int productWaearing;
	private Storage storage;
	private Category category;
	private String productStatus;
	public ProductAndStoAndProCtgDTO() {
	}
	public ProductAndStoAndProCtgDTO(String productCode, String productName, int productRelease, int productWaearing,
			Storage storage, Category category, String productStatus) {
		this.productCode = productCode;
		this.productName = productName;
		this.productRelease = productRelease;
		this.productWaearing = productWaearing;
		this.storage = storage;
		this.category = category;
		this.productStatus = productStatus;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getProductRelease() {
		return productRelease;
	}
	public void setProductRelease(int productRelease) {
		this.productRelease = productRelease;
	}
	public int getProductWaearing() {
		return productWaearing;
	}
	public void setProductWaearing(int productWaearing) {
		this.productWaearing = productWaearing;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	@Override
	public String toString() {
		return "ProductAndStoAndProCtgDTO [productCode=" + productCode + ", productName=" + productName
				+ ", productRelease=" + productRelease + ", productWaearing=" + productWaearing + ", storage=" + storage
				+ ", category=" + category + ", productStatus=" + productStatus + "]";
	}
	
	
}
