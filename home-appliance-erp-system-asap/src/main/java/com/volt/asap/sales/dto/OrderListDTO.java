package com.volt.asap.sales.dto;

import java.util.Date;

import com.volt.asap.logistic.dto.ProductDTO;

public class OrderListDTO {
	private int orderNumber;
	
	private java.sql.Date orderDate;
	
	private long orderPrice;
	private long orderShippingNumber;
	private String productCode;
	private String productName;
	private String orderShippingStatus;
	
	private ProductDTO product;

	public OrderListDTO() {
		super();
	}

	public OrderListDTO(int orderNumber, java.sql.Date orderDate, long orderPrice, long orderShippingNumber,
			String productCode, String productName, String orderShippingStatus, ProductDTO product) {
		super();
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.orderPrice = orderPrice;
		this.orderShippingNumber = orderShippingNumber;
		this.productCode = productCode;
		this.productName = productName;
		this.orderShippingStatus = orderShippingStatus;
		this.product = product;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public java.sql.Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(java.sql.Date orderDate) {
		this.orderDate = orderDate;
	}

	public long getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(long orderPrice) {
		this.orderPrice = orderPrice;
	}

	public long getOrderShippingNumber() {
		return orderShippingNumber;
	}

	public void setOrderShippingNumber(long orderShippingNumber) {
		this.orderShippingNumber = orderShippingNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getOrderShippingStatus() {
		return orderShippingStatus;
	}

	public void setOrderShippingStatus(String orderShippingStatus) {
		this.orderShippingStatus = orderShippingStatus;
	}

	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "OrderListDTO [orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", orderPrice=" + orderPrice
				+ ", orderShippingNumber=" + orderShippingNumber + ", productCode=" + productCode + ", productName="
				+ productName + ", orderShippingStatus=" + orderShippingStatus + ", product=" + product + "]";
	}

	
	
	
}
