package com.volt.asap.sales.dto;

import java.io.Serializable;

import com.volt.asap.logistic.entity.Product;

public class POrderProductPK implements Serializable {
	private static final long serialVersionUID = 5594574106172385811L;

	private int pOrderCode;
	private String product;
	
	public POrderProductPK() {
	}

	public POrderProductPK(int pOrderCode, String product) {
		this.pOrderCode = pOrderCode;
		this.product = product;
	}

	public int getpOrderCode() {
		return pOrderCode;
	}

	public void setpOrderCode(int pOrderCode) {
		this.pOrderCode = pOrderCode;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "POrderProductPK [pOrderCode=" + pOrderCode + ", product=" + product + "]";
	}
	
	
	
	
}
