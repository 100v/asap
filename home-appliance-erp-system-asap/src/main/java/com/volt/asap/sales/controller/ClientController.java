package com.volt.asap.sales.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.Pagenation;
import com.volt.asap.common.paging.SelectCriteriaforDoubleValue;
import com.volt.asap.sales.dto.ClientDTO;
import com.volt.asap.sales.service.ClientService;


@Controller
@RequestMapping("/client")
public class ClientController {
	
	private ClientService clientService;
	
	@Autowired
	public ClientController (ClientService clientService) {
		this.clientService = clientService;
	}
	
	@GetMapping("/list")
	   public ModelAndView findClientList(HttpServletRequest request, ModelAndView mv) {
	      
	      String currentPage = request.getParameter("currentPage");
	      int pageNo = 1;
	      
	      if(!isEmptyString(currentPage)) {
	         pageNo = Integer.parseInt(currentPage);
	      }
	      
	      String searchValue1 = request.getParameter("searchValue1");
	      String searchValue2 = request.getParameter("searchValue2");
	      String searchCondition1 = request.getParameter("searchCondition1");
	      String searchCondition2 = request.getParameter("searchCondition2"); 
	      
	      int totalCount = clientService.selectTotalCount(searchCondition1, searchCondition2, searchValue1, searchValue2);
	      System.out.println("totalCount" + totalCount);
	      
	      int limit = 4;
	      
	      int buttonAmount = 5;
	      
	      SelectCriteriaforDoubleValue selectCriteriaforDoubleValue = null;
	      
	      if(searchValue1 != null && searchValue2 != null) {
	         /* 둘 다 널값이 아닐때 -> 그냥 검색하기 눌렀을때, 진짜 입력 뭐 어디 하나라도 했을때*/
	         selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount, searchCondition1,searchValue1, searchCondition2, searchValue2);
	      } else {
	         /* 맨 처음일 때*/
	         selectCriteriaforDoubleValue = Pagenation.getSelectCriteriaforDoubleValue(pageNo, totalCount, limit, buttonAmount);
	      }
	      
	      List<ClientDTO> clientList = clientService.findClientList(selectCriteriaforDoubleValue);
	      
	      for(ClientDTO client : clientList) {
	         System.out.println(client);
	      }
	      
	      mv.addObject("clientList", clientList);
	      mv.addObject("selectCriteriaforDoubleValue", selectCriteriaforDoubleValue);
	      mv.setViewName("client/list");
	      
	      return mv;
	   }
	
	@GetMapping("/registPop")
	public void registPopPage() {}
	
	@GetMapping("/regist")
	public void registPage() {}
	
	@PostMapping("/regist")
	public ModelAndView registClient(ModelAndView mv, ClientDTO client, RedirectAttributes rttr) {
		
		System.out.println(client);
		
		String maintain = client.getMaintain();
		
		if(maintain == null || "".equals(maintain) ) {
			client.setMaintain("N");
		}
		
		clientService.registNewClient(client);
		
		rttr.addFlashAttribute("registSuccessMessage", "거래처 등록 성공!");
		mv.setViewName("redirect:/client/list");
		
		return mv;
	}
	
	@PostMapping("/modifyMaintain")
	public String modifyClient(HttpServletRequest request, RedirectAttributes rttr, @ModelAttribute ClientDTO client, String url) {
		
		System.out.println(url);
		System.out.println(url);
		
		clientService.modifyMaintain(client);
		
		return "redirect:" + url ;
	}
	
	@GetMapping("/detail")
	public ModelAndView detailPage(int no, ModelAndView mv) {
		
		ClientDTO client = clientService.findClientDetail(no);
		
		System.out.println(client);
		
		mv.addObject("client", client);
		mv.setViewName("client/detail");
		
		return mv;
	}
	
	@GetMapping("/modify/{no}")
	public ModelAndView modifyPage(ModelAndView mv, @PathVariable int no) {
		
		ClientDTO client = clientService.findClientDetail(no);
		
		String phone = client.getPhone();
		String tele = client.getTelephone();
		System.out.println(phone);
		System.out.println(tele);
		
		String phoneArr[] = phone.split("-");
		String teleArr[] = tele.split("-");
		
		Map<String, String> phoneMap = new HashMap<>();
		phoneMap.put("phone1", phoneArr[0]);
		phoneMap.put("phone2", phoneArr[1]);
		phoneMap.put("phone3", phoneArr[2]);

		Map<String, String> teleMap = new HashMap<>();
		teleMap.put("tele1", teleArr[0]);
		teleMap.put("tele2", teleArr[1]);
		teleMap.put("tele3", teleArr[2]);
		
		mv.addObject("client", client);
		mv.addObject("phone", phoneMap);
		mv.addObject("tele", teleMap);
		mv.setViewName("client/modify");
		
		return mv;
	}
	
	@PostMapping("/modify")
	public String modifyPage(RedirectAttributes rttr, @ModelAttribute ClientDTO client) {
		
		clientService.modifyDetail(client);
		
		return "redirect:/client/detail?no=" + client.getNo();
	}
	
	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }
	
}
