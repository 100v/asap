package com.volt.asap.sales.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.volt.asap.hrm.entity.Employee;

@Entity(name = "estimate")
@Table(name = "TBL_ESTIMATE")
@SequenceGenerator(
		name = "ESTI_SEQ_ESTIMATE_CODE",
		sequenceName = "SEQ_ESTIMATE_CODE",
		initialValue = 11,
		allocationSize = 1
)
public class Estimate {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "ESTI_SEQ_ESTIMATE_CODE"
	)
	@Column(name="ESTIMATE_CODE")
	private int code;

	@Column(name = "ESTIMATE_DATE")
	private Date date;

	@Column(name="ESTIMATE_CLIENT")
	private String client; 
	
	@Column(name = "ESTI_EXPIRY_DATE")
	private Date expiryDate;

	@Column(name = "ESTI_SUM")
	private int sumMoney;
	
	@Column(name ="ESTI_PROGRESS")
	private String progress;

	@OneToOne
	@JoinColumn(name = "EMP_CODE")
	private Employee emp;
	
	@Column(name="SEND_STATUS")
	private String status;
	
	@OneToMany
	@JoinColumn(name = "ESTIMATE_CODE")
	private List<QuotationProduct> productList;

	public Estimate() {
	}

	public Estimate(int code, Date date, String client, Date expiryDate, int sumMoney, String progress, Employee emp,
			String status, List<QuotationProduct> productList) {
		this.code = code;
		this.date = date;
		this.client = client;
		this.expiryDate = expiryDate;
		this.sumMoney = sumMoney;
		this.progress = progress;
		this.emp = emp;
		this.status = status;
		this.productList = productList;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public int getSumMoney() {
		return sumMoney;
	}

	public void setSumMoney(int sumMoney) {
		this.sumMoney = sumMoney;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<QuotationProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<QuotationProduct> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "Estimate [code=" + code + ", date=" + date + ", client=" + client + ", expiryDate=" + expiryDate
				+ ", sumMoney=" + sumMoney + ", progress=" + progress + ", emp=" + emp + ", status=" + status
				+ ", productList=" + productList + "]";
	}
	
	
	

	
	
	
	
	
}
