package com.volt.asap.sales.dto;

import com.volt.asap.logistic.entity.Product;

public class POrderProductDTO {

	private int pOrderCode;
	private Product product;
	private int productCount;
	private int productSumPrice;
	public POrderProductDTO() {
	}
	public POrderProductDTO(int pOrderCode, Product product, int productCount, int productSumPrice) {
		this.pOrderCode = pOrderCode;
		this.product = product;
		this.productCount = productCount;
		this.productSumPrice = productSumPrice;
	}
	public int getpOrderCode() {
		return pOrderCode;
	}
	public void setpOrderCode(int pOrderCode) {
		this.pOrderCode = pOrderCode;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getProductCount() {
		return productCount;
	}
	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}
	public int getProductSumPrice() {
		return productSumPrice;
	}
	public void setProductSumPrice(int productSumPrice) {
		this.productSumPrice = productSumPrice;
	}
	@Override
	public String toString() {
		return "POrderProductDTO [pOrderCode=" + pOrderCode + ", product=" + product + ", productCount=" + productCount
				+ ", productSumPrice=" + productSumPrice + "]";
	}
	
	
	
}
