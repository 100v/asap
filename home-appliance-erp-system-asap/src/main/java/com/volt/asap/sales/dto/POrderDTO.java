package com.volt.asap.sales.dto;

import java.sql.Date;
import java.util.List;

import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.sales.entity.POrderProduct;

public class POrderDTO {

	private int code;
	private Date date;
	private Date dueDate;
	private int sumMoney;
	private String progress;
	private EmployeeDTO emp;
	private List<POrderProduct> productList;
	public POrderDTO() {
	}
	public POrderDTO(int code, Date date, Date dueDate, int sumMoney, String progress, EmployeeDTO emp,
			List<POrderProduct> productList) {
		this.code = code;
		this.date = date;
		this.dueDate = dueDate;
		this.sumMoney = sumMoney;
		this.progress = progress;
		this.emp = emp;
		this.productList = productList;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public int getSumMoney() {
		return sumMoney;
	}
	public void setSumMoney(int sumMoney) {
		this.sumMoney = sumMoney;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public EmployeeDTO getEmp() {
		return emp;
	}
	public void setEmp(EmployeeDTO emp) {
		this.emp = emp;
	}
	public List<POrderProduct> getProductList() {
		return productList;
	}
	public void setProductList(List<POrderProduct> productList) {
		this.productList = productList;
	}
	@Override
	public String toString() {
		return "POrderDTO [code=" + code + ", date=" + date + ", dueDate=" + dueDate + ", sumMoney=" + sumMoney
				+ ", progress=" + progress + ", emp=" + emp + ", productList=" + productList + "]";
	}
	
	
	
	
	
	
	
}
