package com.volt.asap.sales.repository;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.sales.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Date>{

	Order findOrderEntityByOrderNumber(int orderNumber);

	
}
