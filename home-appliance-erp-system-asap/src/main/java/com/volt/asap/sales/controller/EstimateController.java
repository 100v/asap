package com.volt.asap.sales.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.sales.dto.EstimateDTO;
import com.volt.asap.sales.dto.POrderDTO;
import com.volt.asap.sales.dto.ProductAndStoAndProCtgDTO;
import com.volt.asap.sales.dto.QuotationProductDTO;
import com.volt.asap.sales.entity.Estimate;
import com.volt.asap.sales.entity.POrderProduct;
import com.volt.asap.sales.entity.QuotationProduct;
import com.volt.asap.sales.service.EstimateService;

@Controller
@RequestMapping("/estimate")
public class EstimateController {

	private EstimateService estimateService;
	
	@Autowired
	public EstimateController(EstimateService estimateService) {
		this.estimateService = estimateService;
	}
	
	@GetMapping("/list")
	public ModelAndView findEstimate(HttpServletRequest request, ModelAndView mv, String date, String empName, String product, String progress) {
		CustomSelectCriteria selectCriteria;
		
		/* 버튼 넘기는거 */
		int limit = 4;
		int buttonAmount = 5;
		
		/* 페이지 번호 구해오기 */
		String currentPage = request.getParameter("currentPage");
	      int pageNo = 1;
		
        if(currentPage != null && !"".equals(currentPage)) {
	         pageNo = Integer.parseInt(currentPage);
	      }
	     
	    int totalCount = 0;
	    
	    Map<String, String> searchValueMap = new HashMap<>();
	     searchValueMap.put("date", date == null ? "" : date);
	     searchValueMap.put("empName", empName == null ? "" :empName);
	     searchValueMap.put("product", product == null ? "" : product);
	     searchValueMap.put("progress", progress == null ? "" : progress);

		 if(isEmptyString(date) && isEmptyString(empName) && isEmptyString(product) && isEmptyString(progress)) {
	    	 /* 검색어 없이 모두 검색 */
	    	 totalCount = estimateService.countTotalPOrder();
	    	 selectCriteria = CustomPagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
	     }else {
	    	 /*하나라도 검색어가 있는 경우*/
	    	 totalCount = estimateService.countTotalPOrder(searchValueMap);
	    	 selectCriteria = CustomPagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchValueMap);
	     }
		
		
		/* 옵션에 들어갈 품목 리스트 */
		List<QuotationProduct> productList = estimateService.findProduct();
		
	    List<String> productNameList = new ArrayList<>();
	     
	     for(QuotationProduct productName : productList) {
	    	 String PName = productName.getProduct().getProductName();
	    	 productNameList.add(PName);
	     }
		Set<String> productNameSet = new HashSet<String>(productNameList);
		
		/* 전체 조회 - 검색어에 따라 */
		List<EstimateDTO> estimateList = estimateService.findEesimateList(selectCriteria);  

		/* 옵션을 위한 전체 검색 */
		List<EstimateDTO> estiOptionList = estimateService.findEesimateList();  
		
		/*옵션에 들어갈 이름 리스트 */
		List<String> empNameList = new ArrayList<>();
		for(EstimateDTO estimate : estiOptionList) {
			String empNameOption = estimate.getEmp().getEmpName();
			empNameList.add(empNameOption);
		}
		
		Set<String> empNameSet = new HashSet<String>(empNameList);
		
		mv.addObject("estimateList", estimateList);
		mv.addObject("empName", empNameSet);
		mv.addObject("product", productNameSet);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("searchValueMap", searchValueMap);
		mv.setViewName("estimate/list");
		return mv;
	}
	
	@PostMapping("/modifyMaintain")
	public String modifyClient(RedirectAttributes rttr, String url, int no) {
		
		estimateService.modifyMaintain(no);
		return "redirect:" + url ;
	}
	
	@GetMapping("/detail")
	public ModelAndView detailPage(int code, ModelAndView mv) {
		EstimateDTO estimateDTO = estimateService.findEstiDetail(code);
		
		/*할인 적용 금액 */
		int discountMoney = 0;

		/* 할인률 */
		int discount = 0;
		/* 입고단가 합계 */
		int sumMoneyW = 0;
		/* 수량 합계 */
		int sumCount = 0;
		/* 수량 합계 */
		int sumMoenyR = 0;
		/* 입고 단가,수량, 출고 단가 합계 구하기 */
		for(QuotationProduct esti : estimateDTO.getProductList()) {
			sumCount +=  esti.getProductCount();
			sumMoenyR += esti.getProduct().getProductRelease() * esti.getProductCount();
			sumMoneyW += esti.getProduct().getProductWaearing() * esti.getProductCount();
			discount = esti.getDiscount();
			discountMoney += ((100-discount)*0.01) * ((esti.getProduct().getProductRelease()) * esti.getProductCount());
		}
		
		
		estimateService.updateSumMoeny(code, discountMoney);
		
		mv.addObject("sumCount", sumCount);
		mv.addObject("sumMoenyR", sumMoenyR);
		mv.addObject("sumMoneyW", sumMoneyW);
		mv.addObject("discountMoney", discountMoney);
		mv.addObject("estimate", estimateDTO);
		mv.setViewName("estimate/detail");
		
		return mv;
	}
	
	@GetMapping("/modify")
	public ModelAndView modifyPage(ModelAndView mv, int code) {
		
		EstimateDTO estimateDTO = estimateService.findEstiDetail(code);
		estimateDTO.getProductList().size();
		mv.addObject("estimate", estimateDTO);
		mv.setViewName("estimate/modify");
		return mv;
	}
	
	@Transactional
	@PostMapping("/modify")
	public String test(int estiCode, String codeArr, String countArr, String sendDate, String sendExpiryDate, String discountArr ) {

		String[] productDiscountArr = discountArr.split(",");
		
		/* 품목 코드 배열 */
		String[] productCodeArr = codeArr.split(",");
		
		/* 품목 수량 배열 */
		String[] poProductCountArr = countArr.split(",");
		/* 작성일자 date로 교체 */
		java.sql.Date date = java.sql.Date.valueOf(sendDate);
		/* 작성일자 date로 교체 */
		java.sql.Date expiryDate = java.sql.Date.valueOf(sendExpiryDate);
		
		/* 기존 품목 코드 가져와야함 망할 복합키 */
		EstimateDTO oldProduct = estimateService.findEstiDetail(estiCode);
		/* 삭제하는 것임 */
		for(int i = 0; i < oldProduct.getProductList().size(); i++) {
			String oldProductCode = oldProduct.getProductList().get(i).getProduct().getProductCode();
			estimateService.deleteEstiProduct(estiCode, oldProductCode);
		}
		
		/* 1. product 정보가져와서 */
		/* 2. porderproduct에 차곡 차곡 넣어주기 */
		for(int i = 0; i < productCodeArr.length; i++) {
			estimateService.addEstiProduct(estiCode, productCodeArr[i], poProductCountArr[i], productDiscountArr[i]);
		}
		
		/* 날짜 변경 */
		estimateService.updateOnlyDate(date, expiryDate, estiCode);
		return "redirect:detail?code=" + estiCode ;
	}
	
	@GetMapping("/searchProduct")
	public ModelAndView searchProductPage(HttpServletRequest request, ModelAndView mv, int code, String searchSto, String searchPro) {
		
		/* 옵션에 넣어줄 값 */
		List<ProductAndStoAndProCtgDTO> productList = estimateService.selectOptionProductSearch();
		/* 창고이름 */
		List<String> storageNameList = new ArrayList<>();
		for(int i = 0; i < productList.size(); i++) {
			String storage = productList.get(i).getStorage().getStorageName();
			storageNameList.add(storage);
		}
		
		Set<String> storageNameSet = new HashSet<String>(storageNameList);
		List<String> realStorageName = new ArrayList<>();
		for(String name : storageNameSet) {
			realStorageName.add(name);
			System.out.println(name);
		}
		
		
		
		/* 검색 결과 값 */
		List<ProductAndStoAndProCtgDTO> resultList = estimateService.selectStockList(searchSto, searchPro);
		
		mv.addObject("resultList", resultList);
		mv.addObject("productList", productList);
		mv.addObject("storageName", realStorageName);
		mv.addObject("estiCode", code);
		mv.setViewName("estimate/searchProduct");
		
		return mv;
	}
	
	@PostMapping(value = "/showModifyProductText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> modifyOldProduct(int no, String newProductName) {
		
		Map<String, Object> sendMessage = estimateService.modifyOldProduct(no,newProductName); 
		
		return sendMessage;
	}
	
	@PostMapping("/modifyProduct")
	public String modifyProduct(HttpServletRequest request, RedirectAttributes rttr) {
		
		String estiNum = request.getParameter("estiCode");
		String productCode = request.getParameter("productCode");
		
		System.out.println(estiNum);
		System.out.println(productCode);
		
		if(isEmptyString(productCode)) {
			rttr.addFlashAttribute("mustChoiceProductMessage", "상품을 선택하세요!");
			return "redirect:searchProduct?code=" + estiNum;
		}else {

			int eNum = Integer.valueOf(estiNum);
			
			estimateService.findProductAndRegistEstiProduct(productCode, eNum);
			
			return "redirect:modify?code=" + estiNum ;
		}
	}
	
	@RequestMapping(value = "/delete", method = {RequestMethod.POST})
	@ResponseBody
	public String delete(int code, @RequestParam(value = "productCodeArr[]") List<String> productCodeArr ) {
		System.out.println(code);
		System.out.println(productCodeArr);
		
		/* 물건 지우기~ */
		for(int i = 0; i < productCodeArr.size(); i++) {
			String productCode = productCodeArr.get(i);
			System.out.println(productCode);
			estimateService.deleteEstiProduct(code, productCode);
		}

		estimateService.deleteEsti(code);
		
		
		return "삭제 되었습니다";
	}
	
	@GetMapping("/registPop")
	public ModelAndView registPopPage(ModelAndView mv ) {

		/* 사원 뽑아 오기 */
		List<EmployeeDTO> empList = estimateService.findEmpList();

		/* 모든 창고 가져오기 */
		List<StorageDTO> storageList = estimateService.findAllStorage();
		
		List<ProductDTO> productList = estimateService.findAllProduct();
		mv.addObject("empList", empList);
		mv.addObject("storageList", storageList);
		mv.addObject("productList", productList);
		mv.setViewName("estimate/registPop");
		return mv;
	}

	@GetMapping("/regist")
	public ModelAndView registPage(ModelAndView mv ) {
		
		/* 사원 뽑아 오기 */
		List<EmployeeDTO> empList = estimateService.findEmpList();
		
		/* 모든 창고 가져오기 */
		List<StorageDTO> storageList = estimateService.findAllStorage();
		
		List<ProductDTO> productList = estimateService.findAllProduct();
		mv.addObject("empList", empList);
		mv.addObject("storageList", storageList);
		mv.addObject("productList", productList);
		mv.setViewName("estimate/regist");
		return mv;
	}
	
	@RequestMapping(value = "/regist", method = {RequestMethod.POST})
	@ResponseBody
	public String registPOrder(String date , @RequestParam(value = "productReleaseArr[]") List<String> productReleaseArr ,@RequestParam(value="codeArr[]") List<String> codeArr , 
			@RequestParam(value="countArr[]") List<String> countArr ,@RequestParam(value="discountArr[]") List<String> discountArr, String expiryDate, String empCode, String client) {
		System.out.println(date);
		System.out.println(codeArr);
		System.out.println(countArr);
		System.out.println(expiryDate);
		System.out.println(empCode);
		System.out.println(productReleaseArr);
		System.out.println(discountArr);
		System.out.println(client);
		
		java.sql.Date sqlDate = java.sql.Date.valueOf(date);
		java.sql.Date sqlExpiryDate = java.sql.Date.valueOf(expiryDate);
		
		estimateService.registEstimate(sqlDate, sqlExpiryDate, empCode, codeArr, countArr, productReleaseArr, discountArr, client);
		
		return "작성이 완료 되었습니다.";
	}
	
	@PostMapping(value = "/showRegistProductInfoText", produces = "application/json; charset=UTF-8")
	@ResponseBody
	public Map<String, Object> showRegistProductText(String productCode){
		
		Map<String, Object> resultMap = new HashMap<>();
		String errorMessage = "";
		
		if(isEmptyString(productCode)) {
			errorMessage = "품목명은 반드시 입력해야 합니다.";
			resultMap.put("errorMessage", errorMessage);
			return resultMap;
		}else {
			List<ProductDTO> productList = estimateService.findProductByProductCode(productCode);
			resultMap.put("productList", productList);
		}
		
		
		return resultMap;
	}
	
	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }
}
