package com.volt.asap.sales.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.volt.asap.sales.dto.QuotationProductPK;
import com.volt.asap.sales.entity.QuotationProduct;

public interface QuotationProductRepository extends JpaRepository<QuotationProduct, QuotationProductPK> {

	@Query(value = "SELECT A.*, B.*\r\n"
			+ "  FROM TBL_QUOTATION_PRODUCT A\r\n"
			+ "  JOIN TBL_PRODUCT B ON (A.PRODUCT_CODE = B.PRODUCT_CODE)", nativeQuery = true)
	List<QuotationProduct> findQuoProductName();

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO TBL_QUOTATION_PRODUCT(ESTIMATE_CODE,PRODUCT_CODE,ESTI_DISCOUNT,ESTI_PRODUCT_COUNT,ESTI_PRODUCT_SUMPRICE)\r\n"
			+ "VALUES(SEQ_ESTIMATE_CODE.currval,:productCode, :discount, :count ,:sumMoney)" , nativeQuery = true)
	int insertQuery(String productCode, int count, int sumMoney, int discount);

	
}
