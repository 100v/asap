package com.volt.asap.sales.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.volt.asap.logistic.entity.Product;
import com.volt.asap.sales.dto.POrderProductPK;

@Entity(name = "POderProduct")
@Table(name = "TBL_ORDER_PRODUCT")
@IdClass(POrderProductPK.class)
public class POrderProduct implements Serializable {
	private static final long serialVersionUID = 6560880084527003230L;
	
	@Id
	@Column(name = "P_ORDER_CODE")
	private int pOrderCode;

	@Id
	@ManyToOne
	@JoinColumn(name = "PRODUCT_CODE" )
	private Product product;

	@Column(name = "PRODUCT_COUNT")
	private int productCount;
	
	@Column(name = "PRODUCT_SUMPRICE")
	private int productSumPrice;

	public POrderProduct() {
	}

	public POrderProduct(int pOrderCode, Product product, int productCount, int productSumPrice) {
		this.pOrderCode = pOrderCode;
		this.product = product;
		this.productCount = productCount;
		this.productSumPrice = productSumPrice;
	}

	public int getpOrderCode() {
		return pOrderCode;
	}

	public void setpOrderCode(int pOrderCode) {
		this.pOrderCode = pOrderCode;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public int getProductSumPrice() {
		return productSumPrice;
	}

	public void setProductSumPrice(int productSumPrice) {
		this.productSumPrice = productSumPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "POrderProduct [pOrderCode=" + pOrderCode + ", product=" + product + ", productCount=" + productCount
				+ ", productSumPrice=" + productSumPrice + "]";
	}


	
	
	

	
	
}
