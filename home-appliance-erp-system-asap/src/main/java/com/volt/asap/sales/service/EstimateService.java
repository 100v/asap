package com.volt.asap.sales.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.repository.EmployeeRepository;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.ProductAndStoAndProCtg;
import com.volt.asap.logistic.entity.StockHistoryAndProductAndStorage;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.ProductJoinRepository;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.StockHistoryAndProductAndStorageRepository;
import com.volt.asap.logistic.repository.StorageRepository;
import com.volt.asap.sales.dto.EstimateDTO;
import com.volt.asap.sales.dto.POrderDTO;
import com.volt.asap.sales.dto.POrderProductDTO;
import com.volt.asap.sales.dto.POrderProductPK;
import com.volt.asap.sales.dto.ProductAndStoAndProCtgDTO;
import com.volt.asap.sales.dto.QuotationProductDTO;
import com.volt.asap.sales.dto.QuotationProductPK;
import com.volt.asap.sales.entity.Estimate;
import com.volt.asap.sales.entity.POrder;
import com.volt.asap.sales.entity.POrderProduct;
import com.volt.asap.sales.entity.QuotationProduct;
import com.volt.asap.sales.repository.EstimateRepository;
import com.volt.asap.sales.repository.QuotationProductRepository;

@Service
public class EstimateService {
	
	private final EstimateRepository estimateRepository;
	private final QuotationProductRepository quotationProductRepository;
	private final StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository;
	private final ProductRepository productRepository;
	private final EmployeeRepository employeeRepository;
	private final StorageRepository storageRepository;
	private final ProductJoinRepository productJoinRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public EstimateService (EstimateRepository estimateRepository, ModelMapper modelMapper, QuotationProductRepository quotationProductRepository, 
			StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository,ProductRepository productRepository, EmployeeRepository employeeRepository,
			StorageRepository storageRepository, ProductJoinRepository productJoinRepository) {
		this.estimateRepository = estimateRepository;
		this.modelMapper = modelMapper;
		this.quotationProductRepository = quotationProductRepository;
		this.stockHistoryAndProductAndStorageRepository = stockHistoryAndProductAndStorageRepository;
		this.productRepository = productRepository;
		this.employeeRepository = employeeRepository;
		this.storageRepository = storageRepository;
		this.productJoinRepository = productJoinRepository;
	}

	/* 옵션에 들어갈 품목 리스트 */
	public List<QuotationProduct> findProduct() {

		List<QuotationProduct> productList = quotationProductRepository.findQuoProductName();
		
		return productList.stream().map(product -> modelMapper.map(product, QuotationProduct.class)).collect(Collectors.toList());
	}

	public List<EstimateDTO> findEesimateList(CustomSelectCriteria selectCriteria) {

		 int index = selectCriteria.getPageNo() - 1;         // Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
	        int count = selectCriteria.getLimit();
			
			Pageable paging = PageRequest.of(index, count, Sort.by("code").descending());
			
			List<Estimate> estimateList = null;
			if(selectCriteria.getSearchValueMap() == null) {
				/*전체 조회일때*/
				estimateList = estimateRepository.findAll(paging).toList();
			}else {
				/* 검색어가 있을때 */
				java.sql.Date date = toSqlDate(selectCriteria.getSearchValueMap().get("date"));
				String name = selectCriteria.getSearchValueMap().get("empName");
				String product = selectCriteria.getSearchValueMap().get("product");
				String progress = selectCriteria.getSearchValueMap().get("progress");
				
				if(date != null && !isEmptyString(name) && !isEmptyString(product) && !isEmptyString(progress)) {
					/* 모두 입력했을 때 */
					estimateList = estimateRepository.findByDateAndEmpEmpNameContainingAndProductListProductProductNameAndProgress(date, name, product, progress, paging);
				}else {
					
					/*값이 들어있는 키값을 모아준다. 메소드에 태워 보낼거니까 */
					Set<String> condition = selectCriteria.getSearchValueMap().keySet();
					List<String> haveValCondition = new ArrayList<>();
					
					for(String key : condition) {
						String val = selectCriteria.getSearchValueMap().get(key);
						if( val != null && !"".equals(val)) {
							haveValCondition.add(key);
						}
					}
					
					switch(haveValCondition.size()){
					case 1:
						estimateList = findOneSearchCondition(haveValCondition, selectCriteria, paging);
						return estimateList.stream().map(esti -> modelMapper.map(esti, EstimateDTO.class)).collect(Collectors.toList());
					case 2:
						estimateList = findTwoSearchCondition(haveValCondition, selectCriteria, paging);
						return estimateList.stream().map(esti -> modelMapper.map(esti, EstimateDTO.class)).collect(Collectors.toList());
					case 3:
						estimateList = findThreeSearchCondition(haveValCondition, selectCriteria, paging);
						return estimateList.stream().map(esti -> modelMapper.map(esti, EstimateDTO.class)).collect(Collectors.toList());
				}
					
					
				}
			}
			
			return estimateList.stream().map(esti -> modelMapper.map(esti, EstimateDTO.class)).collect(Collectors.toList());
		}


	private List<Estimate> findThreeSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria,
			Pageable paging) {
		/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		String condition3 = haveValCondition.get(2);
		/*검색 결과 담아둘 string*/
		String value1 = selectCriteria.getSearchValueMap().get(condition1);
		String value2 = selectCriteria.getSearchValueMap().get(condition2);
		String value3 = selectCriteria.getSearchValueMap().get(condition3);
		
		List<Estimate> result = null;
		
		if(condition1 == "date") {
			/*date 가 포함된 검색어*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 == "product") {
				/*product도 포함되었을때*/
				if(condition3 == "empName") {
					/*마지막 검색어는 empName*/
					result = estimateRepository.findByDateAndProductListProductProductNameAndEmpEmpNameContaining(date, value2, value3, paging);
				}else {
					/*마지막 검색어는 progress*/
					result = estimateRepository.findByDateAndProductListProductProductNameAndProgress(date,value2,value3, paging);
				}
			}else {
				/* 두번째 검색어에 product가 포함 되지 않았을때 */
				result = estimateRepository.findByDateAndEmpEmpNameContainingAndProgress(date,value2,value3, paging);
			}
			
		}else{
			/* date가 포함되지 않은 검색어 */
			result = estimateRepository.findByProductListProductProductNameAndEmpEmpNameContainingAndProgress(value1,value2,value3, paging);
		}
		
		return result;
	}

	private List<Estimate> findTwoSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria,
			Pageable paging) {
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		
		String value1 = selectCriteria.getSearchValueMap().get(condition1);
		String value2 = selectCriteria.getSearchValueMap().get(condition2);
		
		List<Estimate> result;
		
		if(condition1 == "date") {
			/*date 타입 포함 검색시 해야할 형변환*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 ==  "empName") {
				result = estimateRepository.findByDateAndEmpEmpNameContaining(date, value2, paging);
			}else if(condition2 ==  "product") {
				result = estimateRepository.findByDateAndProductListProductProductName(date, value2, paging);
			}else {
				result = estimateRepository.findByDateAndProgress(date, value2, paging);
			}
			
		}else if(condition1 == "product") {
			/* product 타입 포함 검색 */
			if(condition2 == "empName") {
				result = estimateRepository.findByProductListProductProductNameAndEmpEmpNameContaining(value1, value2, paging);
			}else {
				result = estimateRepository.findByProductListProductProductNameAndProgress(value1, value2, paging);
			}
		}else {
			/*empName 타입 포함 검색 */
			result = estimateRepository.findByEmpEmpNameContainingAndProgress(value1,value2, paging);
		}
		
		return result;
	}

	private List<Estimate> findOneSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria,
			Pageable paging) {
		String condition = haveValCondition.get(0);
		
		/*검색 결과 담아둘 string*/
		String value = selectCriteria.getSearchValueMap().get(condition);
		List<Estimate> result = null;
		switch (condition) {
		case "date":
				java.sql.Date date = toSqlDate(value);
				result = estimateRepository.findByDate(date, paging);
				break;
		case "empName":
				result = estimateRepository.findByEmpEmpNameContaining(value, paging);
				break;
		case "product":
				result = estimateRepository.findByProductListProductProductName(value, paging);
				break;
		case "progress":
				result = estimateRepository.findByProgress(value, paging);
				break;
		}
		
		return result;
	}

	public int countTotalPOrder() {
		return (int)estimateRepository.count();
	}

	public int countTotalPOrder(Map<String, String> searchValueMap) {
		/*검색어 형변환*/
		java.sql.Date date = toSqlDate(searchValueMap.get("date"));
		String name = searchValueMap.get("empName");
		String product = searchValueMap.get("product");
		String progress = searchValueMap.get("progress");

		/* 리턴값으로 보낼 totalCount */
		int totalCount = 0;
		
		/* 값이 있는 조건어만 담을 것*/
		List<String> haveValCondition = new ArrayList<>();
		
		if(date != null && !isEmptyString(name) && !isEmptyString(product) && !isEmptyString(progress)) {
			/* 4가지 다 입력한경우 */
			totalCount = estimateRepository.countByDateAndEmpEmpNameAndProductListProductProductNameAndProgressContaining(date, name, product, progress);
		}else {
			/*현재 키값을 가져온다.*/
			Set<String> condition = searchValueMap.keySet();
			
			/*값이 들어있는 키값을 모아준다. 메소드에 태워 보낼거니까 */
			for(String key : condition) {
				String val = searchValueMap.get(key);
				if( val != null && !"".equals(val)) {
					haveValCondition.add(key);
				}
			}
		
			switch(haveValCondition.size()){
			case 1:
				totalCount = oneSearchCondition(haveValCondition, searchValueMap);
				break;
			case 2:
				totalCount = twoSearchCondition(haveValCondition, searchValueMap);
				break;
			case 3:
				totalCount = threeSearchCondition(haveValCondition, searchValueMap);
				break;
				}
			}
			
			return totalCount;
	}


    private int threeSearchCondition(List<String> haveValCondition, Map<String, String> searchValueMap) {
    	/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		String condition3 = haveValCondition.get(2);
		/*검색 결과 담아둘 string*/
		String value1 = searchValueMap.get(condition1);
		String value2 = searchValueMap.get(condition2);
		String value3 = searchValueMap.get(condition3);
		/*최종 결과값*/
		int result = 0;
		
		if(condition1 == "date") {
			/*date 가 포함된 검색어*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 == "product") {
				/*product도 포함되었을때*/
				if(condition3 == "empName") {
					/*마지막 검색어는 empName*/
					return result = estimateRepository.countByDateAndProductListProductProductNameAndEmpEmpNameContaining(date, value2, value3);
				}else {
					/*마지막 검색어는 progress*/
					return result = estimateRepository.countByDateAndProductListProductProductNameAndProgress(date,value2,value3);
				}
			}else {
				/* product가 포함 되지 않았을때 */
				return result = estimateRepository.countByDateAndEmpEmpNameContainingAndProgress(date,value2,value3);
			}
			
		}else{
			return result = estimateRepository.countByProductListProductProductNameAndEmpEmpNameContainingAndProgress(value1,value2,value3);
		}
			
	}

	private int twoSearchCondition(List<String> haveValCondition, Map<String, String> searchValueMap) {
		/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		int result = 0;
		
		/*검색 결과 담아둘 string*/
		String value1 = searchValueMap.get(condition1);
		String value2 = searchValueMap.get(condition2);
		
		if(condition1 == "date") {
			/*date 타입 포함 검색시 해야할 형변환*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 ==  "empName") {
				return result = estimateRepository.countByDateAndEmpEmpNameContaining(date, value2);
			}else if(condition2 ==  "product") {
				return result = estimateRepository.countByDateAndProductListProductProductName(date, value2);
			}else {
				return result = estimateRepository.countByDateAndProgress(date, value2);
			}
			
		}else if(condition1 == "product") {
			/* product 타입 포함 검색 */
			if(condition2 == "empName") {
				return result = estimateRepository.countByProductListProductProductNameAndEmpEmpNameContaining(value1, value2);
			}
				return result = estimateRepository.countByProductListProductProductNameAndProgress(value1, value2);
		}else {
			/*empName 타입 포함 검색 */
			return result = estimateRepository.countByEmpEmpNameContainingAndProgress(value1,value2);
		}
		
	}

	private int oneSearchCondition(List<String> haveValCondition, Map<String, String> searchValueMap) {
		String condition = haveValCondition.get(0);
		int result = 0;
		
		/*검색 결과 담아둘 string*/
		String value = searchValueMap.get(condition);
		switch (condition) {
		case "date":
				java.sql.Date date = toSqlDate(value);
				result = estimateRepository.countByDate(date);
				break;
		case "empName":
				result = estimateRepository.countByEmpEmpNameContaining(value);
				break;
		case "product":
				result = estimateRepository.countByProductListProductProductName(value);
				break;
		case "progress":
			result = estimateRepository.countByProgress(value);
			break;

		}
		
		return result;
	}

	private java.sql.Date toSqlDate(String dateString) {
        java.sql.Date endDate = null;

        if(dateString != null && !"".equals(dateString)) {
            endDate = java.sql.Date.valueOf(dateString);
        }

        return endDate;
    }	
	
    public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }

    /* 옵션을 위해! */
	public List<EstimateDTO> findEesimateList() {

		List<Estimate> estimateList = estimateRepository.findAll();
		
		return estimateList.stream().map(esti -> modelMapper.map(esti, EstimateDTO.class)).collect(Collectors.toList());
	}

	public EstimateDTO findEstiDetail(int code) {
		Estimate esti = estimateRepository.findById(code).get();
		return modelMapper.map(esti, EstimateDTO.class);
	}

	public List<ProductAndStoAndProCtgDTO> selectOptionProductSearch() {
		List<ProductAndStoAndProCtg> searchList = productJoinRepository.findByProductStatus("Y");
		
		return searchList.stream().map(stockHistoryAndProductAndStorage -> modelMapper.map(stockHistoryAndProductAndStorage, ProductAndStoAndProCtgDTO.class)).collect(Collectors.toList());
	}

	public List<ProductAndStoAndProCtgDTO> selectStockList(String storage, String product) {
List<ProductAndStoAndProCtg> searchList = new ArrayList<ProductAndStoAndProCtg>();
		
		if(isEmptyString(storage) && isEmptyString(product)) {
			/* 검색 하지 않았을때 */
			searchList = productJoinRepository.findByProductStatus("Y");
		}else {
			/* 검색 했을때 */
			if(isEmptyString(storage)) {
				searchList = productJoinRepository.findByProductNameAndProductStatus(product, "Y");
			}else if (isEmptyString(product)) {
				searchList = productJoinRepository.findByStorageStorageNameAndProductStatus(storage,"Y");
			}else {
				searchList = productJoinRepository.findByStorageStorageNameAndProductNameAndProductStatus(storage, product, "Y");
			}
		}
		
		return searchList.stream().map(productAndStoAndProCtg -> modelMapper.map(productAndStoAndProCtg, ProductAndStoAndProCtgDTO.class)).collect(Collectors.toList());
	}

	public Map<String, Object> modifyOldProduct(int no, String newProductName) {

		Product product = productRepository.findByProductNameAndProductStatus(newProductName , "Y");
		
		System.out.println(product);
		System.out.println(product.getProductCode());
		System.out.println(product.getProductRelease());
		System.out.println(product.getProductRelease());
		
		Map<String, Object> result = new HashMap<>();
		result.put("productCode", product.getProductCode());
		result.put("productW", product.getProductWaearing());
		result.put("productR", product.getProductRelease());
		
		return result;
	}

	@Transactional
	public void findProductAndRegistEstiProduct(String productCode, int eNum) {
		
		/* 복합키에 담을 엔티티 */
		Product product = productRepository.findById(productCode).get();
		int productCount = 0;
		int productSumPrice = 0;
		

		QuotationProductDTO quoProduct = new QuotationProductDTO();
		quoProduct.setEstimateCode(eNum);
		quoProduct.setProduct(product);
		quoProduct.setProductCount(productCount);
		quoProduct.setSumPrice(productSumPrice);
		quoProduct.setDiscount(0);
		
		quotationProductRepository.save(modelMapper.map(quoProduct, QuotationProduct.class));
	}

	public void deleteEstiProduct(int estiCode, String oldProductCode) {

		QuotationProductPK quoProductID = new QuotationProductPK();
		quoProductID.setEstimateCode(estiCode);
		quoProductID.setProduct(oldProductCode);
		
		quotationProductRepository.deleteById(quoProductID);
		
	}

	@Transactional
	public void addEstiProduct(int estiCode, String productCode, String count, String discount) {
		Product product = productRepository.findById(productCode).get();
		int productRMoney = product.getProductRelease();
		int productCount = Integer.valueOf(count); 
		int productdisCount = Integer.valueOf(discount); 
		int productSumPrice = productCount * productRMoney;
		
		
		QuotationProductDTO quoProduct = new QuotationProductDTO();
		quoProduct.setEstimateCode(estiCode);
		quoProduct.setProduct(product);
		quoProduct.setProductCount(productCount);
		quoProduct.setSumPrice(productSumPrice);
		quoProduct.setDiscount(productdisCount);
		
		quotationProductRepository.save(modelMapper.map(quoProduct, QuotationProduct.class));
	}

	public void updateOnlyDate(Date date, Date expiryDate, int estiCode) {

		Estimate estimate = estimateRepository.findById(estiCode).get();
		estimate.setDate(date);
		estimate.setExpiryDate(expiryDate);
		System.out.println(estimate);
		estimateRepository.updateDate(date, expiryDate, estiCode);
	}

	@Transactional
	public void modifyMaintain(int no) {
		Estimate foundEsti = estimateRepository.findById(no).get();
		if(foundEsti.getProgress().equals("처리중")) {
			foundEsti.setProgress("완료");
		}else {
			foundEsti.setProgress("처리중");
		}
		System.out.println(foundEsti);
	}

	public void deleteEsti(int code) {

		estimateRepository.deleteById(code);
	}

	public List<EmployeeDTO> findEmpList() {
		List<Employee> emplist = employeeRepository.findAll();
		
		return emplist.stream().map(emp -> modelMapper.map(emp, EmployeeDTO.class)).collect(Collectors.toList());
	}

	public List<StorageDTO> findAllStorage() {
		List<Storage> storageList = storageRepository.findAll();
		
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findAllProduct() {
		List<Product> productList = productRepository.findAllByProductStatus("Y");
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findProductByStorageCode(int storage) {
		List<Product> productList = productRepository.findAllByProductStatusAndStorageNo("Y" , storage);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	public List<ProductDTO> findProductByProductCode(String productCode) {
		List<Product> productList = productRepository.findByProductCode(productCode);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	@Transactional
	public void updateSumMoeny(int code, int discountMoney) {
		
		estimateRepository.updateSumMoeny(code,discountMoney);
		
	}

	/* 인서트 작업 하기 */
	@Transactional
	public void registEstimate(Date sqlDate, Date sqlExpiryDate, String empCode, List<String> codeArr,
			List<String> countArr, List<String> productReleaseArr, List<String> discountArr, String client) {
		int code = Integer.valueOf(empCode);
		
		Employee emp = employeeRepository.findById(code).get();
		
		/* esimate 추가할거 추가하고 O */
		/* product 추가할거 추가하고 */
		/* esimate 합계 마지막으로 추가 다 하고 */
		/* 레포지토리에 세이브 */
		Estimate estimate = new Estimate();
		estimate.setDate(sqlDate);
		estimate.setExpiryDate(sqlExpiryDate);
		estimate.setClient(client);
		estimate.setProgress("처리중");
		estimate.setStatus("N");
		estimate.setEmp(emp);
		
		int count = 0;
		int discount = 0;
		int sumMoney = 0;
		int release = 0;
		int sumMoneyInEsti =0;
		for(int i = 0; i < codeArr.size(); i++) {
			discount = Integer.valueOf(discountArr.get(i));
			release = Integer.valueOf(productReleaseArr.get(i));
			sumMoney = (int) (((100 - discount)*0.01) * release);
			sumMoneyInEsti += sumMoney;
			estimate.setSumMoney(sumMoneyInEsti);
		}
		
		estimateRepository.save(estimate);
		for(int i = 0; i < codeArr.size(); i++) {
			String productCode = codeArr.get(i);
			discount = Integer.valueOf(discountArr.get(i));
			count = Integer.valueOf(countArr.get(i));
			release = Integer.valueOf(productReleaseArr.get(i));
			sumMoney = (int) (((100 - discount)*0.01) * release);
			sumMoneyInEsti += sumMoney;

			int regist = quotationProductRepository.insertQuery(productCode, count, sumMoney, discount);
		
		}
			

		
		
	}
	


		
}



















