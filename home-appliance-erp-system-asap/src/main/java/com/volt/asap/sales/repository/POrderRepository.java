package com.volt.asap.sales.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.volt.asap.sales.entity.POrder;

public interface POrderRepository extends JpaRepository<POrder, Integer> {

	int countByDateAndEmpEmpNameAndProductListProductProductNameAndProgressContaining(Date date, String name, String product,
			String progress);

	int countByDate(Date date);

	int countByEmpEmpNameContaining(String name);

	int countByProductListProductProductName(String value);

	int countByProgress(String value);

	int countByDateAndEmpEmpNameContaining(Date date, String value2);

	int countByDateAndProductListProductProductName(Date date, String value2);

	int countByDateAndProgress(Date date, String value2);

	int countByProductListProductProductNameAndEmpEmpNameContaining(String value1, String value2);

	int countByProductListProductProductNameAndProgress(String value1, String value2);

	int countByEmpEmpNameContainingAndProgress(String value1, String value2);

	int countByProductListProductProductNameAndEmpEmpNameContainingAndProgress(String value1, String value2,
			String value3);

	int countByDateAndProductListProductProductNameAndEmpEmpNameContaining(Date date, String value2, String value3);

	int countByDateAndProductListProductProductNameAndProgress(Date date, String value2, String value3);

	int countByDateAndEmpEmpNameContainingAndProgress(Date date, String value2, String value3);

	List<POrder> findByDateAndEmpEmpNameContainingAndProductListProductProductNameAndProgress(Date date, String name,
			String product, String progress, Pageable paging);

	List<POrder> findByDate(Date date, Pageable paging);

	List<POrder> findByEmpEmpNameContaining(String value, Pageable paging);

	List<POrder> findByProductListProductProductName(String value, Pageable paging);

	List<POrder> findByProgress(String value, Pageable paging);

	List<POrder> findByDateAndEmpEmpNameContaining(Date date, String value2, Pageable paging);

	List<POrder> findByDateAndProductListProductProductName(Date date, String value2, Pageable paging);

	List<POrder> findByDateAndProgress(Date date, String value2, Pageable paging);

	List<POrder> findByProductListProductProductNameAndEmpEmpNameContaining(String value1, String value2,
			Pageable paging);

	List<POrder> findByProductListProductProductNameAndProgress(String value1, String value2, Pageable paging);

	List<POrder> findByEmpEmpNameContainingAndProgress(String value1, String value2, Pageable paging);

	List<POrder> findByDateAndProductListProductProductNameAndEmpEmpNameContaining(Date date, String value2,
			String value3, Pageable paging);

	List<POrder> findByDateAndProductListProductProductNameAndProgress(Date date, String value2, String value3,
			Pageable paging);

	List<POrder> findByDateAndEmpEmpNameContainingAndProgress(Date date, String value2, String value3, Pageable paging);

	List<POrder> findByProductListProductProductNameAndEmpEmpNameContainingAndProgress(String value1, String value2,
			String value3, Pageable paging);

	@Modifying
	@Query("UPDATE PO a set a.date = :date , a.dueDate = :dueDate where a.code = :poCode")
	void updateDate(@Param("date") Date date,@Param("dueDate") Date dueDate,@Param("poCode") int poCode);



}
