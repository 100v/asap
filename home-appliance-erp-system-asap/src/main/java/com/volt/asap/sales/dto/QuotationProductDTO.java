package com.volt.asap.sales.dto;

import com.volt.asap.logistic.entity.Product;

public class QuotationProductDTO {

	private int estimateCode;
	private Product product;
	private int discount;
	private int productCount;
	private int sumPrice;
	public QuotationProductDTO() {
	}
	public QuotationProductDTO(int estimateCode, Product product, int discount, int productCount, int sumPrice) {
		this.estimateCode = estimateCode;
		this.product = product;
		this.discount = discount;
		this.productCount = productCount;
		this.sumPrice = sumPrice;
	}
	public int getEstimateCode() {
		return estimateCode;
	}
	public void setEstimateCode(int estimateCode) {
		this.estimateCode = estimateCode;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getProductCount() {
		return productCount;
	}
	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}
	public int getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(int sumPrice) {
		this.sumPrice = sumPrice;
	}
	@Override
	public String toString() {
		return "QuotationProductDTO [estimateCode=" + estimateCode + ", product=" + product + ", discount=" + discount
				+ ", productCount=" + productCount + ", sumPrice=" + sumPrice + "]";
	}
	
	

	
	
	
}
