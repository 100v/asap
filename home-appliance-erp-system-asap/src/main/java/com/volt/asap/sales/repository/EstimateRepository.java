package com.volt.asap.sales.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.volt.asap.sales.entity.Estimate;

public interface EstimateRepository extends JpaRepository<Estimate, Integer> {

	int countByDateAndEmpEmpNameAndProductListProductProductNameAndProgressContaining(Date date, String name,
			String product, String progress);

	int countByDate(Date date);

	int countByEmpEmpNameContaining(String value);

	int countByProductListProductProductName(String value);

	int countByProgress(String value);

	int countByDateAndEmpEmpNameContaining(Date date, String value2);

	int countByDateAndProductListProductProductName(Date date, String value2);

	int countByDateAndProgress(Date date, String value2);

	int countByProductListProductProductNameAndEmpEmpNameContaining(String value1, String value2);

	int countByProductListProductProductNameAndProgress(String value1, String value2);

	int countByEmpEmpNameContainingAndProgress(String value1, String value2);

	int countByDateAndProductListProductProductNameAndEmpEmpNameContaining(Date date, String value2, String value3);

	int countByDateAndProductListProductProductNameAndProgress(Date date, String value2, String value3);

	int countByDateAndEmpEmpNameContainingAndProgress(Date date, String value2, String value3);

	int countByProductListProductProductNameAndEmpEmpNameContainingAndProgress(String value1, String value2,
			String value3);

	List<Estimate> findByDateAndEmpEmpNameContainingAndProductListProductProductNameAndProgress(Date date, String name,
			String product, String progress, Pageable paging);

	List<Estimate> findByDate(Date date, Pageable paging);

	List<Estimate> findByEmpEmpNameContaining(String value, Pageable paging);

	List<Estimate> findByProductListProductProductName(String value, Pageable paging);

	List<Estimate> findByProgress(String value, Pageable paging);

	List<Estimate> findByDateAndEmpEmpNameContaining(Date date, String value2, Pageable paging);

	List<Estimate> findByDateAndProductListProductProductName(Date date, String value2, Pageable paging);

	List<Estimate> findByDateAndProgress(Date date, String value2, Pageable paging);

	List<Estimate> findByProductListProductProductNameAndEmpEmpNameContaining(String value1, String value2,
			Pageable paging);

	List<Estimate> findByProductListProductProductNameAndProgress(String value1, String value2, Pageable paging);

	List<Estimate> findByEmpEmpNameContainingAndProgress(String value1, String value2, Pageable paging);

	List<Estimate> findByDateAndProductListProductProductNameAndEmpEmpNameContaining(Date date, String value2,
			String value3, Pageable paging);

	List<Estimate> findByDateAndProductListProductProductNameAndProgress(Date date, String value2, String value3,
			Pageable paging);

	List<Estimate> findByDateAndEmpEmpNameContainingAndProgress(Date date, String value2, String value3,
			Pageable paging);

	List<Estimate> findByProductListProductProductNameAndEmpEmpNameContainingAndProgress(String value1, String value2,
			String value3, Pageable paging);

	@Modifying
	@Query("UPDATE estimate a set a.date = :date , a.expiryDate = :expiryDate where a.code = :estiCode")
	void updateDate(Date date, Date expiryDate, int estiCode);

	@Modifying
	@Query("UPDATE estimate a set a.sumMoney = :discountMoney where a.code = :code")
	void updateSumMoeny(int code, int discountMoney);

}
