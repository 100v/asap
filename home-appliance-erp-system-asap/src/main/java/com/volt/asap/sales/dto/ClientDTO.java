package com.volt.asap.sales.dto;

public class ClientDTO {

	private int no;
	private String name;
	private String representativeName;
	private String phone;
	private String telephone;
	private String email;
	private String address;
	private String maintain;
	
	public ClientDTO() {
	}
	public ClientDTO(int no, String name, String representativeName, String phone, String telephone, String email,
			String address, String maintain) {
		this.no = no;
		this.name = name;
		this.representativeName = representativeName;
		this.phone = phone;
		this.telephone = telephone;
		this.email = email;
		this.address = address;
		this.maintain = maintain;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRepresentativeName() {
		return representativeName;
	}
	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMaintain() {
		return maintain;
	}
	public void setMaintain(String maintain) {
		this.maintain = maintain;
	}
	@Override
	public String toString() {
		return "ClientDTO [no=" + no + ", name=" + name + ", representativeName=" + representativeName + ", phone="
				+ phone + ", telephone=" + telephone + ", email=" + email + ", address=" + address + ", maintain="
				+ maintain + "]";
	}
	
	
}
