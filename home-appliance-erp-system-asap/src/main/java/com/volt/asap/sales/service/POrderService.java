package com.volt.asap.sales.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.repository.EmployeeRepository;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StockHistoryDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.ProductAndStoAndProCtg;
import com.volt.asap.logistic.entity.StockHistoryAndProductAndStorage;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.ProductJoinRepository;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.StockHistoryAndProductAndStorageRepository;
import com.volt.asap.logistic.repository.StorageRepository;
import com.volt.asap.sales.dto.POrderDTO;
import com.volt.asap.sales.dto.POrderProductDTO;
import com.volt.asap.sales.dto.POrderProductPK;
import com.volt.asap.sales.dto.ProductAndStoAndProCtgDTO;
import com.volt.asap.sales.entity.POrder;
import com.volt.asap.sales.entity.POrderProduct;
import com.volt.asap.sales.repository.POrderProductRepository;
import com.volt.asap.sales.repository.POrderRepository;

@Service
public class POrderService {

	private final POrderRepository pOrderRepository;
	private final POrderProductRepository pOrderProductRepository;
	private final StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository;
	private final ProductRepository productRepository;
	private final EmployeeRepository employeeRepository;
	private final StorageRepository storageRepository;
	private final ProductJoinRepository productJoinRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public POrderService(ModelMapper modelMapper ,POrderRepository pOrderRepository, POrderProductRepository pOrderProductRepository,
			StockHistoryAndProductAndStorageRepository stockHistoryAndProductAndStorageRepository, ProductRepository productRepository,EmployeeRepository employeeRepository,
			StorageRepository storageRepository,ProductJoinRepository productJoinRepository) {
		this.pOrderRepository = pOrderRepository;
		this.pOrderProductRepository = pOrderProductRepository;
		this.modelMapper = modelMapper;
		this.stockHistoryAndProductAndStorageRepository = stockHistoryAndProductAndStorageRepository;
		this.productRepository = productRepository;
		this.employeeRepository = employeeRepository;
		this.storageRepository = storageRepository;
		this.productJoinRepository = productJoinRepository;
	}

	/* totalCount 구하기 */
	public int countTotalPOrder(Map<String, String> searchValue) {
		/*검색어 형변환*/
		java.sql.Date date = toSqlDate(searchValue.get("date"));
		String name = searchValue.get("empName");
		String product = searchValue.get("product");
		String progress = searchValue.get("progress");

		/* 리턴값으로 보낼 totalCount */
		int totalCount = 0;
		
		/* 값이 있는 조건어만 담을 것*/
		List<String> haveValCondition = new ArrayList<>();
		
		if(date != null && !isEmptyString(name) && !isEmptyString(product) && !isEmptyString(progress)) {
			/* 4가지 다 입력한경우 */
			totalCount = pOrderRepository.countByDateAndEmpEmpNameAndProductListProductProductNameAndProgressContaining(date, name, product, progress);
		}else {
			/*현재 키값을 가져온다.*/
			Set<String> condition = searchValue.keySet();
			
			/*값이 들어있는 키값을 모아준다. 메소드에 태워 보낼거니까 */
			for(String key : condition) {
				String val = searchValue.get(key);
				if( val != null && !"".equals(val)) {
					haveValCondition.add(key);
				}
			}
			/*검색어 개수에 따라서! 메소드 발동! */
			switch(haveValCondition.size()){
				case 1:
					totalCount = oneSearchCondition(haveValCondition, searchValue);
					break;
				case 2:
					totalCount = twoSearchCondition(haveValCondition, searchValue);
					break;
				case 3:
					totalCount = threeSearchCondition(haveValCondition, searchValue);
					break;
			}
		}
		
		return totalCount;
	}



	/* 검색어가 하나일때 totalCount */
	private int oneSearchCondition(List<String> haveValCondition, Map<String, String> searchValue) {

		String condition = haveValCondition.get(0);
		int result = 0;
		
		/*검색 결과 담아둘 string*/
		String value = searchValue.get(condition);
		switch (condition) {
		case "date":
				java.sql.Date date = toSqlDate(value);
				result = pOrderRepository.countByDate(date);
				break;
		case "empName":
				result = pOrderRepository.countByEmpEmpNameContaining(value);
				break;
		case "product":
				result = pOrderRepository.countByProductListProductProductName(value);
				break;
		case "progress":
			result = pOrderRepository.countByProgress(value);
			break;

		}
		
		return result;
	}

	/* 검색어가 두개 일 때 totalCount */
	private int twoSearchCondition(List<String> haveValCondition, Map<String, String> searchValue) {
		/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		int result = 0;
		
		/*검색 결과 담아둘 string*/
		String value1 = searchValue.get(condition1);
		String value2 = searchValue.get(condition2);
		
		if(condition1 == "date") {
			/*date 타입 포함 검색시 해야할 형변환*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 ==  "empName") {
				return result = pOrderRepository.countByDateAndEmpEmpNameContaining(date, value2);
			}else if(condition2 ==  "product") {
				return result = pOrderRepository.countByDateAndProductListProductProductName(date, value2);
			}else {
				return result = pOrderRepository.countByDateAndProgress(date, value2);
			}
			
		}else if(condition1 == "product") {
			/* product 타입 포함 검색 */
			if(condition2 == "empName") {
				return result = pOrderRepository.countByProductListProductProductNameAndEmpEmpNameContaining(value1, value2);
			}
				return result = pOrderRepository.countByProductListProductProductNameAndProgress(value1, value2);
		}else {
			/*empName 타입 포함 검색 */
			return result = pOrderRepository.countByEmpEmpNameContainingAndProgress(value1,value2);
		}
		
	}

	/* 검색어가 세개일때 totalCount*/
	private int threeSearchCondition(List<String> haveValCondition, Map<String, String> searchValue) {
		/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		String condition3 = haveValCondition.get(2);
		/*검색 결과 담아둘 string*/
		String value1 = searchValue.get(condition1);
		String value2 = searchValue.get(condition2);
		String value3 = searchValue.get(condition3);
		/*최종 결과값*/
		int result = 0;
		
		if(condition1 == "date") {
			/*date 가 포함된 검색어*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 == "product") {
				/*product도 포함되었을때*/
				if(condition3 == "empName") {
					/*마지막 검색어는 empName*/
					return result = pOrderRepository.countByDateAndProductListProductProductNameAndEmpEmpNameContaining(date, value2, value3);
				}else {
					/*마지막 검색어는 progress*/
					return result = pOrderRepository.countByDateAndProductListProductProductNameAndProgress(date,value2,value3);
				}
			}else {
				/* product가 포함 되지 않았을때 */
				return result = pOrderRepository.countByDateAndEmpEmpNameContainingAndProgress(date,value2,value3);
			}
			
		}else{
			return result = pOrderRepository.countByProductListProductProductNameAndEmpEmpNameContainingAndProgress(value1,value2,value3);
		}
			
	}
	/* 첫 페이지 및 검색하기 누르지 않았을때 totalCount */
	public int countTotalPOrder() {
		return (int)pOrderRepository.count();
	}
	
	/*string -> sql date 형변환*/
    private java.sql.Date toSqlDate(String dateString) {
        java.sql.Date endDate = null;

        if(dateString != null && !"".equals(dateString)) {
            endDate = java.sql.Date.valueOf(dateString);
        }

        return endDate;
    }
    
	public boolean isEmptyString(String str) {
		/* 파라미터로 받은 값이 null이거나 ""이다.*/
        return str == null || "".equals(str);
    }

	/* 조회 하기 */
	public List<POrderDTO> findPOrder(CustomSelectCriteria selectCriteria) {

	    int index = selectCriteria.getPageNo() - 1;         // Pageble객체를 사용시 페이지는 0부터 시작(1페이지가 0)
        int count = selectCriteria.getLimit();
		
		Pageable paging = PageRequest.of(index, count, Sort.by("code").descending());
		
		List<POrder> poList = null;
		if(selectCriteria.getSearchValueMap() == null) {
			/*전체 조회일때*/
			poList = pOrderRepository.findAll(paging).toList();
		}else {
			/* 검색어가 있을때 */
			java.sql.Date date = toSqlDate(selectCriteria.getSearchValueMap().get("date"));
			String name = selectCriteria.getSearchValueMap().get("empName");
			String product = selectCriteria.getSearchValueMap().get("product");
			String progress = selectCriteria.getSearchValueMap().get("progress");
			
			if(date != null && !isEmptyString(name) && !isEmptyString(product) && !isEmptyString(progress)) {
				/* 모두 입력했을 때 */
				poList = pOrderRepository.findByDateAndEmpEmpNameContainingAndProductListProductProductNameAndProgress(date, name, product, progress, paging);
			}else {
				
				/*값이 들어있는 키값을 모아준다. 메소드에 태워 보낼거니까 */
				Set<String> condition = selectCriteria.getSearchValueMap().keySet();
				List<String> haveValCondition = new ArrayList<>();
				
				for(String key : condition) {
					String val = selectCriteria.getSearchValueMap().get(key);
					if( val != null && !"".equals(val)) {
						haveValCondition.add(key);
					}
				}
				
				System.out.println(haveValCondition);
				System.out.println(haveValCondition.size());
				
				switch(haveValCondition.size()){
				case 1:
					poList = findOneSearchCondition(haveValCondition, selectCriteria, paging);
					return poList.stream().map(po -> modelMapper.map(po, POrderDTO.class)).collect(Collectors.toList());
				case 2:
					poList = findTwoSearchCondition(haveValCondition, selectCriteria, paging);
					return poList.stream().map(po -> modelMapper.map(po, POrderDTO.class)).collect(Collectors.toList());
				case 3:
					poList = findThreeSearchCondition(haveValCondition, selectCriteria, paging);
					return poList.stream().map(po -> modelMapper.map(po, POrderDTO.class)).collect(Collectors.toList());
			}
				
				
			}
		}
		
		return poList.stream().map(po -> modelMapper.map(po, POrderDTO.class)).collect(Collectors.toList());
	}

	/*검색어 셋일때 리스트 뽑기*/
	private List<POrder> findThreeSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria, Pageable paging) {

		System.out.println("검색어가 세개~");
		/*검색 조건 담아둘 String*/
		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		String condition3 = haveValCondition.get(2);
		/*검색 결과 담아둘 string*/
		String value1 = selectCriteria.getSearchValueMap().get(condition1);
		String value2 = selectCriteria.getSearchValueMap().get(condition2);
		String value3 = selectCriteria.getSearchValueMap().get(condition3);
		
		List<POrder> result = null;
		
		if(condition1 == "date") {
			/*date 가 포함된 검색어*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 == "product") {
				/*product도 포함되었을때*/
				if(condition3 == "empName") {
					/*마지막 검색어는 empName*/
					result = pOrderRepository.findByDateAndProductListProductProductNameAndEmpEmpNameContaining(date, value2, value3, paging);
				}else {
					/*마지막 검색어는 progress*/
					result = pOrderRepository.findByDateAndProductListProductProductNameAndProgress(date,value2,value3, paging);
				}
			}else {
				/* 두번째 검색어에 product가 포함 되지 않았을때 */
				result = pOrderRepository.findByDateAndEmpEmpNameContainingAndProgress(date,value2,value3, paging);
			}
			
		}else{
			/* date가 포함되지 않은 검색어 */
			result = pOrderRepository.findByProductListProductProductNameAndEmpEmpNameContainingAndProgress(value1,value2,value3, paging);
		}
		
		return result;
	}

	/*검색어 둘일때 리스트 뽑기*/
	private List<POrder> findTwoSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria, Pageable paging) {

		String condition1 = haveValCondition.get(0);
		String condition2 = haveValCondition.get(1);
		
		String value1 = selectCriteria.getSearchValueMap().get(condition1);
		String value2 = selectCriteria.getSearchValueMap().get(condition2);
		
		List<POrder> result;
		
		if(condition1 == "date") {
			/*date 타입 포함 검색시 해야할 형변환*/
			java.sql.Date date = toSqlDate(value1);
			if(condition2 ==  "empName") {
				result = pOrderRepository.findByDateAndEmpEmpNameContaining(date, value2, paging);
			}else if(condition2 ==  "product") {
				result = pOrderRepository.findByDateAndProductListProductProductName(date, value2, paging);
			}else {
				result = pOrderRepository.findByDateAndProgress(date, value2, paging);
			}
			
		}else if(condition1 == "product") {
			/* product 타입 포함 검색 */
			if(condition2 == "empName") {
				result = pOrderRepository.findByProductListProductProductNameAndEmpEmpNameContaining(value1, value2, paging);
			}else {
				result = pOrderRepository.findByProductListProductProductNameAndProgress(value1, value2, paging);
			}
		}else {
			/*empName 타입 포함 검색 */
			result = pOrderRepository.findByEmpEmpNameContainingAndProgress(value1,value2, paging);
		}
		
		System.out.println(result.isEmpty());
		return result;
	}

	/* 검색어 하나일때 리스트 뽑기 */
	private List<POrder> findOneSearchCondition(List<String> haveValCondition, CustomSelectCriteria selectCriteria, Pageable paging) {

		String condition = haveValCondition.get(0);
		
		/*검색 결과 담아둘 string*/
		String value = selectCriteria.getSearchValueMap().get(condition);
		List<POrder> result = null;
		switch (condition) {
		case "date":
				java.sql.Date date = toSqlDate(value);
				System.out.println(value.getClass().getName());
				System.out.println(date.getClass().getName());
				result = pOrderRepository.findByDate(date, paging);
				break;
		case "empName":
				System.out.println(value);
				result = pOrderRepository.findByEmpEmpNameContaining(value, paging);
				break;
		case "product":
				System.out.println(value);
				result = pOrderRepository.findByProductListProductProductName(value, paging);
				break;
		case "progress":
				System.out.println(value);
				result = pOrderRepository.findByProgress(value, paging);
				break;
		}
		
		return result;
	}

	/* 옵션 품목 추가 하기 */
	public List<POrderProduct> findProduct() {
		
		List<POrderProduct> productList = pOrderProductRepository.findPOProductName();
		
		return productList.stream().map(po -> modelMapper.map(po, POrderProduct.class)).collect(Collectors.toList());
	}
    
	
	public POrderDTO findPoDetail(int code) {
		
		POrder po = pOrderRepository.findById(code).get();
		return modelMapper.map(po, POrderDTO.class);
	}

	public List<ProductAndStoAndProCtgDTO> selectStockList(String storage, String product) {

		List<ProductAndStoAndProCtg> searchList = new ArrayList<ProductAndStoAndProCtg>();
		
		if(isEmptyString(storage) && isEmptyString(product)) {
			/* 검색 하지 않았을때 */
			searchList = productJoinRepository.findByProductStatus("Y");
		}else {
			/* 검색 했을때 */
			if(isEmptyString(storage)) {
				searchList = productJoinRepository.findByProductNameAndProductStatus(product, "Y");
			}else if (isEmptyString(product)) {
				searchList = productJoinRepository.findByStorageStorageNameAndProductStatus(storage,"Y");
			}else {
				searchList = productJoinRepository.findByStorageStorageNameAndProductNameAndProductStatus(storage, product, "Y");
			}
		}
		
		return searchList.stream().map(productAndStoAndProCtg -> modelMapper.map(productAndStoAndProCtg, ProductAndStoAndProCtgDTO.class)).collect(Collectors.toList());
	}

	/* 검색 옵션에 넣을 값 ( 상품 검색 ) */
	public List<ProductAndStoAndProCtgDTO> selectOptionProductSearch() {
			
		List<ProductAndStoAndProCtg> searchList = productJoinRepository.findByProductStatus("Y");
		
		return searchList.stream().map(stockHistoryAndProductAndStorage -> modelMapper.map(stockHistoryAndProductAndStorage, ProductAndStoAndProCtgDTO.class)).collect(Collectors.toList());
	}

	@Transactional
	public void findProductAndRegistPoProduct(String productCode, int poNum) {

		/* 복합키 엔티티에 담을 product 엔티티를 구해 온다. */
		Product product = productRepository.findById(productCode).get();
		int productCount = 0;
		int productSumPrice = 0;
		
		/* 복함키를 지닌 엔티티에 담기 위해 dto가 필요하다*/
		POrderProductDTO poProduct = new POrderProductDTO();
		poProduct.setpOrderCode(poNum);
		poProduct.setProduct(product);
		poProduct.setProductCount(productCount);
		poProduct.setProductSumPrice(productSumPrice);
		
		pOrderProductRepository.save(modelMapper.map(poProduct, POrderProduct.class));
	}

	public Map<String, Object> modifyOldProduct(int no, String newProductName) {

		Product product = productRepository.findByProductNameAndProductStatus(newProductName , "Y");
		
		System.out.println(product);
		System.out.println(product.getProductCode());
		System.out.println(product.getProductRelease());
		System.out.println(product.getProductRelease());
		
		Map<String, Object> result = new HashMap<>();
		result.put("productCode", product.getProductCode());
		result.put("productW", product.getProductWaearing());
		result.put("productR", product.getProductRelease());
		
		return result;
	}

	public void deletePoProduct(int poCode, String productCodeArr) {
		POrderProductPK poProductId = new POrderProductPK();
		poProductId.setpOrderCode(poCode);
		poProductId.setProduct(productCodeArr);
		
		pOrderProductRepository.deleteById(poProductId);
		
	}

	/* 발주 수정 시 그거 새로 넣는것임. */
	@Transactional
	public void addPoProduct(int poCode, String productCode, String count) {
		Product product = productRepository.findById(productCode).get();
		int productRMoney = product.getProductRelease();
		int productCount = Integer.valueOf(count); 
		int productSumPrice = productCount * productRMoney;
		
		
		POrderProductDTO poProduct = new POrderProductDTO();
		poProduct.setpOrderCode(poCode);
		poProduct.setProduct(product);
		poProduct.setProductCount(productCount);
		poProduct.setProductSumPrice(productSumPrice);
		
		pOrderProductRepository.save(modelMapper.map(poProduct, POrderProduct.class));
	}

	public void updateOnlyDate(Date date, Date dueDate, int poCode) {
		
		POrder po = pOrderRepository.findById(poCode).get();
		po.setDate(date);
		po.setDueDate(dueDate);
		System.out.println(po);
		pOrderRepository.updateDate(date, dueDate, poCode);
	}

	@Transactional
	public void modifyMaintain(int no) {

		POrder foundPo = pOrderRepository.findById(no).get();
		if(foundPo.getProgress().equals("배송중")) {
			foundPo.setProgress("완료");
		}else {
			foundPo.setProgress("배송중");
		}
		System.out.println(foundPo);
	}

	/* 사원 이름 찾기 */
	public List<EmployeeDTO> findEmpList() {

		List<Employee> emplist = employeeRepository.findAll();
		
		return emplist.stream().map(emp -> modelMapper.map(emp, EmployeeDTO.class)).collect(Collectors.toList());
	}

	/*전체 창고 명 조회*/
	public List<StorageDTO> findAllStorage() {

		List<Storage> storageList = storageRepository.findAll();
		
		return storageList.stream().map(storage -> modelMapper.map(storage, StorageDTO.class)).collect(Collectors.toList());
	}

	/* 모든 상품 조회 */
	public List<ProductDTO> findAllProduct() {
			List<Product> productList = productRepository.findAllByProductStatus("Y");
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	/* 부분 상품 조회 */
	public List<ProductDTO> findProductByStorageCode(int storage) {
		
		List<Product> productList = productRepository.findAllByProductStatusAndStorageNo("Y" , storage);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	/* 해당 상품 조회 */
	public List<ProductDTO> findProductByProductCode(String productCode) {

		List<Product> productList = productRepository.findByProductCode(productCode);
		
		return productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
	}

	/* 인서트 작업 하기 */
	@Transactional
	public void registPOrder(Date sqlDate, Date sqlDueDate, String empCode, List<String> codeArr,
			List<String> countArr, List<String> productWaearingArr) {

		int code = Integer.valueOf(empCode);
		
		Employee emp = employeeRepository.findById(code).get();
		
		int sumMoney = 0;
		int count = 0;
		int productWearing = 0;
		for(int i = 0; i < codeArr.size(); i++) {

			count = Integer.valueOf(countArr.get(i));
			productWearing = Integer.valueOf(productWaearingArr.get(i));
			sumMoney += count * productWearing;
			
		}
		
		System.out.println(sumMoney);

		POrder pOrder = new POrder();
		pOrder.setDate(sqlDate);
		pOrder.setDueDate(sqlDueDate);
		pOrder.setEmp(emp);
		pOrder.setProgress("배송중");
		pOrder.setSumMoney(sumMoney);
		
		pOrderRepository.save(pOrder);
		
		for(int i = 0; i < codeArr.size(); i++) {
			
			count = Integer.valueOf(countArr.get(i));
			productWearing = Integer.valueOf(productWaearingArr.get(i));
			sumMoney = count * productWearing;
			String productCode = codeArr.get(i);
			
			int regist = pOrderProductRepository.insertQuery(productCode, sumMoney, count);
		}
		
		
		
	}

	public void deletePo(int code) {
		System.out.println("발주 삭제 등장");
		pOrderRepository.deleteById(code);
	}



	
	
	
	
}// class end
