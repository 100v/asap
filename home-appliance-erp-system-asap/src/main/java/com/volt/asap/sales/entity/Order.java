package com.volt.asap.sales.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name="Order")
@Table(name="TBL_ORDER_LIST")
@SequenceGenerator(
		name = "ORDER_SEQ_GENERATOR",
		sequenceName = "SEQ_ORDER_NO",
		initialValue = 20,
		allocationSize = 1		
		)
public class Order {
	
	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "ORDER_SEQ_GENERATOR"
	)
	@Column(name = "ORDER_NO")
	private int orderNumber;
	
	@Column(name = "ORDER_DATE")
	private java.sql.Date orderDate;

	@Column(name = "ORDER_PRICE")
	private long orderPrice;

	@Column(name = "ORDER_SHIPPING_NO")
	private long orderShippingNumber;
	
	@Column(name = "PRODUCT_CODE")
	private String productCode;
	
	@Column(name = "ORDER_SHIPPING_STATUS")
	private String orderShippingStatus;

	public Order() {
		super();
	}

	public Order(int orderNumber, java.sql.Date orderDate, long orderPrice, long orderShippingNumber,
			String productCode, String orderShippingStatus) {
		super();
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.orderPrice = orderPrice;
		this.orderShippingNumber = orderShippingNumber;
		this.productCode = productCode;
		this.orderShippingStatus = orderShippingStatus;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public java.sql.Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(java.sql.Date orderDate) {
		this.orderDate = orderDate;
	}

	public long getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(long orderPrice) {
		this.orderPrice = orderPrice;
	}

	public long getOrderShippingNumber() {
		return orderShippingNumber;
	}

	public void setOrderShippingNumber(long orderShippingNumber) {
		this.orderShippingNumber = orderShippingNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getOrderShippingStatus() {
		return orderShippingStatus;
	}

	public void setOrderShippingStatus(String orderShippingStatus) {
		this.orderShippingStatus = orderShippingStatus;
	}

	@Override
	public String toString() {
		return "Order [orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", orderPrice=" + orderPrice
				+ ", orderShippingNumber=" + orderShippingNumber + ", productCode=" + productCode
				+ ", orderShippingStatus=" + orderShippingStatus + "]";
	}
	
	
}
