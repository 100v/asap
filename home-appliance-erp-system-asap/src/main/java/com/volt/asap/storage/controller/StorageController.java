package com.volt.asap.storage.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.receiving.dto.ReceivingHistoryViewDTO;
import com.volt.asap.storage.dto.StorageDTO;
import com.volt.asap.storage.entity.Storage;
import com.volt.asap.storage.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/storage/*")
public class StorageController {

    /* 다음 계층(service) 사용을 위해 생성자로 service bean을 주입받는다. */
    private final StorageService storageService;

    @Autowired
    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("list")
    public ModelAndView searchStorage(ModelAndView mv,
                                      @RequestParam(value="storagename", required = false) String name,
                                      Integer page) {

        System.out.println("창고명 키워드가 잘 들어왔는지? : " + name);

        CustomSelectCriteria selectCriteria;

        /* 페이지 파라미터가 없으면 1페이지로 간주 */
        if (page == null) {
            page = 1;
        }

        /* name 변수(검색어)가 없다면 */
        if(ObjectUtils.isEmpty(name)) {
            int totalcount = storageService.countList(null);
            System.out.println("검색어 없을 때 : " + totalcount);
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalcount, 10, 5);

        /* 검색어가 있다면 */
        } else {
            Map<String, String> parameterMap = new HashMap<>();

            parameterMap.put("name", name);

            int totalcount = storageService.countList(parameterMap);
            System.out.println("검색어 있을 때 : " + totalcount);
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalcount, 10, 5, parameterMap);
        }

        /* 창고 목록을 조회해온다. */
        List<StorageDTO> storageList = storageService.findStorageList(selectCriteria);
        System.out.println("조회된 창고 리스트: " + storageList);

        mv.addObject("selectCriteria", selectCriteria);
        mv.addObject("storageList", storageList);
        mv.setViewName("storage/list");

        return mv;
    }

    @GetMapping("regist")
    public ModelAndView registform(ModelAndView mv) {

        List<EmployeeDTO> employeeList = storageService.findEmployee();

        System.out.println("사원 목록 조회되었니? :" + employeeList);

        mv.addObject("employeeList", employeeList);
        mv.setViewName("storage/regist");

        return mv;
    }

    @ResponseBody
    @PostMapping("phone")
    public String getPhonenumber(@RequestParam(name = "memberName") Integer code) {

        System.out.println(code);

        EmployeeDTO numbers = storageService.findPhoneNumber(code);

        String result = numbers.getEmpPhone();

        System.out.println(result);

        System.out.println(numbers);

        return result;
    }

    @PostMapping("storageregist")
    public ModelAndView storageReagist(ModelAndView mv,
                                        StorageDTO storageDTO,
                                        @RequestParam(name="storagename") String sname,
                                        @RequestParam(name="select1") Integer membercode,
                                        @RequestParam(name="phone") String number) {


        storageDTO.setStorageName(sname);
        storageDTO.setEmpCode(membercode);
        storageDTO.setStorageManagerPhone(number);

        Storage result = storageService.registStorage(storageDTO);

        if(!ObjectUtils.isEmpty(result)) {
            mv.addObject("message", "success");
            mv.addObject("comment", "등록에 성공하였습니다.");
        } else {
            mv.addObject("message", "fail");
            mv.addObject("comment", "등록에 실패하였습니다.");
        }

        mv.setViewName("storage/blank");

        return mv;
    }

    @GetMapping("/{storageNo}")
    public ModelAndView storageDetail(ModelAndView mv,  @PathVariable int storageNo) {

        System.out.println("너는 잘 나오는게 맞지??????" + storageNo);

        StorageDTO storageDetail = storageService.findstorageDetail(storageNo);

        List<EmployeeDTO> employeeList = storageService.findEmployee();

        System.out.println("조회가 되어온 창고정보: " + storageDetail);

        mv.addObject("storageDetail", storageDetail);
        mv.addObject("employeeList", employeeList);
        mv.setViewName("storage/modify");

        return mv;
    }

    @PostMapping("delete")
    public ModelAndView deleteStorage(ModelAndView mv,
                                      @RequestParam(name = "storageNo") Integer number) {

        Storage result = storageService.deleteStorage(number);

        if(!ObjectUtils.isEmpty(result)) {
            mv.addObject("message", "success");
            mv.addObject("comment", "삭제에 성공하였습니다.");
        } else {
            mv.addObject("message", "fail");
            mv.addObject("comment", "삭제에 실패하였습니다.");
        }

        mv.setViewName("storage/blank");

        return mv;
    }

    @PostMapping("modify")
    public ModelAndView modifyStorage(ModelAndView mv,
                                      @RequestParam(name = "storageNo") Integer number,
                                      @RequestParam(value = "select1", defaultValue = "") Integer code,
                                      @RequestParam(value = "phone", defaultValue = "") String phone) {

        Storage result = storageService.modityStorage(number, code, phone);

        if(!ObjectUtils.isEmpty(result)) {
            mv.addObject("message", "success");
            mv.addObject("comment", "수정에 성공하였습니다.");
        } else {
            mv.addObject("message", "fail");
            mv.addObject("comment", "수정에 실패하였습니다.");
        }

        mv.setViewName("storage/blank");

        return mv;
    }
}
