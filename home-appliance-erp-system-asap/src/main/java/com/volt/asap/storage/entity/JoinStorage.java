package com.volt.asap.storage.entity;

import com.volt.asap.hrm.entity.Employee;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity(name = "StorageEntity")
@Table(name = "TBL_STORAGE")
@SequenceGenerator(
        name = "STORAGE_SEQ_STORAGE_NO",
        sequenceName = "SEQ_STORAGE_NO",
        initialValue = 6,
        allocationSize = 1
)
public class JoinStorage {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "STORAGE_SEQ_STORAGE_NO"
    )
    @Column(name = "STORAGE_NO")
    private int storageNo;

    @Column(name = "STORAGE_NAME")
    private String storageName;

    @Column(name = "STORAGE_MANAGER_PHONE")
    private String storageManagerPhone;
    @ManyToOne
    @JoinColumn(name = "EMP_CODE")
    private Employee employee;

    @Column(name = "STORAGE_STATUS")
    private String storageStatus;

    public JoinStorage() {
    }

    public JoinStorage(int storageNo, String storageName, String storageManagerPhone, Employee employee, String storageStatus) {
        this.storageNo = storageNo;
        this.storageName = storageName;
        this.storageManagerPhone = storageManagerPhone;
        this.employee = employee;
        this.storageStatus = storageStatus;
    }

    public int getStorageNo() {
        return storageNo;
    }

    public void setStorageNo(int storageNo) {
        this.storageNo = storageNo;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStorageManagerPhone() {
        return storageManagerPhone;
    }

    public void setStorageManagerPhone(String storageManagerPhone) {
        this.storageManagerPhone = storageManagerPhone;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    @Override
    public String toString() {
        return "JoinStorage{" +
                "storageNo=" + storageNo +
                ", storageName='" + storageName + '\'' +
                ", storageManagerPhone='" + storageManagerPhone + '\'' +
                ", employee=" + employee +
                ", storageStatus='" + storageStatus + '\'' +
                '}';
    }
}
