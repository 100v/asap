package com.volt.asap.storage.repository;

import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeListRepository extends JpaRepository<Employee, Integer> {

    Employee findByEmpCode(Integer code);
}
