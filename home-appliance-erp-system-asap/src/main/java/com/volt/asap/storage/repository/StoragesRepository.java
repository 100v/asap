package com.volt.asap.storage.repository;

import com.volt.asap.storage.entity.JoinStorage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoragesRepository extends JpaRepository<JoinStorage, String> {

    JoinStorage findByStorageNo(int storageNo);

    List<JoinStorage> findAllByStorageStatus(String status, Pageable paging);

    int countByStorageStatus(String status);

    int countByStorageNameContainingAndStorageStatus(String name, String status);

    List<JoinStorage> findByStorageNameContainingAndStorageStatus(String storageName, String status, Pageable paging);
}
