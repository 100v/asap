package com.volt.asap.storage.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.hrm.dto.EmployeeDTO;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.storage.dto.StorageDTO;
import com.volt.asap.storage.entity.JoinStorage;
import com.volt.asap.storage.entity.Storage;
import com.volt.asap.storage.repository.EmployeeListRepository;
import com.volt.asap.storage.repository.StoragesRepository;
import com.volt.asap.storage.repository.StoragessRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StorageService {

    private final StoragesRepository storagesRepository;

    private final StoragessRepository storagessRepository;
    private final EmployeeListRepository employeeListRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public StorageService(StoragesRepository storagesRepository, StoragessRepository storagessRepository, EmployeeListRepository employeeListRepository, ModelMapper modelMapper) {
        this.storagesRepository = storagesRepository;
        this.storagessRepository = storagessRepository;
        this.employeeListRepository = employeeListRepository;
        this.modelMapper = modelMapper;
    }
    public int countList(Map<String, String> parameterMap) {

        String status = "N";

        /* 검색조건이 없을 때 (parameterMap이 비어있을 때) */
        if(parameterMap == null) {
            return storagesRepository.countByStorageStatus(status);
        /* 검색조건에 따른 갯수 조회 */
        } else {
            return storagesRepository.countByStorageNameContainingAndStorageStatus(parameterMap.get("name"),status);
        }
    }

    public List<StorageDTO> findStorageList(CustomSelectCriteria selectCriteria) {

        int index = selectCriteria.getPageNo() -1;
        int count = selectCriteria.getLimit();

        Pageable paging = PageRequest.of(index, count, Sort.by(Sort.Direction.ASC, "storageNo"));
        String status = "N";

        List<JoinStorage> storageList;

        if(selectCriteria.getSearchValueMap() == null) {
            storageList = storagesRepository.findAllByStorageStatus(status, paging);

            System.out.println("검색어 없을때입니다!!!!!!!!!!: " + storageList);
        } else {
            String storageName = selectCriteria.getSearchValueMap().get("name");
            storageList = storagesRepository.findByStorageNameContainingAndStorageStatus(storageName, status, paging);
        }

        System.out.println(storageList);
        return storageList.stream().map(JoinStorage -> modelMapper.map(JoinStorage, StorageDTO.class)).collect(Collectors.toList());
    }

    public List<EmployeeDTO> findEmployee() {

        List<Employee> employeeList = employeeListRepository.findAll();

        return employeeList.stream().map(Employee -> modelMapper.map(Employee, EmployeeDTO.class)).collect(Collectors.toList());
    }

    public EmployeeDTO findPhoneNumber(Integer code) {

        Employee number;

        number = employeeListRepository.findByEmpCode(code);

        return modelMapper.map(number, EmployeeDTO.class);
    }

    @Transactional
    public Storage registStorage(StorageDTO storageDTO) {

        Storage result;

        result = storagessRepository.save(modelMapper.map(storageDTO, Storage.class));

        return result;
    }

    public StorageDTO findstorageDetail(int storageNo) {

        JoinStorage result = storagesRepository.findByStorageNo(storageNo);

        return modelMapper.map(result, StorageDTO.class);
    }

    @Transactional
    public Storage deleteStorage(Integer number) {

        Storage result = storagessRepository.findByStorageNo(number);

        result.setStorageStatus("Y");

        return result;
    }

    @Transactional
    public Storage modityStorage(Integer number, Integer code, String phone) {

        Storage result = storagessRepository.findByStorageNo(number);

        result.setEmpCode(code);
        result.setStorageManagerPhone(phone);

        return result;
    }
}
