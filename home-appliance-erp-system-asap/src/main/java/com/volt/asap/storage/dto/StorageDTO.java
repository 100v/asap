package com.volt.asap.storage.dto;

import com.volt.asap.hrm.dto.EmployeeDTO;

public class StorageDTO {

    private int storageNo;
    private String storageName;
    private String storageManagerPhone;
    private int empCode;

    private EmployeeDTO employee;

    public StorageDTO() {
    }

    public StorageDTO(int storageNo, String storageName, String storageManagerPhone, int empCode, EmployeeDTO employee) {
        this.storageNo = storageNo;
        this.storageName = storageName;
        this.storageManagerPhone = storageManagerPhone;
        this.empCode = empCode;
        this.employee = employee;
    }

    public int getStorageNo() {
        return storageNo;
    }

    public void setStorageNo(int storageNo) {
        this.storageNo = storageNo;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStorageManagerPhone() {
        return storageManagerPhone;
    }

    public void setStorageManagerPhone(String storageManagerPhone) {
        this.storageManagerPhone = storageManagerPhone;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "StorageDTO{" +
                "storageNo=" + storageNo +
                ", storageName='" + storageName + '\'' +
                ", storageManagerPhone='" + storageManagerPhone + '\'' +
                ", empCode=" + empCode +
                ", employee=" + employee +
                '}';
    }
}
