package com.volt.asap.storage.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity(name = "JoinStorageEntity")
@DynamicInsert
@Table(name = "TBL_STORAGE")
@SequenceGenerator(
        name = "STORAGE_SEQ_STORAGE_NO",
        sequenceName = "SEQ_STORAGE_NO",
        initialValue = 6,
        allocationSize = 1
)
public class Storage {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "STORAGE_SEQ_STORAGE_NO"
    )
    @Column(name = "STORAGE_NO")
    private int storageNo;

    @Column(name = "STORAGE_NAME")
    private String storageName;

    @Column(name = "STORAGE_MANAGER_PHONE")
    private String storageManagerPhone;

    @Column(name = "EMP_CODE")
    private int empCode;

    @Column(name = "STORAGE_STATUS")
    private String storageStatus;
    public Storage() {
    }

    public Storage(int storageNo, String storageName, String storageManagerPhone, int empCode, String storageStatus) {
        this.storageNo = storageNo;
        this.storageName = storageName;
        this.storageManagerPhone = storageManagerPhone;
        this.empCode = empCode;
        this.storageStatus = storageStatus;
    }

    public int getStorageNo() {
        return storageNo;
    }

    public void setStorageNo(int storageNo) {
        this.storageNo = storageNo;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStorageManagerPhone() {
        return storageManagerPhone;
    }

    public void setStorageManagerPhone(String storageManagerPhone) {
        this.storageManagerPhone = storageManagerPhone;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public String getStorageStatus() {
        return storageStatus;
    }

    public void setStorageStatus(String storageStatus) {
        this.storageStatus = storageStatus;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "storageNo=" + storageNo +
                ", storageName='" + storageName + '\'' +
                ", storageManagerPhone='" + storageManagerPhone + '\'' +
                ", empCode=" + empCode +
                ", storageStatus='" + storageStatus + '\'' +
                '}';
    }
}
