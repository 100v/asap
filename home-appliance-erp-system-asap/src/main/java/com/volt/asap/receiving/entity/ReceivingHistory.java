package com.volt.asap.receiving.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Date;

@DynamicInsert
@Entity(name = "ReceivingHistory1")
@Table(name = "TBL_RECEIVING_HISTORY")
@SequenceGenerator(
        name = "RECEIVING_HISTORY_SEQ_GENERATOR",
        sequenceName = "SEQ_RECEIVING_HISTORY_NO",
        initialValue = 3,
        allocationSize = 1
)
public class ReceivingHistory {

    @Id
    @Column(name = "RECEIVING_HISTORY_NO")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "RECEIVING_HISTORY_SEQ_GENERATOR"
    )
    private int receivingHistoryNo;

    @Column(name = "RECEIVING_COMENT")
    private String receivingComent;

    @Column(name = "INVENTORY_DATE")
    private Date inventoryDate;

    @Column(name = "RECEIVING_AMOUNT")
    private int receivingAmount;

    @Column(name = "PRODUCT_CODE")
    private String productCode;

    @Column(name = "STORAGE_NO")
    private int storageNo;

    @Column(name = "CLIENT_NO")
    private int clientNo;

    @Column(name = "RECEIVING_STATUS")
    @ColumnDefault("Y")
    private String receivingStatus;

    public ReceivingHistory() {
    }

    public ReceivingHistory(int receivingHistoryNo, String receivingComent, Date inventoryDate, int receivingAmount, String productCode, int storageNo, int clientNo, String receivingStatus) {
        this.receivingHistoryNo = receivingHistoryNo;
        this.receivingComent = receivingComent;
        this.inventoryDate = inventoryDate;
        this.receivingAmount = receivingAmount;
        this.productCode = productCode;
        this.storageNo = storageNo;
        this.clientNo = clientNo;
        this.receivingStatus = receivingStatus;
    }

    public int getReceivingHistoryNo() {
        return receivingHistoryNo;
    }

    public void setReceivingHistoryNo(int receivingHistoryNo) {
        this.receivingHistoryNo = receivingHistoryNo;
    }

    public String getReceivingComent() {
        return receivingComent;
    }

    public void setReceivingComent(String receivingComent) {
        this.receivingComent = receivingComent;
    }

    public Date getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(Date inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public int getReceivingAmount() {
        return receivingAmount;
    }

    public void setReceivingAmount(int receivingAmount) {
        this.receivingAmount = receivingAmount;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getStorageNo() {
        return storageNo;
    }

    public void setStorageNo(int storageNo) {
        this.storageNo = storageNo;
    }

    public int getClientNo() {
        return clientNo;
    }

    public void setClientNo(int clientNo) {
        this.clientNo = clientNo;
    }

    public String getReceivingStatus() {
        return receivingStatus;
    }

    public void setReceivingStatus(String receivingStatus) {
        this.receivingStatus = receivingStatus;
    }

    @Override
    public String toString() {
        return "ReceivingHistory{" +
                "receivingHistoryNo=" + receivingHistoryNo +
                ", receivingComent='" + receivingComent + '\'' +
                ", inventoryDate=" + inventoryDate +
                ", receivingAmount=" + receivingAmount +
                ", productCode='" + productCode + '\'' +
                ", storageNo=" + storageNo +
                ", clientNo=" + clientNo +
                ", receivingStatus='" + receivingStatus + '\'' +
                '}';
    }
}
