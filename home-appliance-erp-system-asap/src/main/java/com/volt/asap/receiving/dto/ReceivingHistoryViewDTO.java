package com.volt.asap.receiving.dto;

import javax.persistence.Column;
import java.sql.Date;

public class ReceivingHistoryViewDTO {

    private int receivingHistoryNo;
    private Date inventoryDate;
    private String productName;
    private String ClientName;
    private int receivingAmount;
    private String receivingComent;
    private String storageName;

    private String receivingStatus;

    public ReceivingHistoryViewDTO() {
    }

    public ReceivingHistoryViewDTO(int receivingHistoryNo, Date inventoryDate, String productName, String clientName, int receivingAmount, String receivingComent, String storageName, String receivingStatus) {
        this.receivingHistoryNo = receivingHistoryNo;
        this.inventoryDate = inventoryDate;
        this.productName = productName;
        ClientName = clientName;
        this.receivingAmount = receivingAmount;
        this.receivingComent = receivingComent;
        this.storageName = storageName;
        this.receivingStatus = receivingStatus;
    }

    public int getReceivingHistoryNo() {
        return receivingHistoryNo;
    }

    public void setReceivingHistoryNo(int receivingHistoryNo) {
        this.receivingHistoryNo = receivingHistoryNo;
    }

    public Date getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(Date inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public int getReceivingAmount() {
        return receivingAmount;
    }

    public void setReceivingAmount(int receivingAmount) {
        this.receivingAmount = receivingAmount;
    }

    public String getReceivingComent() {
        return receivingComent;
    }

    public void setReceivingComent(String receivingComent) {
        this.receivingComent = receivingComent;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getReceivingStatus() {
        return receivingStatus;
    }

    public void setReceivingStatus(String receivingStatus) {
        this.receivingStatus = receivingStatus;
    }

    @Override
    public String toString() {
        return "ReceivingHistoryViewDTO{" +
                "receivingHistoryNo=" + receivingHistoryNo +
                ", inventoryDate=" + inventoryDate +
                ", productName='" + productName + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", receivingAmount=" + receivingAmount +
                ", receivingComent='" + receivingComent + '\'' +
                ", storageName='" + storageName + '\'' +
                ", receivingStatus='" + receivingStatus + '\'' +
                '}';
    }
}
