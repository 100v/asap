package com.volt.asap.receiving.repository;

import com.volt.asap.receiving.dto.ReceivingHistoryDTO;
import com.volt.asap.receiving.entity.ReceivingHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceivingHistoryRepository extends JpaRepository<ReceivingHistory, ReceivingHistoryDTO> {

    ReceivingHistory findByReceivingHistoryNo(int receivingHistoryNo);
}
