package com.volt.asap.receiving.repository;

import com.volt.asap.receiving.entity.ReceivingHistory;
import com.volt.asap.receiving.entity.ReceivingHistoryViewEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ReceivingRepository extends JpaRepository<ReceivingHistoryViewEntity, Date> {
    int countByInventoryDateBetweenAndReceivingStatus(java.sql.Date sDate, java.sql.Date eDate, String status);
    long count();
    int countByInventoryDateBetweenAndStorageNameContainingAndReceivingStatus(java.sql.Date sDate, java.sql.Date eDate, String storageName, String status);
    int countByInventoryDateBetweenAndProductNameContainingAndReceivingStatus(java.sql.Date sDate, java.sql.Date eDate, String productName, String status);

    int countByInventoryDateBetweenAndStorageNameContainingAndProductNameContainingAndReceivingStatus(java.sql.Date sDate, java.sql.Date eDate, String storageName, String productName, String status);
    int countByStorageNameContainingAndProductNameContainingAndReceivingStatus(String storageName, String productName, String status);
    int countByProductNameContainingAndReceivingStatus(String productName, String status);
    int countByStorageNameContainingAndReceivingStatus(String storageName, String status);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetween(java.sql.Date sDate, java.sql.Date eDate, Sort sort);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndStorageNameContaining(java.sql.Date sDate, java.sql.Date eDate, String storageName, Sort sort);

    List<ReceivingHistoryViewEntity> findAllByInventoryDateBetweenAndProductNameContaining(java.sql.Date sDate, java.sql.Date eDate, String productName, Sort sort);

    List<ReceivingHistoryViewEntity> findAllByStorageNameContainingAndProductNameContaining(String storageName, String productName);

    List<ReceivingHistoryViewEntity> findAllByStorageNameContaining(String storageName);

    List<ReceivingHistoryViewEntity> findAllByProductNameContaining(String productName);

    int countByReceivingStatus(String status);
}
