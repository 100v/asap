package com.volt.asap.receiving.entity;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity(name = "ReceivingView")
@Table(name = "RECEIVING_HISTORY_LIST")
public class ReceivingHistoryViewEntity {

    @Id
    @Column(name = "RECEIVING_HISTORY_NO")
    private int receivingHistoryNo;

    @Column(name = "INVENTORY_DATE")
    private Date inventoryDate;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "CLIENT_NAME")
    private String ClientName;

    @Column(name = "RECEIVING_AMOUNT")
    private int receivingAmount;

    @Column(name = "RECEIVING_COMENT")
    private String receivingComent;

    @Column(name = "STORAGE_NAME")
    private String storageName;


    @Column(name = "RECEIVING_STATUS")
    @ColumnDefault("Y")
    private String receivingStatus;

    public ReceivingHistoryViewEntity() {
    }

    public ReceivingHistoryViewEntity(int receivingHistoryNo, Date inventoryDate, String productName, String clientName, int receivingAmount, String receivingComent, String storageName, String receivingStatus) {
        this.receivingHistoryNo = receivingHistoryNo;
        this.inventoryDate = inventoryDate;
        this.productName = productName;
        ClientName = clientName;
        this.receivingAmount = receivingAmount;
        this.receivingComent = receivingComent;
        this.storageName = storageName;
        this.receivingStatus = receivingStatus;
    }

    public int getReceivingHistoryNo() {
        return receivingHistoryNo;
    }

    public void setReceivingHistoryNo(int receivingHistoryNo) {
        this.receivingHistoryNo = receivingHistoryNo;
    }

    public Date getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(Date inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public int getReceivingAmount() {
        return receivingAmount;
    }

    public void setReceivingAmount(int receivingAmount) {
        this.receivingAmount = receivingAmount;
    }

    public String getReceivingComent() {
        return receivingComent;
    }

    public void setReceivingComent(String receivingComent) {
        this.receivingComent = receivingComent;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getReceivingStatus() {
        return receivingStatus;
    }

    public void setReceivingStatus(String receivingStatus) {
        this.receivingStatus = receivingStatus;
    }

    @Override
    public String toString() {
        return "ReceivingHistoryViewEntity{" +
                "receivingHistoryNo=" + receivingHistoryNo +
                ", inventoryDate=" + inventoryDate +
                ", productName='" + productName + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", receivingAmount=" + receivingAmount +
                ", receivingComent='" + receivingComent + '\'' +
                ", storageName='" + storageName + '\'' +
                ", receivingStatus='" + receivingStatus + '\'' +
                '}';
    }
}
