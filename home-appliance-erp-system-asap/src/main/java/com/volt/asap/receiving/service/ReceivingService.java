package com.volt.asap.receiving.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.logistic.dto.ProductDTO;
import com.volt.asap.logistic.dto.StorageDTO;
import com.volt.asap.logistic.entity.Product;
import com.volt.asap.logistic.entity.Storage;
import com.volt.asap.logistic.repository.ProductRepository;
import com.volt.asap.logistic.repository.StorageRepository;
import com.volt.asap.receiving.dto.ReceivingHistoryDTO;
import com.volt.asap.receiving.dto.ReceivingHistoryViewDTO;
import com.volt.asap.receiving.entity.ReceivingHistory;
import com.volt.asap.receiving.entity.ReceivingHistoryViewEntity;
import com.volt.asap.receiving.repository.ReceivingHistoryRepository;
import com.volt.asap.receiving.repository.ReceivingRepository;
import com.volt.asap.receiving.repository.ReceivingTextRepository;
import com.volt.asap.sales.dto.ClientDTO;
import com.volt.asap.sales.entity.Client;
import com.volt.asap.sales.repository.ClientRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReceivingService {

    private final ReceivingRepository receivingRepository;
    private final ReceivingTextRepository receivingTextRepository;
    private final StorageRepository storageRepository;
    private final ProductRepository productRepository;
    private final ClientRepository clientRepository;
    private final ReceivingHistoryRepository receivingHistoryRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ReceivingService(ReceivingRepository receivingRepository, ReceivingTextRepository receivingTextRepository, StorageRepository storageRepository, ProductRepository productRepository, ClientRepository clientRepository, ReceivingHistoryRepository receivingHistoryRepository, ModelMapper modelMapper) {
        this.receivingRepository = receivingRepository;
        this.receivingTextRepository = receivingTextRepository;
        this.storageRepository = storageRepository;
        this.productRepository = productRepository;
        this.clientRepository = clientRepository;
        this.receivingHistoryRepository = receivingHistoryRepository;
        this.modelMapper = modelMapper;
    }
    public List<StorageDTO> findStorageName() {

        String status = "N";

        List<Storage> storageList = storageRepository.findAllByStorageStatus(status);

        return storageList.stream().map(Storage -> modelMapper.map(Storage, StorageDTO.class)).collect(Collectors.toList());
    }

    public List<ProductDTO> findProductName() {

        String status = "N";

        List<Product> productList = productRepository.findAllByProductStatus(status);

        return productList.stream().map(Product -> modelMapper.map(Product, ProductDTO.class)).collect(Collectors.toList());
    }

    public List<ClientDTO> findClientName() {

        String status = "Y";

        List<Client> ClientList = clientRepository.findAllByMaintain(status);

        return ClientList.stream().map(Client -> modelMapper.map(Client, ClientDTO.class)).collect(Collectors.toList());
    }

    @Transactional
    public int saveReceivingHistory(ReceivingHistoryDTO receivingHistoryDTO) {

        int result = 0;

        result = receivingHistoryRepository.save(modelMapper.map(receivingHistoryDTO, ReceivingHistory.class)).getReceivingHistoryNo();

        System.out.println("인서트 후에는 무엇으로 반환이 되는지 궁금하다." + result);

        return result;
    }

    public int countSituation(Map<String, String> parameterMap) {
        String status = "Y";

        if(parameterMap == null) {
            return receivingRepository.countByReceivingStatus(status);
        } else {

            java.sql.Date startDate = toSqlDate(parameterMap.get("startDate"));
            java.sql.Date endDate = toSqlDate(parameterMap.get("endDate"));
            String storageName = parameterMap.get("storagename");
            String productName = parameterMap.get("productname");

            if(startDate != null && endDate != null && storageName != null && productName != null) {
                /* 검색조건이 전부 입력되었을 때 */
                return receivingRepository.countByInventoryDateBetweenAndStorageNameContainingAndProductNameContainingAndReceivingStatus(startDate, endDate, storageName, productName, status);
            } else if(startDate == null && endDate == null && productName == null) {
                /* 창고명으로 검색되었을 때 */
                return receivingRepository.countByStorageNameContainingAndReceivingStatus(storageName, status);
            } else if(startDate == null && endDate == null && storageName == null) {
                /* 상품명으로 검색되었을 때 */
                return receivingRepository.countByProductNameContainingAndReceivingStatus(productName, status);
            } else if(storageName == null && productName == null) {
                /* 기간으로 검색되었을 때 */
                return receivingRepository.countByInventoryDateBetweenAndReceivingStatus(startDate, endDate, status);
            } else if(productName == null) {
                /* 기간과 창고명으로 검색되었을 때 */
                return receivingRepository.countByInventoryDateBetweenAndStorageNameContainingAndReceivingStatus(startDate, endDate, storageName, status);
            } else if(storageName == null) {
                /* 기간과 상품명으로 검색되었을 때 */
                return receivingRepository.countByInventoryDateBetweenAndProductNameContainingAndReceivingStatus(startDate, endDate, productName, status);
            } else {
                /* 창고명과 상품명으로 검색되었을 때 */
                return receivingRepository.countByStorageNameContainingAndProductNameContainingAndReceivingStatus(storageName, productName, status);
            }
        }
    }

    private java.sql.Date toSqlDate(String dateString) {
        java.sql.Date endDate = null;

        if(dateString != null && !"".equals(dateString)) {
            endDate = java.sql.Date.valueOf(dateString);
        }

        return endDate;
    }

    public List<ReceivingHistoryViewDTO> findSituation(CustomSelectCriteria selectCriteria) {

        int index = selectCriteria.getPageNo() -1;
        int count = selectCriteria.getLimit();
        String status = "Y";

        Pageable paging = PageRequest.of(index, count, Sort.by(Sort.Direction.DESC, "inventoryDate"));

        List<ReceivingHistoryViewEntity> receivingHistoryList;

        if(selectCriteria.getSearchValueMap() == null ){
            receivingHistoryList = receivingTextRepository.findByReceivingStatus(paging, status);

            System.out.println(receivingHistoryList);
        } else {

            java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
            java.sql.Date endDate = toSqlDate(selectCriteria.getSearchValueMap().get("endDate"));
            String storageName = selectCriteria.getSearchValueMap().get("storagename");
            String productName = selectCriteria.getSearchValueMap().get("productname");

            if(startDate != null && endDate != null && storageName != null && productName != null) {
                /* 검색조건이 전부 입력되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByInventoryDateBetweenAndStorageNameContainingAndProductNameContainingAndReceivingStatus(startDate, endDate, storageName, productName, paging, status);
            } else if(startDate == null && endDate == null && productName == null) {
                /* 창고명으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByStorageNameContainingAndReceivingStatus(storageName, paging, status);
            } else if(startDate == null && endDate == null && storageName == null) {
                /* 상품명으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByProductNameContainingAndReceivingStatus(productName, paging, status);
            } else if(storageName == null && productName == null) {
                /* 기간으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByInventoryDateBetweenAndReceivingStatus(startDate, endDate, paging, status);
            } else if(productName == null) {
                /* 기간과 창고명으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByInventoryDateBetweenAndStorageNameContainingAndReceivingStatus(startDate, endDate, storageName, paging, status);
            } else if(storageName == null) {
                /* 기간과 상품명으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByInventoryDateBetweenAndProductNameContainingAndReceivingStatus(startDate, endDate, productName, paging, status);
            } else {
                /* 창고명과 상품명으로 검색되었을 때 */
                receivingHistoryList = receivingTextRepository.findAllByStorageNameContainingAndProductNameContainingAndReceivingStatus(storageName, productName, paging, status);
            }
        }

        System.out.println(receivingHistoryList);
        return receivingHistoryList.stream().map(ReceivingView -> modelMapper.map(ReceivingView, ReceivingHistoryViewDTO.class)).collect(Collectors.toList());
    }

    public ReceivingHistoryViewDTO findReceivingDetail(int receivingNo) {

        ReceivingHistoryViewEntity result = receivingTextRepository.findByReceivingHistoryNo(receivingNo);

        return modelMapper.map(result, ReceivingHistoryViewDTO.class);
    }


    @Transactional
    public ReceivingHistory deleteReceiving(Integer receivingHistoryNo) {

        ReceivingHistory result;

//        result = receivingTextRepository.deleteByReceivingHistoryNo(receivingHistoryNo);

        result = receivingHistoryRepository.findByReceivingHistoryNo(receivingHistoryNo);

        result.setReceivingStatus("N");

        System.out.println("삭제가되었니?????????????????????????????????????" + result);

        return result;
    }

    @Transactional
    public ReceivingHistory modifyReceivingHistory(ReceivingHistoryDTO receivingHistoryDTO) {

        ReceivingHistory result;

        result = receivingHistoryRepository.findByReceivingHistoryNo(receivingHistoryDTO.getReceivingHistoryNo());

        System.out.println("찾아와졌냐" + result);

        result.setReceivingAmount(receivingHistoryDTO.getReceivingAmount());
        result.setReceivingComent(receivingHistoryDTO.getReceivingComent());

        return result;
    }
}
