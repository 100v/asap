package com.volt.asap.login.repository;

import com.volt.asap.hrm.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Employee, Integer> {
    Employee findEmployeeByEmpCodeAndEmpLeaveYn(Integer employeeId, String empLeaveYn);
}
