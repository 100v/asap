package com.volt.asap.login.service;

import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.login.dto.EmployeeDTO;
import com.volt.asap.login.dto.PermissionDTO;
import com.volt.asap.login.dto.UserImpl;
import com.volt.asap.login.dto.UserPermissionDTO;
import com.volt.asap.login.repository.LoginRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 로그인 서비스를 제공하는 클래스
 * 스프링 시큐리티의 유저 정보에 이용해야 하기 때문에 {@link UserDetailsService}를 구현한다.
 *
 * @see UserDetailsService
 */
@Service
public class LoginService implements UserDetailsService {

    /* 로그인 리포지토리 */
    private final LoginRepository loginRepository;

    /* 객체간 자동 매핑을 위한 ModelMapper */
    private final ModelMapper modelMapper;

    /* 생성자 주입 */
    @Autowired
    public LoginService(LoginRepository loginRepository, ModelMapper modelMapper) {
        this.loginRepository = loginRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * User 정보를 username(employeeId)를 사용해 불러오는 메소드
     * {@link UserDetailsService}의 메소드를 Override한다.
     *
     * @param employeeId the username identifying the user whose data is required.
     * @return 유저 정보와 스프링 시큐리티에 필요한 권한 정보를 담은 User 객체의 구현체인 UserImpl 반환
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String employeeId) throws UsernameNotFoundException {
        Employee employee = loginRepository.findEmployeeByEmpCodeAndEmpLeaveYn(Integer.valueOf(employeeId), "N");

        EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);

        if(employeeDTO == null) {
            employeeDTO = new EmployeeDTO();
        }

        List<GrantedAuthority> authorities = new ArrayList<>();

        if(employeeDTO.getUserPermissionList() != null) {
            for (UserPermissionDTO role : employeeDTO.getUserPermissionList()) {
                PermissionDTO permission = role.getPermission();
                authorities.add(new SimpleGrantedAuthority(permission.getPermissionCode()));
            }
        }

        UserImpl user = new UserImpl(String.valueOf(employee.getEmpCode()), employee.getEmpPwd(), authorities);
        user.setDetails(employeeDTO);

        return user;
    }
}
