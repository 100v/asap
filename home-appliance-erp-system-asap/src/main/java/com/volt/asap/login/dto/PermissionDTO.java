package com.volt.asap.login.dto;

public class PermissionDTO {
    private String permissionCode;                  // PERMISSION_CODE
    private String permissionName;                  // PERMISSION_NAME

    public PermissionDTO() { }

    public PermissionDTO(String permissionCode, String permissionName) {
        this.permissionCode = permissionCode;
        this.permissionName = permissionName;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @Override
    public String toString() {
        return "PermissionDTO{" +
                "permissionCode='" + permissionCode + '\'' +
                ", permissionName='" + permissionName + '\'' +
                '}';
    }
}
