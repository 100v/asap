package com.volt.asap.login.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDTO implements Serializable {
    private static final long serialVersionUID = -6246250240115194593L;
    private int empCode;                    // EMP_CODE
    private String empName;                 // EMP_NAME
    private String empBirth;                // EMP_BIRTH
    private String deptCode;                // DEPT_CODE
    private String empPhone;                // EMP_PHONE
    private String empEmail;                // EMP_EMAIL
    private String empAddress;              // EMP_ADDRESS
    private java.sql.Date empJoindate;      // EMP_JOINDATE
    private java.sql.Date empLeavedate;     // EMP_LEAVEDATE
    private String empLeaveYn;              // EMP_LEAVE_YN
    private String jobCode;                 // JOB_CODE
    private int annualLeftDay;              // ANNUAL_LEFT_DAY
    private String signImg;                 // SIGN_IMG
    private String profileImg;              // PROFILE_IMG
    private String empGender;               // EMP_GENDER
    private String empPwd;                  // EMP_PWD
    private int salaryBase;                 // SALARY_BASE

    private List<UserPermissionDTO> userPermissionList = new ArrayList<>();

    public EmployeeDTO() { }

    public EmployeeDTO(int empCode, String empName, String empBirth, String deptCode, String empPhone, String empEmail, String empAddress, Date empJoindate, Date empLeavedate, String empLeaveYn, String jobCode, int annualLeftDay, String signImg, String profileImg, String empGender, String empPwd, int salaryBase, List<UserPermissionDTO> userPermissionList) {
        this.empCode = empCode;
        this.empName = empName;
        this.empBirth = empBirth;
        this.deptCode = deptCode;
        this.empPhone = empPhone;
        this.empEmail = empEmail;
        this.empAddress = empAddress;
        this.empJoindate = empJoindate;
        this.empLeavedate = empLeavedate;
        this.empLeaveYn = empLeaveYn;
        this.jobCode = jobCode;
        this.annualLeftDay = annualLeftDay;
        this.signImg = signImg;
        this.profileImg = profileImg;
        this.empGender = empGender;
        this.empPwd = empPwd;
        this.salaryBase = salaryBase;
        this.userPermissionList = userPermissionList;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpBirth() {
        return empBirth;
    }

    public void setEmpBirth(String empBirth) {
        this.empBirth = empBirth;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpAddress() {
        return empAddress;
    }

    public void setEmpAddress(String empAddress) {
        this.empAddress = empAddress;
    }

    public Date getEmpJoindate() {
        return empJoindate;
    }

    public void setEmpJoindate(Date empJoindate) {
        this.empJoindate = empJoindate;
    }

    public Date getEmpLeavedate() {
        return empLeavedate;
    }

    public void setEmpLeavedate(Date empLeavedate) {
        this.empLeavedate = empLeavedate;
    }

    public String getEmpLeaveYn() {
        return empLeaveYn;
    }

    public void setEmpLeaveYn(String empLeaveYn) {
        this.empLeaveYn = empLeaveYn;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public int getAnnualLeftDay() {
        return annualLeftDay;
    }

    public void setAnnualLeftDay(int annualLeftDay) {
        this.annualLeftDay = annualLeftDay;
    }

    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getEmpGender() {
        return empGender;
    }

    public void setEmpGender(String empGender) {
        this.empGender = empGender;
    }

    public String getEmpPwd() {
        return empPwd;
    }

    public void setEmpPwd(String empPwd) {
        this.empPwd = empPwd;
    }

    public int getSalaryBase() {
        return salaryBase;
    }

    public void setSalaryBase(int salaryBase) {
        this.salaryBase = salaryBase;
    }

    public List<UserPermissionDTO> getUserPermissionList() {
        return userPermissionList;
    }

    public void setUserPermissionList(List<UserPermissionDTO> userPermissionList) {
        this.userPermissionList = userPermissionList;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "empCode=" + empCode +
                ", empName='" + empName + '\'' +
                ", empBirth='" + empBirth + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", empPhone='" + empPhone + '\'' +
                ", empEmail='" + empEmail + '\'' +
                ", empAddress='" + empAddress + '\'' +
                ", empJoindate=" + empJoindate +
                ", empLeavedate=" + empLeavedate +
                ", empLeaveYn='" + empLeaveYn + '\'' +
                ", jobCode='" + jobCode + '\'' +
                ", annualLeftDay=" + annualLeftDay +
                ", signImg='" + signImg + '\'' +
                ", profileImg='" + profileImg + '\'' +
                ", empGender='" + empGender + '\'' +
                ", empPwd='" + empPwd + '\'' +
                ", salaryBase=" + salaryBase +
                ", userPermissionList=" + userPermissionList +
                '}';
    }
}
