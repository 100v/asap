package com.volt.asap.login.dto;

public class UserPermissionDTO {
    private String empCode;                         // EMP_CODE
    private String permissionCode;                  // PERMISSION_CODE

    private PermissionDTO permission;                  // PERMISSION

    public UserPermissionDTO() {
    }

    public UserPermissionDTO(String empCode, String permissionCode, PermissionDTO permission) {
        this.empCode = empCode;
        this.permissionCode = permissionCode;
        this.permission = permission;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public PermissionDTO getPermission() {
        return permission;
    }

    public void setPermission(PermissionDTO permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return "UserPermissionDTO{" +
                "empCode='" + empCode + '\'' +
                ", permissionCode='" + permissionCode + '\'' +
                ", permission=" + permission +
                '}';
    }
}
