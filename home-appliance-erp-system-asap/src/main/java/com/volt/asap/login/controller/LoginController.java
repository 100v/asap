package com.volt.asap.login.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 로그인 처리 담당 컨트롤러
 * @see Controller
 */
@Controller
public class LoginController {

    /**
     * '/login' 의 GET 메소드를 담당
     * @return login.html
     * @see org.springframework.web.servlet.ViewResolver
     */
    @GetMapping("login")
    public String login(@CookieValue(name = "id", required = false) String id, HttpSession session, Model model) {
        Object message = session.getAttribute("message");

        if(id != null && !"".equals(id)) {
            model.addAttribute("id", id);
        }

        /* 세션스코프에서 메세지 꺼내오고 제거 */
        if(message instanceof String) {
            model.addAttribute("message", message);
            session.removeAttribute("message");
        }

        return "login";
    }
}
