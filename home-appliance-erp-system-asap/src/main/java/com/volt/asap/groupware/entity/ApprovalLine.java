package com.volt.asap.groupware.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Date;

@Entity(name = "ApprovalLine")
@Table(name = "TBL_APPROVAL_LINE")
@DynamicInsert
@DynamicUpdate
@IdClass(ApprovalLineId.class)
public class ApprovalLine {
    @Id
    @Column(name = "EMP_CODE", nullable = false)
    private int empCode;
    @Id
    @Column(name = "DRAFT_NO", nullable = false)
    private int draftNo;
    @Column(name = "APPROVAL_ORDER", nullable = false)
    private int approvalOrder;
    @Column(name = "APPROVAL_STATUS")
    private String approvalStatus;
    @Column(name = "APPROVAL_DATE")
    private java.sql.Date approvalDate;
    @Column(name = "APPROVAL_REASON")
    private String approvalReason;
    @ManyToOne
    @JoinColumn(name = "EMP_CODE", updatable = false, insertable = false)
    private FlatEmployee employee;

    public ApprovalLine() {
    }

    public ApprovalLine(int empCode, int draftNo, int approvalOrder, String approvalStatus, Date approvalDate, String approvalReason, FlatEmployee employee) {
        this.empCode = empCode;
        this.draftNo = draftNo;
        this.approvalOrder = approvalOrder;
        this.approvalStatus = approvalStatus;
        this.approvalDate = approvalDate;
        this.approvalReason = approvalReason;
        this.employee = employee;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(int draftNo) {
        this.draftNo = draftNo;
    }

    public int getApprovalOrder() {
        return approvalOrder;
    }

    public void setApprovalOrder(int approvalOrder) {
        this.approvalOrder = approvalOrder;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getApprovalReason() {
        return approvalReason;
    }

    public void setApprovalReason(String approvalReason) {
        this.approvalReason = approvalReason;
    }

    public FlatEmployee getEmployee() {
        return employee;
    }

    public void setEmployee(FlatEmployee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "ApprovalLine{" +
                "empCode=" + empCode +
                ", draftNo=" + draftNo +
                ", approvalOrder=" + approvalOrder +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", approvalDate=" + approvalDate +
                ", approvalReason='" + approvalReason + '\'' +
                ", employee=" + employee +
                '}';
    }
}
