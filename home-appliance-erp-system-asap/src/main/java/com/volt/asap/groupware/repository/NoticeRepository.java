package com.volt.asap.groupware.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.groupware.entity.Notice;

public interface NoticeRepository extends JpaRepository<Notice, Integer>{

	Notice findNoticeByNoticeNo(int noticeNo);

	Notice findNoticeByNoticeNoAndDeleteStatus(int noticeNo, String string);

	Notice findByNoticeNoAndDeleteStatus(int noticeNo, String string);

}
