package com.volt.asap.groupware.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class DraftDTO implements Serializable {
    private static final long serialVersionUID = -891323810335676389L;

    private int draftNo;                                          // DRAFT_NO
    private java.sql.Date draftDate;                              // DRAFT_DATE
    private String draftTitle;                                    // DRAFT_TITLE
    private String draftType;                                     // DRAFT_TYPE
    private String draftContents;                                 // DRAFT_CONTENTS
    private String draftStatus;                                   // DRAFT_STATUS
    private String approvalStatus;                                // APPROVAL_STATUS
    private String deleteStatus;                                  // DELETE_STATUS
    private int empCode;                                          // EMP_CODE
    private EmployeeDTO author;
    private List<ApprovalLineDTO> approvalLineList;

    public DraftDTO() {
    }

    public DraftDTO(int draftNo, Date draftDate, String draftTitle, String draftType, String draftContents, String draftStatus, String approvalStatus, String deleteStatus, int empCode, EmployeeDTO author, List<ApprovalLineDTO> approvalLineList) {
        this.draftNo = draftNo;
        this.draftDate = draftDate;
        this.draftTitle = draftTitle;
        this.draftType = draftType;
        this.draftContents = draftContents;
        this.draftStatus = draftStatus;
        this.approvalStatus = approvalStatus;
        this.deleteStatus = deleteStatus;
        this.empCode = empCode;
        this.author = author;
        this.approvalLineList = approvalLineList;
    }

    public int getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(int draftNo) {
        this.draftNo = draftNo;
    }

    public Date getDraftDate() {
        return draftDate;
    }

    public void setDraftDate(Date draftDate) {
        this.draftDate = draftDate;
    }

    public String getDraftTitle() {
        return draftTitle;
    }

    public void setDraftTitle(String draftTitle) {
        this.draftTitle = draftTitle;
    }

    public String getDraftType() {
        return draftType;
    }

    public void setDraftType(String draftType) {
        this.draftType = draftType;
    }

    public String getDraftContents() {
        return draftContents;
    }

    public void setDraftContents(String draftContents) {
        this.draftContents = draftContents;
    }

    public String getDraftStatus() {
        return draftStatus;
    }

    public void setDraftStatus(String draftStatus) {
        this.draftStatus = draftStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public EmployeeDTO getAuthor() {
        return author;
    }

    public void setAuthor(EmployeeDTO author) {
        this.author = author;
    }

    public List<ApprovalLineDTO> getApprovalLineList() {
        return approvalLineList;
    }

    public void setApprovalLineList(List<ApprovalLineDTO> approvalLineList) {
        this.approvalLineList = approvalLineList;
    }

    @Override
    public String toString() {
        return "DraftDTO{" +
                "draftNo=" + draftNo +
                ", draftDate=" + draftDate +
                ", draftTitle='" + draftTitle + '\'' +
                ", draftType='" + draftType + '\'' +
                ", draftContents='" + draftContents + '\'' +
                ", draftStatus='" + draftStatus + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", empCode=" + empCode +
                ", author=" + author +
                ", approvalLineList=" + approvalLineList +
                '}';
    }
}
