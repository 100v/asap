package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileUploadRepository extends JpaRepository<FileUpload, Integer> {

    /**
     * 매개변수로 전달받은 파일 이름에 해당하는 엔티티를 삭제하는 메서드
     * @param fileRename 삭제하고자 하는 파일의 이름
     */
    void deleteByFileRename(String fileRename);

    /**
     * 매개변수로 전달받은 파일 이름에 해당하는 엔티티를 찾아서 반환하는 메서드
     * @param fileRename 찾고자 하는 파일의 이름
     * @return {@link FileUpload} 객체
     */
    FileUpload findByFileRename(String fileRename);
}
