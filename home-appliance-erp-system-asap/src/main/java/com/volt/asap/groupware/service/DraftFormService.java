package com.volt.asap.groupware.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.DraftFormDTO;
import com.volt.asap.groupware.entity.DraftForm;
import com.volt.asap.groupware.repository.DraftFormRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 기안서 양식에 대한 서비스를 하는 서비스 클래스
 */
@Service
public class DraftFormService {
    private final DraftFormRepository draftFormRepository;
    private final ModelMapper modelMapper;


    public DraftFormService(DraftFormRepository draftFormRepository, ModelMapper modelMapper) {
        this.draftFormRepository = draftFormRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * 매개변수로 전달된 쿼리로 검색한 결과의 개수를 반환하는 서비스 메소드
     * @param searchValueMap 검색할 쿼리
     * @return 검색된 기안서 양식의 결과 개수
     */
    public int countAllByQueries(Map<String, String> searchValueMap) {
        String type = searchValueMap.get("type");
        String title = searchValueMap.get("title");

        if("".equals(type)) {
            if("".equals(title)) {
                /* type, title 검색 */
                return draftFormRepository.countByDraftFormDivisionAndDraftFormTitleContainingIgnoreCase(type, title);
            } else {
                /* type 검색 */
                return draftFormRepository.countByDraftFormDivision(type);
            }
        } else {
            if("".equals(title)) {
                /* title 검색 */
                return draftFormRepository.countByDraftFormTitleContainingIgnoreCase(title);
            } else {
                /* 전체 검색 */
                return (int) draftFormRepository.count();
            }
        }
    }

    /**
     * 매개변수로 전달 된 쿼리로 검색한 {@link DraftFormDTO}의 리스트를 반환하는 서비스 메소드
     * @param selectCriteria 검색할 정보가 모두 종합되어 있는 객체
     * @return 검색된 기안서 양식의 리스트
     */
    public List<DraftFormDTO> findAllByQueries(CustomSelectCriteria selectCriteria) {
        String type = selectCriteria.getSearchValueMap().get("type");
        String title = selectCriteria.getSearchValueMap().get("title");

        int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();

        Pageable paging = PageRequest.of(index, count, Sort.by("draftFormNo").descending());

        List<DraftForm> draftFormList;
        if(!"".equals(type)) {
            if(!"".equals(title)) {
                /* type, title 검색 */
                draftFormList = draftFormRepository.findByDraftFormDivisionAndDraftFormTitleContainingIgnoreCaseOrderByDraftFormNoDesc(type, title, paging);
            } else {
                /* type 검색 */
                draftFormList = draftFormRepository.findByDraftFormDivisionOrderByDraftFormNoDesc(type, paging);
            }
        } else {
            if(!"".equals(title)) {
                /* title 검색 */
                draftFormList = draftFormRepository.findByDraftFormTitleContainingIgnoreCaseOrderByDraftFormNoDesc(title, paging);
            } else {
                /* 전체 검색 */
                draftFormList = draftFormRepository.findAll(paging).toList();
            }
        }

        return draftFormList.stream().map(draftForm -> modelMapper.map(draftForm, DraftFormDTO.class)).collect(Collectors.toList());
    }

    /**
     * 기안서 양식의 구분을 리스트 형태로 반환하는 서비스 메소드
     * @return 기안서 양식 구분의 리스트
     */
    public List<String> findDraftFormTypeAll() {

        return draftFormRepository.findDraftFormTypeAllNative();
    }

    /**
     * 기안서 양식을 매개변수로 받은 기안서 양식 번호로 찾아서 반환하는 서비스 메소드
     * @param draftFormNo 기안서 양식 번호
     * @return 기안서 양식
     */
    public DraftFormDTO findByDraftFormNo(int draftFormNo) {
        DraftForm draftForm = draftFormRepository.findByDraftFormNo(draftFormNo);

        if(draftForm == null) {
            return null;
        }

        return modelMapper.map(draftForm, DraftFormDTO.class);
    }

    /**
     * 기안서 양식을 수정하는 서비스 메소드
     * @param modifiedDraftForm 수정될 기안서 양식 정보
     */
    @Transactional
    public void updateDraftForm(DraftFormDTO modifiedDraftForm) {
        DraftForm draftForm = draftFormRepository.findByDraftFormNo(modifiedDraftForm.getDraftFormNo());

        draftForm.setDraftFormTitle(modifiedDraftForm.getDraftFormTitle());
        draftForm.setDraftFormDivision(modifiedDraftForm.getDraftFormDivision());
        draftForm.setDraftFormContent(modifiedDraftForm.getDraftFormContent());
    }

    /**
     * 기안서 양식을 등록하는 서비스 메소드
     * @param draftFormDTO 등록될 기안서 양식 정보
     * @return 등록 성공시 true, 실패시 false
     */
    @Transactional
    public boolean insertDraftForm(DraftFormDTO draftFormDTO) {
        try {
            draftFormRepository.save(modelMapper.map(draftFormDTO, DraftForm.class));
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    /**
     * 기안서 양식을 삭제하는 서비스 메소드
     * @param draftFormNo 삭제될 기안서 양식 번호
     * @return 삭제 성공시 true, 실패시 false
     */
    @Transactional
    public boolean deleteDraftForm(int draftFormNo) {
        try {
            draftFormRepository.deleteById(draftFormNo);
        } catch (IllegalArgumentException exception) {
            return false;
        }

        return true;
    }
}
