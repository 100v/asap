package com.volt.asap.groupware.controller;

import java.io.File;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.FileUploadDTO;
import com.volt.asap.groupware.dto.NoticeDTO;
import com.volt.asap.groupware.service.NoticeService;
import com.volt.asap.hrm.dto.EmployeeDTO;

@Controller
@RequestMapping("/notice")
public class NoticeController {

	private final NoticeService noticeService;
	
	@Autowired
	public NoticeController(NoticeService noticeService) {
		this.noticeService = noticeService;
	}
	
	@GetMapping("/list")
	public ModelAndView noticeList(String startDate, String endDate, String searchWord
			,Authentication auth, Integer page, ModelAndView mv) {
		CustomSelectCriteria selectCriteria;
		
		 if (page == null) {
	            page = 1;
	        }
		 
	        if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(searchWord)) {

	            int totalCount = noticeService.countNotice(null);
	            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 10, 5);
	        } else {
	            /* 검색 쿼리로 사용할 값들을 Map에 설정 */
	            Map<String, String> parameterMap = new HashMap<>();

	            parameterMap.put("startDate", startDate);
	            parameterMap.put("endDate", endDate);
	            parameterMap.put("searchWord", searchWord == null ? "" : searchWord);


	            /* 검색어에 대한 전체 엔티티 개수를 설정 */
	            int totalCount = noticeService.countNotice(parameterMap);

	            /* 검색어 객체의 생성 및 검색어 할당 */
	            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 10,
	                    5, parameterMap);
	        }

	        /* 검색어 객체로 조회한 스케줄 리스트 */
	        List<NoticeDTO> noticeList = noticeService.noticeList(selectCriteria);

	        System.out.println("공지사항 확인용 " + noticeList);
	        
			mv.addObject("noticeList", noticeList);
			mv.addObject("selectCriteria", selectCriteria);
			mv.setViewName("notice/list");
			
			return mv;
	    }
		 
		@GetMapping("/{noticeNo}")
		public ModelAndView noticeDetail(ModelAndView mv, @PathVariable int noticeNo) {
			
			NoticeDTO noticeDetail = noticeService.noticeDetail(noticeNo);
			
			System.out.println("파일 불러오기 " + noticeDetail.getAttachedFileList());
			
			mv.addObject("noticeDetail",noticeDetail);
			mv.setViewName("notice/detailview");
			
			return mv;
		}
		
		
		
		@GetMapping("/regist")
		public void registPage() {}
		
	    @ResponseBody()
		@PostMapping(value = "/regist" , produces = "application/json; charset=UTF-8")
		public ResponseEntity<Map<String, String>> registNotice(
//				@RequestParam(name = "stats") String stats,
				@RequestParam(name = "title") String title,
//	            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
	            @RequestParam(name = "content", required = false) String content,
	            @RequestParam(name = "files", required = false) List<MultipartFile> files,
		        Authentication auth) {
	    	NoticeDTO newNotice = new NoticeDTO();
	    	String status = "N";
	    	long date = System.currentTimeMillis();
	    	Date noticedate = new Date(date);
	    	
	    	newNotice.setDeleteStatus(status);
//	    	newNotice.setDeleteStatus(stats);
	    	newNotice.setEmpCode(Integer.parseInt(auth.getName()));
	    	newNotice.setNoticeTitle(title);
	    	newNotice.setNoticeContent(content);
	    	newNotice.setNoticeDate(noticedate);
//	    	newNotice.setNoticeDate(new Date(java.sql.Timestamp.valueOf(endDate).getTime()));
	    	
	    	/* 업로드 파일 처리 로직 */
	        List<FileUploadDTO> fileUploadDTOList = saveUploadFiles(files);
	        if(fileUploadDTOList == null) {
	            Map<String, String> message = new HashMap<>();
	            message.put("message", "파일 업로드 처리 과정에서 오류가 발생했습니다. 지속적으로 발생하면 관리자에게 문의하세요.");
	            return ResponseEntity.internalServerError().body(message);
	        }
			
//			noticeService.registNotice(noticeInsert);
			
//			rttr.addFlashAttribute("registSuccessmessage", "공지사항 등록 성공.");
			
//			mv.setViewName("redirect:/notice/list");
			
	        int insertedNotice = noticeService.registNotice(newNotice, fileUploadDTOList);
	        
	        Map<String, String> messages = new HashMap<>();
	        if(insertedNotice == 0) {
	            messages.put("message", "일정 추가에 실패했습니다. 파일 형식이 잘못되었습니다.");
	            if(fileRemoveAll(fileUploadDTOList)) {
	                System.out.println("파일을 삭제하는 과정에 문제가 발생했습니다.");
	            } else {
	                System.out.println("문제가 생긴 일정의 모든 파일을 성공적으로 삭제했습니다.");
	            }

	            return ResponseEntity.internalServerError().body(messages);
	        }

	        messages.put("status", "success");
	        messages.put("message", "공지사항 등록에 성공했습니다.");
	        messages.put("dateNo", String.valueOf(insertedNotice));

	        return ResponseEntity.ok(messages);
			
		}
	

    @GetMapping("/{noticeNo}/modify")
    public ModelAndView noticeModify(ModelAndView mv, @PathVariable int noticeNo) {
    	
    	NoticeDTO noticeDetail = noticeService.noticeModify(noticeNo);
    	
    	mv.addObject("noticeDetail", noticeDetail);
    	mv.setViewName("notice/modify");
    	
    	return mv;
    }

	/*
	 * @ResponseBody
	 * 
	 * @PostMapping("/{noticeNo}/modify") public String
	 * noticeModify(RedirectAttributes rttr, @ModelAttribute NoticeDTO notice ,
	 * HttpServletRequest request) {
	 * 
	 * noticeService.modifyNotice(notice);
	 * 
	 * System.out.println("데이터 허ㅘㄱ인" + notice);
	 * rttr.addFlashAttribute("modifySuccessMessage", "공지사항 수정 성공!");
	 * 
	 * return "redirect:/notice/list"; }
	 */
    
    @ResponseBody
    @PostMapping("/{noticeNo}/modify")
    public ResponseEntity<Map<String, String>> noticeModify(
				@PathVariable int noticeNo,
//				@RequestParam(name = "stats") String stats,
				@RequestParam(name = "title") String title,
//	            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
	            @RequestParam(name = "content", required = false) String content,
	            @RequestParam(name = "files", required = false) List<MultipartFile> files,
	            @RequestParam(name = "uploadFiles", required = false) List<MultipartFile> uploadFiles,
	            @RequestParam(name = "removeFiles", required = false) List<String> removeFiles
			) {
    	
    	Map<String, String> messages = new HashMap<>();
    	
        if(uploadFiles == null) {
            uploadFiles = new ArrayList<>();
        }

        if(removeFiles == null) {
            removeFiles = new ArrayList<>();
        }
    	
    	NoticeDTO noticedto = new NoticeDTO();
    	
    	String status = "N";
    	long date = System.currentTimeMillis();
    	Date noticedate = new Date(date);
    	
    	noticedto.setNoticeNo(noticeNo);
    	noticedto.setNoticeTitle(title);
    	noticedto.setNoticeContent(content);
    	noticedto.setNoticeDate(noticedate);
    	noticedto.setDeleteStatus(status);
    	
    	System.out.println("데이터 확인" + noticedto);
    	List<FileUploadDTO> fileUploadDTOList = saveUploadFiles(uploadFiles);
        if(fileUploadDTOList == null) {
            Map<String, String> message = new HashMap<>();
            message.put("message", "파일 업로드 처리 과정에서 오류가 발생했습니다. 지속적으로 발생하면 관리자에게 문의하세요.");
            return ResponseEntity.internalServerError().body(message);
        }
    	
        List<FileUploadDTO> removeFileDTOList = new ArrayList<>();

        for (String removeFile : removeFiles) {
            removeFileDTOList.add(noticeService.findUploadFileByFileRename(removeFile));
        }
        
        if(!noticeService.updateNotice(
                 fileUploadDTOList, removeFiles, noticedto
                 
        )) {
            messages.put("message", "일정 등록에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }
        
        if(fileRemoveAll(removeFileDTOList)) {
            messages.put("message", "파일 삭제에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }
        
        messages.put("status", "success");
        messages.put("message", "일정 수정에 성공했습니다.");
        messages.put("dateNo", String.valueOf(noticedto.getNoticeNo()));
        
    	return ResponseEntity.ok(messages);
    }


	public boolean isEmptyString(String str) {
	    return str == null || "".equals(str);
	}
	
	
	
    private List<FileUploadDTO> saveUploadFiles(List<MultipartFile> files) {
        if (files == null) {
            return new ArrayList<>();
        }

        String rootLocation = new File("src/main/resources/static").getAbsolutePath();
        String fileLocation = "/upload/notice";
        String fileUploadRealLocation = rootLocation + fileLocation;
        File directory = new File(fileUploadRealLocation);

        System.out.println("fileUploadRealLocation = " + fileUploadRealLocation);

        if(!directory.exists()) {
            if(!directory.mkdirs()) {
                return null;
            }
        }

        List<FileUploadDTO> fileUploadDTOList = new ArrayList<>();

        for (MultipartFile file : files) {
            if(file.getSize() > 0) {
                try {
                    String originalFileName = file.getOriginalFilename();

                    /* 예외상황 핸들링 */
                    if(originalFileName == null) {
                        System.out.println("업로드 된 파일의 파일 이름이 없거나 잘못된 형식입니다.");
                        throw new Exception("파일 이름 또는 잘못된 형식의 파일");
                    }
                    String ext = originalFileName.substring(originalFileName.lastIndexOf("."));
                    String savedFileName = UUID.randomUUID().toString().replace("-", "") + ext;

                    /* 파일 쓰기 */
                    file.transferTo(new File(fileUploadRealLocation + "/" + savedFileName));
                    System.out.println(fileUploadRealLocation + "/" + savedFileName);

                FileUploadDTO fileUploadDTO = new FileUploadDTO();

                fileUploadDTO.setFileName(originalFileName);
                fileUploadDTO.setFileLocation(fileLocation);
                fileUploadDTO.setFileUploadBoard("schedule");
                fileUploadDTO.setFileUploadDate(new Date(new java.util.Date().getTime()));
                fileUploadDTO.setFileRename(savedFileName);
                
                
                fileUploadDTOList.add(fileUploadDTO);

                } catch (Exception e) {
                    System.out.println("파일을 쓰는 과정에 문제가 발생했습니다.");
                    System.out.println(e.getLocalizedMessage());

                    if(fileRemoveAll(fileUploadDTOList)) {
                        System.out.println("파일을 삭제하는 과정에 문제가 발생했습니다.");
                    } else {
                        System.out.println("문제가 생긴 일정의 모든 파일을 성공적으로 삭제했습니다.");
                    }
                }
            }
        }

        return fileUploadDTOList;
    }
	
    
    private boolean fileRemoveAll(List<FileUploadDTO> files) {
        if(files.isEmpty()) {
            return false;
        }

        String rootLocation = new File("src/main/resources/static").getAbsolutePath();

        int result = 0;
        for (FileUploadDTO file : files) {
            File removeFile = new File(rootLocation + file.getFileLocation() + "/" + file.getFileRename());

            result += removeFile.delete() ? 1 : 0;
        }

        return result != files.size();
    }
    
    @ResponseBody
    @PostMapping("/{noticeNo}/remove")
    public ResponseEntity<Map<String, String>> noticeDelete(@PathVariable int noticeNo) {
        Map<String, String> messages = new HashMap<>();
        noticeService.deletenotice(noticeNo);

        messages.put("status", "success");
        messages.put("message", "일정 삭제에 성공했습니다.");

        return ResponseEntity.ok(messages);
    }
    
}
	





















