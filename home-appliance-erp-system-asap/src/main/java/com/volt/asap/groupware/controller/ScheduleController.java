package com.volt.asap.groupware.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.*;
import com.volt.asap.groupware.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 일정 처리 담당 컨트롤러
 * '/groupware/schedule'에 대한 GET, POST Mapping을 담당
 */
@Controller
@RequestMapping("/groupware/schedule")
public class ScheduleController {
    private final ScheduleService scheduleService;

    @Autowired
    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    /**
     * '/groupware/schedule/'의 GET 메소드를 담당
     * @param startDate 시작날짜
     * @param endDate 종료날짜
     * @param searchWord 검색어
     * @param page 현재 페이지
     * @param auth 스프링 시큐리티 인증 정보
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @return '/groupware/schedule-inquiry.html' 반환
     * @see org.springframework.web.servlet.ViewResolver
     */
    @GetMapping("")
    public String scheduleInquiry(String startDate, String endDate, String searchWord, Integer page,
                                  Authentication auth, Model model) {

        CustomSelectCriteria selectCriteria;

        /* 페이지 파라미터가 없으면 1페이지로 간주 */
        if (page == null) {
            page = 1;
        }

        if(isEmptyString(startDate) && isEmptyString(endDate) && isEmptyString(searchWord)) {

            int totalCount = scheduleService.countScheduleByQueries(null);
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 20, 5);
        } else {
            /* 검색 쿼리로 사용할 값들을 Map에 설정 */
            Map<String, String> parameterMap = new HashMap<>();

            parameterMap.put("startDate", startDate);
            parameterMap.put("endDate", endDate);
            parameterMap.put("searchWord", searchWord == null ? "" : searchWord);


            /* 검색어에 대한 전체 엔티티 개수를 설정 */
            int totalCount = scheduleService.countScheduleByQueries(parameterMap);

            /* 검색어 객체의 생성 및 검색어 할당 */
            selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 20,
                    5, parameterMap);
        }

        /* 검색어 객체로 조회한 스케줄 리스트 */
        List<CompanyDateDTO> scheduleList = scheduleService.findScheduleByQueries(selectCriteria);


        model.addAttribute("selectCriteria", selectCriteria);
        model.addAttribute("scheduleList", scheduleList);

        return "/groupware/schedule/schedule-inquiry";
    }

    /**
     * '/groupware/schedule/{scheduleNo}'의 GET 메소드 담당
     * 사내 일정의 상세보기 페이지
     * @param scheduleNo {@link PathVariable}이며 상세보기로 보여질 일정의 번호
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @return '/groupware/schedule/{scheduleNo}'
     */
    @GetMapping("/{scheduleNo}")
    public String scheduleDetail(@PathVariable int scheduleNo, Model model, RedirectAttributes rttr) {
        CompanyDateDTO schedule = scheduleService.findByDateNo(scheduleNo);

        if(schedule == null) {
            rttr.addFlashAttribute("message", "삭제된 게시글입니다.");

            return "redirect:/groupware/schedule/";
        } else {
            model.addAttribute("authorCode", String.valueOf(schedule.getAuthor().getEmpCode()));
            model.addAttribute("schedule", schedule);

            return "groupware/schedule/schedule-detail";
        }
    }

    /**
     * '/groupware/schedule/add'의 GET 메소드를 담당.
     * 사내 일정의 등록 페이지를 전달한다.
     * @param model 데이터를 페이지로 전달할 객체
     * @return '/groupware/schedule/schedule-insert' 페이지
     */
    @GetMapping("/write")
    public String scheduleInsertPage(Model model) {
        List<DepartmentDTO> departmentList = scheduleService.findAllDepartment();

        model.addAttribute("departmentList", departmentList);

        return "groupware/schedule/schedule-insert";
    }

    /**
     * 스케줄 추가를 담당하는 PostMapping 메서드.
     * REST 방식으로 동작한다. 필요한 정보들은 아래와 같다.
     * @param title 스케줄의 제목
     * @param startDate 스케줄의 시작 일자
     * @param endDate 스케줄의 종료 일자
     * @param dateLocation 스케줄이 발생하는 위치
     * @param attendeeList 스케줄에 참가하는 참석자들
     * @param content 스케줄의 내용
     * @param files 스케줄에 첨부된 첨부파일들
     * @param auth 인증 정보
     * @return 실패하는 경우 실패정보, 성공하는 경우 성공정보를 반환
     */
    @ResponseBody()
    @PreAuthorize("hasAnyRole('ROLE_SCHEDULE_MANAGE')")
    @PostMapping(value = "/write", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> scheduleInsert(
            @RequestParam(name = "title") String title,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            @RequestParam(name = "location", required = false) String dateLocation,
            @RequestParam(name = "attendeeList", required = false) List<Integer> attendeeList,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "files", required = false) List<MultipartFile> files,
            Authentication auth) {
        CompanyDateDTO newSchedule = new CompanyDateDTO();

        newSchedule.setDateTitle(title);
        newSchedule.setDateStart(new Date(java.sql.Timestamp.valueOf(startDate).getTime()));
        newSchedule.setDateEnd(new Date(java.sql.Timestamp.valueOf(endDate).getTime()));
        newSchedule.setDateLocation(dateLocation);
        newSchedule.setDateContent(content);
        newSchedule.setEmpCode(Integer.parseInt(auth.getName()));

        /* 업로드 파일 처리 로직 */
        List<FileUploadDTO> fileUploadDTOList = saveUploadFiles(files);
        if(fileUploadDTOList == null) {
            Map<String, String> message = new HashMap<>();
            message.put("message", "파일 업로드 처리 과정에서 오류가 발생했습니다. 지속적으로 발생하면 관리자에게 문의하세요.");
            return ResponseEntity.internalServerError().body(message);
        }

        /* 참가자 처리 로직 */
        List<CompanyDateAttendDTO> companyDateAttendDTOList = new ArrayList<>();

        if(attendeeList != null) {
            for (Integer attendee : attendeeList) {
                CompanyDateAttendDTO newAttendeeDTO = new CompanyDateAttendDTO();
                newAttendeeDTO.setEmpCode(attendee);

                companyDateAttendDTOList.add(newAttendeeDTO);
            }
        }

        int insertedScheduleDateNo = scheduleService.insertSchedule(newSchedule, fileUploadDTOList, companyDateAttendDTOList);

        Map<String, String> messages = new HashMap<>();
        if(insertedScheduleDateNo == 0) {
            messages.put("message", "일정 추가에 실패했습니다. 파일 형식이 잘못되었습니다.");
            if(fileRemoveAll(fileUploadDTOList)) {
                System.out.println("파일을 삭제하는 과정에 문제가 발생했습니다.");
            } else {
                System.out.println("문제가 생긴 일정의 모든 파일을 성공적으로 삭제했습니다.");
            }

            return ResponseEntity.internalServerError().body(messages);
        }

        messages.put("status", "success");
        messages.put("message", "일정 추가에 성공했습니다.");
        messages.put("dateNo", String.valueOf(insertedScheduleDateNo));

        return ResponseEntity.ok(messages);
    }

    /**
     * '/groupware/schedule/{scheduleNo}/modify'의 GET 메소드를 담당.
     * 사내 일정의 상세 페이지를 전달한다.
     * @param scheduleNo 상세 페이지에 표시할 사내 일정 번호
     * @param model 데이터를 페이지로 전달할 객체
     * @return '/groupware/schedule/schedule-modify' 페이지
     */
    @GetMapping("/{scheduleNo}/modify")
    public String scheduleModifyPage(@PathVariable int scheduleNo, Model model) {
        model.addAttribute("schedule", scheduleService.findByDateNo(scheduleNo));
        model.addAttribute("departmentList", scheduleService.findAllDepartment());

        return "groupware/schedule/schedule-modify";
    }

    /**
     * 사내 일정 수정 정보를 받아서 일정을 수정하는 메서드
     * @param scheduleNo 수정할 스케줄의 번호
     * @param title 스케줄의 제목
     * @param startDate 스케줄의 시작 일자
     * @param endDate 스케줄의 종료 일자
     * @param dateLocation 스케줄이 발생하는 위치
     * @param attendeeList 스케줄에 참가하는 참석자들
     * @param content 스케줄의 내용
     * @param uploadFiles 스케줄에 첨부된 첨부파일들 (추가)
     * @param removeFiles 스케줄에 원래 첨부되어 있어서 삭제될 첨부파일들 (삭제)
     * @return 사내 일정의 수정 결과를 반환
     */
    @ResponseBody
    @PostMapping("/{scheduleNo}/modify")
    public ResponseEntity<Map<String, String>> scheduleModify(
            @PathVariable int scheduleNo,
            @RequestParam(name = "title") String title,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
            @RequestParam(name = "location", required = false) String dateLocation,
            @RequestParam(name = "attendeeList", required = false) List<Integer> attendeeList,
            @RequestParam(name = "content", required = false) String content,
            @RequestParam(name = "uploadFiles", required = false) List<MultipartFile> uploadFiles,
            @RequestParam(name = "removeFiles", required = false) List<String> removeFiles,
            Authentication auth
    ) {
        Map<String, String> messages = new HashMap<>();

        if(attendeeList == null) {
            attendeeList = new ArrayList<>();
        }

        if(uploadFiles == null) {
            uploadFiles = new ArrayList<>();
        }

        if(removeFiles == null) {
            removeFiles = new ArrayList<>();
        }

        /* 1. 일정 정보 업데이트할 DTO 객체 추가 */
        CompanyDateDTO modifiedScheduleDTO = new CompanyDateDTO();
        modifiedScheduleDTO.setDateNo(scheduleNo);
        modifiedScheduleDTO.setDateTitle(title);
        modifiedScheduleDTO.setDateStart(new Date(java.sql.Timestamp.valueOf(startDate).getTime()));
        modifiedScheduleDTO.setDateEnd(new Date(java.sql.Timestamp.valueOf(endDate).getTime()));
        modifiedScheduleDTO.setDateLocation(dateLocation);
        modifiedScheduleDTO.setDateContent(content);

        /* 2. 일정 참가자 변경사항 추가 */
        List<CompanyDateAttendDTO> companyDateAttendDTOList = new ArrayList<>();

        for (Integer attendee : attendeeList) {
            CompanyDateAttendDTO newAttendeeDTO = new CompanyDateAttendDTO();
            newAttendeeDTO.setEmpCode(attendee);
            newAttendeeDTO.setDateNo(scheduleNo);

            companyDateAttendDTOList.add(newAttendeeDTO);
        }

        modifiedScheduleDTO.setAttendList(companyDateAttendDTOList);

        /* 3. 일정에 추가로 첨부될 파일들 처리 */
        List<FileUploadDTO> fileUploadDTOList = saveUploadFiles(uploadFiles);
        if(fileUploadDTOList == null) {
            Map<String, String> message = new HashMap<>();
            message.put("message", "파일 업로드 처리 과정에서 오류가 발생했습니다. 지속적으로 발생하면 관리자에게 문의하세요.");
            return ResponseEntity.internalServerError().body(message);
        }

        /* 4. 삭제할 파일 정보 가져오기 */
        List<FileUploadDTO> removeFileDTOList = new ArrayList<>();

        for (String removeFile : removeFiles) {
            removeFileDTOList.add(scheduleService.findUploadFileByFileRename(removeFile));
        }

        /* 5. 일정 정보 업데이트 (트랜잭션) */
        if(!scheduleService.updateSchedule(
                modifiedScheduleDTO, fileUploadDTOList, companyDateAttendDTOList, removeFiles, auth
        )) {
            messages.put("message", "일정 등록에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }

        /* 6. 파일 삭제 */
        if(fileRemoveAll(removeFileDTOList)) {
            messages.put("message", "파일 삭제에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }

        /* 7. 검증 후 결과 반환 */
        messages.put("status", "success");
        messages.put("message", "일정 수정에 성공했습니다.");
        messages.put("dateNo", String.valueOf(modifiedScheduleDTO.getDateNo()));
        return ResponseEntity.ok(messages);
    }

    /**
     * REST.
     * {@link PathVariable}로 전달받은 일정 번호의 일정을 삭제하는 컨트롤러 메서드.
     * @param scheduleNo 삭제할 일정의 번호
     * @return 삭제 후 이동해야할 페이지 주소
     */
    @ResponseBody
    @PostMapping("/{scheduleNo}/remove")
    public ResponseEntity<Map<String, String>> scheduleDelete(@PathVariable int scheduleNo, Authentication auth) {
        Map<String, String> messages = new HashMap<>();
        scheduleService.deleteScheduleByDateNo(scheduleNo, auth);

        messages.put("status", "success");
        messages.put("message", "일정 삭제에 성공했습니다.");

        return ResponseEntity.ok(messages);
    }

    /**
     * 전달받은 날짜 구간의 일정을 조회해 리스트로 반환하는 컨트롤러 메서드 (POST, REST)
     * @param startDate 일정 검색에 사용할 시작 일자
     * @param endDate 일정 검색에 사용할 종료 일자
     * @param auth 로그인한 사원의 정보
     * @return 조회된 일정 리스트
     */
    @ResponseBody
    @PostMapping("/getSchedule")
    public ResponseEntity<List<FullCalendarDTO>> getSchedule(
            @RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate startDate,
            @RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDate endDate,
            Authentication auth
    ) {
        List<CompanyDateDTO> companyDateDTOList = scheduleService.findScheduleByDate(startDate, endDate, auth.getName());

        List<FullCalendarDTO> fullCalendarDTOList = new ArrayList<>();
        for (CompanyDateDTO companyDateDTO : companyDateDTOList) {
            fullCalendarDTOList.add(companyDateDTO.toFullCalendarDTO());
        }

        return ResponseEntity.ok(fullCalendarDTOList);
    }

    /**
     * '/groupware/schedule/getDepartmentMembers'의 POST 메서드를 담당.
     * REST 방식으로 부서원 정보를 응답해준다.
     * @param dept 부서 정보
     * @return 부서원 정보가 담긴 {@link Map} 객체
     */
    @ResponseBody()
    @PostMapping(value = "/getDepartmentMembers", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> getDepartmentMembers(@RequestParam(name = "dept") String dept) {
        List<EmployeeDTO> employeeList = scheduleService.findEmployeeByDeptNo(dept);

        Map<String, String> data = new HashMap<>();

        for (EmployeeDTO employee : employeeList) {
            if("N".equals(employee.getEmpLeaveYn())) {
                data.put(String.valueOf(employee.getEmpCode()), employee.getEmpName());
            }
        }

        return ResponseEntity.ok(data);
    }

    /**
     * REST 방식으로 일정에 포함된 참석자들을 전달하는 메서드
     * @param scheduleNo 참석자를 조회하고자 하는 일정 번호
     * @return Map 형태로 된 참석자 정보가 ok 코드와 함께 전달 됨
     */
    @ResponseBody
    @PostMapping(value = "/getAttendee", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> getAttendeeByScheduleNo(@RequestParam("scheduleNo") Integer scheduleNo) {
        List<EmployeeDTO> employeeDTOList = scheduleService.findAttendeeByDateNo(scheduleNo);

        Map<String, String> data = new HashMap<>();

        for (EmployeeDTO employeeDTO : employeeDTOList) {
            data.put(String.valueOf(employeeDTO.getEmpCode()), employeeDTO.getEmpName());
        }

        return ResponseEntity.ok(data);
    }

    /**
     * 매개변수로 전달된 문자열 객체가 비어있는지 확인한다.
     * @param str 확인할 문자열 객체
     * @return 문자열이 null이거나 비어있으면 true, 비어있지 않으면 false.
     */
    public boolean isEmptyString(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 파일에 대한 정보를 읽고 파일을 업로드 경로에 쓰고 DTO에 취합해서 DTO List를 반환하는 메서드
     * @param files Multipart FormData 형식으로 넘겨받은 파일 객체 리스트
     * @return 각종 정보를 저장한 {@link FileUploadDTO}의 리스트 형태를 반환
     */
    private List<FileUploadDTO> saveUploadFiles(List<MultipartFile> files) {
        if (files == null) {
            return new ArrayList<>();
        }

        String rootLocation = new File("src/main/resources/static").getAbsolutePath();
        String fileLocation = "/upload/schedule";
        String fileUploadRealLocation = rootLocation + fileLocation;
        File directory = new File(fileUploadRealLocation);

        System.out.println("fileUploadRealLocation = " + fileUploadRealLocation);

        if(!directory.exists()) {
            if(!directory.mkdirs()) {
                return null;
            }
        }

        List<FileUploadDTO> fileUploadDTOList = new ArrayList<>();

        for (MultipartFile file : files) {
            if(file.getSize() > 0) {
                try {
                    String originalFileName = file.getOriginalFilename();

                    /* 예외상황 핸들링 */
                    if(originalFileName == null) {
                        System.out.println("업로드 된 파일의 파일 이름이 없거나 잘못된 형식입니다.");
                        throw new Exception("파일 이름 또는 잘못된 형식의 파일");
                    }
                    String ext = originalFileName.substring(originalFileName.lastIndexOf("."));
                    String savedFileName = UUID.randomUUID().toString().replace("-", "") + ext;

                    /* 파일 쓰기 */
                    file.transferTo(new File(fileUploadRealLocation + "/" + savedFileName));
                    System.out.println(fileUploadRealLocation + "/" + savedFileName);

                FileUploadDTO fileUploadDTO = new FileUploadDTO();

                fileUploadDTO.setFileName(originalFileName);
                fileUploadDTO.setFileLocation(fileLocation);
                fileUploadDTO.setFileUploadBoard("schedule");
                fileUploadDTO.setFileUploadDate(new Date(new java.util.Date().getTime()));
                fileUploadDTO.setFileRename(savedFileName);

                fileUploadDTOList.add(fileUploadDTO);

                } catch (Exception e) {
                    System.out.println("파일을 쓰는 과정에 문제가 발생했습니다.");
                    System.out.println(e.getLocalizedMessage());

                    if(fileRemoveAll(fileUploadDTOList)) {
                        System.out.println("파일을 삭제하는 과정에 문제가 발생했습니다.");
                    } else {
                        System.out.println("문제가 생긴 일정의 모든 파일을 성공적으로 삭제했습니다.");
                    }
                }
            }
        }

        return fileUploadDTOList;
    }

    /**
     * 매개변수로 들어온 파일 정보로 모든 파일을 삭제해주는 메서드
     * @param files 삭제할 파일 리스트
     * @return 성공적으로 모두 삭제되면 false, 실패시 true 반환
     */
    private boolean fileRemoveAll(List<FileUploadDTO> files) {
        if(files.isEmpty()) {
            return false;
        }

        String rootLocation = new File("src/main/resources/static").getAbsolutePath();

        int result = 0;
        for (FileUploadDTO file : files) {
            File removeFile = new File(rootLocation + file.getFileLocation() + "/" + file.getFileRename());

            result += removeFile.delete() ? 1 : 0;
        }

        return result != files.size();
    }
}
