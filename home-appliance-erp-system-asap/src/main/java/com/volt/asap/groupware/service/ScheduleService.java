package com.volt.asap.groupware.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.*;
import com.volt.asap.groupware.entity.CompanyDate;
import com.volt.asap.groupware.entity.CompanyDateAttend;
import com.volt.asap.groupware.entity.FileUpload;
import com.volt.asap.groupware.repository.CompanyDateAttendRepository;
import com.volt.asap.groupware.repository.CompanyDateRepository;
import com.volt.asap.groupware.repository.DepartmentMemberRepository;
import com.volt.asap.groupware.repository.FileUploadRepository;
import com.volt.asap.hrm.entity.DepartmentMember;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 사내 일정을 위한 서비스
 */
@Service
public class ScheduleService {

    private final CompanyDateRepository companyDateRepository;
    private final DepartmentMemberRepository departmentMemberRepository;
    private final FileUploadRepository fileUploadRepository;
    private final CompanyDateAttendRepository companyDateAttendRepository;
    private final ModelMapper modelMapper;

    /* Autowired를 사용한 의존성 주입 */
    @Autowired
    public ScheduleService(CompanyDateRepository companyDateRepository, DepartmentMemberRepository departmentMemberRepository, FileUploadRepository fileUploadRepository, CompanyDateAttendRepository companyDateAttendRepository, ModelMapper modelMapper) {
        this.companyDateRepository = companyDateRepository;
        this.departmentMemberRepository = departmentMemberRepository;
        this.fileUploadRepository = fileUploadRepository;
        this.companyDateAttendRepository = companyDateAttendRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * 쿼리로 검색된 엔티티의 개수를 반환하는 메서드
     * @param parameterMap 쿼리가 포함되어 있는 {@link Map}
     * @return 쿼리로 검색된 엔티티의 개수
     */
    public int countScheduleByQueries(Map<String, String> parameterMap) {
        if (parameterMap == null) {

            return companyDateRepository.countByDeleteStatus("N");
        } else {
            /* 검색어 입력을 통한 부분 조회 */
            Date startDate = toSqlDate(parameterMap.get("startDate"));
            Date endDate = toSqlDate(LocalDate.parse(parameterMap.get("endDate")).plusDays(1).toString());
            String searchWord = parameterMap.get("searchWord");

            if(startDate != null && endDate != null) {
                if("".equals(searchWord)) {
                    /* 날짜 검색 */
                    return companyDateRepository.countByDateStartGreaterThanAndDateEndLessThanAndDeleteStatus(
                            startDate, endDate, "N"
                    );
                } else {
                    /* 날짜, 검색어 */
                    return companyDateRepository.countByDateStartGreaterThanAndDateEndLessThanAndDateTitleContainingAndDeleteStatus(
                            startDate, endDate, parameterMap.get("searchWord"), "N"
                    );
                }
            } else {
                /* 검색어 */
                return companyDateRepository.countByDateTitleContainingAndDeleteStatus(
                        parameterMap.get("searchWord"), "N"
                );
            }
        }
    }

    /**
     * 쿼리로 검색된 엔티티를 반환하는 메서드
     * 반환은 엔티티가 아닌 DTO 객체를 반환한다.
     * 조건은 총 4가지 경우가 있으며 다음과 같다.
     * 1. 전체 조회
     * 2. 날짜로 검색된 조회
     * 3. 검색어로 검색된 조회
     * 4. 날짜와 검색어로 검색된 조회
     * @param selectCriteria 검색에 사용되는 각종 설정들과 검색어가 포함된 객체 {@link CustomSelectCriteria}
     * @return 쿼리에 의해 검색되어 List<DTO> 형태로 변환된 엔티티들
     */
    public List<CompanyDateDTO> findScheduleByQueries(CustomSelectCriteria selectCriteria) {

        /* Pageable 객체는 1페이지를 0번으로 인식 */
        int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();

        Pageable paging = PageRequest.of(index, count, Sort.by("dateNo"));

        List<CompanyDate> scheduleList;

        if(selectCriteria.getSearchValueMap() == null) {
            /* 전체 조회 */
            scheduleList = companyDateRepository.findByDeleteStatusOrderByDateStartDescDateEndDesc("N", paging);
        } else {
            /* 검색어 입력을 통한 부분 조회 */
            java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
            java.sql.Date endDate = toSqlDate(LocalDate.parse(
                    selectCriteria.getSearchValueMap().get("endDate")
            ).plusDays(1).toString());
            String searchWord = selectCriteria.getSearchValueMap().get("searchWord");

            if(startDate != null && endDate != null) {
                if("".equals(searchWord)) {
                    /* 날짜 검색 */
                    scheduleList = companyDateRepository.findByDateStartGreaterThanAndDateEndLessThanAndDeleteStatusOrderByDateStartDescDateEndDesc(
                            startDate, endDate, "N", paging
                    );
                } else {
                    /* 날짜, 검색어 */
                    scheduleList = companyDateRepository.findByDateStartGreaterThanAndDateEndLessThanAndDateTitleContainingAndDeleteStatusOrderByDateStartDescDateEndDesc(
                            startDate, endDate, selectCriteria.getSearchValueMap().get("searchWord"), "N", paging
                    );
                }
            } else {
                /* 검색어 */
                scheduleList = companyDateRepository.findByDateTitleContainingAndDeleteStatusOrderByDateStartDescDateEndDesc(
                        selectCriteria.getSearchValueMap().get("searchWord"), "N", paging
                );
            }
        }
        return scheduleList.stream().map(schedule -> modelMapper.map(schedule, CompanyDateDTO.class)).collect(Collectors.toList());
    }

    /**
     * 사내 일정 정보가 담긴 DTO를 반환해주는 서비스 메소드
     * @param scheduleNo 사내 일정 번호
     * @return 매칭 성공시 사내 일정 정보, 매칭 실패시 null
     */
    public CompanyDateDTO findByDateNo(int scheduleNo) {
        CompanyDate schedule = companyDateRepository.findByDateNoAndDeleteStatus(scheduleNo, "N");

        if (schedule == null) {
            return null;
        }

        return modelMapper.map(schedule, CompanyDateDTO.class);
    }

    /**
     * 부서 정보가 담긴 DTO를 리스트로 반환해주는 서비스 메소드
     * @return 부서 정보가 담긴 {@link DepartmentDTO} 리스트
     */
    public List<DepartmentDTO> findAllDepartment() {
        List<DepartmentMember> departmentList = departmentMemberRepository.findAll();

        return departmentList.stream().map(department -> modelMapper.map(department, DepartmentDTO.class)).collect(Collectors.toList());
    }

    /**
     * 매개변수로 입력받은 부서코드를 사용해 부서에 속한 부서원들의 리스트를 반환하는 서비스 메소드
     * @param dept 부서코드
     * @return 해당 부서의 부서원인 {@link EmployeeDTO} 리스트
     */
    public List<EmployeeDTO> findEmployeeByDeptNo(String dept) {
        DepartmentMember departmentMember = departmentMemberRepository.findByCode(dept);

        return departmentMember.getEmployee().stream().map(employee -> modelMapper.map(employee, EmployeeDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * {@link String} 으로 되어있는 날짜를 {@link java.sql.Date} 형태로 변환해주는 메서드
     * @param dateString {@link String} 날짜
     * @return dateString이 null 이거나 비어있는 문자열인 경우 null 반환.
     *         그 외에는 {@link java.sql.Date} 으로 변환된 객체 반환.
     */
    private java.sql.Date toSqlDate(String dateString) {
        java.sql.Date endDate = null;

        if(dateString != null && !"".equals(dateString)) {
            endDate = java.sql.Date.valueOf(dateString);
        }

        return endDate;
    }

    /**
     * 영속성 컨텍스트에 스케줄을 추가하고 스케줄의 ID를 반환하는 메서드
     * @param newScheduleDTO 추가할 스케줄의 정보를 담고있는 DTO 객체
     * @return 추가된 스케줄의 ID
     */
    @Transactional
    public int insertSchedule(
            CompanyDateDTO newScheduleDTO,
            List<FileUploadDTO> fileUploadDTOList,
            List<CompanyDateAttendDTO> companyDateAttendDTOList) {

        /* 영속성 컨텍스트에 스케줄 엔티티 추가 */
        int insertedDateNo = companyDateRepository.save(modelMapper.map(newScheduleDTO, CompanyDate.class)).getDateNo();

        /* 영속성 컨텍스트에 첨부파일 엔티티 추가 */
        List<FileUpload> fileUploadList = fileUploadDTOList.stream().map(fileUploadDTO -> {
                    /* 람다 구문내에서 스케줄 번호 추가 */
                    fileUploadDTO.setDateNo(insertedDateNo);
                    return modelMapper.map(fileUploadDTO, FileUpload.class);
                }).collect(Collectors.toList());

        if(fileUploadRepository.saveAll(fileUploadList).size() != fileUploadDTOList.size()) {
            return 0;
        }

        /* 영속성 컨텍스트에 참석자 엔티티 추가 */
        List<CompanyDateAttend> companyDateAttendList = companyDateAttendDTOList.stream().map(companyDateAttendDTO -> {
                    companyDateAttendDTO.setDateNo(insertedDateNo);
                    return modelMapper.map(companyDateAttendDTO, CompanyDateAttend.class);
                }).collect(Collectors.toList());

        if(companyDateAttendRepository.saveAll(companyDateAttendList).size() != companyDateAttendDTOList.size()) {
            return 0;
        }

        return insertedDateNo;
    }

    /**
     * 매개변수로 받은 일정 번호에 해당하는 참석자 리스트를 반환하는 서비스 메서드
     * @param scheduleNo 찾을 일정 번호
     * @return 참석자 리스트
     */
    public List<EmployeeDTO> findAttendeeByDateNo(Integer scheduleNo) {
        List<CompanyDateAttend> scheduleAttendeeList = companyDateAttendRepository.findByDateNo(scheduleNo);

        return scheduleAttendeeList.stream().map(
                scheduleAttendee -> modelMapper.map(scheduleAttendee.getEmployee(), EmployeeDTO.class)
        ).collect(Collectors.toList());
    }

            /**
     * 사내 일정 정보를 업데이트하는 서비스 메서드
     *
     * @param modifiedScheduleDTO      업데이트할 일정 정보가 담긴 DTO
     * @param fileUploadDTOList        업데이트 될 일정에 추가될 파일들의 리스트
     * @param companyDateAttendDTOList 업데이트 될 일정에 수정사항이 반영될 참석자 리스트
     * @param removeFiles              업데이트 될 일정에 원래 있던 파일 중 삭제될 파일의 이름 리스트
     * @return 업데이트 성공시 true, 실패시 false를 반환
     */
    @Transactional
    public boolean updateSchedule(CompanyDateDTO modifiedScheduleDTO, List<FileUploadDTO> fileUploadDTOList,
                                  List<CompanyDateAttendDTO> companyDateAttendDTOList, List<String> removeFiles, Authentication auth) {
        CompanyDate schedule = companyDateRepository.findByDateNoAndDeleteStatus(modifiedScheduleDTO.getDateNo(), "N");

        /* 일정 관리 권한이 있거나 작성자의 경우 수정 가능 */
        if(auth.getAuthorities().stream().noneMatch(a -> "ROLE_SCHEDULE_MANAGE".equals(a.getAuthority()))) {
            if(schedule.getAuthor().getEmpCode() != Integer.parseInt(auth.getName())) {
                System.out.println("?????");
                return false;
            }
        }

        schedule.setDateTitle(modifiedScheduleDTO.getDateTitle());
        schedule.setDateLocation(modifiedScheduleDTO.getDateLocation());
        schedule.setDateStart(modifiedScheduleDTO.getDateStart());
        schedule.setDateEnd(modifiedScheduleDTO.getDateEnd());
        schedule.setDateContent(modifiedScheduleDTO.getDateContent());

        companyDateAttendRepository.deleteAllByDateNo(modifiedScheduleDTO.getDateNo());

        List<CompanyDateAttend> attendList = companyDateAttendDTOList.stream().map(
                    companyDateAttendDTO -> modelMapper.map(companyDateAttendDTO, CompanyDateAttend.class
                )).collect(Collectors.toList());

        if(companyDateAttendRepository.saveAll(attendList).size() != companyDateAttendDTOList.size()) {
            System.out.println("ScheduleService.updateSchedule");
            System.out.println("attendList");
            return false;
        }

        List<FileUpload> fileUploadList = fileUploadDTOList.stream().map(fileUploadDTO -> {
            /* 람다 구문내에서 스케줄 번호 추가 */
            fileUploadDTO.setDateNo(modifiedScheduleDTO.getDateNo());
            return modelMapper.map(fileUploadDTO, FileUpload.class);
        }).collect(Collectors.toList());

        if(fileUploadRepository.saveAll(fileUploadList).size() != fileUploadDTOList.size()) {
            System.out.println("ScheduleService.updateSchedule");
            System.out.println("fileUploadList");
            return false;
        }

        for (String removeFile : removeFiles) {
            fileUploadRepository.deleteByFileRename(removeFile);
        }

        return true;
    }

    /**
     * 파일 이름으로 {@link FileUpload} DTO를 반환하는 서비스 메서드
     * @param fileRename 찾아야 할 파일의 이름
     * @return 검색된 {@link FileUpload} DTO 객체
     */
    public FileUploadDTO findUploadFileByFileRename(String fileRename) {
        FileUpload fileUpload = fileUploadRepository.findByFileRename(fileRename);

        return modelMapper.map(fileUpload, FileUploadDTO.class);
    }

    /**
     * 매개변수로 전달받은 일정 번호에 해당하는 일정을 삭제처리하는 서비스 메서드
     * @param scheduleNo 삭제처리 할 일정 번호
     */
    @Transactional
    public void deleteScheduleByDateNo(int scheduleNo, Authentication auth) {
        CompanyDate schedule = companyDateRepository.findByDateNoAndDeleteStatus(scheduleNo, "N");

        /* 일정 관리 권한이 있거나 작성자의 경우 삭제 가능 */
        if(auth.getAuthorities().stream().noneMatch(a -> "ROLE_SCHEDULE_MANAGE".equals(a.getAuthority()))) {
            if(schedule.getAuthor().getEmpCode() != Integer.parseInt(auth.getName())) {
                return;
            }
        }

        schedule.setDeleteStatus("Y");
    }

    public List<CompanyDateDTO> findScheduleByDate(LocalDate startDate, LocalDate endDate, String empCode) {
        Date convertedStartDate = Date.valueOf(startDate.toString());
        Date convertedEndDate = Date.valueOf(endDate.toString());
        int convertedEmpCode = Integer.parseInt(empCode);

        List<CompanyDate> companyDateList = companyDateRepository
                .findByDateStartGreaterThanAndDateEndLessThanAndAttendListEmpCodeAndDeleteStatus(
                        convertedStartDate, convertedEndDate, convertedEmpCode, "N"
                );

        return companyDateList.stream().map(companyDate -> modelMapper.map(companyDate, CompanyDateDTO.class))
                .collect(Collectors.toList());
    }
}
