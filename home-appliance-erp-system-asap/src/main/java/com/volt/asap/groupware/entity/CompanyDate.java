package com.volt.asap.groupware.entity;

import com.volt.asap.hrm.entity.Employee;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * CompanyDate Entity
 */
@DynamicInsert
@Entity(name = "CompanyDate")
@Table(name = "TBL_COMPANY_DATE")
@SequenceGenerator(
        name = "DATE_NO_SEQ_GENERATOR",
        sequenceName = "SEQ_COMPANY_DATE_CODE",
        initialValue = 6,
        allocationSize = 1
)
public class CompanyDate {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "DATE_NO_SEQ_GENERATOR"
    )
    @Column(name = "DATE_NO", nullable = false)
    private int dateNo;                             // DATE_NO

    @Column(name = "DATE_TITLE", nullable = false)
    private String dateTitle;                          // DATE_TITLE

    @Column(name = "DATE_LOCATION")
    private String dateLocation;                       // DATE_LOCATION

    @Column(name = "DATE_START", nullable = false)
    private java.sql.Date dateStart;                          // DATE_START

    @Column(name = "DATE_END", nullable = false)
    private java.sql.Date dateEnd;                            // DATE_END

    @Lob
    @Column(name = "DATE_CONTENT", columnDefinition = "CLOB")
    private String dateContent;                               // DATE_CONTENT

    @Column(name = "DELETE_STATUS")
    private String deleteStatus;

    @Column(name = "EMP_CODE", nullable = false)
    private int empCode;                            // EMP_CODE

    @ManyToOne
    @JoinColumn(name = "EMP_CODE", updatable = false, insertable = false)
    private Employee author;

    @OneToMany
    @JoinColumn(name = "DATE_NO")
    private List<CompanyDateAttend> attendList;

    @OneToMany
    @JoinColumn(name = "DATE_NO")
    private List<FileUpload> attachedFileList;

    public CompanyDate() {
    }

    public CompanyDate(int dateNo, String dateTitle, String dateLocation, Date dateStart, Date dateEnd, String dateContent, String deleteStatus, int empCode, Employee author, List<CompanyDateAttend> attendList, List<FileUpload> attachedFileList) {
        this.dateNo = dateNo;
        this.dateTitle = dateTitle;
        this.dateLocation = dateLocation;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dateContent = dateContent;
        this.deleteStatus = deleteStatus;
        this.empCode = empCode;
        this.author = author;
        this.attendList = attendList;
        this.attachedFileList = attachedFileList;
    }

    public int getDateNo() {
        return dateNo;
    }

    public void setDateNo(int dateNo) {
        this.dateNo = dateNo;
    }

    public String getDateTitle() {
        return dateTitle;
    }

    public void setDateTitle(String dateTitle) {
        this.dateTitle = dateTitle;
    }

    public String getDateLocation() {
        return dateLocation;
    }

    public void setDateLocation(String dateLocation) {
        this.dateLocation = dateLocation;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDateContent() {
        return dateContent;
    }

    public void setDateContent(String dateContent) {
        this.dateContent = dateContent;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public Employee getAuthor() {
        return author;
    }

    public void setAuthor(Employee author) {
        this.author = author;
    }

    public List<CompanyDateAttend> getAttendList() {
        return attendList;
    }

    public void setAttendList(List<CompanyDateAttend> attendList) {
        this.attendList = attendList;
    }

    public List<FileUpload> getAttachedFileList() {
        return attachedFileList;
    }

    public void setAttachedFileList(List<FileUpload> attachedFileList) {
        this.attachedFileList = attachedFileList;
    }

    @Override
    public String toString() {
        return "CompanyDate{" +
                "dateNo=" + dateNo +
                ", dateTitle='" + dateTitle + '\'' +
                ", dateLocation='" + dateLocation + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", dateContent='" + dateContent + '\'' +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", empCode=" + empCode +
                ", author=" + author +
                ", attendList=" + attendList +
                ", attachedFileList=" + attachedFileList +
                '}';
    }
}