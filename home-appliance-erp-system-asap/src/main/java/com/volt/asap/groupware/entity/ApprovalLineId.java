package com.volt.asap.groupware.entity;

import java.io.Serializable;
import java.util.Objects;

public class ApprovalLineId implements Serializable {
    private static final long serialVersionUID = 3751673116016706706L;
    private int empCode;
    private int draftNo;

    public ApprovalLineId() {
    }

    public ApprovalLineId(int empCode, int draftNo) {
        this.empCode = empCode;
        this.draftNo = draftNo;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(int draftNo) {
        this.draftNo = draftNo;
    }

    @Override
    public String toString() {
        return "ApprovalLineId{" +
                "empCode=" + empCode +
                ", draftNo=" + draftNo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApprovalLineId that = (ApprovalLineId) o;
        return empCode == that.empCode && draftNo == that.draftNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(empCode, draftNo);
    }
}
