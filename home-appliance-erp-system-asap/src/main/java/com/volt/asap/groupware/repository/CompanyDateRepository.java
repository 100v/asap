package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.CompanyDate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;

public interface CompanyDateRepository extends JpaRepository<CompanyDate, Integer> {

    /**
     * ID로 검색한 엔티티의 개수를 반환하는 메서드
     * 접속한 사용자에 해당되는 모든 일정을 조회할 때 사용
     * @param empCode 사원 코드(ID)
     * @return ID로 조회된 엔티티의 개수
     */
    int countByDeleteStatus(String deleteStatus);


    /**
     * ID로 검색한 엔티티 리스트를 반환하는 메서드. 접속한 사용자에 해당되는 모든 일정을 조회할 때 사용
     * @param paging 페이징 객체 {@link Pageable}
     * @return ID로 조회된 엔티티 리스트
     */
    List<CompanyDate> findByDeleteStatusOrderByDateStartDescDateEndDesc(String deleteStatus, Pageable paging);

    /**
     * 날짜와 ID로 검색한 엔티티의 개수를 반환하는 메서드. 해당하는 날짜 범위안에 포함되는 것만 검색됨
     * @param startDate 검색할 날짜 범위 중 시작 날짜
     * @param endDate 검색할 날짜 범위 중 종료 날짜
     * @return 날짜와 ID로 조회된 엔티티의 개수
     */
    int countByDateStartGreaterThanAndDateEndLessThanAndDeleteStatus(Date startDate, Date endDate, String deleteStatus);

    /**
     * 날짜와 ID로 검색한 엔티티 리스트를 반환하는 메서드. 해당하는 날짜 범위안에 포함되는 것만 검색됨
     * @param startDate 검색할 날짜 범위 중 시작 날짜
     * @param endDate 검색할 날짜 범위 중 종료 날짜
     * @param paging 페이징 객체 {@link Pageable}
     * @return 날짜와 ID로 조회된 엔티티 리스트
     */
    List<CompanyDate> findByDateStartGreaterThanAndDateEndLessThanAndDeleteStatusOrderByDateStartDescDateEndDesc(Date startDate, Date endDate, String deleteStatus, Pageable paging);

    /**
     * 날짜로 검색한 사내 일정 리스트를 반환하는 메서드.
     * @param startDate 검색할 날짜 범위 중 시작날짜
     * @param endDate 검색할 날짜 범위 중 종료 날짜
     * @param deleteStatus 삭제되지 않은 일정이면 "N", 삭제된 일정이면 "Y"
     * @return 날자로 검색된 사내 일정 리스트
     */
    List<CompanyDate> findByDateStartGreaterThanAndDateEndLessThanAndAttendListEmpCodeAndDeleteStatus(Date startDate, Date endDate, int empCode, String deleteStatus);

    /**
     * 검색어와 ID로 검색한 엔티티의 개수를 반환하는 메서드.
     * @param searchWord 검색어 (제목)
     * @return 검색어와 ID로 조회된 엔티티의 개수
     */
    int countByDateTitleContainingAndDeleteStatus(String searchWord, String deleteStatus);

    /**
     * 검색어와 ID로 검색한 엔티티 리스트를 반환하는 메서드.
     * @param searchWord 검색어 (제목)
     * @param paging 페이징 객체 {@link Pageable}
     * @return 검색어와 ID로 조회된 엔티티 리스트
     */
    List<CompanyDate> findByDateTitleContainingAndDeleteStatusOrderByDateStartDescDateEndDesc(String searchWord, String deleteStatus, Pageable paging);

    /**
     * 시작 날짜, 종료 날짜, 제목, ID 로 검색한 엔티티의 개수를 반환하는 메서드
     * @param startDate 시작 날짜
     * @param endDate 종료 날짜
     * @param dateTitle 제목
     * @return 검색된 엔티티의 개수
     */
    int countByDateStartGreaterThanAndDateEndLessThanAndDateTitleContainingAndDeleteStatus(
            java.sql.Date startDate, java.sql.Date endDate, String dateTitle, String deleteStatus
    );

    /**
     * 시작 날짜, 종료 날짜, 제목, ID 로 검색한 엔티티를 반환하는 메서드.
     * 페이징 객체의 설정에 따라 페이징처리도 한다.
     * @param startDate 시작 날짜
     * @param endDate 종료 날짜
     * @param dateTitle 제목
     * @param paging 페이징 객체. 각종 페이징 설정들이 들어 있음 {@link Pageable}
     * @return 검색된 엔티티를 페이징처리 한 리스트
     */
    List<CompanyDate> findByDateStartGreaterThanAndDateEndLessThanAndDateTitleContainingAndDeleteStatusOrderByDateStartDescDateEndDesc(
            java.sql.Date startDate, java.sql.Date endDate, String dateTitle, String deleteStatus, Pageable paging
    );

    /**
     * 사내 일정 번호로 일정정보 엔티티를 찾아주는 메서드
     * @param dateNo 사내 일정 번호
     * @return 사내 일정 엔티티
     */
    CompanyDate findByDateNoAndDeleteStatus(int dateNo, String deleteStatus);

}
