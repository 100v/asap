package com.volt.asap.groupware.dto;

public class DraftFormDTO {
    private int draftFormNo;                             // DRAFT_FORM_NO
    private String draftFormTitle;                       // DRAFT_FORM_TITLE
    private String draftFormContent;                     // DRAFT_FORM_CONTENT
    private String draftFormDivision;                    // DRAFT_FORM_DIVISION

    public DraftFormDTO() {
    }

    public DraftFormDTO(int draftFormNo, String draftFormTitle, String draftFormContent, String draftFormDivision) {
        this.draftFormNo = draftFormNo;
        this.draftFormTitle = draftFormTitle;
        this.draftFormContent = draftFormContent;
        this.draftFormDivision = draftFormDivision;
    }

    public int getDraftFormNo() {
        return draftFormNo;
    }

    public void setDraftFormNo(int draftFormNo) {
        this.draftFormNo = draftFormNo;
    }

    public String getDraftFormTitle() {
        return draftFormTitle;
    }

    public void setDraftFormTitle(String draftFormTitle) {
        this.draftFormTitle = draftFormTitle;
    }

    public String getDraftFormContent() {
        return draftFormContent;
    }

    public void setDraftFormContent(String draftFormContent) {
        this.draftFormContent = draftFormContent;
    }

    public String getDraftFormDivision() {
        return draftFormDivision;
    }

    public void setDraftFormDivision(String draftFormDivision) {
        this.draftFormDivision = draftFormDivision;
    }

    @Override
    public String toString() {
        return "DraftFormDTO{" +
                "draftFormNo=" + draftFormNo +
                ", draftFormTitle='" + draftFormTitle + '\'' +
                ", draftFormContent='" + draftFormContent + '\'' +
                ", draftFormDivision='" + draftFormDivision + '\'' +
                '}';
    }
}
