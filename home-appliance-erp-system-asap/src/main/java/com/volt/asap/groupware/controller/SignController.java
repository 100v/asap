package com.volt.asap.groupware.controller;

import com.volt.asap.groupware.dto.EmployeeDTO;
import com.volt.asap.groupware.service.SignService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/groupware/sign")
public class SignController {
    private final SignService signService;

    public SignController(SignService signService) {
        this.signService = signService;
    }

    /**
     * 서명 조회 페이지를 담당하는 컨트롤러 메소드 (GET)
     * @param auth 로그인한 사용자의 정보가 담긴 {@link Authentication} 객체
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @return 로드 성공시 사원 정보와 함께 서명 조회 페이지를 전달, 로드 실패시 로드 실패 메시지 전달
     */
    @GetMapping("")
    public String signInquiry(Authentication auth, Model model) {
        EmployeeDTO employeeDTO = signService.findEmployeeById(Integer.parseInt(auth.getName()));

        if(employeeDTO == null) {
            model.addAttribute("message", "내 정보 로드에 실패했습니다.");
            return "groupware/sign/sign-inquiry";
        }

        model.addAttribute("myInfo", employeeDTO);

        return "groupware/sign/sign-inquiry";
    }

    /**
     * 서명파일 수정을 담당하는 컨트롤러 메소드 (POST, REST)
     * @param sign base64로 변환된 서명 파일
     * @param auth 로그인한 사용자의 정보가 담긴 {@link Authentication} 객체
     * @return 서명파일 수정 성공시 성공 메시지, 수정 실패시 실패 메시지 전달
     */
    @ResponseBody
    @PostMapping(value = "/modify", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> modifySign(@RequestParam("sign") String sign, Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        if(!signService.updateSignById(sign, Integer.parseInt(auth.getName()))) {
            messages.put("message", "잘못된 접근입니다.");
            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("message", "서명파일 수정에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }

    /**
     * 서명파일 삭제를 담당하는 컨트롤러 메소드 (POST, REST)
     * @param auth 로그인한 사용자의 정보가 담긴 {@link Authentication} 객체
     * @return 서명파일 삭제 성공시 성공 메시지, 삭제 실패시 실패 메시지 전달
     */
    @ResponseBody
    @PostMapping(value = "/delete", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> deleteSign(Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        if(!signService.deleteSingById(Integer.parseInt(auth.getName()))) {
            messages.put("message", "잘못된 접근입니다.");
            return ResponseEntity.badRequest().body(messages);
        }
        
        messages.put("message", "서명파일 삭제에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }
}
