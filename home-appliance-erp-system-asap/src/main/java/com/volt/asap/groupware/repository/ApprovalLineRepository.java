package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.ApprovalLine;
import com.volt.asap.groupware.entity.ApprovalLineId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApprovalLineRepository extends JpaRepository<ApprovalLine, ApprovalLineId> {

    void deleteAllByDraftNo(int draftNo);

    List<ApprovalLine> findByDraftNoOrderByApprovalOrder(int draftNo);

    ApprovalLine findByDraftNoAndEmpCodeOrderByApprovalOrder(int draftNo, int empCode);

    List<ApprovalLine> findByDraftNoAndApprovalStatusOrderByApprovalOrder(int draftNo, String n);
}
