package com.volt.asap.groupware.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.ApprovalLineDTO;
import com.volt.asap.groupware.dto.DepartmentDTO;
import com.volt.asap.groupware.dto.DraftDTO;
import com.volt.asap.groupware.dto.DraftFormDTO;
import com.volt.asap.groupware.service.DraftFormService;
import com.volt.asap.groupware.service.DraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 기안서 관리 페이지를 위한 컨트롤러
 */
@Controller
@RequestMapping("/groupware/draft")
public class DraftController {
    private final DraftService draftService;
    private final DraftFormService draftFormService;

    @Autowired
    public DraftController(DraftService draftService, DraftFormService draftFormService) {
        this.draftService = draftService;
        this.draftFormService = draftFormService;
    }

    /**
     * 기안서 목록 페이지를 담당하는 컨트롤러 메소드 (GET)
     * @param page 현재 페이지 번호
     * @param draftStatus 검색할 기안서 상태
     * @param startDate 검색할 기간 중 시작 날짜
     * @param endDate 검색할 기간 중 종료 날짜
     * @param author 검색할 기안서 작성자
     * @param draftType 검색할 기안서 구분
     * @param searchWord 검색할 기안서 제목
     * @param model 뷰로 데이터를 전달하기 위한 모델 객체
     * @return 기안서 목록 페이지
     */
    @GetMapping("")
    public String draftManageMain(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "draftStatus", defaultValue = "전체") String draftStatus,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            @RequestParam(value = "author", required = false) String author,
            @RequestParam(value = "draftType", required = false) String draftType,
            @RequestParam(value = "searchWord", required = false) String searchWord,
            Model model
    ) {
        Map<String, String> searchValueMap = new HashMap<>();
        if(!"전체".equals(draftStatus)) searchValueMap.put("draftStatus", draftStatus);
        if(startDate != null) searchValueMap.put("startDate", startDate.toString());
        if(endDate != null) searchValueMap.put("endDate", endDate.toString());
        if(StringUtils.hasText(author)) searchValueMap.put("author", author);
        if(StringUtils.hasText(draftType)) searchValueMap.put("draftType", draftType);
        if(StringUtils.hasText(searchWord)) searchValueMap.put("searchWord", searchWord);
        searchValueMap.put("deleteStatus", "N");

        CustomSelectCriteria selectCriteria = CustomPagenation.getSelectCriteria(page, (int) draftService.countDraftByQueries(searchValueMap), 20, 10, searchValueMap);

        List<DraftDTO> draftDTOList = draftService.findDraftByQueries(selectCriteria);

        model.addAttribute("draftList", draftDTOList);
        model.addAttribute("selectCriteria", selectCriteria);

        return "groupware/draft/draft-inquiry";
    }

    /**
     * 기안서 상세보기를 담당하는 컨트롤러 메소드 (GET)
     * @param draftNo 기안서 번호
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @param rttr Redirect 방식에서 메시지 전달을 위한 {@link RedirectAttributes} 객체
     * @return 기안서 정보를 성공적으로 가져왔을 때는 draft-detail 페이지, 정보 가져오기를 실패한 경우 목록으로 이동
     */
    @GetMapping("{draftNo}")
    public String draftDetailPage(@PathVariable int draftNo, Model model, RedirectAttributes rttr) {
        DraftDTO draftDTO = draftService.findDraftByDraftNo(draftNo);
        if (draftDTO != null) {
            model.addAttribute("draft", draftDTO);
            for (ApprovalLineDTO approvalLineDTO : draftDTO.getApprovalLineList()) {
                if("N".equals(approvalLineDTO.getApprovalStatus())) {
                    model.addAttribute("approveEmployee", String.valueOf(approvalLineDTO.getEmpCode()));
                    break;
                }
            }
        } else {
            rttr.addFlashAttribute("message", "이미 삭제되었거나 존재하지 않는 기안서입니다.");
            return "redirect:/groupware/draft/";
        }

        return "groupware/draft/draft-detail";
    }

    /**
     * 기안서 작성 페이지를 담당하는 컨트롤러 메소드 (GET)
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @return 기안서 작성 페이지
     */
    @GetMapping("write")
    public String draftWritePage(
            @RequestParam(value = "formNo", required = false) Integer formNo,
            Model model
    ) {
        DraftFormDTO draftForm = null;
        if(formNo != null) {
            draftForm = draftFormService.findByDraftFormNo(formNo);
            if(draftForm == null) {
                model.addAttribute("message", "기안서 양식 로드에 실패했습니다.");
                return "groupware/draft/draft-insert";
            }
        }

        model.addAttribute("draftForm", draftForm);
        model.addAttribute("departmentList", draftService.findAllDepartment());
        return "groupware/draft/draft-insert";
    }

    /**
     * 기안서 작성을 담당하는 컨트롤러 메소드 (POST, REST)
     * @param title 작성할 기안서의 제목
     * @param type 작성할 기안서의 구분
     * @param content 작성할 기안서의 내용
     * @param approveEmployee 작성할 기안서의 결재 라인 리스트
     * @param auth 기안서의 작성자 정보를 가져오기 위한 인증({@link Authentication}) 객체
     * @return 기안서 작성에 성공하면 성공 메시지, 실패하면 실패 메시지를 전달
     */
    @ResponseBody
    @PostMapping(value = "write", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> draftWrite(
            @RequestParam("title") String title,
            @RequestParam("type") String type,
            @RequestParam("content") String content,
            @RequestParam("approveEmployee") List<Integer> approveEmployee,
            @RequestParam(value = "saveType", defaultValue = "") String saveType,
            Authentication auth
    ) {
        Map<String, String> messages = new HashMap<>();

        if(!"save".equals(saveType) && !"temp".equals(saveType)) {
            messages.put("message", "잘못된 접근입니다.");
            return ResponseEntity.badRequest().body(messages);
        }

        DraftDTO draftDTO = new DraftDTO();
        draftDTO.setDraftTitle(title);
        draftDTO.setDraftType(type);
        draftDTO.setDraftDate(new Date(System.currentTimeMillis()));
        draftDTO.setDraftContents(content);
        draftDTO.setDraftStatus("save".equals(saveType) ? "진행중" : "임시저장");
        draftDTO.setEmpCode(Integer.parseInt(auth.getName()));

        List<ApprovalLineDTO> approvalLineDTOList = new ArrayList<>();
        for (int i = 0; i < approveEmployee.size(); i++) {
            ApprovalLineDTO approvalLineDTO = new ApprovalLineDTO();
            approvalLineDTO.setEmpCode(approveEmployee.get(i));
            approvalLineDTO.setApprovalOrder(i + 1);

            approvalLineDTOList.add(approvalLineDTO);
        }

        int draftNo = draftService.saveDraft(draftDTO, approvalLineDTOList);

        if(draftNo == 0) {
            messages.put("message", "잘못된 입력입니다. 다시 시도해주세요.");
            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("draftNo", String.valueOf(draftNo));
        messages.put("message", "기안서 작성에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }

    /**
     * 기안서 수정 페이지를 담당하는 컨트롤러 메소드 (GET)
     * @param draftNo 기안서 번호
     * @param model 데이터 전달을 위한 {@link Model} 객체
     * @param rttr Redirect시 전달될 데이터를 위한 {@link RedirectAttributes} 객체
     * @return 존재하는 기안서를 요청했을 경우 기안서 수정 페이지, 존재하지 않는 경우 기안서 목록 페이지로 Redirect
     */
    @GetMapping("{draftNo}/modify")
    public String draftModifyPage(@PathVariable int draftNo, Model model, RedirectAttributes rttr) {
        DraftDTO draft = draftService.findDraftByDraftNo(draftNo);
        List<DepartmentDTO> departmentList = draftService.findAllDepartment();

        if(draft == null) {
            rttr.addFlashAttribute("message", "이미 삭제되었거나 잘못된 접근 입니다.");
            return "redirect:/draft";
        }

        model.addAttribute("draft", draft);
        model.addAttribute("departmentList", departmentList);
        return "groupware/draft/draft-modify";
    }

    /**
     * 기안서 수정을 담당하는 컨트롤러 메소드 (POST, REST)
     * @param no 수정할 기안서의 번호
     * @param title 수정할 제목
     * @param content 수정할 내용
     * @param approveEmployee 수정할 결재라인 리스트
     * @param auth 권한체크용 {@link Authentication} 객체
     * @return 수정 결과
     */
    @ResponseBody
    @PostMapping(value = "modify", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> draftModify(
            @RequestParam("draftNo") int no,
            @RequestParam("title") String title,
            @RequestParam("content") String content,
            @RequestParam("approveEmployee") List<Integer> approveEmployee,
            Authentication auth
    ) {
        Map<String, String> messages = new HashMap<>();
        DraftDTO draftDTO = new DraftDTO();
        draftDTO.setDraftNo(no);
        draftDTO.setDraftTitle(title);
        draftDTO.setDraftContents(content);

        List<ApprovalLineDTO> approvalLineDTOList = new ArrayList<>();
        for (int i = 0; i < approveEmployee.size(); i++) {
            ApprovalLineDTO approvalLineDTO = new ApprovalLineDTO();
            approvalLineDTO.setDraftNo(no);
            approvalLineDTO.setEmpCode(approveEmployee.get(i));
            approvalLineDTO.setApprovalOrder(i + 1);

            approvalLineDTOList.add(approvalLineDTO);
        }

        if(!draftService.updateDraft(draftDTO, approvalLineDTOList, auth)) {
            messages.put("message", "기안서 수정에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }

        messages.put("draftNo", String.valueOf(no));
        messages.put("message", "기안서 수정에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }

    /**
     * 임시저장 되어있는 기안서를 진행중으로 변경하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 변경할 기안서 번호
     * @param auth 요청한 사용자의 정보가 담겨있는 {@link Authentication} 객체
     * @return 기안 상태 변경에 성공하면 성공 메시지, 실패하면 실패 메시지를 전달
     */
    @ResponseBody
    @PostMapping(value = "updateStatus", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> updateStatus(@RequestParam("draftNo") int draftNo, Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        if(!draftService.updateDraftStatus(draftNo, Integer.parseInt(auth.getName()))) {
            messages.put("message", "상신에 실패했습니다.");
            return ResponseEntity.internalServerError().body(messages);
        }

        messages.put("draftNo", String.valueOf(draftNo));
        messages.put("message", "상신에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }

    /**
     * 기안서 삭제를 담당하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 삭제할 기안서의 번호
     * @param auth 권한체크를 위한 {@link Authentication} 객체
     * @return 삭제에 성공하면 성공 메시지, 실패하면 실패 메시지를 전달
     */
    @ResponseBody
    @PostMapping(value = "delete", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> draftDelete(@RequestParam("draftNo") int draftNo, Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        if(!draftService.deleteByDraftNo(draftNo, auth)) {
            messages.put("message", "잘못된 요청입니다.");

            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("message", "기안서 삭제에 성공했습니다.");

        return ResponseEntity.ok().body(messages);
    }

    /**
     * 기안서 결재를 담당하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 결재할 기안서 번호
     * @param auth 현재 접속한 사원의 {@link Authentication} 객체
     * @return 결재 성공여부를 알리는 메시지
     */
    @ResponseBody
    @PostMapping(value = "approve", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> approveDraft(@RequestParam("draftNo") int draftNo, Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        try {
            draftService.approveDraft(draftNo, Integer.parseInt(auth.getName()));
        } catch (RuntimeException e) {
            messages.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("message", "기안서 결재에 성공했습니다.");
        return ResponseEntity.ok().body(messages);
    }

    /**
     * 기안서 반려를 담당하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 반려할 기안서 번호
     * @param auth 현재 접속한 사원의 {@link Authentication} 객체
     * @return 반려 성공여부를 알리는 메시지
     */
    @ResponseBody
    @PostMapping(value = "reject", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> rejectDraft(@RequestParam("draftNo") int draftNo, Authentication auth) {
        Map<String, String> messages = new HashMap<>();

        try {
            draftService.rejectDraft(draftNo, Integer.parseInt(auth.getName()));
        } catch (RuntimeException e) {
            messages.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("message", "기안서 반려에 성공했습니다.");
        return ResponseEntity.ok().body(messages);
    }

    /**
     * 기안서 양식을 선택할 수 있는 팝업 페이지를 담당하는 컨트롤러 메소드 (GET)
     * @param type 기안서 양식 검색에 사용되는 구분
     * @param title 기안서 양식 검색에 사용되는 제목 쿼리
     * @param page 기안서 양식 페이징에 사용되는 현재 페이지 번호
     * @param model 데이터 전달에 사용되는 {@link Model} 객체
     * @return 기안서 양식 선택 페이지
     */
    @GetMapping(value = "draftForm")
    public String draftFormPage(
            @RequestParam(value = "type", defaultValue = "") String type,
            @RequestParam(value = "title", defaultValue = "") String title,
            @RequestParam(value = "page", defaultValue = "1") int page,
            Model model
    ) {
        Map<String, String> searchValueMap = new HashMap<>();
        searchValueMap.put("type", type);
        searchValueMap.put("title", title);

        int totalCount = draftFormService.countAllByQueries(searchValueMap);

        CustomSelectCriteria selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 10, 10, searchValueMap);

        List<DraftFormDTO> draftFormList = draftFormService.findAllByQueries(selectCriteria);

        model.addAttribute("selectCriteria", selectCriteria);
        model.addAttribute("draftFormList", draftFormList);

        return "/groupware/draft/load-draft-form-popup";
    }
    
    /**
     * 기안서에 할당된 결재라인 정보를 전달하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 결재라인 정보를 가져올 기안서 번호
     * @return 결재라인에 포함된 사원번호 리스트
     */
    @ResponseBody
    @PostMapping(value = "getApprovalLine", produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<String>> getApprovalLine(@RequestParam("draftNo") int draftNo) {
        List<String> data = new ArrayList<>();

        List<ApprovalLineDTO> approvalLineDTOList = draftService.findApprovalLineByDraftNo(draftNo);

        if(approvalLineDTOList == null) {
            data.add("잘못된 접근입니다.");
            return ResponseEntity.badRequest().body(data);
        }

        for (ApprovalLineDTO approvalLineDTO : approvalLineDTOList) {
            data.add(String.valueOf(approvalLineDTO.getEmpCode()));
        }

        return ResponseEntity.ok().body(data);
    }

    /**
     * 특정 기안서의 결재라인 중 특정 사원의 정보를 반환하는 컨트롤러 메소드 (POST, REST)
     * @param draftNo 검색할 결재라인의 기안서 번호
     * @param empCode 검색할 결재라인의 사원 번호
     * @return 결재라인 정보 및 사원 정보
     */
    @ResponseBody
    @GetMapping(value = "getApprovalLineInfo", produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> getApprovalLineInfo(
            @RequestParam("draftNo") int draftNo,
            @RequestParam("empCode") int empCode
    ) {
        Map<String, String> messages = new HashMap<>();

        ApprovalLineDTO approvalLineDTO = draftService.findApprovalEmployeeByDraftNoAndEmpCode(draftNo, empCode);

        if(approvalLineDTO == null) {
            messages.put("message", "잘못된 요청입니다.");
            return ResponseEntity.badRequest().body(messages);
        }

        messages.put("jobName", approvalLineDTO.getEmployee().getJobGrade().getJobName());
        messages.put("approvalStatus", approvalLineDTO.getApprovalStatus());
        if("Y".equals(approvalLineDTO.getApprovalStatus())) {
            messages.put("sign", approvalLineDTO.getEmployee().getSignImg());
            messages.put("date", approvalLineDTO.getApprovalDate().toString());
        }
        messages.put("empName", approvalLineDTO.getEmployee().getEmpName());

        return ResponseEntity.ok().body(messages);
    }

    /**
     * 기안서 구분 리스트를 문자열 배열로 반환하는 컨트롤러 메소드 (GET, REST)
     * @return 기안서 구분 전체 리스트
     */
    @ResponseBody
    @GetMapping("getDraftType")
    public ResponseEntity<List<String>> getDraftType() {
        List<String> data = draftService.findDraftTypeAll();

        if(data == null) {
            data = new ArrayList<>();
            data.add("기안서 구분을 가져오는데 실패했습니다.");

            return ResponseEntity.internalServerError().body(data);
        }

        return ResponseEntity.ok(data);
    }

    /**
     * 중복제거된 기안서 작성자 리스트를 {@link Map} 형태로 반환하는 컨트롤러 메소드 (GET, REST)
     * @return 기안서 작성자 리스트
     */
    @ResponseBody
    @GetMapping("getDraftAuthor")
    public ResponseEntity<Map<String, String>> getDraftAuthor() {
        Map<String, String> data = draftService.findDraftAuthorAll();

        if(data == null) {
            data = new HashMap<>();
            data.put("message", "기안서 작성자를 가져오는데 실패했습니다.");

            return ResponseEntity.internalServerError().body(data);
        }

        return ResponseEntity.ok(data);
    }
}
