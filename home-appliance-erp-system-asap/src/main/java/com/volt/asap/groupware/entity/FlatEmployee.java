package com.volt.asap.groupware.entity;

import com.volt.asap.hrm.entity.JobGrade;

import javax.persistence.*;
import java.sql.Date;

@Entity(name = "FlatEmployee")
@Table(name = "TBL_EMPLOYEE")
public class FlatEmployee {
    @Id
    @Column(name = "EMP_CODE", nullable = false, insertable = false, updatable = false)
    private int empCode;                                          // EMP_CODE

    @Column(name = "EMP_NAME", nullable = false, insertable = false, updatable = false)
    private String empName;                                       // EMP_NAME

    @Column(name = "EMP_BIRTH", nullable = false, insertable = false, updatable = false)
    private String empBirth;                                      // EMP_BIRTH

    @Column(name = "DEPT_CODE", nullable = false, insertable = false, updatable = false)
    private String deptCode;                                      // DEPT_CODE

    @Column(name = "EMP_PHONE", nullable = false, insertable = false, updatable = false)
    private String empPhone;                                      // EMP_PHONE

    @Column(name = "EMP_EMAIL", nullable = false, insertable = false, updatable = false)
    private String empEmail;                                      // EMP_EMAIL

    @Column(name = "EMP_ADDRESS", nullable = false, insertable = false, updatable = false)
    private String empAddress;                                    // EMP_ADDRESS

    @Column(name = "EMP_JOINDATE", nullable = false, insertable = false, updatable = false)
    private java.sql.Date empJoindate;                            // EMP_JOINDATE

    @Column(name = "EMP_LEAVEDATE", insertable = false, updatable = false)
    private java.sql.Date empLeavedate;                           // EMP_LEAVEDATE

    @Column(name = "EMP_LEAVE_YN", nullable = false, insertable = false, updatable = false)
    private String empLeaveYn;                                    // EMP_LEAVE_YN

    @Column(name = "JOB_CODE", nullable = false, insertable = false, updatable = false)
    private String jobCode;                                       // JOB_CODE

    @Column(name = "ANNUAL_LEFT_DAY", nullable = false, insertable = false, updatable = false)
    private int annualLeftDay;                                    // ANNUAL_LEFT_DAY

    @Lob
    @Column(name = "SIGN_IMG", columnDefinition = "CLOB")
    private String signImg;                                       // SIGN_IMG

    @Column(name = "PROFILE_NAME", insertable = false, updatable = false)
    private String profileName;                                   // PROFILE_NAME

    @Column(name = "PROFILE_RENAME", insertable = false, updatable = false)
    private String profileRename;                                 // PROFILE_RENAME

    @Column(name = "PROFILE_PATH", insertable = false, updatable = false)
    private String profilePath;                                   // PROFILE_PATH

    @Column(name = "EMP_GENDER", nullable = false, insertable = false, updatable = false)
    private String empGender;                                     // EMP_GENDER

    @Column(name = "EMP_PWD", nullable = false, insertable = false, updatable = false)
    private String empPwd;                                        // EMP_PWD

    @Column(name = "SALARY_BASE", nullable = false, insertable = false, updatable = false)
    private int salaryBase;                                       // SALARY_BASE

    @ManyToOne
    @JoinColumn(name = "JOB_CODE")
    private JobGrade jobGrade;

    public FlatEmployee() {
    }

    public FlatEmployee(int empCode, String empName, String empBirth, String deptCode, String empPhone, String empEmail, String empAddress, Date empJoindate, Date empLeavedate, String empLeaveYn, String jobCode, int annualLeftDay, String signImg, String profileName, String profileRename, String profilePath, String empGender, String empPwd, int salaryBase, JobGrade jobGrade) {
        this.empCode = empCode;
        this.empName = empName;
        this.empBirth = empBirth;
        this.deptCode = deptCode;
        this.empPhone = empPhone;
        this.empEmail = empEmail;
        this.empAddress = empAddress;
        this.empJoindate = empJoindate;
        this.empLeavedate = empLeavedate;
        this.empLeaveYn = empLeaveYn;
        this.jobCode = jobCode;
        this.annualLeftDay = annualLeftDay;
        this.signImg = signImg;
        this.profileName = profileName;
        this.profileRename = profileRename;
        this.profilePath = profilePath;
        this.empGender = empGender;
        this.empPwd = empPwd;
        this.salaryBase = salaryBase;
        this.jobGrade = jobGrade;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpBirth() {
        return empBirth;
    }

    public void setEmpBirth(String empBirth) {
        this.empBirth = empBirth;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpAddress() {
        return empAddress;
    }

    public void setEmpAddress(String empAddress) {
        this.empAddress = empAddress;
    }

    public Date getEmpJoindate() {
        return empJoindate;
    }

    public void setEmpJoindate(Date empJoindate) {
        this.empJoindate = empJoindate;
    }

    public Date getEmpLeavedate() {
        return empLeavedate;
    }

    public void setEmpLeavedate(Date empLeavedate) {
        this.empLeavedate = empLeavedate;
    }

    public String getEmpLeaveYn() {
        return empLeaveYn;
    }

    public void setEmpLeaveYn(String empLeaveYn) {
        this.empLeaveYn = empLeaveYn;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public int getAnnualLeftDay() {
        return annualLeftDay;
    }

    public void setAnnualLeftDay(int annualLeftDay) {
        this.annualLeftDay = annualLeftDay;
    }

    public String getSignImg() {
        return signImg;
    }

    public void setSignImg(String signImg) {
        this.signImg = signImg;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileRename() {
        return profileRename;
    }

    public void setProfileRename(String profileRename) {
        this.profileRename = profileRename;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getEmpGender() {
        return empGender;
    }

    public void setEmpGender(String empGender) {
        this.empGender = empGender;
    }

    public String getEmpPwd() {
        return empPwd;
    }

    public void setEmpPwd(String empPwd) {
        this.empPwd = empPwd;
    }

    public int getSalaryBase() {
        return salaryBase;
    }

    public void setSalaryBase(int salaryBase) {
        this.salaryBase = salaryBase;
    }

    public JobGrade getJobGrade() {
        return jobGrade;
    }

    public void setJobGrade(JobGrade jobGrade) {
        this.jobGrade = jobGrade;
    }

    @Override
    public String toString() {
        return "FlatEmployee{" +
                "empCode=" + empCode +
                ", empName='" + empName + '\'' +
                ", empBirth='" + empBirth + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", empPhone='" + empPhone + '\'' +
                ", empEmail='" + empEmail + '\'' +
                ", empAddress='" + empAddress + '\'' +
                ", empJoindate=" + empJoindate +
                ", empLeavedate=" + empLeavedate +
                ", empLeaveYn='" + empLeaveYn + '\'' +
                ", jobCode='" + jobCode + '\'' +
                ", annualLeftDay=" + annualLeftDay +
                ", signImg='" + signImg + '\'' +
                ", profileName='" + profileName + '\'' +
                ", profileRename='" + profileRename + '\'' +
                ", profilePath='" + profilePath + '\'' +
                ", empGender='" + empGender + '\'' +
                ", empPwd='" + empPwd + '\'' +
                ", salaryBase=" + salaryBase +
                ", jobGrade=" + jobGrade +
                '}';
    }
}
