package com.volt.asap.groupware.dto;

import java.io.Serializable;

public class CompanyDateAttendDTO implements Serializable {
    private static final long serialVersionUID = 5559805252620602490L;

    private int empCode;                // EMP_CODE
    private int dateNo;                 // DATE_NO
    private EmployeeDTO employee;

    public CompanyDateAttendDTO() {
    }

    public CompanyDateAttendDTO(int empCode, int dateNo, EmployeeDTO employee) {
        this.empCode = empCode;
        this.dateNo = dateNo;
        this.employee = employee;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDateNo() {
        return dateNo;
    }

    public void setDateNo(int dateNo) {
        this.dateNo = dateNo;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CompanyDateAttendDTO{" +
                "empCode=" + empCode +
                ", dateNo=" + dateNo +
                ", employee=" + employee +
                '}';
    }
}
