package com.volt.asap.groupware.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.FileUploadDTO;
import com.volt.asap.groupware.dto.NoticeDTO;
import com.volt.asap.groupware.entity.FileUpload;
import com.volt.asap.groupware.entity.Notice;
import com.volt.asap.groupware.entity.NoticeAndEmployee;
import com.volt.asap.groupware.repository.FileUploadRepository;
import com.volt.asap.groupware.repository.NoticeAndEmployeeRepository;
import com.volt.asap.groupware.repository.NoticeRepository;

@Service
public class NoticeService {
	
	private final NoticeRepository noticeRepository;
	private final NoticeAndEmployeeRepository noticeAndEmployeeRepository;
	private final ModelMapper modelMapper;
    private final FileUploadRepository fileUploadRepository;
    
	public NoticeService(NoticeRepository noticeRepository, NoticeAndEmployeeRepository noticeAndEmployeeRepository
			, ModelMapper modelMapper, FileUploadRepository fileUploadRepository){
		this.noticeRepository = noticeRepository;
		this.noticeAndEmployeeRepository = noticeAndEmployeeRepository;
		this.modelMapper = modelMapper;
		this.fileUploadRepository = fileUploadRepository;
	}
	

	public int countNotice(Map<String, String> parameterMap) {
		if (parameterMap == null) {
			
			return noticeAndEmployeeRepository.countByDeleteStatus("N");
		} else {
			Date startDate = toSqlDate(parameterMap.get("startDate"));
			Date endDate = toSqlDate(parameterMap.get("endDate"));
			String searchWord = parameterMap.get("searchWord");
			
			if(startDate != null && endDate != null) {
                if("".equals(searchWord)) {
                    /* 날짜 검색 */
                    return noticeAndEmployeeRepository.countByNoticeDateBetweenAndDeleteStatus(
                            startDate, endDate, "N"
                    );
                } else {
                    /* 날짜, 검색어 */
                    return noticeAndEmployeeRepository.countByNoticeDateBetweenAndNoticeTitleContainingAndDeleteStatus(
                            startDate, endDate, parameterMap.get("searchWord"), "N"
                    );
                }
            } else {
                /* 검색어 */
                return noticeAndEmployeeRepository.countByNoticeTitleContainingAndDeleteStatus(
                		parameterMap.get("searchWord"),  "N"
                );
            }	
			
		}
	}

	public List<NoticeDTO> noticeList(CustomSelectCriteria selectCriteria) {

		int index = selectCriteria.getPageNo() - 1;
        int count = selectCriteria.getLimit();
        
        Pageable paging = PageRequest.of(index, count, Sort.by("noticeNo"));
        
		List<NoticeAndEmployee> noticeList = null;
		
		if(selectCriteria.getSearchValueMap() == null) {
            /* 전체 조회 */
			noticeList = noticeAndEmployeeRepository.findByDeleteStatus("N", paging);
        } else {
        	 java.sql.Date startDate = toSqlDate(selectCriteria.getSearchValueMap().get("startDate"));
             java.sql.Date endDate = toSqlDate(selectCriteria.getSearchValueMap().get("endDate"));
             String searchWord = selectCriteria.getSearchValueMap().get("searchWord");
             
             if(startDate != null && endDate != null) {
                 if("".equals(searchWord)) {
                     /* 날짜 검색 */
                	 noticeList = noticeAndEmployeeRepository.findByNoticeDateBetweenAndDeleteStatus(
                             startDate, endDate, "N", paging
                     );
                 } else {
                     /* 날짜, 검색어 */
                	 noticeList = noticeAndEmployeeRepository.findByNoticeDateBetweenAndNoticeTitleContainingAndDeleteStatus(
                             startDate, endDate, selectCriteria.getSearchValueMap().get("searchWord"), "N", paging
                     );
                 }
             } else {
                 /* 검색어 */
            	 noticeList = noticeAndEmployeeRepository.findByNoticeTitleContainingAndDeleteStatus(
                         selectCriteria.getSearchValueMap().get("searchWord"),  "N", paging
                 );
             }
		
        }
		
		return noticeList.stream().map(notice -> modelMapper.map(notice, NoticeDTO.class)).collect(Collectors.toList());
	}
	


	public NoticeDTO noticeDetail(int noticeNo) {
		
		NoticeAndEmployee noticedetail = noticeAndEmployeeRepository.findNoticeByNoticeNo(noticeNo);
		
		return modelMapper.map(noticedetail, NoticeDTO.class);
	}
	
	
	
	private java.sql.Date toSqlDate(String dateString) {
		java.sql.Date endDate = null;
		
		if (dateString != null && !"".equals(dateString)) {
			endDate = java.sql.Date.valueOf(dateString);
		}
		
		return endDate;
	}


//	public void registNotice(NoticeDTO noticeInsert) {
//		
//		noticeRepository.save(modelMapper.map(noticeInsert, Notice.class));
//	}
	
	@Transactional
	public int registNotice(NoticeDTO newNotice, List<FileUploadDTO> fileUploadDTOList) {

		  int insertNoticeNo = noticeRepository.save(modelMapper.map(newNotice, Notice.class)).getNoticeNo();

	        /* 영속성 컨텍스트에 첨부파일 엔티티 추가 */
	        List<FileUpload> fileUploadList = fileUploadDTOList.stream().map(fileUploadDTO -> {
	                    /* 람다 구문내에서 스케줄 번호 추가 */
	                    fileUploadDTO.setNoticeNo(insertNoticeNo);
	                    return modelMapper.map(fileUploadDTO, FileUpload.class);
	                }).collect(Collectors.toList());

	        if(fileUploadRepository.saveAll(fileUploadList).size() != fileUploadDTOList.size()) {
	            return 0;
	        }
	        
	        return insertNoticeNo;
	}


	public NoticeDTO noticeModify(int noticeNo) {
		
		NoticeAndEmployee noticeAndEmployee = noticeAndEmployeeRepository.findNoticeByNoticeNo(noticeNo);
		return modelMapper.map(noticeAndEmployee, NoticeDTO.class);
	}


	public void modifyNotice(NoticeDTO notice) {
		Notice modifynotice = noticeRepository.findNoticeByNoticeNo(notice.getNoticeNo());
		
		modifynotice.setNoticeTitle(notice.getNoticeTitle());
		modifynotice.setNoticeContent(notice.getNoticeContent());
		modifynotice.setNoticeDate(notice.getNoticeDate());
		
		
	}


	public FileUploadDTO findUploadFileByFileRename(String removeFile) {
        FileUpload fileUpload = fileUploadRepository.findByFileRename(removeFile);

        return modelMapper.map(fileUpload, FileUploadDTO.class);
	}


	public boolean updateNotice(List<FileUploadDTO> fileUploadDTOList, List<String> removeFiles, NoticeDTO noticedto) {
		
		Notice notice = noticeRepository.findNoticeByNoticeNoAndDeleteStatus(noticedto.getNoticeNo(), "N");
		notice.setNoticeTitle(noticedto.getNoticeTitle());
		notice.setNoticeContent(noticedto.getNoticeContent());
		notice.setNoticeDate(noticedto.getNoticeDate());
		notice.setDeleteStatus(noticedto.getDeleteStatus());
		
		List<FileUpload> fileUploadList = fileUploadDTOList.stream().map(fileUploadDTO -> {
            /* 람다 구문내에서 스케줄 번호 추가 */
            fileUploadDTO.setNoticeNo(noticedto.getNoticeNo());
            return modelMapper.map(fileUploadDTO, FileUpload.class);
        }).collect(Collectors.toList());

        if(fileUploadRepository.saveAll(fileUploadList).size() != fileUploadDTOList.size()) {
            return false;
        }

        for (String removeFile : removeFiles) {
            fileUploadRepository.deleteByFileRename(removeFile);
        }
		return true;
	}

    @Transactional
	public void deletenotice(int noticeNo) {
		Notice notice = noticeRepository.findByNoticeNoAndDeleteStatus(noticeNo, "N");
		notice.setDeleteStatus("Y");
	}
	
}
