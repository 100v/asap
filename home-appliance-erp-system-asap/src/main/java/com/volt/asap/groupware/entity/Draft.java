package com.volt.asap.groupware.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@DynamicInsert
@DynamicUpdate
@Entity(name = "Draft")
@Table(name = "TBL_DRAFT")
@SequenceGenerator(
        name = "DRAFT_NO_SEQ_GENERATOR",
        sequenceName = "SEQ_DRAFT_NO",
        allocationSize = 1
)
public class Draft {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "DRAFT_NO_SEQ_GENERATOR"
    )
    @Column(name = "DRAFT_NO", nullable = false)
    private int draftNo;                                          // DRAFT_NO

    @Column(name = "DRAFT_DATE", nullable = false)
    private java.sql.Date draftDate;                              // DRAFT_DATE

    @Column(name = "DRAFT_TITLE", nullable = false)
    private String draftTitle;                                    // DRAFT_TITLE

    @Column(name = "DRAFT_TYPE", nullable = false)
    private String draftType;

    @Column(name = "DRAFT_CONTENTS")
    private String draftContents;                                 // DRAFT_CONTENTS

    @Column(name = "DRAFT_STATUS", nullable = false)
    private String draftStatus;                                   // DRAFT_STATUS

    @Column(name = "APPROVAL_STATUS")
    private String approvalStatus;                                // APPROVAL_STATUS

    @Column(name = "DELETE_STATUS")
    private String deleteStatus;                                  // DELETE_STATUS

    @Column(name = "EMP_CODE", nullable = false)
    private int empCode;                                          // EMP_CODE

    @ManyToOne
    @JoinColumn(name = "EMP_CODE", insertable = false, updatable = false)
    private FlatEmployee author;

    @OneToMany
    @OrderBy("approvalOrder")
    @JoinColumn(name = "DRAFT_NO", insertable = false, updatable = false)
    private List<ApprovalLine> approvalLineList;

    public Draft() {
    }

    public Draft(int draftNo, Date draftDate, String draftTitle, String draftType, String draftContents, String draftStatus, String approvalStatus, String deleteStatus, int empCode, FlatEmployee author, List<ApprovalLine> approvalLineList) {
        this.draftNo = draftNo;
        this.draftDate = draftDate;
        this.draftTitle = draftTitle;
        this.draftType = draftType;
        this.draftContents = draftContents;
        this.draftStatus = draftStatus;
        this.approvalStatus = approvalStatus;
        this.deleteStatus = deleteStatus;
        this.empCode = empCode;
        this.author = author;
        this.approvalLineList = approvalLineList;
    }

    public int getDraftNo() {
        return draftNo;
    }

    public void setDraftNo(int draftNo) {
        this.draftNo = draftNo;
    }

    public Date getDraftDate() {
        return draftDate;
    }

    public void setDraftDate(Date draftDate) {
        this.draftDate = draftDate;
    }

    public String getDraftTitle() {
        return draftTitle;
    }

    public void setDraftTitle(String draftTitle) {
        this.draftTitle = draftTitle;
    }

    public String getDraftType() {
        return draftType;
    }

    public void setDraftType(String draftType) {
        this.draftType = draftType;
    }

    public String getDraftContents() {
        return draftContents;
    }

    public void setDraftContents(String draftContents) {
        this.draftContents = draftContents;
    }

    public String getDraftStatus() {
        return draftStatus;
    }

    public void setDraftStatus(String draftStatus) {
        this.draftStatus = draftStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public FlatEmployee getAuthor() {
        return author;
    }

    public void setAuthor(FlatEmployee author) {
        this.author = author;
    }

    public List<ApprovalLine> getApprovalLineList() {
        return approvalLineList;
    }

    public void setApprovalLineList(List<ApprovalLine> approvalLineList) {
        this.approvalLineList = approvalLineList;
    }

    @Override
    public String toString() {
        return "Draft{" +
                "draftNo=" + draftNo +
                ", draftDate=" + draftDate +
                ", draftTitle='" + draftTitle + '\'' +
                ", draftType='" + draftType + '\'' +
                ", draftContents='" + draftContents + '\'' +
                ", draftStatus='" + draftStatus + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", empCode=" + empCode +
                ", author=" + author +
                ", approvalLineList=" + approvalLineList +
                '}';
    }
}
