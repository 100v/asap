package com.volt.asap.groupware.entity;

import javax.persistence.*;

@Entity(name = "DraftForm")
@Table(name = "TBL_DRAFT_FORM")
@SequenceGenerator(
        name = "DRAFT_FORM_NO_SEQ_GENERATOR",
        sequenceName = "SEQ_DRAFT_FORM_NO",
        allocationSize = 1
)
public class DraftForm {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "DRAFT_FORM_NO_SEQ_GENERATOR"
    )
    @Column(name = "DRAFT_FORM_NO", nullable = false)
    private int draftFormNo;                             // DRAFT_FORM_NO
    @Column(name = "DRAFT_FORM_TITLE")
    private String draftFormTitle;                       // DRAFT_FORM_TITLE
    @Column(name = "DRAFT_FORM_CONTENT")
    private String draftFormContent;                     // DRAFT_FORM_CONTENT
    @Column(name = "DRAFT_FORM_DIVISION")
    private String draftFormDivision;                    // DRAFT_FORM_DIVISION

    public DraftForm() {
    }

    public DraftForm(int draftFormNo, String draftFormTitle, String draftFormContent, String draftFormDivision) {
        this.draftFormNo = draftFormNo;
        this.draftFormTitle = draftFormTitle;
        this.draftFormContent = draftFormContent;
        this.draftFormDivision = draftFormDivision;
    }

    public int getDraftFormNo() {
        return draftFormNo;
    }

    public void setDraftFormNo(int draftFormNo) {
        this.draftFormNo = draftFormNo;
    }

    public String getDraftFormTitle() {
        return draftFormTitle;
    }

    public void setDraftFormTitle(String draftFormTitle) {
        this.draftFormTitle = draftFormTitle;
    }

    public String getDraftFormContent() {
        return draftFormContent;
    }

    public void setDraftFormContent(String draftFormContent) {
        this.draftFormContent = draftFormContent;
    }

    public String getDraftFormDivision() {
        return draftFormDivision;
    }

    public void setDraftFormDivision(String draftFormDivision) {
        this.draftFormDivision = draftFormDivision;
    }

    @Override
    public String toString() {
        return "DraftForm{" +
                "draftFormNo=" + draftFormNo +
                ", draftFormTitle='" + draftFormTitle + '\'' +
                ", draftFormContent='" + draftFormContent + '\'' +
                ", draftFormDivision='" + draftFormDivision + '\'' +
                '}';
    }
}
