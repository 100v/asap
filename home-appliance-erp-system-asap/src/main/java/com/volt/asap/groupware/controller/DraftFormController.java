package com.volt.asap.groupware.controller;

import com.volt.asap.common.paging.CustomPagenation;
import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.DraftFormDTO;
import com.volt.asap.groupware.service.DraftFormService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 기안서 양식에 대한 MVC 컨트롤러
 */
@Controller
@RequestMapping("/groupware/draftForm")
public class DraftFormController {
    private final DraftFormService draftFormService;

    public DraftFormController(DraftFormService draftFormService) {
        this.draftFormService = draftFormService;
    }

    /**
     * 기안서 양식의 리스트를 보여주는 페이지로 전달하는 컨트롤러 메서드 (GET)
     * @param type 기안서 양식의 구분
     * @param title 기안서 양식의 제목
     * @param page 페이징 처리에 사용될 현재 페이지
     * @param model View로의 값 전달에 사용할 Model 객체
     * @return 기안서 양식 조회 페이지
     */
    @GetMapping("")
    public String draftFormInquiry(
            @RequestParam(value = "type", defaultValue = "") String type,
            @RequestParam(value = "title", defaultValue = "") String title,
            @RequestParam(value = "page", defaultValue = "1") int page,
            Model model
    ) {
        Map<String, String> searchValueMap = new HashMap<>();
        searchValueMap.put("type", type);
        searchValueMap.put("title", title);

        int totalCount = draftFormService.countAllByQueries(searchValueMap);

        CustomSelectCriteria selectCriteria = CustomPagenation.getSelectCriteria(page, totalCount, 20, 10, searchValueMap);

        List<DraftFormDTO> draftFormList = draftFormService.findAllByQueries(selectCriteria);

        model.addAttribute("selectCriteria", selectCriteria);
        model.addAttribute("draftFormList", draftFormList);

        return "groupware/draft-form/draft-form-inquiry";
    }

    /**
     * 기안서 양식을 수정하는 페이지로 전달하는 컨트롤러 메서드 (GET)
     * @param draftFormNo 수정을 원하는 기안서 양식 번호
     * @param model View로의 값 전달에 사용할 Model 객체
     * @return 기안서 양식 수정 페이지
     */
    @GetMapping("{draftFormNo}/modify")
    public String modifyDraftForm(@PathVariable int draftFormNo, Model model, RedirectAttributes rttr) {
        DraftFormDTO draftFormDTO = draftFormService.findByDraftFormNo(draftFormNo);

        if(draftFormDTO == null) {
            rttr.addFlashAttribute("message", "이미 삭제된 기안서 양식입니다.");

            return "redirect:/groupware/draftForm/";
        } else {
            model.addAttribute("draftForm", draftFormDTO);

            return "groupware/draft-form/draft-form-modify";
        }
    }

    /**
     * 기안서 양식을 수정하는 컨트롤러 메서드 (POST, REST)
     * @param draftFormNo 수정하려는 기안서 양식 번호
     * @param draftFormTitle 수정할 기안서 양식 제목
     * @param draftFormType 수정할 기안서 양식 구분
     * @param draftFormContent 수정할 기안서 양식 내용
     * @return 성공 메시지
     */
    @ResponseBody
    @PostMapping("/modify")
    public ResponseEntity<Map<String, String>> modifyDraftForm(
            @RequestParam("no") int draftFormNo,
            @RequestParam("title") String draftFormTitle,
            @RequestParam("type") String draftFormType,
            @RequestParam(value = "content", required = false) String draftFormContent
    ) {
        Map<String, String> messages = new HashMap<>();

        DraftFormDTO modifiedDraftForm = new DraftFormDTO();
        modifiedDraftForm.setDraftFormNo(draftFormNo);
        modifiedDraftForm.setDraftFormTitle(draftFormTitle);
        modifiedDraftForm.setDraftFormDivision(draftFormType);
        modifiedDraftForm.setDraftFormContent(draftFormContent);

        draftFormService.updateDraftForm(modifiedDraftForm);
        messages.put("status", "success");
        messages.put("message", "기안서 양식 수정에 성공했습니다.");
        return ResponseEntity.ok(messages);
    }

    /**
     * 기안서 양식을 등록하는 페이지로 전달하는 컨트롤러 메서드 (GET)
     * @return 기안서 양식 등록 페이지
     */
    @GetMapping("/write")
    public String insertDraftForm() {

        return "groupware/draft-form/draft-form-insert";
    }

    /**
     * 기안서 양식을 등록하는 컨트롤러 메서드 (POST, REST)
     * @param draftFormTitle 등록할 기안서 양식의 제목
     * @param draftFormType 등록할 기안서 양식의 구분
     * @param draftFormContent 등록할 기안서 양식의 내용
     * @return 기안서 양식 등록
     */
    @ResponseBody
    @PostMapping("/write")
    public ResponseEntity<Map<String, String>> insertDraftForm(
            @RequestParam("title") String draftFormTitle,
            @RequestParam("type") String draftFormType,
            @RequestParam(value = "content", required = false) String draftFormContent
    ) {
        Map<String, String> messages = new HashMap<>();

        DraftFormDTO draftFormDTO = new DraftFormDTO();
        draftFormDTO.setDraftFormTitle(draftFormTitle);
        draftFormDTO.setDraftFormDivision(draftFormType);
        draftFormDTO.setDraftFormContent(draftFormContent);

        if(draftFormService.insertDraftForm(draftFormDTO)) {
            messages.put("message", "기안서 양식 등록에 성공했습니다.");
            return ResponseEntity.ok(messages);
        } else {
            messages.put("message", "기안서 양식 등록에 실패했습니다.");
            return ResponseEntity.badRequest().body(messages);
        }
    }

    /**
     * 기안서 양식을 삭제하는 컨트롤러 메소드 (POST, REST)
     * @param draftFormNo 삭제할 기안서 양식의 번호
     * @return 기안서 양식 삭제 성공 여부
     */
    @ResponseBody
    @PostMapping("/delete")
    public ResponseEntity<Map<String, String>> deleteDraftForm(@RequestParam("draftFormNo") int draftFormNo) {
        Map<String, String> messages = new HashMap<>();

        if(draftFormService.deleteDraftForm(draftFormNo)) {
            messages.put("message", "기안서 양식 삭제에 성공했습니다.");
            return ResponseEntity.ok(messages);
        } else {
            messages.put("message", "기안서 양식 삭제에 실패했습니다.");
            return ResponseEntity.badRequest().body(messages);
        }

    }


    /**
     * 기안서 양식 구분을 리스트 형태로 전달하는 REST 컨트롤러 메서드 (GET)
     * @return 기안서 양식 구분의 리스트 형태
     */
    @ResponseBody
    @GetMapping(value = "/getDraftFormType", produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<String>> getDraftFormType() {
        List<String> data = draftFormService.findDraftFormTypeAll();

        return ResponseEntity.ok(data);
    }
}
