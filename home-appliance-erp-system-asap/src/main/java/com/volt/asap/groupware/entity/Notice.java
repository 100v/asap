package com.volt.asap.groupware.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "Notice")
@Table(name = "TBL_NOTICE")
@SequenceGenerator(
		name = "SEQ_NOTICE_NO_GENERATOR",
		sequenceName = "SEQ_NOTICE_CODE",
		initialValue = 6,
		allocationSize = 1
	)
public class Notice {

	@Id
	@GeneratedValue(
		strategy = GenerationType.SEQUENCE,
        generator = "SEQ_NOTICE_NO_GENERATOR"
	)
	
	@Column(name = "NOTICE_NO")
	private int noticeNo;
	
	@Column(name = "NOTICE_TITLE")
	private String noticeTitle;
	
	@Column(name = "NOTICE_CONTENT")
	private String noticeContent;
	
	@Column(name = "NOTICE_DATE")
	private Date noticeDate;
	
	@Column(name = "DELETE_STATUS")
	private String deleteStatus;
	
	@Column(name = "EMP_CODE")
	private int empCode;
	
	public Notice() {
		super();
	}

	public Notice(int noticeNo, String noticeTitle, String noticeContent, Date noticeDate, String deleteStatus,
			int empCode) {
		super();
		this.noticeNo = noticeNo;
		this.noticeTitle = noticeTitle;
		this.noticeContent = noticeContent;
		this.noticeDate = noticeDate;
		this.deleteStatus = deleteStatus;
		this.empCode = empCode;
	}

	public int getNoticeNo() {
		return noticeNo;
	}

	public void setNoticeNo(int noticeNo) {
		this.noticeNo = noticeNo;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public String getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(String deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	@Override
	public String toString() {
		return "Notice [noticeNo=" + noticeNo + ", noticeTitle=" + noticeTitle + ", noticeContent=" + noticeContent
				+ ", noticeDate=" + noticeDate + ", deleteStatus=" + deleteStatus + ", empCode=" + empCode + "]";
	}
	
	
}





















