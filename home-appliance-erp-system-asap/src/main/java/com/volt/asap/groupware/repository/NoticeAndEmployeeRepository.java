package com.volt.asap.groupware.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.groupware.entity.NoticeAndEmployee;

public interface NoticeAndEmployeeRepository extends JpaRepository<NoticeAndEmployee, Integer>{

	NoticeAndEmployee findNoticeByNoticeNo(int noticeNo);

	List<NoticeAndEmployee> findByNoticeDateBetweenAndDeleteStatus(Date startDate, Date endDate, String string,
			Pageable paging);

	List<NoticeAndEmployee> findByNoticeDateBetweenAndNoticeTitleContainingAndDeleteStatus(Date startDate, Date endDate,
			String string, String string2, Pageable paging);

	List<NoticeAndEmployee> findByNoticeTitleContainingAndDeleteStatus(String string, String string2, Pageable paging);

	List<NoticeAndEmployee> findByDeleteStatus(String string, Pageable paging);

	int countByDeleteStatus(String string);

	int countByNoticeDateBetweenAndDeleteStatus(Date startDate, Date endDate, String string);

	int countByNoticeTitleContainingAndDeleteStatus(String string, String string2);

	int countByNoticeDateBetweenAndNoticeTitleContainingAndDeleteStatus(Date startDate, Date endDate, String string,
			String string2);



}
