package com.volt.asap.groupware.entity;

import com.volt.asap.hrm.entity.Employee;

import javax.persistence.*;
import java.io.Serializable;

/**
 * CompanyDateAttend Entity
 */
@Entity(name = "CompanyDateAttend")
@Table(name = "TBL_COMPANY_DATE_ATTEND")
@IdClass(CompanyDateAttendId.class)
public class CompanyDateAttend implements Serializable {
    private static final long serialVersionUID = -5564896436167424944L;

    @Id
    @Column(name = "EMP_CODE", nullable = false)
    private int empCode;                // EMP_CODE

    @Id
    @Column(name = "DATE_NO", nullable = false)
    private int dateNo;                 // DATE_NO

    @ManyToOne
    /* IdClass로 인해서 복합키에 대해 정의하는 과정에서 Join 되는 컬럼의 컬럼이름이 겹치는 경우 아래의 옵션을 추가해야 함 */
    @JoinColumn(name = "EMP_CODE", updatable = false, insertable = false)
    private Employee employee;

    public CompanyDateAttend() {
    }

    public CompanyDateAttend(int empCode, int dateNo, Employee employee) {
        this.empCode = empCode;
        this.dateNo = dateNo;
        this.employee = employee;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public int getDateNo() {
        return dateNo;
    }

    public void setDateNo(int dateNo) {
        this.dateNo = dateNo;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CompanyDateAttend{" +
                "empCode=" + empCode +
                ", dateNo=" + dateNo +
                ", employee=" + employee +
                '}';
    }
}
