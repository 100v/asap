package com.volt.asap.groupware.service;

import com.volt.asap.groupware.dto.EmployeeDTO;
import com.volt.asap.groupware.entity.FlatEmployee;
import com.volt.asap.groupware.repository.FlatEmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class SignService {
    private final FlatEmployeeRepository flatEmployeeRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public SignService(FlatEmployeeRepository employeeRepository, ModelMapper modelMapper) {
        this.flatEmployeeRepository = employeeRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * 사원정보를 조회해서 반환하는 서비스 메소드
     * @param empCode 조회할 사원의 사원 번호
     * @return 조회 실패시 null, 조회 성공시 {@link EmployeeDTO} 객체
     */
    public EmployeeDTO findEmployeeById(int empCode) {
        Optional<FlatEmployee> optionalEmployee = flatEmployeeRepository.findById(empCode);

        if(optionalEmployee.isEmpty()) {
            return null;
        }

        return modelMapper.map(optionalEmployee.get(), EmployeeDTO.class);
    }

    /**
     * 해당 사원의 서명 파일을 수정하는 서비스 메소드
     * @param sign base64로 변환된 서명 파일
     * @param empCode 서명 파일을 수정할 사원의 사원 번호
     * @return 수정 성공시 true, 실패시 false
     */
    @Transactional
    public boolean updateSignById(String sign, int empCode) {
        Optional<FlatEmployee> optionalFlatEmployee = flatEmployeeRepository.findById(empCode);

        if(optionalFlatEmployee.isEmpty()) return false;

        optionalFlatEmployee.get().setSignImg(sign);

        return true;
    }

    /**
     * 해당 사원의 서명 파일을 삭제하는 서비스 메소드
     * @param empCode 서명 파일을 삭제할 사원의 사원 번호
     * @return 삭제 성공시 true, 실패시 false
     */
    @Transactional
    public boolean deleteSingById(int empCode) {
        Optional<FlatEmployee> optionalFlatEmployee = flatEmployeeRepository.findById(empCode);

        if(optionalFlatEmployee.isEmpty()) return false;

        optionalFlatEmployee.get().setSignImg(null);

        return true;
    }
}
