package com.volt.asap.groupware.service;

import com.volt.asap.common.paging.CustomSelectCriteria;
import com.volt.asap.groupware.dto.ApprovalLineDTO;
import com.volt.asap.groupware.dto.DepartmentDTO;
import com.volt.asap.groupware.dto.DraftDTO;
import com.volt.asap.groupware.entity.ApprovalLine;
import com.volt.asap.groupware.entity.Draft;
import com.volt.asap.groupware.repository.ApprovalLineRepository;
import com.volt.asap.groupware.repository.DepartmentMemberRepository;
import com.volt.asap.groupware.repository.DraftRepository;
import com.volt.asap.hrm.entity.DepartmentMember;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DraftService {
    @PersistenceContext
    private EntityManager entityManager;
    private final DraftRepository draftRepository;
    private final DepartmentMemberRepository departmentMemberRepository;
    private final ApprovalLineRepository approvalLineRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public DraftService(DraftRepository draftRepository, DepartmentMemberRepository departmentMemberRepository, ApprovalLineRepository approvalLineRepository, ModelMapper modelMapper) {
        this.draftRepository = draftRepository;
        this.departmentMemberRepository = departmentMemberRepository;
        this.approvalLineRepository = approvalLineRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * 매개변수로 받은 쿼리 정보로 검색되는 기안서의 개수를 반환하는 서비스 메소드.
     * jpql을 이용한 동적 쿼리 적용
     * @param searchValueMap 쿼리로 사용될 데이터가 담겨있는 {@link Map}
     * @return 검색된 기안서의 개수
     */
    public long countDraftByQueries(Map<String, String> searchValueMap) {
        StringBuilder jpql = buildQueryString(searchValueMap, true);

        Query query = entityManager.createQuery(jpql.toString());

        SimpleDateFormat dateFormat;
        try {
            for (Map.Entry<String, String> searchValue : searchValueMap.entrySet()) {
                switch (searchValue.getKey()) {
                    case "startDate":
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        query.setParameter(searchValue.getKey(), dateFormat.parse(searchValue.getValue()));
                        break;
                    case "endDate":
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        query.setParameter(
                                searchValue.getKey(),
                                dateFormat.parse(LocalDate.parse(searchValue.getValue()).plusDays(1).toString())
                        );
                        break;
                    case "author":
                        query.setParameter(searchValue.getKey(), Integer.valueOf(searchValue.getValue())) ;
                        break;
                    default:
                        query.setParameter(searchValue.getKey(), searchValue.getValue());
                        break;
                }
            }
        } catch (ParseException e) {
            System.out.println("DraftService.countDraftByQueries");
            System.out.println("날짜를 변환하는 과정에서 오류가 발생했습니다.");

            return 0;
        }

        long result;
        try {
            result = (long) query.getSingleResult();
        } catch (NoResultException e) {
            return 0;
        }

        return result;
    }

    /**
     * 검색 조건에 맞는 기안서 리스트를 반환하는 서비스 메소드
     * jpql을 활용한 동적 쿼리 적용
     * @param selectCriteria 쿼리로 사용될 데이터와 페이징 정보가 담겨있는 {@link CustomSelectCriteria} 객체
     * @return 페이징 처리 된 기안서 리스트
     */
    public List<DraftDTO> findDraftByQueries(CustomSelectCriteria selectCriteria) {
        StringBuilder query = buildQueryString(selectCriteria.getSearchValueMap(), false);

        int firstResult = (selectCriteria.getPageNo() - 1) * selectCriteria.getLimit();
        int maxResult = selectCriteria.getPageNo() * selectCriteria.getLimit();

        TypedQuery<Draft> typedQuery = entityManager.createQuery(query.toString(), Draft.class);

        SimpleDateFormat dateFormat;
        try {
            for (Map.Entry<String, String> searchValue : selectCriteria.getSearchValueMap().entrySet()) {
                switch (searchValue.getKey()) {
                    case "startDate":
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        typedQuery.setParameter(searchValue.getKey(), dateFormat.parse(searchValue.getValue()));
                        break;
                    case "endDate":
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        typedQuery.setParameter(
                                searchValue.getKey(),
                                dateFormat.parse(LocalDate.parse(searchValue.getValue()).plusDays(1).toString())
                        );
                        break;
                    case "author":
                        typedQuery.setParameter(searchValue.getKey(), Integer.valueOf(searchValue.getValue())) ;
                        break;
                    default:
                        typedQuery.setParameter(searchValue.getKey(), searchValue.getValue());
                        break;
                }
            }
        } catch (ParseException e) {
            System.out.println("DraftService.countDraftByQueries");
            System.out.println("날짜를 변환하는 과정에서 오류가 발생했습니다.");

            return null;
        }

        List<Draft> draftList = typedQuery.setFirstResult(firstResult).setMaxResults(maxResult).getResultList();

        return draftList.stream().map(draft -> modelMapper.map(draft, DraftDTO.class)).collect(Collectors.toList());
    }

    /**
     * 매개변수로 전달받은 기안서 번호로 기안서 정보를 반환하는 서비스 메소드
     * @param draftNo 기안서 번호
     * @return 기안서 정보 {@link DraftDTO}
     */
    public DraftDTO findDraftByDraftNo(int draftNo) {
        DraftDTO draftDTO;
        try {
            draftDTO = modelMapper.map(draftRepository.findByDraftNoAndDeleteStatus(draftNo, "N"), DraftDTO.class);
        } catch (IllegalArgumentException e) {
            return null;
        }
        return draftDTO;
    }

    /**
     * 중복제거된 기안서 구분 리스트를 반환하는 서비스 메서드
     * @return 기안서 구분 리스트
     */
    public List<String> findDraftTypeAll() {
        return draftRepository.findDraftTypeAllNative();
    }

    /**
     * 중복제거된 기안서 작성자를 반환하는 서비스 메서드
     * @return 기안서 작성자 리스트
     */
    public Map<String, String> findDraftAuthorAll() {
        List<Map<String, Object>> draftAuthorList = draftRepository.findDraftAuthorAll();

        Map<String, String> dataList = new HashMap<>();
        for (Map<String, Object> draftAuthor : draftAuthorList) {
            dataList.put(String.valueOf(draftAuthor.get("empCode")), (String) draftAuthor.get("empName"));
        }

        return dataList;
    }

    /**
     * 매개변수로 전달받은 번호의 기안서를 삭제처리하는 서비스 메소드
     * @param draftNo 삭제할 기안서 번호
     * @return 삭제 완료 여부.
     */
    @Transactional
    public boolean deleteByDraftNo(int draftNo, Authentication auth) {
        try {
            Optional<Draft> draft = draftRepository.findById(draftNo);
            if(draft.isEmpty()) return false;
            if(auth.getAuthorities().stream().noneMatch(a -> "ROLE_DRAFT_MANAGE".equals(a.getAuthority()))) {
                if(draft.get().getAuthor().getEmpCode() != Integer.parseInt(auth.getName())) {
                    return false;
                }
            }

            draft.get().setDeleteStatus("Y");
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    /**
     * 매개변수로 전달된 기안서 정보와 결재라인 정보로 기안서를 수정하는 서비스 메소드
     * @param draftDTO 기안서 정보
     * @param approvalLineDTOList 결재라인 정보
     * @param auth 권한체크를 위한 {@link Authentication} 객체
     * @return 수정 성공시 true, 실패시 false
     */
    @Transactional
    public boolean updateDraft(DraftDTO draftDTO, List<ApprovalLineDTO> approvalLineDTOList, Authentication auth) {
        try {
            Draft draft = draftRepository.findByDraftNoAndDeleteStatus(draftDTO.getDraftNo(), "N");
            if(draft == null) return false;
            if(auth.getAuthorities().stream().noneMatch(a -> "ROLE_DRAFT_MANAGE".equals(a.getAuthority()))) {
                if(draft.getAuthor().getEmpCode() != Integer.parseInt(auth.getName())) {
                    return false;
                }
            }

            draft.setDraftTitle(draftDTO.getDraftTitle());
            draft.setDraftContents(draftDTO.getDraftContents());

            if("임시저장".equals(draft.getDraftStatus())) {
                approvalLineRepository.deleteAllByDraftNo(draft.getDraftNo());
                for (ApprovalLineDTO approvalLineDTO : approvalLineDTOList) {

                    ApprovalLine approvalLine = new ApprovalLine();
                    approvalLine.setEmpCode(approvalLineDTO.getEmpCode());
                    approvalLine.setDraftNo(approvalLineDTO.getDraftNo());
                    approvalLine.setApprovalOrder(approvalLineDTO.getApprovalOrder());

                    approvalLineRepository.save(approvalLine);
                }
            }
        } catch (IllegalArgumentException e) {
            return false;
        }

        return true;
    }

    /**
     * 모든 부서 목록을 반환하는 메소드
     * @return 전체 부서 목록
     */
    public List<DepartmentDTO> findAllDepartment() {
        List<DepartmentMember> departmentMemberList = departmentMemberRepository.findAll();

        if(departmentMemberList.isEmpty()) {
            return null;
        }

        return departmentMemberList.stream().map(
                departmentMember -> modelMapper.map(departmentMember, DepartmentDTO.class
                )).collect(Collectors.toList());
    }

    /**
     * 매개변수로 전달받은 기안서 번호에 해당하는 결재 라인 정보를 모두 반환하는 서비스 메소드
     * @param draftNo 결재 라인 정보를 확인할 기안서 번호
     * @return 결재라인 정보 리스트
     */
    public List<ApprovalLineDTO> findApprovalLineByDraftNo(int draftNo) {
        List<ApprovalLine> approvalLineList;

        try {
            approvalLineList = approvalLineRepository.findByDraftNoOrderByApprovalOrder(draftNo);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return approvalLineList.stream().map(
                approvalLine -> modelMapper.map(approvalLine, ApprovalLineDTO.class
                )).collect(Collectors.toList());
    }

    /**
     * 매개변수로 전달된 검색어로 쿼리스트링을 생성해 반환하는 메서드
     * @param searchValueMap 검색어 정보가 담겨있는 {@link Map} 객체
     * @return 생성된 쿼리 스트링
     */
    private StringBuilder buildQueryString(Map<String, String> searchValueMap, Boolean isCount) {
        StringBuilder query;
        if (isCount) {
            query = new StringBuilder("SELECT COUNT(*) FROM Draft A\n");
        } else {
            query = new StringBuilder("SELECT A FROM Draft A\n");
        }

        /* WHERE 조건 생성 */
        int i = 0;
        for (Map.Entry<String, String> searchValue : searchValueMap.entrySet()) {
            if(i == 0) {
                query.append("WHERE ");
            } else {
                query.append("AND ");
            }

            switch (searchValue.getKey()) {
                case "draftStatus":
                    query.append("A.draftStatus = :draftStatus\n");
                    break;
                case "startDate":
                    query.append("A.draftDate > :startDate\n");
                    break;
                case "endDate":
                    query.append("A.draftDate <= :endDate\n");
                    break;
                case "author":
                    query.append("A.empCode = :author\n");
                    break;
                case "draftType":
                    query.append("A.draftType = :draftType\n");
                    break;
                case "searchWord":
                    query.append("A.draftTitle LIKE CONCAT('%',:searchWord,'%')\n");
                    break;
                case "deleteStatus":
                    query.append("A.deleteStatus = :deleteStatus\n");
            }

            i++;
        }

        if (!isCount) {
            query.append("ORDER BY A.draftNo DESC");
        }

        return query;
    }

    /**
     * 기안서 작성을 위한 서비스 메소드
     * @param draftDTO 기안서 정보가 담긴 객체
     * @param approvalLineDTOList 기안서의 결재 라인에 대한 정보가 담긴 객체 리스트
     * @return 성공시 기안서 번호, 실패시 0 반환
     */
    @Transactional
    public int saveDraft(DraftDTO draftDTO, List<ApprovalLineDTO> approvalLineDTOList) {
        int draftNo;
        try {
            draftNo = draftRepository.save(modelMapper.map(draftDTO, Draft.class)).getDraftNo();

            for (ApprovalLineDTO approvalLineDTO : approvalLineDTOList) {
                approvalLineDTO.setDraftNo(draftNo);
                approvalLineRepository.save(modelMapper.map(approvalLineDTO, ApprovalLine.class));
            }
        } catch (Exception e) {
            return 0;
        }

        return draftNo;
    }

    /**
     * 결재라인 정보를 반환하는 서비스 메소드
     * @param draftNo 찾을 결재라인의 기안서 번호
     * @param empCode 찾을 결재라인의 사원 번호
     * @return 결재라인 정보
     */
    public ApprovalLineDTO findApprovalEmployeeByDraftNoAndEmpCode(int draftNo, int empCode) {

        ApprovalLine approvalLine;
        try {
            approvalLine = approvalLineRepository.findByDraftNoAndEmpCodeOrderByApprovalOrder(draftNo, empCode);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return modelMapper.map(approvalLine, ApprovalLineDTO.class);
    }

    /**
     * 결재를 진행하는 서비스 메소드
     * @param draftNo 결재할 기안서 번호
     * @param empCode 결재할 사원 번호
     * @throws RuntimeException 결재 실패시 예외 발생
     */
    @Transactional
    public void approveDraft(int draftNo, int empCode) {
        List<ApprovalLine> leftApprovalLineList = approvalLineRepository.findByDraftNoAndApprovalStatusOrderByApprovalOrder(draftNo, "N");
        String error = "결재에 실패했습니다.";
        String alreadyRejected = "이미 반려된 기안서입니다.";

        if(leftApprovalLineList.isEmpty()) throw new RuntimeException(error);
        /* 현재 결재가 내 차례가 아니면 실패로 간주 */
        if(leftApprovalLineList.get(0).getEmpCode() != empCode) throw new RuntimeException(error);

        Optional<Draft> optionalDraft = draftRepository.findById(draftNo);
        if(optionalDraft.isEmpty()) throw new RuntimeException(error);
        if("반려".equals(optionalDraft.get().getDraftStatus())) throw new RuntimeException(alreadyRejected);

        if(leftApprovalLineList.size() == 1) {
            optionalDraft.get().setApprovalStatus("Y");
            optionalDraft.get().setDraftStatus("결재완료");
        }

        leftApprovalLineList.get(0).setApprovalStatus("Y");
        leftApprovalLineList.get(0).setApprovalDate(new Date(System.currentTimeMillis()));
    }

    /**
     * 반려를 진행하는 서비스 메소드
     * @param draftNo 반려할 기안서 번호
     * @param empCode 반려할 사원 번호
     * @throws RuntimeException 결재 실패시 예외 발생
     */
    @Transactional
    public void rejectDraft(int draftNo, int empCode) {
        List<ApprovalLine> leftApprovalLineList = approvalLineRepository.findByDraftNoAndApprovalStatusOrderByApprovalOrder(draftNo, "N");
        String error = "반려에 실패했습니다.";

        if(leftApprovalLineList.isEmpty()) throw new RuntimeException(error);
        /* 현재 결재가 내 차례가 아니면 실패로 간주 */
        if(leftApprovalLineList.get(0).getEmpCode() != empCode) throw new RuntimeException(error);

        Optional<Draft> optionalDraft = draftRepository.findById(draftNo);
        if(optionalDraft.isPresent()) {
            optionalDraft.get().setDraftStatus("반려");
        } else {
            throw new RuntimeException(error);
        }

        leftApprovalLineList.get(0).setApprovalDate(new Date(System.currentTimeMillis()));
    }

    /**
     * 임시저장 되어있는 기안서를 진행중으로 변경하는 서비스 메소드
     * @param draftNo 변경할 기안서 번호
     * @param empCode 기안서 변경을 요청한 사원번호
     * @return 변경 성공시 true, 실패시 false 반환
     */
    public boolean updateDraftStatus(int draftNo, int empCode) {
        Optional<Draft> optionalDraft;
        try {
            optionalDraft = draftRepository.findById(draftNo);
        } catch (IllegalArgumentException e) {
            return false;
        }

        if(optionalDraft.isEmpty()) return false;
        if(empCode == optionalDraft.get().getEmpCode()) return false;
        if(!"임시저장".equals(optionalDraft.get().getDraftStatus())) return false;

        optionalDraft.get().setDraftStatus("진행중");
        return true;
    }
}
