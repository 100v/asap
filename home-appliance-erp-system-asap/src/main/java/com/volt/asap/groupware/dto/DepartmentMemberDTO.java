package com.volt.asap.groupware.dto;

import java.io.Serializable;

public class DepartmentMemberDTO implements Serializable {
    private static final long serialVersionUID = -1900488770153938420L;

    private String deptCode;                           // DEPT_CODE
    private String deptName;                           // DEPT_NAME
    private String deptUseYn;                          // DEPT_USE_YN
    private EmployeeDTO employee;

    public DepartmentMemberDTO() {
    }

    public DepartmentMemberDTO(String deptCode, String deptName, String deptUseYn, EmployeeDTO employee) {
        this.deptCode = deptCode;
        this.deptName = deptName;
        this.deptUseYn = deptUseYn;
        this.employee = employee;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptUseYn() {
        return deptUseYn;
    }

    public void setDeptUseYn(String deptUseYn) {
        this.deptUseYn = deptUseYn;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "DepartmentAndEmployeeDTO{" +
                "deptCode='" + deptCode + '\'' +
                ", deptName='" + deptName + '\'' +
                ", deptUseYn='" + deptUseYn + '\'' +
                ", employee=" + employee +
                '}';
    }
}
