package com.volt.asap.groupware.repository;

import com.volt.asap.groupware.entity.Draft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface DraftRepository extends JpaRepository<Draft, Integer> {

    /**
     * <p>중복제거한 기안서 구분을 모두 리스트로 반환하는 리포지토리 메소드 (NativeQuery).</p>
     * <code>SELECT DISTINCT A.DRAFT_TYPE FROM TBL_DRAFT A</code>
     * @return 기안서 구분 리스트
     */
    @Query(value = "SELECT DISTINCT A.DRAFT_TYPE FROM TBL_DRAFT A WHERE A.DELETE_STATUS = 'N'", nativeQuery = true)
    List<String> findDraftTypeAllNative();

    /**
     * <p>중복제거한 기안서 작성자를 모두 리스트로 반환하는 리포지토리 메소드 (JPQL)</p>
     * <code>SELECT DISTINCT new map (B.empCode as empCode, B.empName as empName) FROM Draft A JOIN Employee B ON (A.empCode = B.empCode)</code>
     * @return 기안서 작성자 리스트
     */
    @Query("SELECT DISTINCT new map (B.empCode as empCode, B.empName as empName) FROM Draft A JOIN Employee B ON (A.empCode = B.empCode) where A.deleteStatus = 'N'")
    List<Map<String, Object>> findDraftAuthorAll();

    Draft findByDraftNoAndDeleteStatus(int draftNo, String deleteStatus);
}
