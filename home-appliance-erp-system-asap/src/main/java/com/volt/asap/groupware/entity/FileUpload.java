package com.volt.asap.groupware.entity;

import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@DynamicInsert
@Entity(name = "FileUpload")
@Table(name = "TBL_FILE_UPLOAD")
@SequenceGenerator(
        name = "FILE_NO_SEQ_GENERATOR",
        sequenceName = "SEQ_FILE_NO",
        allocationSize = 1
)
public class FileUpload {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "FILE_NO_SEQ_GENERATOR"
    )
    @Column(name = "FILE_NO", nullable = false)
    private int fileNo;                            // FILE_NO

    @Column(name = "FILE_NAME", nullable = false)
    private String fileName;                       // FILE_NAME

    @Column(name = "FILE_LOCATION", nullable = false)
    private String fileLocation;                   // FILE_LOCATION

    @Column(name = "FILE_UPLOAD_BOARD", nullable = false)
    private String fileUploadBoard;                // FILE_UPLOAD_BOARD

    @Column(name = "FILE_UPLOAD_DATE", nullable = false)
    private java.sql.Date fileUploadDate;          // FILE_UPLOAD_DATE

    @Column(name = "FILE_RENAME", nullable = false)
    private String fileRename;                     // FILE_RENAME

    @Column(name = "NOTICE_NO")
    private Integer noticeNo;                          // NOTICE_NO

    @Column(name = "DATE_NO")
    private Integer dateNo;                            // DATE_NO

    public FileUpload() {
    }

    public FileUpload(int fileNo, String fileName, String fileLocation, String fileUploadBoard, Date fileUploadDate, String fileRename, Integer noticeNo, Integer dateNo) {
        this.fileNo = fileNo;
        this.fileName = fileName;
        this.fileLocation = fileLocation;
        this.fileUploadBoard = fileUploadBoard;
        this.fileUploadDate = fileUploadDate;
        this.fileRename = fileRename;
        this.noticeNo = noticeNo;
        this.dateNo = dateNo;
    }

    public int getFileNo() {
        return fileNo;
    }

    public void setFileNo(int fileNo) {
        this.fileNo = fileNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileUploadBoard() {
        return fileUploadBoard;
    }

    public void setFileUploadBoard(String fileUploadBoard) {
        this.fileUploadBoard = fileUploadBoard;
    }

    public Date getFileUploadDate() {
        return fileUploadDate;
    }

    public void setFileUploadDate(Date fileUploadDate) {
        this.fileUploadDate = fileUploadDate;
    }

    public String getFileRename() {
        return fileRename;
    }

    public void setFileRename(String fileRename) {
        this.fileRename = fileRename;
    }

    public Integer getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(Integer noticeNo) {
        this.noticeNo = noticeNo;
    }

    public Integer getDateNo() {
        return dateNo;
    }

    public void setDateNo(Integer dateNo) {
        this.dateNo = dateNo;
    }

    @Override
    public String toString() {
        return "FileUpload{" +
                "fileNo=" + fileNo +
                ", fileName='" + fileName + '\'' +
                ", fileLocation='" + fileLocation + '\'' +
                ", fileUploadBoard='" + fileUploadBoard + '\'' +
                ", fileUploadDate=" + fileUploadDate +
                ", fileRename='" + fileRename + '\'' +
                ", noticeNo=" + noticeNo +
                ", dateNo=" + dateNo +
                '}';
    }
}
