package com.volt.asap.groupware.dto;

import com.volt.asap.groupware.entity.FileUpload;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Clob;
import java.sql.Date;
import java.util.List;

public class CompanyDateDTO implements Serializable {
    private static final long serialVersionUID = 6735409328387414681L;

    private int dateNo;                                 // DATE_NO
    private String dateTitle;                           // DATE_TITLE
    private String dateLocation;                        // DATE_LOCATION
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private java.sql.Date dateStart;                    // DATE_START
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private java.sql.Date dateEnd;                      // DATE_END
    private String dateContent;                         // DATE_CONTENT
    private String deleteStatus;                        // DELETE_STATUS
    private int empCode;                                // EMP_CODE
    private EmployeeDTO author;
    private List<CompanyDateAttendDTO> attendList;
    private List<FileUploadDTO> attachedFileList;

    public CompanyDateDTO() {
    }

    public CompanyDateDTO(int dateNo, String dateTitle, String dateLocation, Date dateStart, Date dateEnd, String dateContent, String deleteStatus, int empCode, EmployeeDTO author, List<CompanyDateAttendDTO> attendList, List<FileUploadDTO> attachedFileList) {
        this.dateNo = dateNo;
        this.dateTitle = dateTitle;
        this.dateLocation = dateLocation;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.dateContent = dateContent;
        this.deleteStatus = deleteStatus;
        this.empCode = empCode;
        this.author = author;
        this.attendList = attendList;
        this.attachedFileList = attachedFileList;
    }

    public FullCalendarDTO toFullCalendarDTO() {
        FullCalendarDTO fullCalendarDTO = new FullCalendarDTO();
        fullCalendarDTO.setId(String.valueOf(this.dateNo));
        fullCalendarDTO.setStart(this.dateStart.toString());
        fullCalendarDTO.setEnd(this.dateEnd.toString());
        fullCalendarDTO.setTitle(this.dateTitle);
        fullCalendarDTO.setUrl("/groupware/schedule/" + this.dateNo);

        return fullCalendarDTO;
    }

    public int getDateNo() {
        return dateNo;
    }

    public void setDateNo(int dateNo) {
        this.dateNo = dateNo;
    }

    public String getDateTitle() {
        return dateTitle;
    }

    public void setDateTitle(String dateTitle) {
        this.dateTitle = dateTitle;
    }

    public String getDateLocation() {
        return dateLocation;
    }

    public void setDateLocation(String dateLocation) {
        this.dateLocation = dateLocation;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDateContent() {
        return dateContent;
    }

    public void setDateContent(String dateContent) {
        this.dateContent = dateContent;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public int getEmpCode() {
        return empCode;
    }

    public void setEmpCode(int empCode) {
        this.empCode = empCode;
    }

    public EmployeeDTO getAuthor() {
        return author;
    }

    public void setAuthor(EmployeeDTO author) {
        this.author = author;
    }

    public List<CompanyDateAttendDTO> getAttendList() {
        return attendList;
    }

    public void setAttendList(List<CompanyDateAttendDTO> attendList) {
        this.attendList = attendList;
    }

    public List<FileUploadDTO> getAttachedFileList() {
        return attachedFileList;
    }

    public void setAttachedFileList(List<FileUploadDTO> attachedFileList) {
        this.attachedFileList = attachedFileList;
    }

    @Override
    public String toString() {
        return "CompanyDateDTO{" +
                "dateNo=" + dateNo +
                ", dateTitle='" + dateTitle + '\'' +
                ", dateLocation='" + dateLocation + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", dateContent='" + dateContent + '\'' +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", empCode=" + empCode +
                ", author=" + author +
                ", attendList=" + attendList +
                ", attachedFileList=" + attachedFileList +
                '}';
    }
}
