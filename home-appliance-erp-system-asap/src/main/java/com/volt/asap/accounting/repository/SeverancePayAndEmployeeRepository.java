package com.volt.asap.accounting.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.accounting.entity.SeverancePayAndEmployee;

public interface SeverancePayAndEmployeeRepository extends JpaRepository<SeverancePayAndEmployee, Integer>{

	int countByRetireStatusContaining(String searchValue);

	int countByEmployeeEmpNameContaining(String searchValue);

	List<SeverancePayAndEmployee> findByretireStatusContaining(String searchValue, Pageable paging);

	List<SeverancePayAndEmployee> findByEmployeeEmpNameContaining(String searchValue, Pageable paging);

	SeverancePayAndEmployee findByEmpCodeLike(int empCode);


}
