package com.volt.asap.accounting.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.accounting.dto.EmployeeDTO;
import com.volt.asap.accounting.dto.SeverancePayDTO;
import com.volt.asap.accounting.entity.SalaryAndEmployee;
import com.volt.asap.accounting.entity.SeverancePay;
import com.volt.asap.accounting.entity.SeverancePayAndEmployee;
import com.volt.asap.accounting.repository.Employee2Repository;
import com.volt.asap.accounting.repository.SalaryRepository;
import com.volt.asap.accounting.repository.SeverancePayAndEmployeeRepository;
import com.volt.asap.accounting.repository.SeverancePayRepository;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.repository.EmployeeRepository;

@Service
public class SeverancePayService {
	
	private final SalaryRepository salaryRepository;
	private final SeverancePayRepository severancePayRepository;
	private final EmployeeRepository employeeRepository;
	private final SeverancePayAndEmployeeRepository severancePayAndEmployeeRepository;
	private final Employee2Repository employee2Repository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public SeverancePayService(SalaryRepository salaryRepository, SeverancePayRepository severancePayRepository
								, EmployeeRepository employeeRepository, SeverancePayAndEmployeeRepository severancePayAndEmployeeRepository
								, Employee2Repository employee2Repository, ModelMapper modelMapper) {
		this.salaryRepository = salaryRepository;
		this.severancePayRepository = severancePayRepository;
		this.employeeRepository = employeeRepository;
		this.severancePayAndEmployeeRepository = severancePayAndEmployeeRepository;
		this.employee2Repository = employee2Repository;
		this.modelMapper = modelMapper;
	}
	
	/* 퇴직금 조회 */
	public List<SeverancePayDTO> findSeverancePayList(SelectCriteria selectCriteria) {
		
		int index = selectCriteria.getPageNo() -1;
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();
		
		Pageable paging = PageRequest.of(index, count, Sort.by("retireStatus"));
		
		List<SeverancePayAndEmployee> sevList = new ArrayList<SeverancePayAndEmployee>();
		
		if(searchValue != null) {
			
			if("retireStatus".equals(selectCriteria.getSearchCondition())) {
				sevList = severancePayAndEmployeeRepository.findByretireStatusContaining(selectCriteria.getSearchValue(), paging);
			}
			
			if("empName".equals(selectCriteria.getSearchCondition())) {
				sevList = severancePayAndEmployeeRepository.findByEmployeeEmpNameContaining(selectCriteria.getSearchValue(), paging);
			}
			
		} else {
			sevList = severancePayAndEmployeeRepository.findAll(paging).toList();
		}
		
		return sevList.stream().map(severancePayAndEmployee -> modelMapper.map(severancePayAndEmployee, SeverancePayDTO.class)).collect(Collectors.toList());
	}

	
	/* 퇴직금 등록 */
	@Transactional
	public void registSevPay(SeverancePayDTO severancePay) {
		 
//		System.out.println("사원코드" + severancePay.getEmpCode() + "/ ");
		
		severancePayRepository.save(modelMapper.map(severancePay, SeverancePay.class));
	}

	
	/* 퇴직금 페이징 */
	public int selectTotalCount(String searchCondition, String searchValue) {
		
		int count = 0;
		
		if(searchValue != null) {
			if("retireStatus".equals(searchCondition)) {
				count = severancePayAndEmployeeRepository.countByRetireStatusContaining(searchValue);
			}
			if("empName".equals(searchCondition)) {
				count = severancePayAndEmployeeRepository.countByEmployeeEmpNameContaining(searchValue);
			}
		} else {
			count = (int)severancePayAndEmployeeRepository.count();
			System.out.println("count: " + count);
		}
		return count;
		
	}

	public SeverancePayDTO findSevPayByCode(int empCode) {
		
		SeverancePayAndEmployee sevPay = severancePayAndEmployeeRepository.findByEmpCodeLike(empCode);
		return modelMapper.map(sevPay, SeverancePayDTO.class);
	}

	/* 퇴직금 내역 수정 */
	@Transactional
	public void modifySevPay(SeverancePayDTO severancePay) {
		
		SeverancePay severancePay2 = severancePayRepository.findByEmpCodeLike(severancePay.getEmpCode());
		severancePay2.setRetireStatus(severancePay.getRetireStatus());
		severancePay2.setRetirePay(severancePay.getRetirePay());
		
	}

	/* 퇴직금 내역 삭제 */
	public int deleteSevPay(Integer empCode) {
		
		int result = 0;
		
		result = severancePayRepository.deleteByEmpCode(empCode);
		
		System.out.println("삭제 되셨습니까!!!!!!!!!!!!!" + result);
		return result;
	}

	/* 퇴직금 내역 등록을 위한 사원명 조회 */
//	public List<EmployeeDTO> findEmpName() {
//		
//		List<Employee> empList = employee2Repository.findAll(Sort.by("empName"));
//		return empList.stream().map(employee -> modelMapper.map(employee, EmployeeDTO.class)).collect(Collectors.toList());
//		
//	}

//	public List<EmployeeDTO> findEmpCodeByEmpCode(EmployeeDTO employee) {
//		
//		List<Employee> empList = employee2Repository.findEmpCodeEmpName();
//		
//		return int;
//	}

	/* ajax 사원명 조회 */
	public List<EmployeeDTO> findEmployeeList() {
		
		List<Employee> empList = employee2Repository.findAll(Sort.by("empName"));
		System.out.println("서비스에서 확인!!!!!!" + empList);
		
		return empList.stream().map(employee -> modelMapper.map(employee, EmployeeDTO.class)).collect(Collectors.toList());
	}

	
//	public SeverancePayDTO findSeverancePayList(int empCode) {
//		
//		SeverancePay result = severancePayRepository.findBySevPayByCode(empCode);
//		return modelMapper.map(result, SeverancePayDTO.class);
//	}
}
