package com.volt.asap.accounting.dto;

import com.volt.asap.hrm.entity.Employee;

public class SeverancePayDTO {
	
	private int retirePay;
	private int empCode;
	private String retireStatus;
	private Employee employee;
	
	public SeverancePayDTO() {
	}

	public SeverancePayDTO(int retirePay, int empCode, String retireStatus, Employee employee) {
		super();
		this.retirePay = retirePay;
		this.empCode = empCode;
		this.retireStatus = retireStatus;
		this.employee = employee;
	}

	public int getRetirePay() {
		return retirePay;
	}

	public void setRetirePay(int retirePay) {
		this.retirePay = retirePay;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public String getRetireStatus() {
		return retireStatus;
	}

	public void setRetireStatus(String retireStatus) {
		this.retireStatus = retireStatus;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "SeverancePayDTO [retirePay=" + retirePay + ", empCode=" + empCode + ", retireStatus=" + retireStatus
				+ ", employee=" + employee + "]";
	}

}
