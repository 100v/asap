package com.volt.asap.accounting.dto;

import com.volt.asap.hrm.entity.Attendance;
import com.volt.asap.hrm.entity.Employee;

public class SalaryDTO {
	
	private int overAllowance;
	private int actualPayment;
	private int salNo;
	private int empCode;
	private int salaryMonth;
	private int salaryYear;
	private Employee employee;
	private Attendance Attendance;
	
	public SalaryDTO() {
	}

	public SalaryDTO(int overAllowance, int actualPayment, int salNo, int empCode, int salaryMonth, int salaryYear,
			Employee employee, com.volt.asap.hrm.entity.Attendance attendance) {
		super();
		this.overAllowance = overAllowance;
		this.actualPayment = actualPayment;
		this.salNo = salNo;
		this.empCode = empCode;
		this.salaryMonth = salaryMonth;
		this.salaryYear = salaryYear;
		this.employee = employee;
		Attendance = attendance;
	}

	public int getOverAllowance() {
		return overAllowance;
	}

	public void setOverAllowance(int overAllowance) {
		this.overAllowance = overAllowance;
	}

	public int getActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(int actualPayment) {
		this.actualPayment = actualPayment;
	}

	public int getSalNo() {
		return salNo;
	}

	public void setSalNo(int salNo) {
		this.salNo = salNo;
	}

	public int getEmpCode() {
		return empCode;
	}

	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}

	public int getSalaryMonth() {
		return salaryMonth;
	}

	public void setSalaryMonth(int salaryMonth) {
		this.salaryMonth = salaryMonth;
	}

	public int getSalaryYear() {
		return salaryYear;
	}

	public void setSalaryYear(int salaryYear) {
		this.salaryYear = salaryYear;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Attendance getAttendance() {
		return Attendance;
	}

	public void setAttendance(Attendance attendance) {
		Attendance = attendance;
	}

	@Override
	public String toString() {
		return "SalaryDTO [overAllowance=" + overAllowance + ", actualPayment=" + actualPayment + ", salNo=" + salNo
				+ ", empCode=" + empCode + ", salaryMonth=" + salaryMonth + ", salaryYear=" + salaryYear + ", employee="
				+ employee + ", Attendance=" + Attendance + "]";
	}

}

