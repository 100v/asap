package com.volt.asap.accounting.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.volt.asap.accounting.entity.SalaryAndAttendance;

public interface SalaryAndAttendanceRepository extends JpaRepository<SalaryAndAttendance, Integer>{

}
