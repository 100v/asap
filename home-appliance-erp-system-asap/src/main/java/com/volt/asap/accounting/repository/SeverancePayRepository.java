package com.volt.asap.accounting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.volt.asap.accounting.entity.SeverancePay;

public interface SeverancePayRepository extends JpaRepository<SeverancePay, Integer>{

	SeverancePay findByEmpCodeLike(int empCode);

	@Transactional
	int deleteByEmpCode(Integer empCode);

//	SeverancePay findBySevPayByCode(int empCode);

}
