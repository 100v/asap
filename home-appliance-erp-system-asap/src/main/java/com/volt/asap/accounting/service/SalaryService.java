package com.volt.asap.accounting.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.volt.asap.accounting.dto.EmployeeDTO;
import com.volt.asap.accounting.dto.SalaryDTO;
import com.volt.asap.accounting.entity.Salary;
import com.volt.asap.accounting.entity.SalaryAndEmployee;
import com.volt.asap.accounting.repository.Employee2Repository;
import com.volt.asap.accounting.repository.SalaryAndAttendanceRepository;
import com.volt.asap.accounting.repository.SalaryAndEmployeeRepository;
import com.volt.asap.accounting.repository.SalaryRepository;
import com.volt.asap.accounting.repository.SeverancePayRepository;
import com.volt.asap.common.paging.SelectCriteria;
import com.volt.asap.hrm.entity.Employee;
import com.volt.asap.hrm.repository.EmployeeRepository;

@Service
public class SalaryService {
	
	private final SalaryRepository salaryRepository;
	private final SalaryAndEmployeeRepository salaryAndEmployeeRepository;
	private final ModelMapper modelMapper;
	private final Employee2Repository employee2Repository;
	private final SalaryAndAttendanceRepository salaryAndAttendanceRepository;
	
	@Autowired
	public SalaryService(SalaryRepository salaryRepository, SeverancePayRepository severancePayRepository
						, EmployeeRepository employeeRepository, SalaryAndEmployeeRepository salaryAndEmployeeRepository
						, Employee2Repository employee2Repository, SalaryAndAttendanceRepository salaryAndAttendanceRepository, ModelMapper modelMapper) {
		this.salaryRepository = salaryRepository;
		this.salaryAndEmployeeRepository = salaryAndEmployeeRepository;
		this.employee2Repository = employee2Repository;
		this.salaryAndAttendanceRepository = salaryAndAttendanceRepository;
		this.modelMapper = modelMapper;
	}
	
	/* 급여내역 조회 */
	public List<SalaryDTO> findSalaryList(SelectCriteria selectCriteria) {
		
		int index = selectCriteria.getPageNo() -1;
		int count = selectCriteria.getLimit();
		String searchValue = selectCriteria.getSearchValue();
		
		Pageable paging = PageRequest.of(index, count, Sort.by("salaryMonth").descending());
		
		List<SalaryAndEmployee> salList = new ArrayList<SalaryAndEmployee>();
		
		if(searchValue != null) {
			
			if("salaryMonth".equals(selectCriteria.getSearchCondition())) {
				salList = salaryAndEmployeeRepository.findBySalaryMonthContaining(Integer.valueOf(selectCriteria.getSearchValue()), paging);
			}
			
			if("empName".equals(selectCriteria.getSearchCondition())) {
				salList = salaryAndEmployeeRepository.findByEmployeeEmpNameContaining(selectCriteria.getSearchValue(), paging);
			}
			
		} else {
			salList = salaryAndEmployeeRepository.findAll(paging).toList();
		}
		
//		List<SalaryAndEmployee> salList = salaryAndEmployeeRepository.findAll(Sort.by("employeeEmpCode"));
		
		return salList.stream().map(salaryAndEmployee -> modelMapper.map(salaryAndEmployee, SalaryDTO.class)).collect(Collectors.toList());
		
	}


	public int selectTotalCount(String searchCondition, String searchValue) {
		
		int count = 0;
		
		if(searchValue != null) {
			if("salaryMonth".equals(searchCondition)) {
				count = salaryAndEmployeeRepository.countBySalaryMonthContaining(Integer.valueOf(searchValue));
			}
			if("empName".equals(searchCondition)) {
				count = salaryAndEmployeeRepository.countByEmployeeEmpNameContaining(searchValue);
			}
		} else {
			count = (int)salaryAndEmployeeRepository.count();
			System.out.println("count: " + count);
		}
		return count;
	}

	
	/* 급여 등록 */
//	@Transactional
//	public void registSalary(SalaryDTO salary) {
//		
//		salaryRepository.save(modelMapper.map(salary, Salary.class));
//		
//	}

	/* 급여 내역 수정 */
	public SalaryDTO findModSalaryByNo(int salNo) {
		
		SalaryAndEmployee salary = salaryAndEmployeeRepository.findBySalNoLike(salNo);
		
		System.out.println("서비스에서 salary 담긴 값 확인");
		System.out.println(salary);
		
		return modelMapper.map(salary, SalaryDTO.class);
	}
	
	/* 급여 내역 수정 */
	@Transactional
	public void modifySalary(SalaryDTO salary) {
		
		System.out.println("salNo 조회 : " + salary);

		Salary modSalary = salaryRepository.findBySalNo(salary.getSalNo());
		
		modSalary.setSalaryMonth(salary.getSalaryMonth());
		modSalary.setOverAllowance(salary.getOverAllowance());
		modSalary.setActualPayment(salary.getActualPayment());
		/* Employee 테이블 SalaryBase 업데이트도 필요 */
	}

//	/* 급여 등록을 위한 사원 조회 */ 
//	public List<EmployeeDTO> findEmpNameAll() {
//	
//		List<Employee> empList = employee2Repository.findAll(Sort.by("empName"));
//		return empList.stream().map(employee -> modelMapper.map(employee, EmployeeDTO.class)).collect(Collectors.toList());
//	}
//
//	public List<SalaryDTO> findSalaryInfoByCode() {
//		return null;
//	}
//
	/* ajax 사원명 가져오기 */
	public List<EmployeeDTO> findEmployeeList() {
		
		List<Employee> empList = employee2Repository.findAll(Sort.by("empName"));
		System.out.println("서비스에서 확인!!!!!!" + empList);
		
		return empList.stream().map(employee -> modelMapper.map(employee, EmployeeDTO.class)).collect(Collectors.toList());	
	}
//	/* 급여 등록을 위한 해당 사원의 급여 조회 */
//	public SalaryDTO findSalaryList(int empCode) {
//		
//		SalaryAndEmployee salary = salaryAndEmployeeRepository.findByEmpCode(empCode);
//		return modelMapper.map(salary, SalaryDTO.class);
//
//	}

	
	/* 급여 내역 삭제 */
	public int deleteSalary(int salNo) {
		
		int result = 0;
		
		result = salaryRepository.deleteBySalNo(salNo);
		
		System.out.println("서비스의 result: " + result);
		return result;
	}

	public SalaryDTO findSalaryByEmpCode(int empCode) {
		return null;
	}

	/* 급여 등록 */
	@Transactional
	public void registSalary(SalaryDTO salary) {
		
		System.out.println("급여 서비스!!!!!" + salary);
		salaryRepository.save(modelMapper.map(salary, Salary.class));
		
	}

	
	/* 제발 급여!!!!!!! 등록 */

	
	/* !!!!!!급여 등록을 위한 샐러리 조회!!!!!!!*/
//	public SalaryDTO findSalaryByNo(int empCode) {
//		 
//		SalaryAndAttendance salary = SalaryAndAttendanceRepository.findSalaryByEmpCode(empCode);
//		return salary.stream().map(salaryAndAttendance -> modelMapper.map(salaryAndAttendance, SalaryDTO.class)).collect(Collectors.toList());
//	}


//	/* 급여 등록을 위한 사원 급여 내역 조회 */
//	public List<SalaryDTO> findSalaryInfo() {
//		
//		List<Salary> salList = salaryRepository.findAll();
//		
//		System.out.println("서비스에서 사원 내역이 나와야 합니다.");
//		System.out.println(salList);
//		return salList.stream().map(salary -> modelMapper.map(salary, SalaryDTO.class)).collect(Collectors.toList());
//	}


}
