package com.volt.asap.accounting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.volt.asap.accounting.dto.SalaryDTO;
import com.volt.asap.accounting.entity.Salary;

public interface SalaryRepository extends JpaRepository<Salary, Integer>{

	Salary findBySalNo(int salNo);

	@Transactional
	int deleteBySalNo(int salNo);


}

