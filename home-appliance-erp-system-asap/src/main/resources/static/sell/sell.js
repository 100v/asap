/**
 * 입력된 부서의 부서원 정보를 AJAX 방식으로 가져오는 함수
 * @param element 부서를 선택할 수 있는 <Select> 엘리먼트
 * @see '/groupware/schedule/getDepartmentMembers'
 */
 
 
window.onload = function () {
    
    /* 부서 드롭다운 리스트 값 변경시 발생하는 이벤트 할당 - change */
    const strorageSelector = document.querySelector('#content select#storage');
    strorageSelector.addEventListener('change', () => {
        getProductNames(strorageSelector);
    });
}
function getProductNames(element) {
    const storage = element.value;

    if(storage === 'none') {
        updateProductNamesSelector({
            none: '이름'
        });

        return;
    }

    $.ajax({
        type: 'post',
        url: '/sell/getProductNames',
        data: {
            product: product
        },
        success: function (data) {
            updateProductNamesSelector(data);
        },
        error: function (error) {
            console.log('error: ', error);
        }
    });
}