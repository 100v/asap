/**
 * 결재라인이 저장되는 리스트
 */
let approveEmployeeList = [];
let draftNo = document.location.pathname.split('/')[3];

window.onload = function () {
    tinymce.init({
        selector: 'div#webEditor',
        width: '100%',
        height: '600px',
        skin: 'oxide',
        plugins: [
            'advlist', 'autolink', 'link', 'image', 'lists', 'charmap', 'preview', 'anchor', 'pagebreak',
            'searchreplace', 'wordcount', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'insertdatetime',
            'media', 'table', 'emoticons', 'template', 'help'
        ],
        toolbar: 'undo redo | styles | bold italic | alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
            'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help'
    });

    /* 기안서 수정 취소 버튼 클릭시 발생하는 이벤트 할당 - click */
    let cancelBtn = document.querySelector('#content .draft-navigation button#cancelBtn');
    cancelBtn.addEventListener('click', () => {
        if(confirm('정말로 취소하시겠습니까?')) {
            location.href = "/groupware/draft/" + draftNo;
        }
    });

    /* 결재자 추가 버튼 클릭시 발생하는 이벤트 할당 - click */
    let addApproveEmployeeBtn = document.querySelector('#content .draft-approve-employee button#addApproveEmployee');
    addApproveEmployeeBtn.addEventListener('click', () => {
        addApproveEmployee(document.querySelector('#content select#draftApproveEmployee').value);
    });

    /* 부서 드롭다운 리스트 값 변경시 발생하는 이벤트 할당 - change */
    let departmentSelector = document.querySelector('#content select#department');
    departmentSelector.addEventListener('change', () => {
        getDepartmentMembers(departmentSelector);
    });

    /* 기안서 임시저장 버튼 클릭시 발생하는 이벤트 할당 - click */
    let tempSaveBtn = document.querySelector('#content .draft-navigation button#tempSaveBtn');
    tempSaveBtn.addEventListener('click', () => {
        doWriteDraft('temp');
    });

    /* 기안서 기안요청 버튼 클릭시 발생하는 이벤트 할당 - click */
    let requestBtn = document.querySelector('#content .draft-navigation button#requestBtn');
    requestBtn.addEventListener('click', () => {
        doWriteDraft('save');
    });

    loadApprovalLine();
    updateDataList();
}

/**
 * 기안서 작성을 담당하는 함수. (AJAX)
 */
function doWriteDraft(saveType) {
    if(saveType === 'save') {
        if(!confirm("기안이 저장되어 진행중으로 변경되면 결재 라인을 변경할 수 없습니다. 그래도 진행 하시겠습니까?")) {
            return;
        }
    }
    if(!isValidWriteContent()) {
        return;
    }

    $.ajax({
        type: 'post',
        url: '/groupware/draft/write',
        traditional: true,
        contentType: false,
        processData: false,
        data: getData(saveType),
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.href = '/groupware/draft/' + data.draftNo;
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('알수없는 이유로 수정에 실패했습니다.');
            }
        }
    });
}

/**
 * 작성한 기안서의 유효성을 검사하는 함수
 * @returns {boolean} 제목 및 구분을 모두 입력했으면 true, 아니면 false를 반환
 */
function isValidWriteContent() {
    let draftTitle = document.querySelector('#content #titleInfo input#draftTitle');
    if (draftTitle.value === '') {
        alert('제목을 입력해주세요.');
        draftTitle.focus();
        return false;
    }

    let draftType = document.querySelector('#content #titleInfo input#draftType');
    if (draftType.value === '') {
        alert('구분을 입력해주세요.');
        draftType.focus();
        return false;
    }

    return true;
}

/**
 * 결재자의 사원번호를 받아서 결재 테이블에 추가하는 함수
 * @param empCode 결재자의 사원번호
 */
function addApproveEmployee(empCode) {
    if(!empCode) return;
    if(hasEmpCode(approveEmployeeList, empCode)) return;
    if(approveEmployeeList.length >= 4) {
        alert('현재는 세명까지만 추가할 수 있습니다.');
        return;
    }

    $.ajax({
        type: 'post',
        url: '/employee/getEmployeeInfo',
        data: {
            empCode: empCode
        },
        success: function (data) {
            let trs = document.querySelectorAll('.author-and-approval #approvalInfo tr');

            let employeeElements = {
                empCode: empCode,
                jobName: document.createElement('td'),
                sign: document.createElement('td'),
                empName: document.createElement('td'),
                date: document.createElement('td')
            }

            employeeElements.jobName.innerText = data.jobName;
            employeeElements.empName.innerText = data.empName;
            employeeElements.date.innerHTML = '&nbsp;';

            trs[0].appendChild(employeeElements.jobName);
            trs[1].appendChild(employeeElements.sign);
            trs[2].appendChild(employeeElements.empName);
            trs[3].appendChild(employeeElements.date);

            approveEmployeeList.push(employeeElements);
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('알수없는 이유로 사원 정보를 로드하는데 실패했습니다.');
            }
        }
    });
}

/**
 * 결재라인 리스트({@link approveEmployeeList})에서 해당 사원이 있는지 체크하는 함수
 * @param list 확인할 리스트
 * @param empCode 확인할 사원번호
 * @returns {boolean} 있으면 true, 없으면 false
 */
function hasEmpCode(list, empCode) {
    for (const item of list) {
        if(item.empCode === empCode) {
            return true;
        }
    }

    return false;
}

/**
 * 입력된 부서의 부서원 정보를 AJAX 방식으로 가져오는 함수
 * @param element 부서를 선택할 수 있는 <Select> 엘리먼트
 */
function getDepartmentMembers(element) {
    const dept = element.value;

    if(dept === 'none') {
        updateDepartmentMembersSelector({
            none: '이름'
        });

        return;
    }

    $.ajax({
        type: 'post',
        url: '/employee/getMembersFromDepartment',
        data: {
            dept: dept
        },
        success: function (data) {
            updateDepartmentMembersSelector(data);
        },
        error: function (error) {
            console.log('error: ', error);
        }
    });
}

/**
 * 부서에 속해있는 부서원들의 정보를 받아서 <Select> 엘리먼트를 업데이트하는 함수
 * @param data 부서원들의 정보가 담긴 오브젝트
 */
function updateDepartmentMembersSelector(data) {
    const selector = document.querySelector('#content select#draftApproveEmployee');

    /* Select 엘리먼트의 기존 내용 삭제 */
    selector.innerHTML = '';

    for (const key in data) {
        const option = document.createElement('option');
        option.value = key;
        option.text = data[key];
        selector.appendChild(option);
    }
}

/**
 * 페이지 로드시에 기안 상신자인 본인을 추가하는 함수
 */
function loadApprovalLine() {
    $.ajax({
        type: 'get',
        url: '/groupware/getEmpCode',
        success: function (data) {
            addApproveEmployee(data.empCode);
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('알수없는 이유로 내 정보를 로드하는데 실패했습니다.');
            }
        }
    });
}

/**
 * 기안서 작성을 위해 작성한 데이터를 {@link FormData}로 모아주는 함수
 * @returns {FormData} 기안서 수정 데이터
 */
function getData(saveType) {
    let formData = new FormData();

    formData.append("title", document.querySelector('#content #titleInfo input#draftTitle').value);
    formData.append("type", document.querySelector('#content #titleInfo input#draftType').value);
    formData.append("content", tinymce.get('webEditor').getContent());
    formData.append("saveType", saveType);

    approveEmployeeList.forEach((item) => {
        formData.append("approveEmployee", item.empCode);
    });

    return formData;
}

/**
 * 자동완성을 위한 기안서 구분 리스트를 AJAX 방식으로 가져오고, 자동완성 함수를 호출하는 함수
 */
function updateDataList() {
    $.ajax({
        type: 'get',
        url: '/groupware/draft/getDraftType',
        success: function (data) {
            autocomplete(document.querySelector('#content #titleInfo input#draftType'), data);
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('기안서 구분을 로드하는데 실패했습니다.');
            }
        }
    });
}