window.onload = function () {
    setTimeout(() => {
        if(message) {
            alert(message);
        }
    }, 500);

    /* 기안 작성 버튼을 클릭하면 발생하는 이벤트 할당 - click */
    let writeDraftBtn = document.querySelector('#content .search-option button#writeDraft');
    writeDraftBtn.addEventListener('click', () => {
        openDraftFormSelector()
    });

    loadSelectCriteria();

    addEventToTable();

    addEventToStatusSelector();
}

/**
 * 기안서 양식을 선택하는 팝업창을 열어주는 함수
 */
function openDraftFormSelector() {
    let url = "/groupware/draft/draftForm";
    let name = "기안서 양식 선택";

    let option = "width=600, height=600, top=220, left=650";
    let draftFormSelectorPopup = window.open(url, name, option);
}

function moveToDraftWrite(draftFormNo) {
    if(draftFormNo === null) {
        location.href = '/groupware/draft/write';
        return;
    }

    location.href = '/groupware/draft/write?formNo=' + draftFormNo;
}

/**
 * 검색어 정보를 AJAX 방식으로 가져오고 기존 검색어를 세팅하는 함수
 */
function loadSelectCriteria() {
    let draftStatusId;
    switch (draftStatusQuery) {
        case "전체":
            draftStatusId = "all"; break;
        case "진행중":
            draftStatusId = "inProgress"; break;
        case "반려":
            draftStatusId = "reject"; break;
        case "결재완료":
            draftStatusId = "approved"; break;
        case "임시저장":
            draftStatusId = "tempSave"; break;
        default:
            draftStatusId = "all"; break;
    }

    /* 기안 상태 */
    selectElement(document.querySelector('#content .selector-list span#' + draftStatusId));

    /* 기안서 구분 */
    $.ajax({
        type: 'get',
        url: '/groupware/draft/getDraftType',
        success: function (data) {
            let typeSelector = document.querySelector('#content .search-option select#draftType');
            for (const item of data) {
                let typeOption = document.createElement('option');
                typeOption.text = item;
                typeOption.value = item;
                typeSelector.appendChild(typeOption);
            }

            if(typeQuery) {
                typeSelector.querySelector('option[value="' + typeQuery +'"]').selected = true;
            }
        },
        error: function (error) {
            if (error[0]) {
                alert(error[0]);
            } else {
                alert('기안서 구분의 로드를 실패했습니다.');
            }
        }
    });

    /* 기안자 */
    $.ajax({
        type: 'get',
        url: '/groupware/draft/getDraftAuthor',
        success: function (data) {
            let authorSelector = document.querySelector('#content .search-option select#author');

            for (const dataKey in data) {
                let authorOption = document.createElement('option');
                authorOption.text = data[dataKey];
                authorOption.value = dataKey;
                authorSelector.appendChild(authorOption);
            }

            if(authorQuery) {
                console.info(authorSelector);
                authorSelector.querySelector('option[value="' + authorQuery +'"]').selected = true;
            }
        },
        error: function (error) {
            alert('기안서 작성자의 로드에 실패했습니다.');
        }
    });
}

/**
 * 테이블의 행들을 클릭했을 때 해당하는 상세보기 페이지로 이동하는 이벤트를 등록하는 함수
 */
function addEventToTable() {
    const elements = document.querySelectorAll('.result .table-list tbody tr');

    elements.forEach((element) => {
        element.addEventListener('click', (event) => {
            const draftNo = element.querySelector('#draftNo').innerText;
            location.href = '/groupware/draft/' + draftNo;
        });
    });
}

/**
 * 기안서 상태 선택기를 누르면 동작할 이벤트 할당
 */
function addEventToStatusSelector() {
    let searchForm = document.querySelector('#content form#searchForm');
    let selectorList = document.querySelector('#content div.selector-list');
    let selectorInput = document.querySelector('#content .search-option input#draftStatus');

    /* 전체 */
    selectorList.children[0].addEventListener('click', () => {
        selectElement(selectorList.children[0]);
        selectorInput.value = '전체';
        searchForm.submit();
    });

    /* 진행중 */
    selectorList.children[1].addEventListener('click', () => {
        selectElement(selectorList.children[1]);
        selectorInput.value = '진행중';
        searchForm.submit();
    });

    /* 반려 */
    selectorList.children[2].addEventListener('click', () => {
        selectElement(selectorList.children[2]);
        selectorInput.value = '반려';
        searchForm.submit();
    });

    /* 결재 */
    selectorList.children[3].addEventListener('click', () => {
        selectElement(selectorList.children[3]);
        selectorInput.value = '결재완료';
        searchForm.submit();
    });

    /* 결재 */
    selectorList.children[4].addEventListener('click', () => {
        selectElement(selectorList.children[4]);
        selectorInput.value = '임시저장';
        searchForm.submit();
    });
}

/**
 * 기안서 상태 선택기를 눌렀을 때 선택되어 있도록 selected 클래스를 토글해주는 함수
 * @param element selected 클래스가 추가될 {@link HTMLElement}
 */
function selectElement(element) {
    let children = document.querySelector('#content div.selector-list').children;

    for (const child of children) {
        child.classList.remove('selected');
    }

    element.classList.add('selected');
}