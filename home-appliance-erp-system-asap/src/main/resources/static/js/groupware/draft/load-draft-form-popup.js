window.onload = function () {
    setTimeout(() => {
        if(message) {
            alert(message);
        }
    }, 500);

    let noDraftBtn = document.querySelector('#content div.search-option button#noDraftBtn');
    if(noDraftBtn) {
        noDraftBtn.addEventListener('click', () => {
            window.opener.moveToDraftWrite(null);
            window.close();
        });
    }

    loadDraftFormType();

    addEventToTable();
}

/**
 * 기안서 양식 구분을 AJAX 방식으로 가져오는 함수
 */
function loadDraftFormType() {
    $.ajax({
        type: 'get',
        url: '/groupware/draftForm/getDraftFormType',
        success: function (data) {
            let typeSelector = document.querySelector('#content .search-option select#type');
            for (const item of data) {
                let typeOption = document.createElement('option');
                typeOption.text = item;
                typeOption.value = item;
                typeSelector.appendChild(typeOption);
            }

            if(typeQuery) {
                typeSelector.querySelector('option[value=' + typeQuery +']').selected = true;
            }
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('기안서 양식 구분의 로드를 실패했습니다.');
            }
        }
    });
}

/**
 * 테이블의 행들을 클릭했을 때 창을 닫고 기안서 작성 페이지로 이동하는 이벤트를 등록하는 함수
 */
function addEventToTable() {
    const elements = document.querySelectorAll('.result .table-list tbody tr');

    elements.forEach((element) => {
        element.addEventListener('click', (event) => {
            const draftFormNo = element.querySelector('#draftFormNo').innerText;
            window.opener.moveToDraftWrite(draftFormNo);
            window.close();
        });
    });
}