window.onload = function () {
    let iFrame = document.querySelector( '#content iframe#draft-ifr' );
    resizeIFrameToFitContent(iFrame);

    let deleteBtn = document.querySelector('#content .draft-navigation button#deleteBtn');
    deleteBtn.addEventListener('click', () => {
        doDeleteDraft();
    });

    let editBtn = document.querySelector('#content .draft-navigation button#editBtn');
    if(editBtn) {
        editBtn.addEventListener('click', () => {
            location.href = '/groupware/draft/' + document.location.pathname.split('/')[3] + '/modify';
        });
    }

    let submitBtn = document.querySelector('#content .draft-navigation button#submitBtn');
    if(submitBtn) {
        submitBtn.addEventListener('click', () => {
            doDraftStatusUpdate();
        });
    }

    let approveBtn = document.querySelector('#content .draft-navigation button#approveBtn');
    if(approveBtn) {
        approveBtn.addEventListener('click', () => {
            doApprove();
        });
    }

    let rejectBtn = document.querySelector('#content .draft-navigation button#rejectBtn');
    if(rejectBtn) {
        rejectBtn.addEventListener('click', () => {
            doReject();
        });
    }
}

function resizeIFrameToFitContent(iFrame) {
    if(iFrame instanceof HTMLIFrameElement) {
        iFrame.style.height = iFrame.contentWindow.document.body.scrollHeight + 40 + 'px';
    }
}

/**
 * 기안서를 삭제하는 함수
 */
function doDeleteDraft() {
    $.ajax({
        type: 'post',
        url: '/groupware/draft/delete',
        data: {
            draftNo: document.location.pathname.split('/')[3]
        },
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.href = '/groupware/draft';
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('알수없는 이유로 삭제에 실패했습니다.');
            }
        }
    });
}

/**
 * 기안서 상태 변경요청을 하는 함수
 */
function doDraftStatusUpdate() {
    $.ajax({
        type: 'post',
        url: '/groupware/draft/updateStatus',
        data: {
            draftNo: document.location.pathname.split('/')[3]
        },
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
        },
        error: function (error) {
            if (error?.responseJSON?.message) {
                alert(error?.responseJSON?.message)
            } else {
                alert('알수없는 이유로 상신에 실패했습니다.');
            }
        }
    });
}

/**
 * 기안서에 결재하는 함수
 */
function doApprove() {
    $.ajax({
        type: 'post',
        url: '/groupware/draft/approve',
        data: {
            draftNo: document.location.pathname.split('/')[3]
        },
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.reload();
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('결재에 실패했습니다.');
            }
        }
    });
}

/**
 * 기안서 반려하는 함수
 */
function doReject() {
    $.ajax({
        type: 'post',
        url: '/groupware/draft/reject',
        data: {
            draftNo: document.location.pathname.split('/')[3]
        },
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.reload();
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('반려에 실패했습니다.');
            }
        }
    });
}