window.onload = function () {
    setTimeout(() => {
        if(message) {
            alert(message);
        }
    }, 500);

    loadDraftFormType();

    addEventToTable();
}

/**
 * 기안서 양식 구분을 AJAX 방식으로 가져오는 함수
 */
function loadDraftFormType() {
    $.ajax({
        type: 'get',
        url: '/groupware/draftForm/getDraftFormType',
        success: function (data) {
            let typeSelector = document.querySelector('#content .search-option select#type');
            for (const item of data) {
                let typeOption = document.createElement('option');
                typeOption.text = item;
                typeOption.value = item;
                typeSelector.appendChild(typeOption);
            }

            if(typeQuery) {
                typeSelector.querySelector('option[value=' + typeQuery +']').selected = true;
            }
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('기안서 양식 구분의 로드를 실패했습니다.');
            }
        }
    });
}

/**
 * 테이블의 행들을 클릭했을 때 해당하는 상세보기 페이지로 이동하는 이벤트를 등록하는 함수
 */
function addEventToTable() {
    const elements = document.querySelectorAll('.result .table-list tbody tr');

    elements.forEach((element) => {
        element.addEventListener('click', (event) => {
            const draftFormNo = element.querySelector('#draftFormNo').innerText;
            location.href = '/groupware/draftForm/' + draftFormNo + '/modify';
        });
    });
}