let attendeeMap = new Map();
let fileMap = new Map();
let attendeeListElement;

window.onload = function () {
    tinymce.init({
        selector: 'div#webEditor',
        width: '100%',
        height: '600px',
        skin: 'oxide',
        plugins: [
            'advlist', 'autolink', 'link', 'image', 'lists', 'charmap', 'preview', 'anchor', 'pagebreak',
            'searchreplace', 'wordcount', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'insertdatetime',
            'media', 'table', 'emoticons', 'template', 'help'
        ],
        toolbar: 'undo redo | styles | bold italic | alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
            'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help'
    });

    /* 참여자를 추가/제거할 때 페이지에 표시될 엘리멘트 */
    attendeeListElement = document.querySelector('#content div#attendeeList');

    /* 수정 버튼 누르면 발생하는 이벤트 할당 - click */
    let submitButton = document.querySelector('#content #modifyBtn');
    submitButton.addEventListener('click', () => {
        if(isValidWriteContent()) {
            doScheduleModify();
        }
    });

    /* 업로드 된 파일 테이블의 <tr> 클릭하면 발생하는 이벤트 할당 */
    let uploadedFileTablesTr = document.querySelectorAll('#content .schedule-attached-file table tbody tr');
    uploadedFileTablesTr.forEach((tr) => {
        tr.addEventListener('click', () => {
            removeFile(tr.querySelector('#realPath').innerText, tr.querySelector('.check'));
        });
    });

    /* 부서 드롭다운 리스트 값 변경시 발생하는 이벤트 할당 - change */
    let departmentSelector = document.querySelector('#content select#department');
    departmentSelector.addEventListener('change', () => {
        getDepartmentMembers(departmentSelector);
    });

    /* 참석자 추가 버튼 누르면 발생하는 이벤트 할당 - click */
    let addAttendeeButton = document.querySelector('#content button#addAttendee');
    addAttendeeButton.addEventListener('click', () => {
        let attendeeSelector = document.querySelector('#content select#scheduleAttendee');
        addAttendee({
            empCode: attendeeSelector.value,
            empName: attendeeSelector.options[attendeeSelector.selectedIndex].text
        });
    });

    loadAttendee();
}

/**
 * 일정 입력시 입력값에 대해 검증하는 함수.
 * 제목, 날짜를 입력하지 않았거나 시작 날짜보다 종료 날짜가 빠른 경우를 검증
 * @returns {boolean} 검증에 문제가 없다면 true, 하나라도 잘못 입력되었다면 false를 반환한다.
 */
function isValidWriteContent() {

    let titleElement = document.querySelector('.schedule-info #scheduleTitle');
    if(titleElement.value === "") {
        alert('제목을 입력해주세요.');
        titleElement.focus();
        return false;
    }

    let startDateElement = document.querySelector('.schedule-info #scheduleStartDate');
    if(startDateElement.value === "") {
        alert('시작 날짜를 입력해주세요.');
        startDateElement.focus();
        return false;
    }

    let endDateElement = document.querySelector('.schedule-info #scheduleEndDate');
    if(endDateElement.value === "") {
        alert('종료 날짜를 입력해주세요.');
        endDateElement.focus();
        return false;
    }

    if(new Date(startDateElement.value) >= new Date(endDateElement.value)) {
        alert('일정의 종료 날짜가 시작 날짜보다 커야합니다.');
        startDateElement.focus();
        return false;
    }

    return true;
}

/**
 * AJAX 방식으로 일정 정보를 보내 일정을 등록하는 함수
 * 데이터는 {@link getData} 함수를 사용해 받아온다.
 */
function doScheduleModify() {
    $.ajax({
        type: 'post',
        url: '/groupware/schedule/' + scheduleNo + '/modify',
        traditional: true,
        contentType: false,     // multipart/form-data 로 전송하기 위해 기본 설정된 contentType을 끔
        processData: false,     // FormData를 전송하기 위해서 적어야함
        data: getData(),
        success: function (data) {
            if(data.status === 'success') {
                alert(data.message);
                location.href = '/groupware/schedule/' + data.dateNo;
            }
        },
        error: function (error) {
            if(error.message) {
                alert(error.message);
            } else {
                alert('필수 기입 항목을 작성하지 않았거나 잘못 된 접근입니다.');
            }
        }
    });
}

/**
 * 일정 입력에 사용될 정보들을 모아서 {@link FormData} 객체로 반환하는 함수
 * @returns {FormData} 일정 정보가 담긴 폼 데이터 객체
 */
function getData() {
    let formData = new FormData();
    let files = document.querySelector('.schedule-attached-file #files').files;

    formData.append('title', document.querySelector('.schedule-info #scheduleTitle').value);
    formData.append('startDate', document.querySelector('.schedule-info #scheduleStartDate').value);
    formData.append('endDate', document.querySelector('.schedule-info #scheduleEndDate').value);
    formData.append('location', document.querySelector('.schedule-info #scheduleLocation').value);

    attendeeMap.forEach((value, key) => {
        formData.append('attendeeList', key);
    });

    formData.append('content', tinymce.get('webEditor').getContent());

    for (let i = 0; i < files.length; i++) {
        formData.append("uploadFiles", files[i]);
    }

    fileMap.forEach((value, key) => {
        formData.append('removeFiles', key);
    });

    return formData;
}

/**
 * 입력된 부서의 부서원 정보를 AJAX 방식으로 가져오는 함수
 * @param element 부서를 선택할 수 있는 <Select> 엘리먼트
 * @see '/groupware/schedule/getDepartmentMembers'
 */
function getDepartmentMembers(element) {
    const dept = element.value;

    if(dept === 'none') {
        updateDepartmentMembersSelector({
            none: '이름'
        });

        return;
    }

    $.ajax({
        type: 'post',
        url: '/groupware/schedule/getDepartmentMembers',
        data: {
            dept: dept
        },
        success: function (data) {
            updateDepartmentMembersSelector(data);
        },
        error: function (error) {
            console.log('error: ', error);
        }
    });
}

/**
 * 부서에 속해있는 부서원들의 정보를 받아서 <Select> 엘리먼트를 업데이트하는 함수
 * @param data 부서원들의 정보가 담긴 오브젝트
 */
function updateDepartmentMembersSelector(data) {
    const selector = document.querySelector('#content select#scheduleAttendee');

    /* Select 엘리먼트의 기존 내용 삭제 */
    selector.innerHTML = '';

    for (const key in data) {
        const option = document.createElement('option');
        option.value = key;
        option.text = data[key];
        selector.appendChild(option);
    }
}

/**
 * 일정의 모든 참석자를 불러오고 페이지에도 표시하는 함수
 */
function loadAttendee() {
    $.ajax({
        type: 'post',
        url: '/groupware/schedule/getAttendee',
        data: {
            scheduleNo: scheduleNo
        },
        success: function (data) {
            for (const key in data) {
                addAttendee({
                    empCode: key,
                    empName: data[key]
                });
            }
        },
        error: function (error) {
            alert('참석자를 불러오는데 실패했습니다.')
        }
    });
}

/**
 * 일정 참가자의 추가를 담당
 * @param attendee 참가자의 정보 {empCode, empName}
 */
function addAttendee(attendee) {
    /* 기본값인 '이름'을 선택하고 추가 버튼을 누른 경우 아무것도 안함 */
    if (attendee.empCode === '') {
        return;
    }

    /* 이미 추가된 사원이면 추가 안함 */
    if(attendeeMap.has(attendee.empCode)) {
        return;
    }

    const attendeeElement = document.createElement('span');
    attendeeElement.innerText = attendee.empName;
    attendeeElement.addEventListener('click', () => {
        removeAttendee(attendee);
    });

    attendeeMap.set(attendee.empCode, attendeeElement);

    attendeeListElement.appendChild(attendeeElement);
}

/**
 * 일정 참가자의 제거를 담당
 * @param attendee 참가자의 정보 {empCode, empName}
 */
function removeAttendee(attendee) {
    attendeeMap.get(attendee.empCode).remove();
    attendeeMap.delete(attendee.empCode);
}

/**
 * 제거할 파일을 Map에 기록하는 함수
 * @param renamedFileName UUID로 만들어진 지워질 파일의 이름
 * @param checkElement 체크여부를 표시하는 HTML 엘리먼트
 */
function removeFile(renamedFileName, checkElement) {
    if(!fileMap.has(renamedFileName)) {
        checkElement.innerText = 'delete';
        fileMap.set(renamedFileName, checkElement);
    } else {
        checkElement.innerText = '';
        fileMap.delete(renamedFileName);
    }

    console.info(fileMap);
}