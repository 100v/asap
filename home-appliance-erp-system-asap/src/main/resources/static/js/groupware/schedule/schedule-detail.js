window.onload = function () {
    let scheduleNo = document.location.pathname.split('/')[2];

    let iFrame = document.querySelector( '#content iframe#schedule-ifr' );
    resizeIFrameToFitContent(iFrame);

    let deleteBtn = document.querySelector('#content .schedule-navigation button#deleteBtn');
    deleteBtn.addEventListener('click', () => {
        if(!isNaN(parseInt(scheduleNo))) {
            $.ajax({
                type: 'post',
                url: '/groupware/schedule/' + scheduleNo + '/remove',
                success: function (data) {
                    alert(data.message);
                    location.href = '/groupware/schedule/';
                },
                error: function (error) {
                    if(error.message) {
                        alert(error.message);
                    } else {
                        alert('알수없는 이유로 일정 삭제에 실패했습니다.');
                    }
                }
            });
        }
    });
}

function resizeIFrameToFitContent(iFrame) {
    if(iFrame instanceof HTMLIFrameElement) {
        iFrame.style.height = iFrame.contentWindow.document.body.scrollHeight + 40 + 'px';
    }
}