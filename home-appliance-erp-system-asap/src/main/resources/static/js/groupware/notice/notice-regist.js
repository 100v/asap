const attendeeMap = new Map();
let attendeeListElement;

window.onload = function () {
    tinymce.init({
        selector: 'div#webEditor',
        width: '100%',
        height: '600px',
        skin: 'oxide',
        plugins: [
            'advlist', 'autolink', 'link', 'image', 'lists', 'charmap', 'preview', 'anchor', 'pagebreak',
            'searchreplace', 'wordcount', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'insertdatetime',
            'media', 'table', 'emoticons', 'template', 'help'
        ],
        toolbar: 'undo redo | styles | bold italic | alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
            'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help'
    });

    /* 참여자를 추가/제거할 때 페이지에 표시될 엘리멘트 */

    /* 등록 버튼 누르면 발생하는 이벤트 할당 - click */
    const submitButton = document.querySelector('#content #insertBtn');
    submitButton.addEventListener('click', () => {
        if(isValidWriteContent()) {
            doNoticeInsert();
        }
    });

    /* 부서 드롭다운 리스트 값 변경시 발생하는 이벤트 할당 - change */

    /* 참석자 추가 버튼 누르면 발생하는 이벤트 할당 - click */
}

/**
 * 일정 입력시 입력값에 대해 검증하는 함수.
 * 제목, 날짜를 입력하지 않았거나 시작 날짜보다 종료 날짜가 빠른 경우를 검증
 * @returns {boolean} 검증에 문제가 없다면 true, 하나라도 잘못 입력되었다면 false를 반환한다.
 */
function isValidWriteContent() {

    let titleElement = document.querySelector('.schedule-info #scheduleTitle');
    if(titleElement.value === "") {
        alert('제목을 입력해주세요.');
        titleElement.focus();
        return false;
    }



    return true;
}

/**
 * AJAX 방식으로 일정 정보를 보내 일정을 등록하는 함수
 * 데이터는 {@link getData} 함수를 사용해 받아온다.
 */
function doNoticeInsert() {
    $.ajax({
        type: 'post',
        url: '/notice/regist',
        traditional: true,
        contentType: false,     // multipart/form-data 로 전송하기 위해 기본 설정된 contentType을 끔
        processData: false,     // FormData를 전송하기 위해서 적어야함
        data: getData(),
        success: function (data) {
            if(data.status === 'success') {
                alert(data.message);
                location.href = '/notice/' + data.dateNo;
            }
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('필수 기입 항목을 작성하지 않았거나 잘못 된 접근입니다.');
            }
        }
    });
}

/**
 * 일정 입력에 사용될 정보들을 모아서 {@link FormData} 객체로 반환하는 함수
 * @returns {FormData} 일정 정보가 담긴 폼 데이터 객체
 */
function getData() {
    let formData = new FormData();
    let files = document.querySelector('.schedule-attached-file #files').files;

    formData.append('title', document.querySelector('.schedule-info #scheduleTitle').value);
    formData.append('content', tinymce.get('webEditor').getContent());

    console.info(attendeeMap);

    for (let i = 0; i < files.length; i++) {
        formData.append("files", files[i]);
    }

    return formData;
}

/**
 * 입력된 부서의 부서원 정보를 AJAX 방식으로 가져오는 함수
 * @param element 부서를 선택할 수 있는 <Select> 엘리먼트
 * @see '/groupware/schedule/getDepartmentMembers'
 */

/**
 * 부서에 속해있는 부서원들의 정보를 받아서 <Select> 엘리먼트를 업데이트하는 함수
 * @param data 부서원들의 정보가 담긴 오브젝트
 */

/**
 * 일정 참가자의 추가를 담당
 * @param attendee 참가자의 정보 {empCode, empName}
 */

/**
 * 일정 참가자의 제거를 담당
 * @param attendee 참가자의 정보 {empCode, empName}
 */
