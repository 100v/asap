window.onload = function () {
    let changeBtn = document.querySelector('#content div.buttons button#changeBtn');
    if(changeBtn) {
        changeBtn.addEventListener('click', () => {
            getSign();
        });
    }

    let addBtn = document.querySelector('#content div.buttons button#addBtn');
    if(addBtn) {
        addBtn.addEventListener('click', () => {
            getSign();
        });
    }

    let deleteBtn = document.querySelector('#content div.buttons button#deleteBtn');
    if(deleteBtn) {
        deleteBtn.addEventListener('click', () => {
            if(confirm('서명파일을 삭제하시겠습니까?')) {
                doDeleteSign();
            }
        });
    }
}

/**
 * 서명파일을 사용자에게 받아서 base64로 변환 후 AJAX 함수를 호출하는 이벤트를 파일 입력폼에게 등록하는 함수
 */
function getSign() {
    const KB = 1024;
    let maxFileSize = 100 * KB;
    let file = document.createElement('input');
    file.type = 'file';
    file.addEventListener('change', () => {
        if(file.files.length === 0) {
            alert('서명 파일을 선택해 주세요.');
            return;
        }
        if(file.files[0].size > maxFileSize) {
            alert('파일이 너무 큽니다. 100KB 이하의 파일을 등록해 주세요.');
            return;
        }
        if(file.files[0].type !== 'image/png') {
            alert('배경이 투명한 PNG 파일을 등록해 주세요.');
            return;
        }

        let reader = new FileReader();
        reader.onloadend = function () {
            doUpdateSign(reader.result);
        };
        reader.readAsDataURL(file.files[0]);
    });

    file.click();
}

/**
 * base64로 변환된 서명파일을 전달받아 AJAX로 등록하는 함수
 * @param sign base64로 변환된 서명파일
 */
function doUpdateSign(sign) {
    $.ajax({
        type: 'post',
        url: '/groupware/sign/modify',
        data: {
            sign: sign
        },
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.reload();
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('서명파일 수정에 실패했습니다.');
            }
        }
    });
}

/**
 * 서명파일을 삭제하는 함수
 */
function doDeleteSign() {
    $.ajax({
        type: 'post',
        url: '/groupware/sign/delete',
        success: function (data) {
            if(data.message) {
                alert(data.message);
            }
            location.reload();
        },
        error: function (error) {
            if (error.message) {
                alert(error.message)
            } else {
                alert('서명파일 삭제에 실패했습니다.');
            }
        }
    });
}