
/* 테이블 생성*/

/* 부서 */
CREATE TABLE TBL_DEPT
(
    DEPT_CODE    VARCHAR2(10) NOT NULL,
    DEPT_NAME    VARCHAR2(30) NOT NULL,
    DEPT_USE_YN    VARCHAR2(3) DEFAULT 'Y' NOT NULL
);

COMMENT ON COLUMN TBL_DEPT.DEPT_CODE IS '부서코드';

COMMENT ON COLUMN TBL_DEPT.DEPT_NAME IS '부서명';

COMMENT ON COLUMN TBL_DEPT.DEPT_USE_YN IS '사용여부';

COMMENT ON TABLE TBL_DEPT IS '부서';

CREATE UNIQUE INDEX TBL_DEPT_PK ON TBL_DEPT
( DEPT_CODE );

ALTER TABLE TBL_DEPT
 ADD CONSTRAINT TBL_DEPT_PK PRIMARY KEY ( DEPT_CODE )
 USING INDEX TBL_DEPT_PK;

/* 직급 */
CREATE TABLE TBL_JOB_GRADE
(
    JOB_CODE    VARCHAR2(120) NOT NULL,
    JOB_NAME    VARCHAR2(120) NOT NULL
);

COMMENT ON COLUMN TBL_JOB_GRADE.JOB_CODE IS '직급코드';

COMMENT ON COLUMN TBL_JOB_GRADE.JOB_NAME IS '직급명';

COMMENT ON TABLE TBL_JOB_GRADE IS '직급';

CREATE UNIQUE INDEX TBL_JOB_GRADE_PK ON TBL_JOB_GRADE
( JOB_CODE );

ALTER TABLE TBL_JOB_GRADE
 ADD CONSTRAINT TBL_JOB_GRADE_PK PRIMARY KEY ( JOB_CODE )
 USING INDEX TBL_JOB_GRADE_PK;

/* 사원 */
CREATE TABLE TBL_EMPLOYEE
(
    EMP_CODE    NUMBER NOT NULL,
    EMP_NAME    VARCHAR2(20) NOT NULL,
    EMP_BIRTH    VARCHAR2(20) NOT NULL,
    DEPT_CODE    VARCHAR2(10) NOT NULL,
    EMP_PHONE    VARCHAR2(30) NOT NULL,
    EMP_EMAIL    VARCHAR2(50) NOT NULL,
    EMP_ADDRESS    VARCHAR2(255) NOT NULL,
    EMP_JOINDATE    DATE NOT NULL,
    EMP_LEAVEDATE    DATE,
    EMP_LEAVE_YN    VARCHAR2(3) DEFAULT 'N' NOT NULL,
    JOB_CODE    VARCHAR2(120) NOT NULL,
    ANNUAL_LEFT_DAY    NUMBER DEFAULT 15 NOT NULL,
    SIGN_IMG    CLOB,
    PROFILE_NAME    VARCHAR2(500),
    PROFILE_RENAME    VARCHAR2(500),
    PROFILE_PATH    VARCHAR2(500),
    EMP_GENDER    VARCHAR2(20) NOT NULL,
    EMP_PWD    VARCHAR2(500) NOT NULL,
    SALARY_BASE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_NAME IS '이름';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_BIRTH IS '생년월일';

COMMENT ON COLUMN TBL_EMPLOYEE.DEPT_CODE IS '부서코드';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_PHONE IS '전화번호';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_EMAIL IS '이메일';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_ADDRESS IS '주소';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_JOINDATE IS '입사일자';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_LEAVEDATE IS '퇴사일자';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_LEAVE_YN IS '퇴직여부';

COMMENT ON COLUMN TBL_EMPLOYEE.JOB_CODE IS '직급코드';

COMMENT ON COLUMN TBL_EMPLOYEE.ANNUAL_LEFT_DAY IS '휴가잔여일수';

COMMENT ON COLUMN TBL_EMPLOYEE.SIGN_IMG IS '서명';

COMMENT ON COLUMN TBL_EMPLOYEE.PROFILE_NAME IS '프로필 사진 이름';

COMMENT ON COLUMN TBL_EMPLOYEE.PROFILE_RENAME IS '프로필 사진 리네임';

COMMENT ON COLUMN TBL_EMPLOYEE.PROFILE_PATH IS '프로필 사진 경로';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_GENDER IS '성별';

COMMENT ON COLUMN TBL_EMPLOYEE.EMP_PWD IS '비밀번호';

COMMENT ON COLUMN TBL_EMPLOYEE.SALARY_BASE IS '기본급';

COMMENT ON TABLE TBL_EMPLOYEE IS '사원';

CREATE UNIQUE INDEX TBL_EMPLOYEE_PK ON TBL_EMPLOYEE
( EMP_CODE );

ALTER TABLE TBL_EMPLOYEE
 ADD CONSTRAINT TBL_EMPLOYEE_PK PRIMARY KEY ( EMP_CODE )
 USING INDEX TBL_EMPLOYEE_PK;

ALTER TABLE TBL_EMPLOYEE
 ADD CONSTRAINT TBL_EMPLOYEE_FK FOREIGN KEY ( JOB_CODE )
 REFERENCES TBL_JOB_GRADE (JOB_CODE );

ALTER TABLE TBL_EMPLOYEE
 ADD CONSTRAINT TBL_EMPLOYEE_FK1 FOREIGN KEY ( DEPT_CODE )
 REFERENCES TBL_DEPT (DEPT_CODE );

/* 권한 */
CREATE TABLE TBL_PERMISSION
(
    PERMISSION_CODE    VARCHAR2(200) NOT NULL,
    PERMISSION_NAME    VARCHAR2(200) NOT NULL
);

COMMENT ON COLUMN TBL_PERMISSION.PERMISSION_CODE IS '권한코드';

COMMENT ON COLUMN TBL_PERMISSION.PERMISSION_NAME IS '권한명';

COMMENT ON TABLE TBL_PERMISSION IS '권한';

CREATE UNIQUE INDEX TBL_PERMISSION_PK ON TBL_PERMISSION
( PERMISSION_CODE );

ALTER TABLE TBL_PERMISSION
 ADD CONSTRAINT TBL_PERMISSION_PK PRIMARY KEY ( PERMISSION_CODE )
 USING INDEX TBL_PERMISSION_PK;

/* 회원별 권한 */
CREATE TABLE TBL_USER_PERMISSION
(
    EMP_CODE    NUMBER NOT NULL,
    PERMISSION_CODE    VARCHAR2(200) NOT NULL
);

COMMENT ON COLUMN TBL_USER_PERMISSION.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_USER_PERMISSION.PERMISSION_CODE IS '권한코드';

COMMENT ON TABLE TBL_USER_PERMISSION IS '회원별권한';


/* 근태 */
CREATE TABLE TBL_ATTENDANCE
(
    ATTENDANCE_CODE    NUMBER NOT NULL,
    WORK_DATE    DATE NOT NULL,
    GOWORK_TIME    VARCHAR2(50) NOT NULL,
    OUTWORK_TIME    VARCHAR2(50) NOT NULL,
    WORK_TIME    NUMBER NOT NULL,
    OVERTIME_ALLOWANCE    NUMBER NOT NULL,
    EMP_CODE    NUMBER,
    ATTENDANCE_STATUS VARCHAR(5) DEFAULT 'N' NOT NULL
);

COMMENT ON COLUMN TBL_ATTENDANCE.ATTENDANCE_CODE IS '근태코드';

COMMENT ON COLUMN TBL_ATTENDANCE.WORK_DATE IS '출근 날짜';

COMMENT ON COLUMN TBL_ATTENDANCE.GOWORK_TIME IS '출근시각';

COMMENT ON COLUMN TBL_ATTENDANCE.OUTWORK_TIME IS '퇴근시각';

COMMENT ON COLUMN TBL_ATTENDANCE.WORK_TIME IS '업무 시간';

COMMENT ON COLUMN TBL_ATTENDANCE.OVERTIME_ALLOWANCE IS '초과 시간';

COMMENT ON COLUMN TBL_ATTENDANCE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_ATTENDANCE.ATTENDANCE_STATUS IS '삭제여부';

COMMENT ON TABLE TBL_ATTENDANCE IS '근태';

CREATE UNIQUE INDEX TBL_ATTENDANCE_PK ON TBL_ATTENDANCE
( ATTENDANCE_CODE );

ALTER TABLE TBL_ATTENDANCE
 ADD CONSTRAINT TBL_ATTENDANCE_PK PRIMARY KEY ( ATTENDANCE_CODE )
 USING INDEX TBL_ATTENDANCE_PK;

ALTER TABLE TBL_ATTENDANCE
 ADD CONSTRAINT TBL_ATTENDANCE_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

/* 휴가내역 */
CREATE TABLE TBL_ANNUAL
(
    ANNUAL_CODE    NUMBER NOT NULL,
    ANNUAL_NAME    VARCHAR2(30) NOT NULL,
    DEPT_CODE    VARCHAR2(30) NOT NULL,
    ANNUAL_START    DATE NOT NULL,
    ANNUAL_LEAVE_DAT    NUMBER NOT NULL,
    EMP_CODE    NUMBER,
    ANNUAL_END    DATE NOT NULL,
    ANNUAL_YN    VARCHAR2(30) DEFAULT 'N' NOT NULL
);

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_CODE IS '휴가코드';

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_NAME IS '휴가명';

COMMENT ON COLUMN TBL_ANNUAL.DEPT_CODE IS '부서명';

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_START IS '휴가시작일';

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_LEAVE_DAT IS '휴가사용일수';

COMMENT ON COLUMN TBL_ANNUAL.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_END IS '휴가종료일';

COMMENT ON COLUMN TBL_ANNUAL.ANNUAL_YN IS '확정여부';

COMMENT ON TABLE TBL_ANNUAL IS '휴가 내역';

CREATE UNIQUE INDEX TBL_ANNUAL_PK ON TBL_ANNUAL
( ANNUAL_CODE );

ALTER TABLE TBL_ANNUAL
 ADD CONSTRAINT TBL_ANNUAL_PK PRIMARY KEY ( ANNUAL_CODE )
 USING INDEX TBL_ANNUAL_PK;

ALTER TABLE TBL_ANNUAL
 ADD CONSTRAINT TBL_ANNUAL_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

/* 급여내역 */
CREATE TABLE TBL_SALARY
(
    OVERTIME_ALLOWANCE_SUM    NUMBER DEFAULT 0 NOT NULL,
    ACTUAL_PAYMENT    NUMBER NOT NULL,
    SALARY_NO    NUMBER NOT NULL,
    EMP_CODE    NUMBER,
    SALARY_MONTH    NUMBER NOT NULL,
    SALARY_YEAR    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_SALARY.OVERTIME_ALLOWANCE_SUM IS '초과 수당 합계';

COMMENT ON COLUMN TBL_SALARY.ACTUAL_PAYMENT IS '급여';

COMMENT ON COLUMN TBL_SALARY.SALARY_NO IS '급여번호';

COMMENT ON COLUMN TBL_SALARY.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_SALARY.SALARY_MONTH IS '월별구분';

COMMENT ON COLUMN TBL_SALARY.SALARY_YEAR IS '년도구분';

COMMENT ON TABLE TBL_SALARY IS '급여내역';

CREATE UNIQUE INDEX TBL_SALARY_PK ON TBL_SALARY
( SALARY_NO );

ALTER TABLE TBL_SALARY
 ADD CONSTRAINT TBL_SALARY_PK PRIMARY KEY ( SALARY_NO )
 USING INDEX TBL_SALARY_PK;

ALTER TABLE TBL_SALARY
 ADD CONSTRAINT TBL_SALARY_FK1 FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

/* 퇴직금 내역 */
CREATE TABLE TBL_RETIRING_ALLOWANCE
(
    RETIREMENT_ACTUAL_PAYMENT    NUMBER NOT NULL,
    EMP_CODE    NUMBER NOT NULL,
    RETIREMENT_STATUS    VARCHAR2(3) DEFAULT 'N' NOT NULL
);

COMMENT ON COLUMN TBL_RETIRING_ALLOWANCE.RETIREMENT_ACTUAL_PAYMENT IS '퇴직금 실지급액';

COMMENT ON COLUMN TBL_RETIRING_ALLOWANCE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_RETIRING_ALLOWANCE.RETIREMENT_STATUS IS '퇴직금 지급여부';

COMMENT ON TABLE TBL_RETIRING_ALLOWANCE IS '퇴직금 내역';

CREATE UNIQUE INDEX TBL_RETIRING_ALLOWANCE_PK ON TBL_RETIRING_ALLOWANCE
( EMP_CODE );

ALTER TABLE TBL_RETIRING_ALLOWANCE
 ADD CONSTRAINT TBL_RETIRING_ALLOWANCE_PK PRIMARY KEY ( EMP_CODE )
 USING INDEX TBL_RETIRING_ALLOWANCE_PK;

ALTER TABLE TBL_RETIRING_ALLOWANCE
 ADD CONSTRAINT TBL_RETIRING_ALLOWANCE_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

/* 증명서 종류 */
CREATE TABLE TBL_CERTIFICATION
(
    CERTIFICATE_TYPE_CODE    VARCHAR2(30) NOT NULL,
    CERTIFICATE_TYPE    VARCHAR2(300) NOT NULL
);

COMMENT ON COLUMN TBL_CERTIFICATION.CERTIFICATE_TYPE_CODE IS '증명서 종류 코드';

COMMENT ON COLUMN TBL_CERTIFICATION.CERTIFICATE_TYPE IS '증명서 종류';

COMMENT ON TABLE TBL_CERTIFICATION IS '증명서 종류';

CREATE UNIQUE INDEX TBL_CERTIFICATION_PK ON TBL_CERTIFICATION
( CERTIFICATE_TYPE_CODE );

ALTER TABLE TBL_CERTIFICATION
 ADD CONSTRAINT TBL_CERTIFICATION_PK PRIMARY KEY ( CERTIFICATE_TYPE_CODE )
 USING INDEX TBL_CERTIFICATION_PK;

/* 증명서 */
CREATE TABLE TBL_CERTIFICATE
(
    CERTIFICATE_CODE    NUMBER NOT NULL,
    EMP_NAME    VARCHAR2(10) NOT NULL,
    PURPOSE    VARCHAR2(500) NOT NULL,
    PUBLISHED_DATE    VARCHAR2(20) NOT NULL,
    EMP_CODE    NUMBER,
    CERTIFICATE_TYPE_CODE    VARCHAR2(30) NOT NULL
);

COMMENT ON COLUMN TBL_CERTIFICATE.CERTIFICATE_CODE IS '증명서코드';

COMMENT ON COLUMN TBL_CERTIFICATE.EMP_NAME IS '성명';

COMMENT ON COLUMN TBL_CERTIFICATE.PURPOSE IS '용도';

COMMENT ON COLUMN TBL_CERTIFICATE.PUBLISHED_DATE IS '발행일';

COMMENT ON COLUMN TBL_CERTIFICATE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_CERTIFICATE.CERTIFICATE_TYPE_CODE IS '증명서 종류 코드';

COMMENT ON TABLE TBL_CERTIFICATE IS '증명서';

CREATE UNIQUE INDEX TBL_CERTIFICATE_PK ON TBL_CERTIFICATE
( CERTIFICATE_CODE );

ALTER TABLE TBL_CERTIFICATE
 ADD CONSTRAINT TBL_CERTIFICATE_PK PRIMARY KEY ( CERTIFICATE_CODE )
 USING INDEX TBL_CERTIFICATE_PK;

ALTER TABLE TBL_CERTIFICATE
 ADD CONSTRAINT TBL_CERTIFICATE_FK FOREIGN KEY ( CERTIFICATE_TYPE_CODE )
 REFERENCES TBL_CERTIFICATION (CERTIFICATE_TYPE_CODE );

ALTER TABLE TBL_CERTIFICATE
 ADD CONSTRAINT TBL_CERTIFICATE_FK1 FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

/* AS 관리 */
CREATE TABLE TBL_AS
(
    AS_TAKE_NO    NUMBER NOT NULL,
    AS_TAKE_NAME    VARCHAR2(20) NOT NULL,
    AS_TAKE_ADDRESS    VARCHAR2(50) NOT NULL,
    AS_TAKE_AGE    NUMBER NOT NULL,
    AS_TAKE_GENDER    VARCHAR2(3) NOT NULL,
    AS_COMPLETE_YN    VARCHAR2(3) NOT NULL,
    EMP_CODE    NUMBER
);

COMMENT ON COLUMN TBL_AS.AS_TAKE_NO IS '접수번호';

COMMENT ON COLUMN TBL_AS.AS_TAKE_NAME IS '접수자 이름';

COMMENT ON COLUMN TBL_AS.AS_TAKE_ADDRESS IS '접수자 주소';

COMMENT ON COLUMN TBL_AS.AS_TAKE_AGE IS '접수자 나이';

COMMENT ON COLUMN TBL_AS.AS_TAKE_GENDER IS '접수자 성별';

COMMENT ON COLUMN TBL_AS.AS_COMPLETE_YN IS '처리 여부';

COMMENT ON COLUMN TBL_AS.EMP_CODE IS '담당 사원';

COMMENT ON TABLE TBL_AS IS 'A/S 관리';

CREATE UNIQUE INDEX TBL_AS_PK ON TBL_AS
( AS_TAKE_NO );

ALTER TABLE TBL_AS
 ADD CONSTRAINT TBL_AS_PK PRIMARY KEY ( AS_TAKE_NO )
 USING INDEX TBL_AS_PK;

ALTER TABLE TBL_AS
 ADD CONSTRAINT TBL_AS_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

------------------------------------------------------------
/*상현*/
CREATE TABLE TBL_NOTICE
(
    NOTICE_NO    NUMBER NOT NULL,
    NOTICE_TITLE    VARCHAR2(255) NOT NULL,
    NOTICE_CONTENT    CLOB,
    NOTICE_DATE    DATE NOT NULL,
    DELETE_STATUS   VARCHAR2(10) DEFAULT 'N' NOT NULL,
    EMP_CODE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_NOTICE.NOTICE_TITLE IS '공지사항 제목';

COMMENT ON COLUMN TBL_NOTICE.NOTICE_DATE IS '작성일자(시간)';

COMMENT ON COLUMN TBL_NOTICE.NOTICE_NO IS '공지사항 번호';

COMMENT ON COLUMN TBL_NOTICE.EMP_CODE IS '작성자';

COMMENT ON COLUMN TBL_NOTICE.NOTICE_CONTENT IS '공지사항 내용';

COMMENT ON COLUMN TBL_NOTICE.DELETE_STATUS IS '삭제 여부';

COMMENT ON TABLE TBL_NOTICE IS '공지사항';

CREATE UNIQUE INDEX TBL_NOTICE_PK ON TBL_NOTICE
( NOTICE_NO );

ALTER TABLE TBL_NOTICE
 ADD CONSTRAINT TBL_NOTICE_PK PRIMARY KEY ( NOTICE_NO )
 USING INDEX TBL_NOTICE_PK;

ALTER TABLE TBL_NOTICE
 ADD CONSTRAINT TBL_NOTICE_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );


CREATE TABLE TBL_FILE_UPLOAD
(
    FILE_NO    NUMBER NOT NULL,
    FILE_NAME    VARCHAR2(500) NOT NULL,
    FILE_LOCATION    VARCHAR2(500) NOT NULL,
    FILE_UPLOAD_BOARD    VARCHAR2(120) NOT NULL,
    FILE_UPLOAD_DATE    DATE NOT NULL,
    FILE_RENAME    VARCHAR2(1000) NOT NULL,
    NOTICE_NO    NUMBER,
    DATE_NO    NUMBER
);

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_NO IS '첨부파일 번호';

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_NAME IS '첨부파일 원래 이름';

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_LOCATION IS '첨부파일 위치';

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_UPLOAD_BOARD IS '첨부 된 게시판 구분';

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_UPLOAD_DATE IS '첨부 날짜(시간)';

COMMENT ON COLUMN TBL_FILE_UPLOAD.FILE_RENAME IS '첨부파일 리네임';

COMMENT ON COLUMN TBL_FILE_UPLOAD.NOTICE_NO IS '공지사항 번호';

COMMENT ON COLUMN TBL_FILE_UPLOAD.DATE_NO IS '일정번호';

COMMENT ON TABLE TBL_FILE_UPLOAD IS '첨부파일';

CREATE UNIQUE INDEX TBL_FILE_UPLOAD_PK ON TBL_FILE_UPLOAD
( FILE_NO );

ALTER TABLE TBL_FILE_UPLOAD
 ADD CONSTRAINT TBL_FILE_UPLOAD_PK PRIMARY KEY ( FILE_NO )
 USING INDEX TBL_FILE_UPLOAD_PK;

CREATE TABLE TBL_COMPANY_DATE
(
    DATE_NO    NUMBER NOT NULL,
    DATE_TITLE    VARCHAR2(255) NOT NULL,
    DATE_LOCATION    VARCHAR2(255),
    DATE_START    DATE NOT NULL,
    DATE_END    DATE NOT NULL,
    DATE_CONTENT    CLOB,
    DELETE_STATUS     VARCHAR2(10) DEFAULT 'N' NOT NULL,
    EMP_CODE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_NO IS '일정번호';

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_TITLE IS '제목';

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_LOCATION IS '장소';

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_START IS '시작날짜';

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_END IS '종료날짜';

COMMENT ON COLUMN TBL_COMPANY_DATE.DATE_CONTENT IS '내용';

COMMENT ON COLUMN TBL_COMPANY_DATE.DELETE_STATUS IS '삭제 여부';

COMMENT ON COLUMN TBL_COMPANY_DATE.EMP_CODE IS '작성자';

COMMENT ON TABLE TBL_COMPANY_DATE IS '사내 일정';

CREATE UNIQUE INDEX TBL_COMPANY_DATE_PK ON TBL_COMPANY_DATE
( DATE_NO );

ALTER TABLE TBL_COMPANY_DATE
 ADD CONSTRAINT TBL_COMPANY_DATE_PK PRIMARY KEY ( DATE_NO )
 USING INDEX TBL_COMPANY_DATE_PK;

ALTER TABLE TBL_COMPANY_DATE
 ADD CONSTRAINT TBL_COMPANY_DATE_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

CREATE TABLE TBL_COMPANY_DATE_ATTEND
(
    EMP_CODE    NUMBER NOT NULL,
    DATE_NO    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_COMPANY_DATE_ATTEND.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_COMPANY_DATE_ATTEND.DATE_NO IS '일정번호';

COMMENT ON TABLE TBL_COMPANY_DATE_ATTEND IS '사내 일정 참가자';

CREATE UNIQUE INDEX TBL_COMPANY_DATE_ATTEND_PK ON TBL_COMPANY_DATE_ATTEND
( EMP_CODE,DATE_NO );

ALTER TABLE TBL_COMPANY_DATE_ATTEND
 ADD CONSTRAINT TBL_COMPANY_DATE_ATTEND_PK PRIMARY KEY ( EMP_CODE,DATE_NO )
 USING INDEX TBL_COMPANY_DATE_ATTEND_PK;

ALTER TABLE TBL_COMPANY_DATE_ATTEND
 ADD CONSTRAINT TBL_COMPANY_DATE_ATTEND_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

ALTER TABLE TBL_COMPANY_DATE_ATTEND
 ADD CONSTRAINT TBL_COMPANY_DATE_ATTEND_FK1 FOREIGN KEY ( DATE_NO )
 REFERENCES TBL_COMPANY_DATE (DATE_NO );


CREATE TABLE TBL_NOTICE_APPLIED_DEPT
(
    NOTICE_NO    NUMBER NOT NULL,
    DEPT_CODE    VARCHAR2(10) NOT NULL
);

COMMENT ON COLUMN TBL_NOTICE_APPLIED_DEPT.NOTICE_NO IS '공지사항 번호';

COMMENT ON COLUMN TBL_NOTICE_APPLIED_DEPT.DEPT_CODE IS '부서코드';

COMMENT ON TABLE TBL_NOTICE_APPLIED_DEPT IS '공지사항 적용 부서';

CREATE UNIQUE INDEX TBL_NOTICE_APPLIED_DEPT_PK ON TBL_NOTICE_APPLIED_DEPT
( NOTICE_NO,DEPT_CODE );

ALTER TABLE TBL_NOTICE_APPLIED_DEPT
 ADD CONSTRAINT TBL_NOTICE_APPLIED_DEPT_PK PRIMARY KEY ( NOTICE_NO,DEPT_CODE )
 USING INDEX TBL_NOTICE_APPLIED_DEPT_PK;


CREATE TABLE TBL_DRAFT_FORM
(
    DRAFT_FORM_NO    NUMBER NOT NULL,
    DRAFT_FORM_TITLE    VARCHAR2(255),
    DRAFT_FORM_CONTENT    CLOB,
    DRAFT_FORM_DIVISION    VARCHAR2(255)
);

COMMENT ON COLUMN \_FORM.DRAFT_FORM_NO IS '기안서 양식 번호';

COMMENT ON COLUMN TBL_DRAFT_FORM.DRAFT_FORM_TITLE IS '기안서 양식 제목';

COMMENT ON COLUMN TBL_DRAFT_FORM.DRAFT_FORM_CONTENT IS '기안서 양식 내용';

COMMENT ON COLUMN TBL_DRAFT_FORM.DRAFT_FORM_DIVISION IS '기안서 양식 구분';

COMMENT ON TABLE TBL_DRAFT_FORM IS '기안서 양식';

CREATE UNIQUE INDEX TBL_DRAFT_FORM_PK ON TBL_DRAFT_FORM
( DRAFT_FORM_NO );

ALTER TABLE TBL_DRAFT_FORM
 ADD CONSTRAINT TBL_DRAFT_FORM_PK PRIMARY KEY ( DRAFT_FORM_NO )
 USING INDEX TBL_DRAFT_FORM_PK;


CREATE TABLE TBL_DRAFT
(
    DRAFT_NO        NUMBER NOT NULL,
    DRAFT_DATE      DATE NOT NULL ,
    DRAFT_TITLE     VARCHAR2(255) NOT NULL,
    DRAFT_TYPE      VARCHAR2(255) NOT NULL,
    DRAFT_CONTENTS  CLOB,
    DRAFT_STATUS    VARCHAR2(255) NOT NULL,
    APPROVAL_STATUS VARCHAR2(10) DEFAULT 'N' NOT NULL,
    DELETE_STATUS   VARCHAR2(10) DEFAULT 'N' NOT NULL,
    EMP_CODE        NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_DRAFT.DRAFT_NO IS '기안서 번호';

COMMENT ON COLUMN TBL_DRAFT.DRAFT_DATE IS '기안일자';

COMMENT ON COLUMN TBL_DRAFT.DRAFT_TITLE IS '기안서 제목';

COMMENT ON COLUMN TBL_DRAFT.DRAFT_TYPE IS '기안 구분';

COMMENT ON COLUMN TBL_DRAFT.DRAFT_CONTENTS IS '내용';

COMMENT ON COLUMN TBL_DRAFT.DRAFT_STATUS IS '기안 진행상태';

COMMENT ON COLUMN TBL_DRAFT.APPROVAL_STATUS IS '결재 여부';

COMMENT ON COLUMN TBL_DRAFT.DELETE_STATUS IS '삭제 여부';

COMMENT ON COLUMN TBL_DRAFT.EMP_CODE IS '기안자';

COMMENT ON TABLE TBL_DRAFT IS '기안서';

CREATE UNIQUE INDEX TBL_DRAFT_PK ON TBL_DRAFT
( DRAFT_NO );

ALTER TABLE TBL_DRAFT
 ADD CONSTRAINT TBL_DRAFT_PK PRIMARY KEY ( DRAFT_NO )
 USING INDEX TBL_DRAFT_PK;

ALTER TABLE TBL_DRAFT
 ADD CONSTRAINT TBL_DRAFT_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );


CREATE TABLE TBL_APPROVAL_LINE
(
    EMP_CODE            NUMBER NOT NULL,
    DRAFT_NO            NUMBER NOT NULL,
    APPROVAL_ORDER      NUMBER NOT NULL,
    APPROVAL_STATUS     VARCHAR2(10) DEFAULT 'N' NOT NULL,
    APPROVAL_DATE       DATE,
    APPROVAL_REASON     VARCHAR2(4000)
);

COMMENT ON COLUMN TBL_APPROVAL_LINE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_APPROVAL_LINE.DRAFT_NO IS '기안서 번호';

COMMENT ON COLUMN TBL_APPROVAL_LINE.APPROVAL_ORDER IS '결재 순서';

COMMENT ON COLUMN TBL_APPROVAL_LINE.APPROVAL_STATUS IS '결재 여부';

COMMENT ON COLUMN TBL_APPROVAL_LINE.APPROVAL_DATE IS '결재 일자';

COMMENT ON COLUMN TBL_APPROVAL_LINE.APPROVAL_REASON IS '사유';

COMMENT ON TABLE TBL_APPROVAL_LINE IS '결재라인';

CREATE UNIQUE INDEX TBL_APPROVAL_LINE_PK ON TBL_APPROVAL_LINE
( EMP_CODE,DRAFT_NO );

ALTER TABLE TBL_APPROVAL_LINE
 ADD CONSTRAINT TBL_APPROVAL_LINE_PK PRIMARY KEY ( EMP_CODE,DRAFT_NO )
 USING INDEX TBL_APPROVAL_LINE_PK;

ALTER TABLE TBL_APPROVAL_LINE
 ADD CONSTRAINT TBL_APPROVAL_LINE_FK FOREIGN KEY ( DRAFT_NO )
 REFERENCES TBL_DRAFT ( DRAFT_NO );

ALTER TABLE TBL_APPROVAL_LINE
 ADD CONSTRAINT TBL_APPROVAL_LINE_FK1 FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE ( EMP_CODE );



/* 수목 */
-- 상품 카테고리 테이블 생성
CREATE TABLE TBL_PRODUCT_CATEGORY
(
    CATEGORY_CODE    VARCHAR2(10) NOT NULL,
    CATEGORY_NAME    VARCHAR2(20) NOT NULL
);

COMMENT ON COLUMN TBL_PRODUCT_CATEGORY.CATEGORY_CODE IS '카테고리 번호';

COMMENT ON COLUMN TBL_PRODUCT_CATEGORY.CATEGORY_NAME IS '카테고리 이름';

COMMENT ON TABLE TBL_PRODUCT_CATEGORY IS '상품 카테고리';

CREATE UNIQUE INDEX TBL_PRODUCT_CATEGORY_PK ON TBL_PRODUCT_CATEGORY
( CATEGORY_CODE );

ALTER TABLE TBL_PRODUCT_CATEGORY
 ADD CONSTRAINT TBL_PRODUCT_CATEGORY_PK PRIMARY KEY ( CATEGORY_CODE )
 USING INDEX TBL_PRODUCT_CATEGORY_PK;

-- 창고관리 테이블 생성
CREATE TABLE TBL_STORAGE
(
    STORAGE_NO    NUMBER NOT NULL,
    STORAGE_NAME    VARCHAR2(120) NOT NULL,
    STORAGE_MANAGER_PHONE    VARCHAR2(120) NOT NULL,
    EMP_CODE    NUMBER,
    STORAGE_STATUS    VARCHAR2(255) DEFAULT 'N'
);

COMMENT ON COLUMN TBL_STORAGE.STORAGE_NO IS '창고번호';

COMMENT ON COLUMN TBL_STORAGE.STORAGE_NAME IS '창고명';

COMMENT ON COLUMN TBL_STORAGE.STORAGE_MANAGER_PHONE IS '연락처';

COMMENT ON COLUMN TBL_STORAGE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_STORAGE.STORAGE_STATUS IS '삭제여부';

COMMENT ON TABLE TBL_STORAGE IS '창고관리';

CREATE UNIQUE INDEX TBL_STORAGE_PK ON TBL_STORAGE
    ( STORAGE_NO );

ALTER TABLE TBL_STORAGE
    ADD CONSTRAINT TBL_STORAGE_PK PRIMARY KEY ( STORAGE_NO )
        USING INDEX TBL_STORAGE_PK;

ALTER TABLE TBL_STORAGE
    ADD CONSTRAINT TBL_STORAGE_FK FOREIGN KEY ( EMP_CODE )
        REFERENCES TBL_EMPLOYEE (EMP_CODE );

-- 상품 관리 테이블 생성
CREATE TABLE TBL_PRODUCT
(
    PRODUCT_CODE    VARCHAR2(255) NOT NULL,
    PRODUCT_NAME    VARCHAR2(50) NOT NULL,
    PRODUCT_RELEASE    NUMBER NOT NULL,
    PRODUCT_WAEARING    NUMBER NOT NULL,
    STORAGE_NO    NUMBER,
    CATEGORY_CODE    VARCHAR2(10) NOT NULL,
    PRODUCT_STATUS    VARCHAR2(3) NOT NULL
);

COMMENT ON COLUMN TBL_PRODUCT.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_PRODUCT.PRODUCT_NAME IS '상품명';

COMMENT ON COLUMN TBL_PRODUCT.PRODUCT_RELEASE IS '출고단가';

COMMENT ON COLUMN TBL_PRODUCT.PRODUCT_WAEARING IS '입고단가';

COMMENT ON COLUMN TBL_PRODUCT.STORAGE_NO IS '창고번호';

COMMENT ON COLUMN TBL_PRODUCT.CATEGORY_CODE IS '카테고리 번호';

COMMENT ON COLUMN TBL_PRODUCT.PRODUCT_STATUS IS '삭제여부';

COMMENT ON TABLE TBL_PRODUCT IS '상품 관리';

CREATE UNIQUE INDEX TBL_PRODUCT_PK ON TBL_PRODUCT
( PRODUCT_CODE );

ALTER TABLE TBL_PRODUCT
 ADD CONSTRAINT TBL_PRODUCT_PK PRIMARY KEY ( PRODUCT_CODE )
 USING INDEX TBL_PRODUCT_PK;

ALTER TABLE TBL_PRODUCT
 ADD CONSTRAINT TBL_PRODUCT_FK FOREIGN KEY ( CATEGORY_CODE )
 REFERENCES TBL_PRODUCT_CATEGORY (CATEGORY_CODE );

ALTER TABLE TBL_PRODUCT
 ADD CONSTRAINT TBL_PRODUCT_FK1 FOREIGN KEY ( STORAGE_NO )
 REFERENCES TBL_STORAGE (STORAGE_NO );

CREATE TABLE TBL_CLIENT
(
    CLIENT_NO    NUMBER NOT NULL,
    CLIENT_NAME    VARCHAR2(255) NOT NULL,
    CLIENT_REPRESENTATIVE_NAME    VARCHAR2(255) NOT NULL,
    CLIENT_PHONE    VARCHAR2(255),
    CLIENT_TELEPHONE    VARCHAR2(255) NOT NULL,
    CLIENT_EMAIL    VARCHAR2(255),
    CLIENT_ADDRESS    VARCHAR2(255) NOT NULL,
    CLIENT_MAINTAIN    CHAR(1) NOT NULL
);

COMMENT ON COLUMN TBL_CLIENT.CLIENT_NO IS '거래처 코드';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_NAME IS '거래처 명';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_REPRESENTATIVE_NAME IS '대표자 명';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_PHONE IS '전화번호';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_TELEPHONE IS '핸드폰 번호';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_EMAIL IS '거래처 이메일';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_ADDRESS IS '거래처 주소';

COMMENT ON COLUMN TBL_CLIENT.CLIENT_MAINTAIN IS '사용구분';

COMMENT ON TABLE TBL_CLIENT IS '거래처';

CREATE UNIQUE INDEX TBL_CLIENT_PK ON TBL_CLIENT
( CLIENT_NO );

ALTER TABLE TBL_CLIENT
 ADD CONSTRAINT TBL_CLIENT_PK PRIMARY KEY ( CLIENT_NO )
 USING INDEX TBL_CLIENT_PK;

-- 입고 내역 테이블 생성
CREATE TABLE TBL_RECEIVING_HISTORY
(
    RECEIVING_HISTORY_NO    NUMBER NOT NULL,
    RECEIVING_COMENT    VARCHAR2(500) NOT NULL,
    INVENTORY_DATE    DATE NOT NULL,
    RECEIVING_AMOUNT    NUMBER NOT NULL,
    PRODUCT_CODE    VARCHAR2(255),
    STORAGE_NO    NUMBER,
    CLIENT_NO    NUMBER,
    RECEIVING_STATUS    VARCHAR2(255) DEFAULT 'Y' NOT NULL
);

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.RECEIVING_HISTORY_NO IS '입고내역 번호';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.RECEIVING_COMENT IS '내용';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.INVENTORY_DATE IS '입고일자';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.RECEIVING_AMOUNT IS '입고량';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.STORAGE_NO IS '창고번호';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.CLIENT_NO IS '거래처 코드';

COMMENT ON COLUMN TBL_RECEIVING_HISTORY.RECEIVING_STATUS IS '삭제유무';

COMMENT ON TABLE TBL_RECEIVING_HISTORY IS '입고내역';

CREATE UNIQUE INDEX TBL_RECEIVING_HISTORY_PK ON TBL_RECEIVING_HISTORY
    ( RECEIVING_HISTORY_NO );

ALTER TABLE TBL_RECEIVING_HISTORY
    ADD CONSTRAINT TBL_RECEIVING_HISTORY_PK PRIMARY KEY ( RECEIVING_HISTORY_NO )
        USING INDEX TBL_RECEIVING_HISTORY_PK;

ALTER TABLE TBL_RECEIVING_HISTORY
    ADD CONSTRAINT TBL_RECEIVING_HISTORY_FK FOREIGN KEY ( PRODUCT_CODE )
        REFERENCES TBL_PRODUCT (PRODUCT_CODE );

ALTER TABLE TBL_RECEIVING_HISTORY
    ADD CONSTRAINT TBL_RECEIVING_HISTORY_FK1 FOREIGN KEY ( CLIENT_NO )
        REFERENCES TBL_CLIENT (CLIENT_NO );

ALTER TABLE TBL_RECEIVING_HISTORY
    ADD CONSTRAINT TBL_RECEIVING_HISTORY_FK2 FOREIGN KEY ( STORAGE_NO )
        REFERENCES TBL_STORAGE (STORAGE_NO );

-- 재고 현황
CREATE TABLE TBL_STOCK_HISTORY
(
    STOCK_NO    NUMBER NOT NULL,
    STOCK_QUANTITY    NUMBER DEFAULT 0 NOT NULL,
    PRODUCT_CODE    VARCHAR2(255),
    STORAGE_NO    NUMBER NOT NULL
);


COMMENT ON COLUMN TBL_STOCK_HISTORY.STOCK_NO IS '재고번호';

COMMENT ON COLUMN TBL_STOCK_HISTORY.STOCK_QUANTITY IS '재고량';

COMMENT ON COLUMN TBL_STOCK_HISTORY.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_STOCK_HISTORY.STORAGE_NO IS '창고번호';

COMMENT ON TABLE TBL_STOCK_HISTORY IS '재고현황';

CREATE UNIQUE INDEX TBL_STOCK_HISTORY_PK ON TBL_STOCK_HISTORY
( STOCK_NO );

ALTER TABLE TBL_STOCK_HISTORY
 ADD CONSTRAINT TBL_STOCK_HISTORY_PK PRIMARY KEY ( STOCK_NO )
 USING INDEX TBL_STOCK_HISTORY_PK;

ALTER TABLE TBL_STOCK_HISTORY
 ADD CONSTRAINT TBL_STOCK_HISTORY_FK FOREIGN KEY ( PRODUCT_CODE )
 REFERENCES TBL_PRODUCT (PRODUCT_CODE );

ALTER TABLE TBL_STOCK_HISTORY
 ADD CONSTRAINT TBL_STOCK_HISTORY_FK1 FOREIGN KEY ( STORAGE_NO )
 REFERENCES TBL_STORAGE (STORAGE_NO );

-- 재고 수불부 테이블 형성
CREATE TABLE TBL_INVENTORY
(
    INVENTORY_NO    NUMBER NOT NULL,
    STOCK_NO    NUMBER NOT NULL,
    PRODUCT_CODE    VARCHAR2(255) NOT NULL,
    STORAGE_NO    NUMBER NOT NULL,
    CLIENT_NO    NUMBER NOT NULL,
    RECEIVING_HISTORY_NO    NUMBER,
    SELL_NO    NUMBER
);

COMMENT ON COLUMN TBL_INVENTORY.INVENTORY_NO IS '재고수불부 번호';

COMMENT ON COLUMN TBL_INVENTORY.STOCK_NO IS '재고번호';

COMMENT ON COLUMN TBL_INVENTORY.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_INVENTORY.STORAGE_NO IS '창고번호';

COMMENT ON COLUMN TBL_INVENTORY.CLIENT_NO IS '거래처 코드';

COMMENT ON COLUMN TBL_INVENTORY.RECEIVING_HISTORY_NO IS '입고내역 번호';

COMMENT ON COLUMN TBL_INVENTORY.SELL_NO IS '판매내역번호';

COMMENT ON TABLE TBL_INVENTORY IS '재고수불부';

CREATE UNIQUE INDEX TBL_INVENTORY_PK ON TBL_INVENTORY
( INVENTORY_NO );

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_PK PRIMARY KEY ( INVENTORY_NO )
 USING INDEX TBL_INVENTORY_PK;

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_FK FOREIGN KEY ( CLIENT_NO )
 REFERENCES TBL_CLIENT (CLIENT_NO );

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_FK1 FOREIGN KEY ( PRODUCT_CODE )
 REFERENCES TBL_PRODUCT (PRODUCT_CODE );

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_FK2 FOREIGN KEY ( STOCK_NO )
 REFERENCES TBL_STOCK_HISTORY (STOCK_NO );

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_FK3 FOREIGN KEY ( RECEIVING_HISTORY_NO )
 REFERENCES TBL_RECEIVING_HISTORY (RECEIVING_HISTORY_NO );

ALTER TABLE TBL_INVENTORY
 ADD CONSTRAINT TBL_INVENTORY_FK4 FOREIGN KEY ( STORAGE_NO )
 REFERENCES TBL_STORAGE (STORAGE_NO );

-- 재고 수불부 샘플 데이터 따로 없음!

CREATE TABLE TBL_ORDER_LIST
(
    ORDER_NO    NUMBER NOT NULL,
    ORDER_DATE    DATE NOT NULL,
    ORDER_PRICE    NUMBER NOT NULL,
    ORDER_SHIPPING_NO    NUMBER NOT NULL,
    PRODUCT_CODE    VARCHAR2(255),
    ORDER_SHIPPING_STATUS    VARCHAR2(100) NOT NULL
);



COMMENT ON COLUMN TBL_ORDER_LIST.ORDER_NO IS '주문번호';

COMMENT ON COLUMN TBL_ORDER_LIST.ORDER_DATE IS '결제일자';

COMMENT ON COLUMN TBL_ORDER_LIST.ORDER_PRICE IS '주문금액';

COMMENT ON COLUMN TBL_ORDER_LIST.ORDER_SHIPPING_NO IS '송장번호';

COMMENT ON COLUMN TBL_ORDER_LIST.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_ORDER_LIST.ORDER_SHIPPING_STATUS IS '배송상태';

COMMENT ON TABLE TBL_ORDER_LIST IS '주문 내역';

CREATE UNIQUE INDEX TBL_ORDER_LIST_PK ON TBL_ORDER_LIST
( ORDER_NO );

ALTER TABLE TBL_ORDER_LIST
 ADD CONSTRAINT TBL_ORDER_LIST_PK PRIMARY KEY ( ORDER_NO )
 USING INDEX TBL_ORDER_LIST_PK;

ALTER TABLE TBL_ORDER_LIST
 ADD CONSTRAINT TBL_ORDER_LIST_FK FOREIGN KEY ( PRODUCT_CODE )
 REFERENCES TBL_PRODUCT (PRODUCT_CODE );

CREATE TABLE TBL_SELL_LIST
(
    SELL_NO    NUMBER NOT NULL,
    SELL_DATE    DATE NOT NULL,
    SELL_AMOUNT    NUMBER NOT NULL,
    SELL_TOTAL_PRICE    NUMBER NOT NULL,
    PRODUCT_CODE    VARCHAR2(255),
    CLIENT_NO    NUMBER,
    STORAGE_NO  NUMBER,
    SELL_PRICE    NUMBER NOT NULL
);


COMMENT ON COLUMN TBL_SELL_LIST.SELL_NO IS '판매내역번호';

COMMENT ON COLUMN TBL_SELL_LIST.SELL_DATE IS '일자';

COMMENT ON COLUMN TBL_SELL_LIST.SELL_AMOUNT IS '수량';

COMMENT ON COLUMN TBL_SELL_LIST.SELL_TOTAL_PRICE IS '금액합계';

COMMENT ON COLUMN TBL_SELL_LIST.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_SELL_LIST.CLIENT_NO IS '거래처 코드';

COMMENT ON COLUMN TBL_SELL_LIST.STORAGE_NO IS '창고 코드';

COMMENT ON COLUMN TBL_SELL_LIST.SELL_PRICE IS '단가';

COMMENT ON TABLE TBL_SELL_LIST IS '판매내역';

CREATE UNIQUE INDEX TBL_SELL_LIST_PK ON TBL_SELL_LIST
( SELL_NO );

ALTER TABLE TBL_SELL_LIST
 ADD CONSTRAINT TBL_SELL_LIST_PK PRIMARY KEY ( SELL_NO )
 USING INDEX TBL_SELL_LIST_PK;

ALTER TABLE TBL_SELL_LIST
 ADD CONSTRAINT TBL_SELL_LIST_FK FOREIGN KEY ( CLIENT_NO )
 REFERENCES TBL_CLIENT (CLIENT_NO );
 
ALTER TABLE TBL_SELL_LIST
 ADD CONSTRAINT TBL_SELL_LIST_FK1 FOREIGN KEY ( STORAGE_NO )
 REFERENCES TBL_STORAGE (STORAGE_NO ); 

ALTER TABLE TBL_SELL_LIST
 ADD CONSTRAINT TBL_SELL_LIST_FK2 FOREIGN KEY ( PRODUCT_CODE )
 REFERENCES TBL_PRODUCT (PRODUCT_CODE );



----------------------------------------------------------------------
/* 혜영 */
-- 발주내역
CREATE TABLE TBL_P_ORDER
(
    P_ORDER_CODE    NUMBER NOT NULL,
    P_ORDER_DATE    DATE NOT NULL,
    P_ORDER_DEUDATE    DATE NOT NULL,
    P_ORDER_SUMMONEY    NUMBER NOT NULL,
    P_ORDER_PROGRESS    VARCHAR2(10) NOT NULL,
    EMP_CODE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_P_ORDER.P_ORDER_CODE IS '발주 코드';

COMMENT ON COLUMN TBL_P_ORDER.P_ORDER_DATE IS '작성 일자';

COMMENT ON COLUMN TBL_P_ORDER.P_ORDER_DEUDATE IS '납기일자';

COMMENT ON COLUMN TBL_P_ORDER.P_ORDER_SUMMONEY IS '총액';

COMMENT ON COLUMN TBL_P_ORDER.P_ORDER_PROGRESS IS '진행 상태';

COMMENT ON COLUMN TBL_P_ORDER.EMP_CODE IS '사원코드';

COMMENT ON TABLE TBL_P_ORDER IS '발주내역';

CREATE UNIQUE INDEX TBL_P_ORDER_PK ON TBL_P_ORDER
( P_ORDER_CODE );

ALTER TABLE TBL_P_ORDER
 ADD CONSTRAINT TBL_P_ORDER_PK PRIMARY KEY ( P_ORDER_CODE )
 USING INDEX TBL_P_ORDER_PK;

ALTER TABLE TBL_P_ORDER
 ADD CONSTRAINT TBL_P_ORDER_FK FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );

-- 발주상품
CREATE TABLE TBL_ORDER_PRODUCT
(
    PRODUCT_CODE    VARCHAR2(255) NOT NULL,
    P_ORDER_CODE    NUMBER NOT NULL,
    PRODUCT_COUNT    NUMBER NOT NULL,
    PRODUCT_SUMPRICE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_ORDER_PRODUCT.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_ORDER_PRODUCT.P_ORDER_CODE IS '발주 코드';

COMMENT ON COLUMN TBL_ORDER_PRODUCT.PRODUCT_COUNT IS '수량';

COMMENT ON COLUMN TBL_ORDER_PRODUCT.PRODUCT_SUMPRICE IS '입고총액';

COMMENT ON TABLE TBL_ORDER_PRODUCT IS '발주 상품';

CREATE UNIQUE INDEX TBL_ORDER_PRODUCT_PK ON TBL_ORDER_PRODUCT
( PRODUCT_CODE,P_ORDER_CODE );

ALTER TABLE TBL_ORDER_PRODUCT
 ADD CONSTRAINT TBL_ORDER_PRODUCT_PK PRIMARY KEY ( PRODUCT_CODE,P_ORDER_CODE )
 USING INDEX TBL_ORDER_PRODUCT_PK;

-- 회사정보
CREATE TABLE TBL_COMPANY_INFO
(
    COMPANY_NAME    VARCHAR2(500) NOT NULL,
    CEO_NAME    VARCHAR2(500) NOT NULL,
    COMPANY_PHONE    VARCHAR2(500) NOT NULL,
    COMPANY_ADDRESS    VARCHAR2(500) NOT NULL
);

COMMENT ON COLUMN TBL_COMPANY_INFO.COMPANY_NAME IS '회사명';
COMMENT ON COLUMN TBL_COMPANY_INFO.CEO_NAME IS '대표자명';
COMMENT ON COLUMN TBL_COMPANY_INFO.COMPANY_PHONE IS '대표번호';
COMMENT ON COLUMN TBL_COMPANY_INFO.COMPANY_ADDRESS IS '주소';
COMMENT ON TABLE TBL_COMPANY_INFO IS '회사정보';
CREATE UNIQUE INDEX TBL_COMPANY_INFO_PK ON TBL_COMPANY_INFO
( COMPANY_NAME );
ALTER TABLE TBL_COMPANY_INFO
 ADD CONSTRAINT TBL_COMPANY_INFO_PK PRIMARY KEY ( COMPANY_NAME )
 USING INDEX TBL_COMPANY_INFO_PK;

-- 견적서 상품
CREATE TABLE TBL_QUOTATION_PRODUCT
(
    ESTIMATE_CODE    NUMBER NOT NULL,
    PRODUCT_CODE    VARCHAR2(255) NOT NULL,
    ESTI_DISCOUNT    NUMBER NOT NULL,
    ESTI_PRODUCT_COUNT    NUMBER NOT NULL,
    ESTI_PRODUCT_SUMPRICE    NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_QUOTATION_PRODUCT.ESTIMATE_CODE IS '견적서 코드';

COMMENT ON COLUMN TBL_QUOTATION_PRODUCT.PRODUCT_CODE IS '상품코드';

COMMENT ON COLUMN TBL_QUOTATION_PRODUCT.ESTI_DISCOUNT IS '할인률';

COMMENT ON COLUMN TBL_QUOTATION_PRODUCT.ESTI_PRODUCT_COUNT IS '수량';

COMMENT ON COLUMN TBL_QUOTATION_PRODUCT.ESTI_PRODUCT_SUMPRICE IS '출고 총액';

COMMENT ON TABLE TBL_QUOTATION_PRODUCT IS '견적서 상품';

CREATE UNIQUE INDEX TBL_QUOTATION_PRODUCT_PK ON TBL_QUOTATION_PRODUCT
( ESTIMATE_CODE,PRODUCT_CODE );

ALTER TABLE TBL_QUOTATION_PRODUCT
 ADD CONSTRAINT TBL_QUOTATION_PRODUCT_PK PRIMARY KEY ( ESTIMATE_CODE,PRODUCT_CODE )
 USING INDEX TBL_QUOTATION_PRODUCT_PK;

-- 견적서
CREATE TABLE TBL_ESTIMATE
(
    ESTIMATE_CODE    NUMBER NOT NULL,
    ESTIMATE_DATE    DATE NOT NULL,
    ESTIMATE_CLIENT    VARCHAR2(500) NOT NULL,
    ESTI_EXPIRY_DATE    DATE NOT NULL,
    ESTI_SUM    NUMBER NOT NULL,
    ESTI_PROGRESS    VARCHAR2(20) NOT NULL,
    EMP_CODE    NUMBER NOT NULL,
    SEND_STATUS    VARCHAR2(12) DEFAULT 'N' NOT NULL
);

COMMENT ON COLUMN TBL_ESTIMATE.ESTIMATE_CODE IS '견적서 코드';

COMMENT ON COLUMN TBL_ESTIMATE.ESTIMATE_DATE IS '작성일자';

COMMENT ON COLUMN TBL_ESTIMATE.ESTIMATE_CLIENT IS '요청 거래처명';

COMMENT ON COLUMN TBL_ESTIMATE.ESTI_EXPIRY_DATE IS '유효기간';

COMMENT ON COLUMN TBL_ESTIMATE.ESTI_SUM IS '견적금액 합계';

COMMENT ON COLUMN TBL_ESTIMATE.ESTI_PROGRESS IS '진행상태';

COMMENT ON COLUMN TBL_ESTIMATE.EMP_CODE IS '사원코드';

COMMENT ON COLUMN TBL_ESTIMATE.SEND_STATUS IS '발송유무';

COMMENT ON TABLE TBL_ESTIMATE IS '견적서';

CREATE UNIQUE INDEX TBL_ESTIMATE_PK ON TBL_ESTIMATE
( ESTIMATE_CODE );

ALTER TABLE TBL_ESTIMATE
 ADD CONSTRAINT TBL_ESTIMATE_PK PRIMARY KEY ( ESTIMATE_CODE )
 USING INDEX TBL_ESTIMATE_PK;

ALTER TABLE TBL_ESTIMATE
 ADD CONSTRAINT TBL_ESTIMATE_FK3 FOREIGN KEY ( EMP_CODE )
 REFERENCES TBL_EMPLOYEE (EMP_CODE );




----------------------------------------------------------------------
/*시퀀스 삭제*/
-- 증명서 종류 코드
DROP SEQUENCE SEQ_CERTIFICATE_TYPE_CODE;
CREATE SEQUENCE SEQ_CERTIFICATE_TYPE_CODE
 START WITH 3
 NOCACHE;

-- 증명서 코드
DROP SEQUENCE SEQ_CERTIFICATE_CODE;
CREATE SEQUENCE SEQ_CERTIFICATE_CODE
 START WITH 47
 NOCACHE;

-- 부서코드
DROP SEQUENCE SEQ_DEPT_CODE;
CREATE SEQUENCE SEQ_DEPT_CODE
 START WITH 6
 NOCACHE;

-- 직급코드
DROP SEQUENCE SEQ_JOB_CODE;
CREATE SEQUENCE SEQ_JOB_CODE
 START WITH 9
 NOCACHE;

-- 휴가내역 코드
DROP SEQUENCE SEQ_ANNUAL_CODE;
CREATE SEQUENCE SEQ_ANNUAL_CODE
 START WITH 14
 NOCACHE;

-- 근태코드
DROP SEQUENCE SEQ_ATTENDANCE_CODE;
CREATE SEQUENCE SEQ_ATTENDANCE_CODE
 START WITH 65
 NOCACHE;

-- 사원코드
DROP SEQUENCE SEQ_EMP_CODE;
CREATE SEQUENCE SEQ_EMP_CODE
 START WITH 33
 NOCACHE;

-- 급여코드
DROP SEQUENCE SEQ_SALARY_NO;
CREATE SEQUENCE SEQ_SALARY_NO
 START WITH 65
 NOCACHE;

-- 은행코드
DROP SEQUENCE SEQ_BANK_CODE;
CREATE SEQUENCE SEQ_BANK_CODE
 START WITH 6
 NOCACHE;

-- 권한코드
DROP SEQUENCE SEQ_PERMISSION_CODE;
CREATE SEQUENCE SEQ_PERMISSION_CODE
 START WITH 15
 NOCACHE;

-- 사내 일정 시퀀스
DROP SEQUENCE SEQ_COMPANY_DATE_CODE;
CREATE SEQUENCE SEQ_COMPANY_DATE_CODE
 START WITH 6
 NOCACHE;

-- 공지사항 시퀀스
DROP SEQUENCE SEQ_NOTICE_CODE;
CREATE SEQUENCE SEQ_NOTICE_CODE
 START WITH 6
 NOCACHE;

-- 첨부파일 시퀀스
DROP SEQUENCE SEQ_FILE_NO;
CREATE SEQUENCE SEQ_FILE_NO
  NOCACHE;

-- 기안서 양식 시퀀스
DROP SEQUENCE SEQ_DRAFT_FORM_NO;
CREATE SEQUENCE SEQ_DRAFT_FORM_NO
  START WITH 4
  NOCACHE;

-- 기안서 시퀀스
DROP SEQUENCE SEQ_DRAFT_NO;
CREATE SEQUENCE SEQ_DRAFT_NO
  NOCACHE;

-- 상품 카테고리 시퀀스
DROP SEQUENCE SEQ_CATEGORY_CODE;
-- 창고 관리 시퀀스
DROP SEQUENCE SEQ_STORAGE_NO;
-- 입고 내역 시퀀스
DROP SEQUENCE SEQ_RECEIVING_HISTORY_NO;
-- 재고 시퀀스
DROP SEQUENCE SEQ_STOCK_NO;
-- 재고 수불부 시퀀스
DROP SEQUENCE SEQ_INVENTORY_NO;
-- 거래처 시퀀스
DROP SEQUENCE SEQ_CLIENT_NO;
-- 주문번호 시퀀스
DROP SEQUENCE SEQ_ORDER_NO;
-- 판매내역 시퀀스
DROP SEQUENCE SEQ_SELL_NO;
-- 발주코드 시퀀스
DROP SEQUENCE SEQ_P_ORDER_CODE;
-- 견적서코드 시퀀스
DROP SEQUENCE SEQ_ESTIMATE_CODE;
-----------------------------------------------------------------------
/* 시퀀스 생성 */
-- 상품 카테고리 시퀀스
CREATE SEQUENCE SEQ_CATEGORY_CODE
 START WITH 3
 NOCACHE;

-- 창고 관리 시퀀스
CREATE SEQUENCE SEQ_STORAGE_NO
 START WITH 6
 NOCACHE;

-- 입고 내역 시퀀스
CREATE SEQUENCE SEQ_RECEIVING_HISTORY_NO
 START WITH 31
 NOCACHE;

-- 재고 시퀀스
CREATE SEQUENCE SEQ_STOCK_NO
 START WITH 31
 NOCACHE;

-- 재고 수불부 시퀀스
CREATE SEQUENCE SEQ_INVENTORY_NO
 START WITH 1
 NOCACHE;

-- 거래처 시퀀스
CREATE SEQUENCE SEQ_CLIENT_NO
 START WITH 14
 NOCACHE;

-- 주문번호 시퀀스
CREATE SEQUENCE SEQ_ORDER_NO
 START WITH 21
 NOCACHE;

-- 판매내역 시퀀스
CREATE SEQUENCE SEQ_SELL_NO
 START WITH 14
 NOCACHE;

--발주코드 시퀀스
CREATE SEQUENCE SEQ_P_ORDER_CODE
 START WITH 24
 NOCACHE;

-- 견적서코드 시퀀스
CREATE SEQUENCE SEQ_ESTIMATE_CODE
 START WITH 11
 NOCACHE;

-- 뷰
CREATE OR REPLACE FORCE EDITIONABLE VIEW "C##ASAP"."RECEIVING_HISTORY_LIST"
("RECEIVING_HISTORY_NO", "RECEIVING_COMENT", "INVENTORY_DATE", "RECEIVING_AMOUNT"
, "PRODUCT_NAME", "STORAGE_NAME", "CLIENT_NAME") AS
  SELECT
       A.RECEIVING_HISTORY_NO,
       A.RECEIVING_COMENT,
       A.INVENTORY_DATE,
       A.RECEIVING_AMOUNT,
       B.PRODUCT_NAME,
       C.STORAGE_NAME,
       D.CLIENT_NAME
  FROM TBL_RECEIVING_HISTORY A
  JOIN TBL_PRODUCT B ON A.PRODUCT_CODE = B.PRODUCT_CODE
  JOIN TBL_STORAGE C ON A.STORAGE_NO = C.STORAGE_NO
  JOIN TBL_CLIENT D ON A.CLIENT_NO = D.CLIENT_NO;

COMMIT;